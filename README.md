# Dokumentation zur EfA-Parametrisieurng

Die Seite aus diesem Projekt wird (aus dem Main-Branch) hier veröffentlicht: https://docs.fitko.de/efa-parametrisierung/

> Allgemeine Informationen zur Nutzung des Föderalen Entwicklungsportals finden sich unter https://docs.fitko.de/meta/.

## Lokale Entwicklung
To preview your changes as you edit the files, you can run a local development server that will serve your website and reflect the latest changes.

```shell
$ yarn
$ yarn start
```

See https://docusaurus.io/docs/installation#running-the-development-server for details.

## Dokumentation des Inhalts

> Weitere Informationen finden sich in der [Anleitung zur Inhaltsbearbeitung](https://docs.fitko.de/meta/usage).

Für Inhalte können generell Markdown-Dateien verwendet werden. Markdown ist eine einfache Art und Weise Texte einfach und schnell zu formatieren [(mehr)](https://www.markdownguide.org/getting-started/). Diese Dateien Enden auf `.md`. Daneben gibt es noch Dateien mit der Endung `.mdx`. Diese nutzen MDX, eine Erweiterung von Markdown, um JSX-Komponenten (in diesem Fall React Komponenten) in das Markdown einzubetten [(mehr)](https://mdxjs.com).

Ein Beispiel hierfür ist die Nutzung der ApiSpec-Komponente, die erlaubt eine OpenAPI-Spezifikation via RapiDoc einzubetten.


```jsx
import ApiSpec from '@site/src/components/ApiSpec'

<ApiSpec />
```

### Versionierte Dokumentation (optional)

Versionierung der Dokumentation ist [hier](https://docusaurus.io/docs/versioning) beschrieben.

### Die Navigationselemente

Navigationselemente werden in der Datei `docusaurus.config.js` unter `themeConfig > navbar` konfiguriert. Die weitere bzw. genauere Nutzung der Navigationsleiste ist [hier](https://docusaurus.io/docs/versioning) beschrieben.

### Die Seitennavigation

Die Seitennavigation ist in `sidebar.js` definiert. Hierin können Einträge/slugs/DocumentIds gruppiert und aufgelistet werden. Die Seitenleiste wird nur auf Seiten angezeigt, die auch in der Seitennavigationsdefinition aufgeführt sind.

Weitere Informationen sind in der [Dokumentation](https://docusaurus.io/docs/sidebar) zu finden.

## Deployment via CI/CD

> Weitere Informationen zum Deployment finden sich in der [Dokumentation zum Deployment](https://docs.fitko.de/meta/deploy).

Das Deployment ist aktuell in `.gitlab-ci.yml` vordefiniert. Sie funktioniert, wie folgt:

![CI/CD](cicd.png)

In einem ersten Schritt wird die Dokumentation immer gebaut und überprüft. Zur Überprüfung gehört hier u. a. die Validierung interner Links bzw. korrekte Bild-URLs.

Danach wird im Falles des Standard-Branches (`main`) diese gebaute Version live veröffentlicht, aktuell auf Uberspace. Ist der betroffene Branch nicht der `main`-Branch, dann wird die gebaute Version in einer Review-Umgebung veröffentlicht, um sie z. B. vor einem Merge zentral zu überprüfen.

Der hier als letztes dargestellte Job `stop:preview` wird ausgeführt, wenn der Merge-Request geschlossen wurde, der zugehörige Branch gelöscht wurde oder die Lebenszeit der Umgebung abgelaufen ist (diese ist in `.gitlab-ci.yml` auf 14 Tage festgelegt). Sollte die Review-Umgebung dennoch noch einmal notwendig sein, so kann sie manuell über die Weboberfläche von Gitlab deployed werden.

Wenn die CI/CD Konfiguration angepasst werden soll, dann kann entsprechend der [Gitalb CI Dokumentation](https://docs.gitlab.com/ee/ci/) eine eigene Konfiguration in der `.gitlab-ci.yml` definiert werden.

## Erstellen von Diagrammen mit Mermaid

Diagramme können mit [Mermaid.js](https://mermaid.js.org/intro/) direkt in der Dokumentation erstellt werden. Dazu legt man einfach einen Code Block mit `mermaid` als Sprache an:
````
```mermaid
  sequenceDiagram

  # Participants
  participant A as Alice
  participant B as Bob

  # Relations
  A ->> B: Hi Bob!
  B ->> A: Nice to meet you, Alice
```
````

# Lokale Apache-Testumgebung für Docusaurus

Diese Docker-Konfiguration ermöglicht das lokale Testen von Docusaurus-Builds inklusive .htaccess-Regeln.

## Voraussetzungen

- Docker
- Docker Compose
- Docusaurus Build im `build/` Verzeichnis
- .htaccess-Datei im `static/` Verzeichnis

## Schnellstart

1. Build der Docusaurus-Seite erstellen:
```bash
$ yarn install
$ yarn build
```

2. Docker-Container starten:
```bash
$ docker-compose up --build
```

Die Seite ist dann unter `http://localhost:8090` erreichbar.

## Projektstruktur

```
.
├── build/              # Docusaurus Build-Output
├── static/
│   └── .htaccess      # Apache-Konfiguration
├── docker-compose.yml
├── Dockerfile
└── my-httpd.conf      # Apache-Hauptkonfiguration
```

## Entwicklung

- Änderungen an der .htaccess werden durch das Volume-Mounting sofort wirksam
- Bei Änderungen am Build-Output einfach den Container neu starten

## Steuerung

```bash
# Container im Vordergrund starten
docker-compose up --build

# Container im Hintergrund starten
docker-compose up -d --build

# Container stoppen
docker-compose down
```

## License
Source code is licensed under the [EUPL](./LICENSE).

Unless stated otherwise, the content of this website is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
