module.exports = {
  defaultSidebar: [
    {
      type: 'category',
      label: 'Überblick',
      link: {
        type: 'doc',
        id: 'index',
      },
      collapsible: false,
      items: [
      ]
    },
    {
      type: 'category',
      label: 'Parametrisierungsleitfaden',
      link: {
        type: 'doc',
        id: 'parametrisierungsleitfaden',
      },
      collapsible: false,
      items: [
        'kategorisierung_der_parametrisierung',
        {
          type: 'category',
          label: 'Parameterkatalog',
          link: {
            type: 'doc',
            id: 'parameterkatalog'
          },
          collapsible: true,
          items: [
            'parameter_katalog/zustellungskanal_osci_xta',
            'parameter_katalog/zustellungskanal_destinationsignature',
            'parameter_katalog/zuständigkeiten_organisationseinheit_rolle',
            'parameter_katalog/paymentservicebaseurl_des_bezahldienstes',
            'parameter_katalog/versionen_der_schnittstelle_des_bezahldienstes',
            'parameter_katalog/zertifikat_des_bezahldienstes',
            'parameter_katalog/übermittlung_von_daten_des_antragstellers_an_bezahldienst',
            'parameter_katalog/referenz_auf_bezahldienst',
            'parameter_katalog/zahlungsweisen',
            'parameter_katalog/originator_id',
            'parameter_katalog/endpoint_id',
            'parameter_katalog/paymentitem_reference',
            'parameter_katalog/paymentitem_bookingdata',
            'parameter_katalog/paymentitem',
            'parameter_katalog/vertrauensniveau',
            'parameter_katalog/identifizierungsmittel',
            'parameter_katalog/onlinedienst_bezeichnung',
            'parameter_katalog/onlinedienst_links',
            'parameter_katalog/onlinedienst_status',
            'parameter_katalog/onlinedienst_idglobal',
            'parameter_katalog/onlinedienst_durchführungsprachen',
            'parameter_katalog/link_zur_regionalen_datenschutzerklärung',
            'parameter_katalog/link_zum_regionalen_impressum',
            'parameter_katalog/link_zum_kontakt_formular',
            'parameter_katalog/link_für_die_möglichkeit_zur_abgabe_von_feedback',
            'parameter_katalog/link_zur_barrierefreiheitserklärung',
            'parameter_katalog/callback_url_nach_abschluss_des_antrags',
            'parameter_katalog/anzeige_von_kontaktdaten_nach_antragsversand',
            'parameter_katalog/name_der_organisationseinheit',
            'parameter_katalog/kurzbezeichnung_der_organisationseinheit',
            'parameter_katalog/beschreibung_der_organisationseinheit',
            'parameter_katalog/anschriften_der_organisationseinheit',
            'parameter_katalog/zuständige_kontaktpersonen_der_organisationseinheit',
            'parameter_katalog/kennzeichen_schriftformerfordernis',
            'parameter_katalog/leistungsadressaten',
            'parameter_katalog/weiterführende_informationen_zur_leistung',
            'parameter_katalog/erforderliche_unterlagen',
            'parameter_katalog/rechtsgrundlagen',
            'parameter_katalog/bearbeitungsdauer',
            'parameter_katalog/fristen',
            'parameter_katalog/formularbezeichnung',
            'parameter_katalog/formularkurzbezeichnung',
            'parameter_katalog/formular_singnaturerfordernis',
            'parameter_katalog/formular_link'
          ]
        },
        'standardisierung_versionierung_parameterkatalog',
        'versionierung_onlinedienst_produkt',
        {
          type: 'category',
          label: 'Verfahren und Best Practices bei der Parametrisierung',
          link: {
            type: 'doc',
            id: 'parametrisierung_verfahren_und_best_practices'
          },
          collapsible: true,
          items: [
            'parametrisierung_anhänge/verortung_xzufi_parameter_mit_bezug_im_pvog',
            'parametrisierung_anhänge/ermittlung_der_id_der_leistungsbeschreibung',
            'parametrisierung_anhänge/ermittlung_daten_der_leistungsbeschreibung_im_pvog',
            'parametrisierung_anhänge/ermittlung_zuständiger_onlinedienst_im_pvog',
            'parametrisierung_anhänge/ermittlung_xzufi_parameter_mit_bezug_im_pvog',
            'parametrisierung_anhänge/herausforderungen_mit_paymentitem_parametern',
            'parametrisierung_anhänge/encodierung_liste_schlüssel_wert_paare',
            'parametrisierung_anhänge/ermittlung_zuständige_organisationseinheit_im_pvog',
            'parametrisierung_anhänge/best_practices_url_wertige_parameter',
            'parametrisierung_anhänge/internationalisierung',
            'parametrisierung_anhänge/best_practice_nutzung_dvdv_bibliothek',
            'parametrisierung_anhänge/ermittlung_dienstelement_bezahldienstserver',
            'parametrisierung_anhänge/mehrere_zuständigkeiten',
            'parametrisierung_anhänge/aktivierung_des_onlinedienstes'
          ]
        }
      ]
    },
    {
      type: 'category',
      label: 'Pflegekonzept',
      link: {
        type: 'doc',
        id: 'pflegekonzept',
      },
      collapsible: true,
      items: [
        'pflegekonzept/herausforderungen',
        'pflegekonzept/pflegeprozesse_parametertypen',
        'pflegekonzept/nachnutzungsprozess',
        'pflegekonzept/rollen_und_zustaendigkeiten',
        'pflegekonzept/aufgabenverteilung',
        'pflegekonzept/verantwortung_fuer_Datenintegritaet',
        'pflegekonzept/change-management_und_versionskontrolle',
        'pflegekonzept/tests_und_produktivsetzung',
        'pflegekonzept/dokumentation_und_schulung',
        'pflegekonzept/kommunikationsprozesse',
        'pflegekonzept/onlinedienst-vorlagen',
      ]
    }
  ]
}
