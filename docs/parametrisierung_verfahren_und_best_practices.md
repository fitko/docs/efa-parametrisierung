---
id: parametrisierung_verfahren_und_best_practices
title: "Verfahren und Best Practices bei der Parametrisierung"
---


:::info[Version]

1.0.0

:::

# Verfahren und Best Practices bei der Parametrisierung

Um die Parametrisierung effektiv durchführen bzw. nutzen zu können, benötigt man eine Reihe von Verfahren und Best Practices, die jeweils für eine Teilmenge der Parameter immer wieder gleich sind. Beispielsweise gibt es die XZuFi-Parameter mit direktem Bezug zum Onlinedienst, die alle auf dieselbe Weise aus dem PVOG ausgelesen werden können.

Die Abschnitte zu Verfahren und Best Practices bei der Parametrisierung sind jeweils in der Dokumentation der Parameter verlinkt, für die sie relevant sind. Um sie leichter auffindbar zu machen, sind sie auch in der Navigationsleiste aufklappbar aufgelistet.



