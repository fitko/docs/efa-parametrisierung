---
id: onlinedienst-vorlagen
title: "Onlinedienst-Vorlagen"
---


# Onlinedienst-Vorlagen

Das Konzept der Onlinedienst-Vorlagen dient der effizienten Konfiguration und Pflege von EfA-Onlinediensten. Vorlagen ermöglichen die Vorbefüllung von Parametern, die für die Bereitstellung und Nutzung von Onlinediensten erforderlich sind, und erleichtern somit den administrativen Aufwand für nachnutzende Stellen. Vorlagen bieten nicht nur eine klare Struktur und vorgegebene Werte, sondern reduzieren auch die Fehleranfälligkeit und den Zeitaufwand bei der Anpassung von Onlinediensten an regionale Besonderheiten. Durch die zentrale Erstellung und Möglichkeit zur Anpassung auf regionaler Ebene unterstützen Vorlagen eine harmonisierte und effektive Verwaltungsdigitalisierung.

## **Arten von Onlinedienst-Vorlagen**

Vorlagen im Kontext von EfA-Onlinediensten bestehen aus vorkonfigurierten Parametersets, die teilweise oder vollständig ausgefüllt sind. Diese Vorlagen können feste Vorgaben, Empfehlungen, Platzhalter oder Standardwerte enthalten, die bei Bedarf angepasst werden können. Sie werden entweder auf Bundesebene von Bereitstellenden erstellt oder von zentralen Stellen auf Landesebene für vollziehende Behörden. Grundsätzlich lassen sich Vorlagen in zwei Hauptkategorien einteilen: Dokumentationsvorlagen und technische Vorlagen.

### **Dokumentationsvorlagen**

Diese Vorlagen beschreiben die einzutragenden Werte in einem Dokument, das manuell in ein Redaktionssystem übertragen wird. Sie sind notwendig, wenn eine automatische Übertragung der vorkonfigurierten Werte nicht möglich ist. Dokumentationsvorlagen sind insbesondere dann sinnvoll, wenn detaillierte Anweisungen für die manuelle Eingabe erforderlich sind.

### **Technische Vorlagen**

Technische Vorlagen bestehen aus Dateien, die eine automatische Übermittlung eines Parametersets ermöglichen. Bei dieser Art von Vorlage können feste Vorgaben schreibgeschützt sein oder in der Eingabemaske ausgeblendet werden. Technische Vorlagen reduzieren den manuellen Aufwand und die Fehleranfälligkeit bei der Pflege von Parametern erheblich.

## **Konkrete Formen von Vorlagen**

Verschiedene Dokumente und Dateien können als Vorlagen in der Parametrisierung von Onlinediensten dienen. Sie übernehmen entweder bereits die Funktion einer Vorlage oder sind so konzipiert, dass sie dies in Zukunft tun können.

### **Anbindungsleitfäden mit Parameterlisten**

Anbindungsleitfäden sind Dokumente die beschreiben, welche Maßnahmen erforderlich sind, um einen Onlinedienst erfolgreich anzubinden, welche Parameter konfiguriert werden müssen und wie diese Parameter festgelegt werden sollten. Ein Anbindungsleitfaden kann somit als Hilfestellung gesehen werden, um die Anforderungen an die Parameter systematisch zu dokumentieren und sicherzustellen, dass alle notwendigen Schritte zur Konfiguration korrekt durchgeführt werden. Diese Dokumente können als eine Art Vorlage betrachtet werden, da sie spezifische Anweisungen und Empfehlungen zur Konfiguration von Parametern eines EfA-Onlinedienstes enthalten. 

In der Regel werden diese Anbindungsleitfäden von den Herstellern der Onlinedienste erstellt und von Roll-Out-Teams an Nachnutzende weitergegeben. Allerdings kann es notwendig sein, dass Landesredaktionen oder Roll-in-Teams eigene Anbindungsleitfäden für vollziehende Behörden verfassen. Diese landesspezifischen Leitfäden sind insbesondere dann erforderlich, wenn es lokale Besonderheiten gibt, die zusätzliche Schritte oder Anpassungen bei der Anbindung erfordern. Es kann zudem vorkommen, dass die von den Herstellern gelieferten Informationen unvollständig sind oder in einem Format vorliegen, das nicht leicht verständlich ist. In solchen Fällen müssen die zentralen Stellen auf Landesebene oft selbst aufwändige Nachforschungen anstellen, um die notwendigen Informationen zu ergänzen oder zu klären.

Ein häufiges Problem bei Anbindungsleitfäden ist, dass sie vorrangig technische Informationen enthalten, die für Mitarbeiter von Landesredaktionen oder Roll-in-Teams nur teilweise relevant und oft schwer verständlich sind, besonders für Personen ohne technischen Hintergrund. Vertreter der Landesredaktionen bemängeln, dass die Leitfäden oft überfrachtet und zu technisch sind, da sie von Entwicklern für Entwickler geschrieben werden. Wichtige Informationen zu den verwendeten Parametern fehlen häufig, und es mangelt an Standardisierung, was das Auffinden notwendiger Informationen erschwert.

Aus Sicht der Entwickler und Bereitsteller ist die Erstellung solcher Leitfäden mit erheblichem Aufwand verbunden. Eine bereits erstellte Dokumentation umzuschreiben kann zeitintensiv sein und die Weiterentwicklung der Produkte verzögern. Da die Entwicklung oft projektbasiert erfolgt, kann es zudem vorkommen, dass die Teams, die die ursprüngliche Dokumentation erstellt haben, nicht mehr bestehen. Darüber hinaus gibt es häufig interne Standardformate für die Dokumentation, die nicht ohne erheblichen Aufwand durch ein standardisiertes Format ersetzt werden können. Bisher fehlt es auch an einem allgemeinen Standardformat für Anbindungsleitfäden, was die Vereinheitlichung weiter erschwert.

Um die hier beschriebenen Herausforderungen mit Anbindungsleitfäden zu überwinden, ist es empfehlenswert, schon während der [Prüfung der Nachnutzbarkeit](./nachnutzungsprozess.md#2-nachnutzende-bundesländer-kommunen-oder-it-dienstleister) den Anbindungsleitfaden eines Onlinedienstes gründlich zu prüfen. Nachnutzende sollten ihre [Anforderungen an die Dokumentation des Onlinedienstes](./kommunikationsprozesse.md#dokumentation-und-schulungsbedarfe) frühzeitig einbringen, wenn möglich bereits vor oder während der Entwicklung des Dienstes. Bereitstellende sollten gleichermaßen proaktiv die Anforderungen potentieller Nachnutzender erfassen und Prozesse zur Anpassung ihrer Dokumentation vorsehen - zu diesem Zweck können [Dokumentationswebseiten](./dokumentation_und_schulung.md#dokumentations-webseiten) besonders nützlich sein. Es ist zudem wichtig, dass Anbindungsleifäden für Nachnutzende leicht und zuverlässig auffindbar sind - hierfür können die Dokumente oder Links zu ihnen auf der [EfA-Marktplatz](https://mp.govdigital.de)-Seite des Onlinedienstes veröffentlicht werden. Auf jeden Fall müssen Roll-Out Teams sicherstellen, dass Nachnutzende Zugriff auf alle Dokumentation haben, die für die Nachnutzung des Onlinedienstes nötig ist. Es kann sinnvoll sein, die Bereitstellung von Dokumentation und die konkreten Anforderungen an diese Dokumentation [vertraglich zu regeln](./kommunikationsprozesse.md#vereinbarungen-und-verträge-zur-nachnutzung).

### **FIM-Stamminformationen**

FIM-Stamminformationen können als eine Art technische Vorlage angesehen werden, da sie strukturierte, standardisierte Informationen zu Verwaltungsleistungen bereitstellen, die in Redaktionssystemen automatisiert aus dem FIM-Katalog übernommen werden können. Diese automatisierte Übernahme erleichtert die Parametrisierung und Anpassung von EfA-Onlinediensten erheblich, da die Stamminformationen direkt in die bestehenden Systeme integriert werden, wodurch manueller Aufwand reduziert und die Fehleranfälligkeit verringert wird.

Das FIM-Leistungssystem ist bereits bundesweit etabliert und wird von einer Vielzahl von Behörden genutzt, um Verwaltungsleistungen konsistent zu dokumentieren und zu standardisieren. Durch die Integration dieser Informationen in Redaktionssysteme können Behörden auf qualitätsgesicherte Daten zugreifen und diese in ihren Onlinediensten verwenden. Diese Systeme bieten zudem die Möglichkeit, zusätzliche Daten zu ergänzen, die spezifisch für eine Region oder Kommune sind, was die Flexibilität und Anpassungsfähigkeit der Onlinedienste weiter erhöht.

Allerdings befindet sich der FIM-Katalog in einer fortlaufenden Weiterentwicklung, und für viele der über 10.000 einzelnen Leistungen fehlen noch vollständige Stamminformationen. Die Verfügbarkeit dieser Informationen ist jedoch oft entscheidend für die Bereitstellung und den Betrieb von Onlinediensten, da diese Dienste in hohem Maße von den im FIM-Katalog hinterlegten Leistungsinformationen abhängig sind.

Einige Vertreter der nachnutzenden Länder kritisieren, dass die im FIM-Katalog enthaltenen Informationen nicht immer ihren Anforderungen entsprechen. In solchen Fällen sehen sich diese Stellen gezwungen, die Stamminformationen lokal vollständig zu ersetzen, anstatt sie nur zu ergänzen. Dies führt zu einem erhöhten Aufwand und kann zu Unstimmigkeiten in der Verwendung der Daten führen.

Eine der größten Herausforderungen im Zusammenhang mit den FIM-Stamminformationen ist die Vererbung von Änderungen. Wenn ein FIM-Stammtext zentral geändert wird, kann dies Inkonsistenzen verursachen, insbesondere wenn diese Änderungen zu Anpassungen an lokal abgewandelten oder überschriebenen Texten führen müssen. Diese Inkonsistenzen können zu erheblichen Verzögerungen und zusätzlichem Arbeitsaufwand führen, da die betroffenen Stellen die Änderungen manuell nachverfolgen und anpassen müssen, um sicherzustellen, dass die Informationen konsistent und aktuell bleiben.

### **Formular-Vorlage der AG EUX**

:::warning

Die Formular-Vorlage befindet sich aktuell noch in Arbeit

:::

Das Formular wurde entwickelt, um eine standardisierte und übersichtliche Dokumentation der Parametrierungsbedarfe für EfA-Onlinedienste zu ermöglichen. Es soll als Ergänzung zu den bestehenden Anbindungsleitfäden dienen und kann sowohl für neue als auch für bereits bestehende Onlinedienste ausgefüllt werden. Ziel ist es, den Informationsaustausch zwischen Herstellern und nachnutzenden Stellen zu verbessern und gleichzeitig die oft kritisierten Mängel der Anbindungsleitfäden – wie fehlende Standardisierung und unvollständige Parameterinformationen – zu beheben.

In der Formular-Vorlage wird für **Standardparameter** abgefragt, welche dieser Parameter vom Onlinedienst benötigt werden und welche Werte für sie zulässig sind. Dies umfasst die Festlegung von festen Werten oder veränderbaren Standardwerten (Defaults). **Individualparameter** können mit detaillierten Angaben wie Bezeichnung, Erklärung, Liste der möglichen Ausprägungen (Codeliste), Datentyp und Kardinalität erfasst werden. Zusätzlich können Anforderungen an das Vorhandensein von Informationen zur Leistung und zur Organisationseinheit spezifiziert werden. Auf diese Weise bietet das Formular zusammen mit dem Anbindungsleitfaden eine vollständige Übersicht über die technischen und organisatorischen Anforderungen, die für den Betrieb des Onlinedienstes notwendig sind.

Hersteller haben die Möglichkeit, ein ausgefülltes Formular online, beispielsweise im EfA-Marktplatz, zur Verfügung zu stellen. Dies ermöglicht potenziellen Interessenten einen schnellen und klaren Überblick über die Parametrierungsbedarfe des Dienstes. Durch die Bereitstellung dieser Informationen können interessierte nachnutzende Stellen schneller einschätzen, ob sie in der Lage sind, die notwendigen Parameter für den Onlinedienst bereitzustellen und zu pflegen. Darüber hinaus bietet das Formular eine Grundlage für die Einschätzung des Aufwands, der für notwendige Anpassungen, beispielsweise im Redaktionssystem, erforderlich ist. Diese Informationen sind entscheidend, um eine fundierte Entscheidung über die Nachnutzung des Onlinedienstes treffen zu können.

Da das Formular in einem Excel-Format bereitgestellt wird, handelt es sich um eine Übergangslösung, die in Zukunft durch eine vollständig digitalisierte Übertragungsart ersetzt werden soll. Trotz seiner manuellen Natur bietet es bereits jetzt eine wertvolle Hilfestellung, um den Parametrierungsprozess zu strukturieren und zu vereinfachen.

### **Vorlage-Funktion im Redaktionssystem**

Die Vorlage-Funktion in Redaktionssystemen bietet eine zentrale Möglichkeit zur effizienten Parametrisierung von EfA-Onlinediensten, wobei diese Funktionalität je nach Redaktionssystem unterschiedlich ausgeprägt sein kann. Im Kern ermöglicht diese Funktion, spezifische Vorlagen zu erstellen, die nicht nur FIM-Stamminformationen, sondern auch zusätzliche Parameter umfassen, die für die Konfiguration und den Betrieb von Onlinediensten notwendig sind. Eine solche Funktion wird z.B. von den Redaktionssystemen der Linie6Plus angeboten, die folgenden Absätze beziehen sich allerdings nicht auf diese spezifische Implementierung, sondern stellen allgemeine Empfehlungen dar.

Der wesentliche Unterschied zur Bereitstellung von FIM-Stammtexten auf Landesebene besteht darin, dass bei diesen Vorlagen auch Parameter erfasst und bereitgestellt werden, die nicht Teil der standardisierten FIM-Informationen zur Leistung sind. Diese Parameter können beispielsweise technische Details betreffen, die für den Betrieb des Onlinedienstes essenziell sind, aber nicht in den FIM-Leistungsinformationen abgebildet werden. Je nach technischer Implementierung des Redaktionssystems könnten solche Vorlage-Funktionen auch als Erweiterungen der Funktion zur Bereitstellung von FIM-Stamminformationen betrachtet werden, indem sie zusätzliche Parameterfelder und Anpassungsmöglichkeiten bieten.

Ein weiteres wichtiges Merkmal dieser Vorlage-Funktion ist die Integration eines Rollenkonzepts, das dazu beitragen kann, die Pflege und Verwaltung der Parameter zu strukturieren. Durch die Begrenzung der Schreibrechte auf bestimmte Parameter könnten beispielsweise zentrale Stellen dafür sorgen, dass nur autorisierte Personen Änderungen an kritischen Parametern vornehmen können. Diese Schreibrechte könnten je nach Parameter an verschiedene Fachlichkeiten verteilt werden: So könnten technische Parameter ausschließlich von der IT-Abteilung gepflegt werden, während fachliche Parameter von Wissensträgern aus den jeweiligen Fachbereichen bearbeitet werden. Um den Pflegeprozess weiter zu vereinfachen, könnten den für die Pflege zuständigen Personen auch spezifische Eingabemasken angezeigt werden, in denen nur die von ihnen zu pflegenden Parameter sichtbar sind. Dies würde nicht nur die Übersichtlichkeit erhöhen, sondern auch die Effizienz bei der Pflege der Parameter verbessern.

Ein Vererbungsmechanismus innerhalb der Vorlage-Funktion stellt, wie auch bei den FIM-Informationen, eine bedeutende Herausforderung dar. Dabei muss berücksichtigt werden, ob lokale Anpassungen lediglich auf einer Kopie von zentral bereitgestellten Daten basieren oder ob eine fortlaufende Verbindung zwischen den zentralen und lokalen Datenobjekten besteht. Diese Verbindung kann problematisch werden, wenn zentrale Parameterwerte geändert werden, da dies potenziell zu Inkonsistenzen oder unbeabsichtigten Änderungen auf lokaler Ebene führen könnte.

Ein möglicher Ansatz zur Bewältigung dieser Herausforderung wäre die Implementierung eines Prozesses zum Umgang mit Änderungen in zentralen Parameterwerten. Denkbar wäre beispielsweise eine automatische Prüfung des Zeitpunktes der letzten Aktualisierung der beiden Objekte (zentral und lokal) oder die Einführung standardisierter Kommunikationsprozesse im Falle von Änderungen an einer Vorlage. Diese Prozesse könnten sicherstellen, dass lokale Stellen rechtzeitig über zentrale Änderungen informiert werden und die Möglichkeit haben, ihre lokalen Anpassungen entsprechend zu aktualisieren oder zu überprüfen.

### **Onlinedienst-Schema / Onlinedienst-Vorlage-Objekt**

Das Onlinedienst-Vorlage-Objekt, das mit der Einführung von XZuFi 2.3 verfügbar sein wird, bietet eine strukturierte Möglichkeit, Parameter für Onlinedienste zu definieren, die konfiguriert werden können oder müssen. Dieses Objekt wird erst nach dem Roll-out von XZuFi 2.3 verfügbar sein und ist in der vorherigen Version XZuFi 2.2 noch nicht enthalten.

In einer Onlinedienstvorlage können verschiedene Eigenschaften angegeben werden, die zur Vorkonfiguration und zur Erleichterung des Roll-outs von Onlinediensten, insbesondere auf kommunaler Ebene, genutzt werden können. Die wichtigsten Eigenschaften umfassen:

- **ParameterEfAVorlage:** Diese enthält eine Liste von EfA-Parametern, die entweder vorbelegt oder mit Ausfüllhinweisen versehen werden können. Es besteht die Möglichkeit, Pflichtparameter zu kennzeichnen, und Auswahllisten zur Vereinfachung der Eingabe zu nutzen. Dies erlaubt es, Parameter zentral zu definieren und in verschiedenen Kontexten anzupassen.

- **ParameterIndividuellVorlage:** Hierbei handelt es sich um Parameter, die nicht in den Standardparametern enthalten sind, sondern spezifisch für den jeweiligen Onlinedienst definiert werden. Diese Individualparameter können Informationen wie Name, Beschreibung, mögliche Werte (Codelisten) und Datentyp enthalten.

- **ZuständigkeitOnlinedienstVorlage:** Diese erlaubt die Konfiguration der Zuständigkeiten, indem IDs für Leistungen und Organisationseinheiten zugeordnet werden. Es ist jedoch wichtig zu beachten, dass die Vorlage keine Möglichkeit bietet, die Inhalte der Leistungsbeschreibung selbst zu konfigurieren. Stattdessen werden nur die Identifikatoren (IDs) verwendet, um die Verbindungen zu bestehenden Leistungs- und Organisationsinformationen herzustellen.


Trotz der Vorteile gibt es auch einige bedeutende Einschränkungen in der aktuellen Spezifikation:

- **Kardinalität der Parameter:** Weder in der `ParameterEfAVorlage` noch in der `ParameterIndividuellVorlage` besteht die Möglichkeit, eine Kardinalität für Parameter anzugeben, die von 1 abweicht. Das heißt, es gibt derzeit keine strukturierte Option, anzugeben, ob ein Parameter mehrmals (mehr als einmal) angegeben werden darf oder muss. Die einzige Möglichkeit zur Kennzeichnung von Pflichtparametern besteht darin, den Parameter "kennzeichenPflicht" auf "true" zu setzen, was jedoch nur für eine Kardinalität von 1 funktioniert. Eine detaillierte Angabe zur Mehrfachangabe von Parametern fehlt, was die Flexibilität einschränkt.

- **Festlegung des Datentyps:** Für Parameter in der `ParameterIndividuellVorlage` gibt es keine strukturierte Möglichkeit, den Datentyp explizit festzulegen. Dies bedeutet, dass die Angabe, ob es sich bei dem Parameter um Text, Zahl, Datum etc. handelt, nicht direkt im Objekt spezifiziert werden kann. Diese wichtige Information müsste stattdessen in das Freitextfeld "ausfuellhinweis" eingetragen werden, was jedoch die automatische Auswertung dieser Daten verhindert.

Diese Einschränkungen können die Anwendung und die automatische Verarbeitung der Vorlagen erschweren und erfordern, dass wichtige Details manuell interpretiert und berücksichtigt werden müssen.

In Zukunft könnte dieses Format genutzt werden, um Onlinedienst-Objekte automatisch von den Herstellern an Redaktionssysteme zu übermitteln. Diese Automatisierung würde es ermöglichen, dass Redaktionssysteme eine automatische Prüfung der Nachnutzbarkeit der Dienste vornehmen können. Ebenso könnte die Eingabemaske für die Parameterpflege basierend auf den übermittelten Daten (halb)automatisch konfiguriert werden. Dies würde den Aufwand für die lokale Anpassung und Pflege erheblich reduzieren und die Effizienz der Parametrisierung und Implementierung von Onlinediensten weiter steigern.
