---
id: change-management_und_versionskontrolle
title: "Change-Management und Versionskontrolle"
---


# Change-Management und Versionskontrolle

## Einführung

Das Change-Management und die Versionskontrolle sind essenzielle Bestandteile des Betriebs und der kontinuierlichen Pflege von Onlinediensten. Es ist wichtig zu betonen, dass der Begriff "Change-Management" in diesem Kontext nicht im Sinne des Veränderungsmanagements von Organisationsstrukturen und -prozessen verwendet wird, wie es in großen Unternehmen der Fall ist. Vielmehr bezieht sich "Change-Management" hier auf das Management von Änderungen im Rahmen des IT-Betriebs, wie im [ITIL-Framework](https://de.wikipedia.org/wiki/Change_Management_(ITIL)) definiert.

Die genannten Prozesse gewährleisten, dass die Dienste trotz technologischer Weiterentwicklungen und sich ändernder Anforderungen stets konform und funktionsfähig bleiben. Die fortlaufende Anpassung und Pflege von Parametern, die von unterschiedlichen Behörden genutzt werden, stellt eine komplexe Herausforderung dar, die ein systematisches Vorgehen erfordert. In diesem Zusammenhang spielen auch die [Tests und Produktivsetzung von Onlinediensten](./tests_und_produktivsetzung.md) eine zentrale Rolle, um sicherzustellen, dass alle Änderungen umfassend geprüft werden, bevor sie in die produktive Umgebung überführt werden.

Ziel dieses Kapitels ist es, die grundlegenden Prozesse und Methoden vorzustellen, die bei der Einführung und Kontrolle von Änderungen an Onlinediensten angewendet werden. Dabei wird besonders darauf eingegangen, wie eine reibungslose Implementierung von neuen Versionen sichergestellt und die Zusammenarbeit zwischen den beteiligten Akteuren organisiert werden kann. Die technischen Details zur Versionierung von Onlinediensten werden in einem separaten Dokument behandelt, auf das im weiteren Verlauf verwiesen wird. Darüber hinaus werden auch die [Dokumentation](./dokumentation_und_schulung.md#dokumentation) und die [Schulung der beteiligten Akteure](./dokumentation_und_schulung.md#schulungen)  sowie die [Kommunikationsprozesse](./kommunikationsprozesse.md) im Rahmen des Change-Managements thematisiert.

## Change-Management-Prozesse

Change-Management umfasst die systematische Planung, Umsetzung und Überwachung von Änderungen an Onlinediensten. Ein zentraler Bestandteil dieses Prozesses ist die Identifikation der betroffenen Komponenten und die Bewertung der potenziellen Auswirkungen auf die bestehenden Systeme und Nutzer. Änderungen müssen sorgfältig dokumentiert und kommuniziert werden, um sicherzustellen, dass alle Beteiligten – von den IT-Dienstleistern bis zu den Datenredakteuren – über die relevanten Anpassungen informiert sind und entsprechende Maßnahmen ergreifen können. Die [Kommunikationsprozesse](./kommunikationsprozesse.md) spielen hierbei eine entscheidende Rolle, um eine effiziente und zielgerichtete Verbreitung der Informationen zu gewährleisten.

Ein wesentlicher Aspekt des Change-Managements ist es, ein Bewusstsein für die Notwendigkeit regelmäßiger Anpassungen zu schaffen. Da sich die Anforderungen an regionale Parameter im Laufe der Zeit aufgrund von Weiterentwicklungen des EfA-Onlinedienstes verändern können, ist es wichtig, dass alle beteiligten Akteure sich dieser Notwendigkeit bewusst sind. Dies sollte aktiv kommuniziert und in die Arbeitsprozesse integriert werden, um sicherzustellen, dass regelmäßig Anpassungen vorgenommen werden und der Onlinedienst stets den aktuellen Anforderungen entspricht.

Die Umsetzung von Änderungen erfolgt in enger Zusammenarbeit zwischen den Fachbehörden, den IT-Dienstleistern und den Datenredakteuren. Im Rahmen der Umsetzung werden die [Tests und Produktivsetzung von Onlinediensten](./tests_und_produktivsetzung.md) detailliert geplant und durchgeführt, um die Funktionsfähigkeit der neuen Versionen unter realen Bedingungen zu prüfen.

Um den Übergang zu neuen Versionen zu erleichtern, sollten [Übergangsfristen](#übergangsfristen-und-parallelbetrieb) vereinbart werden, in denen alte und neue Versionen parallel betrieben werden können. Dies ermöglicht es den beteiligten Behörden und Nutzern, notwendige Anpassungen vorzunehmen, ohne dass es zu Unterbrechungen im Betrieb kommt. Insbesondere bei [kritischen Änderungen](../versionierung_onlinedienst_produkt.md#kritische-änderungsszenarien) ist es notwendig, detaillierte Testverfahren durchzuführen und mögliche Risiken im Vorfeld zu identifizieren und zu minimieren. Die Schulung der beteiligten Akteure ist ein weiterer wichtiger Aspekt, der unter dem Punkt [Schulung und Support](./dokumentation_und_schulung.md#schulungen) genauer beschrieben wird.

Abschließend ist die Dokumentation der durchgeführten Änderungen ein zentraler Bestandteil des Change-Management-Prozesses. Diese Dokumentation sollte alle relevanten Informationen umfassen, einschließlich der durchgeführten Anpassungen, der beteiligten Akteure und der getroffenen Entscheidungen. Dadurch wird sichergestellt, dass zukünftige Änderungen auf einer klaren und umfassenden Informationsbasis durchgeführt werden können. Die kontinuierliche Pflege und Aktualisierung der Dokumentation trägt wesentlich zur Qualitätssicherung und zum langfristigen Erfolg des Change-Managements bei. 

## Mögliche Changes und Herausforderungen

Im Kontext von Onlinediensten umfasst ein "Change" alle Anpassungen, die Funktionalität, Parameter oder die zugrunde liegende IT-Infrastruktur betreffen. Diese Änderungen können technischer oder organisatorischer Natur sein und erfordern eine sorgfältige Planung und Koordination, um die Betriebsstabilität zu gewährleisten und die Einhaltung von Anforderungen sicherzustellen. Es lassen sich verschiedene Arten von Changes unterscheiden, darunter die Änderung von Parameter-Werten, die Anpassung von Leistungsbeschreibungen, zukünftige Änderungen von Parametern und Versionswechsel von IT-Systemen.

Änderungen von Parameter-Werten in Onlinedienst-Datenobjekten sind oft notwendig, um regionale Besonderheiten oder neue gesetzliche Anforderungen zu berücksichtigen. Wenn ein Datenobjekt für mehrere Kommunen gilt, müssen sowohl der Zeitpunkt der Änderung als auch der konkrete Wert abgestimmt werden. Besonders bei FIM-Parametern oder regionalen Ergänzungen ist eine enge Zusammenarbeit zwischen den betroffenen Kommunen und dem FIM-Rahmen wichtig, um Konsistenz und Kompatibilität zu gewährleisten.

Die Anpassung von Leistungsbeschreibungen, die in den Datenobjekten enthalten sind, stellt eine weitere Herausforderung dar, vor allem, wenn diese Beschreibungen für mehrere Kommunen relevant sind. Hier ist es entscheidend, zwischen FIM-Stammtexten und regionalen Ergänzungen zu unterscheiden, da letztere zusätzliche Abstimmungen mit den lokalen Behörden erfordern. Alle Kommunen müssen gleichzeitig über Änderungen informiert werden, um mögliche Inkonsistenzen zu vermeiden.

Bei zukünftigen Änderungen von Parametern, die erst zu einem späteren Zeitpunkt wirksam werden sollen, spielt die Verwaltung der zeitlichen Gültigkeit eine wichtige Rolle. Es muss gewährleistet sein, dass zu jedem Zeitpunkt nur ein gültiger Wert für jeden Parameter existiert. Obwohl das XZuFi-Modell verschiedene Ebenen zur Eintragung von Gültigkeiten anbietet, können in einigen Fällen Zuordnungsprobleme auftreten. Solche Fälle erfordern eine genaue Analyse und gegebenenfalls Anpassungen im Datenmodell, um Überschneidungen oder Lücken zu verhindern.

Eine besonders komplexe Form von Changes stellen Versionswechsel von IT-Systemen dar. Diese erfordern nicht nur technische, sondern auch organisatorische Anpassungen. Übergangsfristen und Parallelbetriebe müssen eingerichtet werden, um einen reibungslosen Übergang zu gewährleisten. Die Koordination zwischen IT-Dienstleistern, Fachbehörden und Nutzern ist hierbei von zentraler Bedeutung, damit alle Beteiligten rechtzeitig auf die neue Version umsteigen und mögliche Probleme frühzeitig erkennen. Zudem sind umfassende Tests erforderlich, um die Funktionsfähigkeit der neuen Version unter realen Bedingungen sicherzustellen.

## Trigger für Changes

Ein Change kann durch verschiedene Auslöser notwendig werden. Diese Trigger bestimmen, welche Maßnahmen im Rahmen des Change-Managements ergriffen werden müssen und wie dringlich die Umsetzung ist. Typische Auslöser für Changes sind Fehlerkorrekturen, Änderungen in der realen Welt, Weiterentwicklungen von Fachverfahren, lokale Anforderungen und externe Weiterentwicklungen.

Fehler, die bei der Implementierung eines Onlinedienstes übersehen wurden, erfordern oft schnelle Korrekturen. Diese Fehler können falsch konfigurierte Parameter, fehlerhafte Formulierungen in Texten oder unzureichend getestete Funktionen betreffen, die bereits in die Produktion gelangt sind. In solchen Fällen ist eine zeitnahe Fehlerkorrektur entscheidend, um die ordnungsgemäße Funktion des Dienstes zu gewährleisten und die Verbreitung falscher Informationen zu verhindern.

Veränderungen in der realen Welt, wie rechtliche Anpassungen oder Änderungen von Zuständigkeiten, sind ebenfalls häufige Auslöser für Changes. Sobald neue Rechtsnormen oder Zuständigkeiten in Kraft treten, müssen diese zeitnah in den Onlinediensten berücksichtigt werden, um deren Gültigkeit zu bewahren. Auch Änderungen in der Liste von Auswahlmöglichkeiten oder anderen Parametern, die auf solche rechtlichen oder faktischen Änderungen zurückzuführen sind, erfordern entsprechende Anpassungen.

Technische Weiterentwicklungen von Fachverfahren, die die Nutzung eines Onlinedienstes betreffen, sind ein weiterer Trigger. Neue oder angepasste Funktionen erfordern häufig eine Umstellung der Parametrisierung des Dienstes, damit die neuen Möglichkeiten vollständig genutzt werden können. Diese Änderungen entstehen entweder durch Weiterentwicklungen des Fachverfahrens selbst oder durch technische Neuerungen bei den Onlinediensten.

Einzelne Bundesländer oder Kommunen können spezifische Anforderungen stellen, die ebenfalls einen Change auslösen. Diese Anforderungen betreffen in manchen Fällen nur bestimmte Regionen, müssen aber dennoch in den zentral bereitgestellten Onlinediensten umgesetzt werden. Solche Änderungen können sowohl Anpassungen in den Parametern als auch in den Funktionen betreffen, um den regionalen Gegebenheiten gerecht zu werden.

Schließlich gibt es auch externe Weiterentwicklungen, die zwar für ein bestimmtes Bundesland oder eine bestimmte Kommune nicht unmittelbar relevant erscheinen, aber dennoch mitgetragen werden müssen, da es nur eine zentrale Version des Onlinedienstes gibt. Diese Art von Changes erfordert eine enge Abstimmung, um sicherzustellen, dass die Einheitlichkeit des Onlinedienstes gewahrt bleibt, auch wenn die Änderungen auf den ersten Blick nicht zwingend notwendig erscheinen.

## Versionskontrolle und Versionierung von Onlinediensten

Die Versionskontrolle und die Versionierung sind zentrale Bestandteile des Change-Managements und gewährleisten, dass alle Änderungen an einem Onlinedienst strukturiert und nachvollziehbar eingeführt werden. Jede Version eines Onlinedienstes muss eindeutig identifiziert und dokumentiert werden, um sicherzustellen, dass alle beteiligten Akteure über den aktuellen Status und die Historie des Dienstes informiert sind. Diese Transparenz ermöglicht es, bei Problemen schnell die Ursache zu identifizieren und bei Bedarf auf eine vorherige, funktionierende Version zurückzugreifen.

Ein wesentlicher Aspekt der Versionskontrolle ist die Verwaltung von Versionsnummern. Jede neue Version eines Onlinedienstes wird mit einer eindeutigen Versionsnummer versehen, die auch in den zugehörigen Parametern im PVOG reflektiert wird. Die Struktur dieser Versionsnummern muss so gestaltet sein, dass die Reihenfolge der Versionen klar erkennbar ist, und es sollte klar definiert sein, welche Änderungen als minor oder major Release klassifiziert werden. Diese Klassifizierung spielt eine wichtige Rolle bei der [Kompatibilitätsprüfung](../versionierung_onlinedienst_produkt.md#auswertung-des-parameters-idonlinedienstglobal), die im Rahmen der Versionskontrolle durchgeführt wird. Die Verwendung des Parameters [idOnlinedienstGlobal](../versionierung_onlinedienst_produkt.md#maßnahmen-zur-fehlererkennung-und-mitigation) hilft dabei, die Kompatibilität der Parameter mit den unterschiedlichen Versionen zu überprüfen und Fehler aufgrund von veralteten oder manipulierten URLs zu vermeiden.

Die Versionierung von Onlinediensten umfasst die Verwaltung und den kontrollierten Einsatz unterschiedlicher Versionen eines Dienstes über dessen Lebenszyklus hinweg. Jede Version eines Onlinedienstes kann neue Funktionen oder Änderungen an bestehenden Funktionen enthalten, was sich direkt auf die Parametrisierungsbedarfe und die Nutzung des Dienstes durch die Anwender auswirkt. Die Bewertung der Auswirkungen jeder neuen Version auf die bestehenden Prozesse und Systeme erfolgt im Rahmen einer Analyse der [unkritischen und kritischen Änderungsszenarien](../versionierung_onlinedienst_produkt.md#kritische-änderungsszenarien). 

Darüber hinaus muss die Versionskontrolle sicherstellen, dass alte Versionen eines Onlinedienstes nur dann deaktiviert werden, wenn sichergestellt ist, dass keine Nutzer oder Systeme mehr auf diese Version angewiesen sind. Ein geordneter Rückfallmechanismus sollte Teil der Versionsstrategie sein, um bei unvorhergesehenen Problemen schnell auf eine stabile Version zurückschalten zu können. Dies ist besonders wichtig, wenn es zu größeren Veränderungen kommt, die potenziell das gesamte System beeinträchtigen könnten. Insbesondere bei [kritischen Änderungsszenarien](../versionierung_onlinedienst_produkt.md#kritische-änderungsszenarien) ist eine sorgfältige Abstimmung mit den beteiligten Fachbehörden und IT-Dienstleistern notwendig.

## Schwierigkeiten bei der Versionierung von Parameter-Sets

Die Versionierung von Parameter-Sets in Onlinediensten stellt eine besondere Herausforderung dar, da diese Sets aus verschiedenen Datenobjekten bestehen, die nicht zwingend denselben Lebenszyklus haben. Während Softwareversionen in der Regel klar nummeriert und vollständig versioniert werden können, ist dies bei Parameter-Sets aufgrund der unterschiedlichen Änderungszyklen der beteiligten Datenobjekte nicht ohne Weiteres möglich.

Parameter-Sets setzen sich aus Datenobjekten zusammen, die unabhängig voneinander geändert werden können. Diese Datenobjekte können z. B. Leistungsbeschreibungen, Zuständigkeiten oder technische Parameter betreffen. Änderungen an diesen Objekten erfolgen oft zu unterschiedlichen Zeitpunkten und durch verschiedene Akteure, was eine einheitliche Versionierung des gesamten Sets erschwert. Der Lebenszyklus einer Leistungsbeschreibung kann etwa deutlich länger sein als der eines regionalen Parameters, der kurzfristig angepasst wird.

Hinzu kommt, dass Änderungen nicht immer zentral koordiniert sind. Oft werden Parameter von verschiedenen Personen oder Organisationen zu unterschiedlichen Anlässen angepasst, was zu einer Entkopplung der Änderungen führt. Das bedeutet, dass die Änderungen an einem Parameter-Set nicht gleichzeitig erfolgen und nicht einheitlich dokumentiert werden können.

Ein weiterer Faktor, der die Versionierung erschwert, ist die Tatsache, dass bestimmte Parameter-Sets je nach Region oder Kommune unterschiedliche Anforderungen haben. Eine einheitliche Versionsnummer für das gesamte Set würde diese regionalen Unterschiede nicht abbilden und könnte zu Fehlinterpretationen führen.

Aufgrund dieser strukturellen Schwierigkeiten ist es in der Praxis oft nicht möglich, Parameter-Sets eindeutig und vollständig zu versionieren. Stattdessen werden einzelne Parameter oder Datenobjekte versioniert, was jedoch eine höhere Komplexität bei der Verwaltung und Nachverfolgung von Änderungen mit sich bringt.

## Risikomanagement bei Versionswechseln

Das Risikomanagement bei Versionswechseln stellt sicher, dass neue Versionen von Onlinediensten ohne unerwartete Probleme eingeführt werden. Eine gründliche Risikobewertung, die mögliche Inkompatibilitäten mit bestehenden Parametrisierungen identifiziert, ist unerlässlich. Vor der Einführung müssen alle Parameter und Konfigurationen gründlich getestet werden, um die Funktionsfähigkeit sicherzustellen und unerwünschte Auswirkungen zu vermeiden.

Ein besonderes Augenmerk liegt auf der Vermeidung von Fehlern durch veraltete oder manipulierte URLs. Eine Überprüfung der URLs und Parameter, wie [idOnlinedienstGlobal](../versionierung_onlinedienst_produkt.md#maßnahmen-zur-fehlererkennung-und-mitigation), stellt sicher, dass die richtige Version für das jeweilige Gebiet genutzt wird.

Langfristige Auswirkungen von Versionswechseln müssen ebenfalls berücksichtigt werden. Dazu gehört die Bewertung der Systemstabilität und die Sicherstellung, dass ausreichend Ressourcen für Wartung und Anpassung bereitgestellt werden.

## Umsetzung des Versionswechsels

Zu Beginn eines Versionswechsels steht die detaillierte Planung, die alle notwendigen Schritte und Verantwortlichkeiten festlegt. Dabei sind insbesondere der Zeitplan für Tests, Schulungen und Übergangsphasen sowie die Abstimmung mit allen betroffenen Organisationseinheiten wichtig. Ein zentraler Ansprechpartner sollte den gesamten Prozess überwachen und als Schnittstelle zwischen den technischen und organisatorischen Einheiten fungieren.

Pilotprojekte können dabei helfen, neue Versionen in einem kleineren Rahmen zu testen, bevor sie breit eingesetzt werden. Dies ermöglicht es, frühzeitig Probleme zu erkennen und zu beheben. Nach einer erfolgreichen Pilotphase kann die schrittweise Einführung der neuen Version beginnen, begleitet von einer engen Abstimmung aller Beteiligten.

Nach Abschluss des Versionswechsels ist eine Evaluierung des Prozesses notwendig, um den Erfolg zu bewerten und Verbesserungspotenziale für zukünftige Wechsel zu identifizieren. Eine transparente Kommunikation über die gewonnenen Erkenntnisse hilft, die Prozesse kontinuierlich zu optimieren und die Zusammenarbeit zu verbessern.

## Übergangsfristen und Parallelbetrieb

Um sicherzustellen, dass alle beteiligten Organisationen die erforderlichen Anpassungen vornehmen können, ohne den Betrieb zu unterbrechen, müssen Übergangsfristen zwischen den Versionen gelten und der Parallelbetrieb von unterschiedlichen Versionen ermöglicht werden.

Übergangsfristen spielen eine entscheidende Rolle, da sie den betroffenen Fachbehörden und Datenredaktionen ausreichend Zeit geben, ihre Fachverfahren sowie die von ihnen gepflegten Parameterwerte an die neue Version anzupassen. Während dieser Fristen kann die alte Version des Onlinedienstes weiterhin genutzt werden, während die neue Version bereits eingeführt und getestet wird. Dies minimiert das Risiko von Unterbrechungen im Betrieb und ermöglicht eine schrittweise Migration, bei der Fehler frühzeitig erkannt und behoben werden können.

Der Parallelbetrieb unterschiedlicher Versionen eines Onlinedienstes ist besonders wichtig, wenn es sich um [kritische Änderungsszenarien](../versionierung_onlinedienst_produkt.md#kritische-änderungsszenarien) handelt, bei denen signifikante Anpassungen notwendig sind. Durch den Parallelbetrieb können verschiedene Versionen des Onlinedienstes gleichzeitig für unterschiedliche geografische Regionen oder Organisationseinheiten aktiv sein. Dies erlaubt es, die neue Version in bestimmten Gebieten bereits produktiv zu setzen, während in anderen Gebieten noch die alte Version genutzt wird, bis alle Anpassungen vorgenommen wurden.

Die technischen Rahmenbedingungen, wie sie im Dokument zur [Versionierung von Onlinedienst-Produkten](../versionierung_onlinedienst_produkt.md) beschrieben sind, ermöglichen es, die Zuständigkeit für spezifische Gebiete in den Landesportalen und dem PVOG gezielt zu steuern. Dies geschieht durch die Verwaltung und Konfiguration von URL-Parametern und Zuständigkeitsinformationen, die den jeweiligen Versionen zugeordnet sind. Ein solcher Parallelbetrieb stellt sicher, dass die Nutzer immer die korrekte Version des Onlinedienstes verwenden, die für ihre spezifische Region oder Organisationseinheit konfiguriert ist.
