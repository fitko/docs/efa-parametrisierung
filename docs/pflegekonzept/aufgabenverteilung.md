---
id: aufgabenverteilung
title: "Aufgabenverteilung und Spezialisierung in der Parameterpflege"
---


# Aufgabenteilung und Spezialisierung in der Parameterpflege

Die Pflege von Parametern für EfA-Onlinedienste stellt eine erhebliche Herausforderung dar, insbesondere aufgrund der Vielzahl und Komplexität der zu verwaltenden Informationen. Angesichts der unterschiedlichen Größen und Ressourcen von Kommunen und Landesbehörden variiert die Fähigkeit zur effizienten Parameterpflege erheblich. In großen Organisationseinheiten können spezialisierte Teams gebildet werden, die sich auf verschiedene Aspekte der Parameterpflege konzentrieren. In kleineren Gemeinden hingegen fehlt oft die notwendige personelle und technische Infrastruktur, was die Pflege der Parameter zusätzlich erschwert.

Diese Seite soll einen umfassenden Überblick über die verschiedenen Möglichkeiten der Aufgabenteilung und Delegierung bei der Pflege von EfA-Parametern bieten. Ziel ist es, Personen in Landes- und Kommunalredaktionen, die für die Parameterpflege verantwortlich sind, detaillierte Handlungsmöglichkeiten aufzuzeigen. Durch die klare Strukturierung der Aufgaben und die Nutzung externer Unterstützung sollen Effizienz und Qualität der Parameterpflege verbessert werden.

Die nachfolgenden Abschnitte erläutern die verschiedenen Ansätze zur Aufgabenteilung, bieten Beispiele für deren Umsetzung und erklären, in welchen Konstellationen diese Ansätze besonders vorteilhaft sein können. Dabei wird auf spezifische Herausforderungen eingegangen, die in unterschiedlichen Organisationsgrößen und -strukturen auftreten können. Das Glossar dient als Referenz und zur Vertiefung der im Dokument behandelten Themen und bietet eine detaillierte Erklärung der relevanten Begriffe und Konzepte. 

### Fachliche und Technische Parameterpflege

Die Pflege der Parameterwerte im Rahmen des EfA-Onlinedienstes unterteilt sich in fachliche und technische Parameter. Fachliche Parameter beinhalten Inhalte zu spezifischen Leistungen, Organisationseinheiten oder Auswahlmöglichkeiten innerhalb eines Onlinedienstes. Beispielsweise könnte es sich um die Beschreibung der Leistungsdetails oder die zuständige Organisationseinheit handeln. Technische Parameter hingegen umfassen Aspekte wie technische Adressdaten, Zertifikatsdaten oder Informationen zu Bezahldienstleistern.

Die Trennung dieser Aufgaben ermöglicht es, dass nicht dieselben Personen sowohl fachliche als auch technische Daten pflegen müssen. In größeren Kommunen kann dies bedeuten, dass spezialisierte Teams oder Personen für die jeweiligen Parameterarten verantwortlich sind. Dies ist vorteilhaft, da es eine effizientere Datenpflege und eine bessere Nutzung von Fachkenntnissen ermöglicht.

### Aufgabenverteilung innerhalb der Dateneingabe

Im Rahmen der Dateneingabe kann man zwischen der Erhebung relevanter Daten und der eigentlichen Eingabe dieser Daten ins Redaktionssystem unterscheiden. Die Datenerhebung könnte durch Wissensträger erfolgen, die keinen direkten Zugang zum Redaktionssystem haben, während die Eingabe von einer anderen Person vorgenommen wird. Dies entlastet die Wissensträger und ermöglicht eine genaue Eingabe durch spezialisierte Kräfte.

In kleineren Kommunen könnte die Trennung der Datenerhebung und -eingabe jedoch schwierig umzusetzen sein. Hier ist es oft notwendig, dass die gleiche Person beide Aufgaben übernimmt oder externe Unterstützung herangezogen wird.

### Spezialisierung innerhalb der Organisationseinheit

Die Spezialisierung innerhalb einer Organisationseinheit zur Pflege von EfA-Onlinedienst-Parametern kann signifikante Effizienz- und Qualitätssteigerungen mit sich bringen. Größere Kommunen oder Landesbehörden haben z.T. die personellen Ressourcen, um spezialisierte Teams oder Unterabteilungen zu bilden, die jeweils auf unterschiedliche Aspekte der Parameterpflege fokussiert sind.

#### Verwaltung Technischer Parameter

Ein spezialisiertes Team für technische Parameter könnte sich beispielsweise um die Eintragung von Adressdaten, Zertifikatsinformationen und die Integration von Bezahldienstleistern kümmern. Diese Spezialisierung erlaubt es, tiefgehende technische Expertise zu entwickeln und technische Herausforderungen schnell und effizient zu bewältigen. Zudem können diese Teams eng mit IT-Abteilungen oder externen technischen Dienstleistern zusammenarbeiten, um sicherzustellen, dass die technischen Anforderungen der Onlinedienste stets erfüllt sind.

#### Fachliche Parameterpflege

Ein anderes Team könnte sich ausschließlich auf die fachliche Pflege der Parameter konzentrieren. Dies umfasst die detaillierte Beschreibung von Leistungen, die Verwaltung von Organisationsdaten und die Pflege spezifischer Auswahlmöglichkeiten innerhalb eines Onlinedienstes. Diese fachliche Spezialisierung stellt sicher, dass die Inhalte korrekt, aktuell und bürgerfreundlich formuliert sind. Die Mitarbeitenden in diesem Team arbeiten häufig eng mit den inhaltlich zuständigen Stellen, wie Fachabteilungen oder Ministerien, zusammen, um die rechtlichen und inhaltlichen Anforderungen präzise abzubilden.

#### Vorteile der Spezialisierung

Die Vorteile der Spezialisierung innerhalb einer Organisationseinheit sind vielfältig:

- **Effizienzsteigerung:** Durch klare Aufgabenverteilung und spezialisierte Teams können Aufgaben schneller und effektiver bearbeitet werden.
- **Qualitätsverbesserung:** Spezialisten in ihrem jeweiligen Bereich können eine höhere Qualität und Genauigkeit der gepflegten Daten gewährleisten.
- **Ressourcennutzung:** Vorhandene personelle und technische Ressourcen werden optimal genutzt, indem Aufgaben an diejenigen delegiert werden, die die entsprechende Expertise besitzen.

Ein Beispiel für eine erfolgreiche Spezialisierung könnte eine größere Stadtverwaltung sein, in der ein IT-Team für die technischen Parameter zuständig ist, während eine Fachabteilung die inhaltliche Pflege übernimmt. Diese Struktur ermöglicht eine effiziente und qualitativ hochwertige Pflege der Parameter.

### Externe Unterstützung und Auslagerung

In kleineren Kommunen oder Organisationseinheiten, die nicht über ausreichende personelle oder technische Ressourcen verfügen, kann die Auslagerung der Parameterpflege an externe Dienstleister eine sinnvolle Lösung sein. Externe Unterstützung bietet verschiedene Modelle und Möglichkeiten, die flexibel an die jeweiligen Bedürfnisse und Kapazitäten angepasst werden können. Bei der Auslagerung von Pflegeaufgaben ist zu jedoch zu beachten, dass die beauftragten Stellen nur dann Effektive Pflege betreiben können, wenn Änderungen von den jeweiligen Wissensträgern in den Organisationseinheiten angestoßen werden. So wird ein beauftragter IT-Dienstleister nicht eigenständig erfahren können, wenn sich z.B. der Ansprechpartner für eine Leistung ändert.  Es ist daher notwendig, effektive [Kommunikationsprozesse](./kommunikationsprozesse.md) zu etablieren.

#### Regionale IT-Dienstleister

Regionale IT-Dienstleister können eine wichtige Rolle bei der Pflege technischer Parameter spielen. Diese Dienstleister verfügen über die notwendige technische Infrastruktur und das Know-how, um die technischen Aspekte der Parameterpflege zu übernehmen. Durch die Zusammenarbeit mit regionalen IT-Dienstleistern wird die lokale Verwaltung entlastet und kann sich auf ihre Kernaufgaben konzentrieren.

#### Gemeindeverbände und regionale Zusammenschlüsse

Ein weiterer Ansatz besteht darin, dass mehrere kleinere Kommunen sich zusammenschließen und die Parameterpflege gemeinsam organisieren. Dies kann durch die Gründung von Verbänden oder regionalen Zusammenschlüssen erfolgen, die spezialisierte Einheiten zur Pflege der Parameter bilden. Ein solcher Zusammenschluss ermöglicht es den beteiligten Kommunen, Ressourcen zu bündeln und von der gemeinsamen Expertise zu profitieren.

#### Private IT-Dienstleister

Private IT-Dienstleister bieten ebenfalls Unterstützung bei der Pflege der EfA-Onlinedienst-Parameter an. Diese Dienstleister können je nach Bedarf beauftragt werden und bieten flexible Lösungen für verschiedene Anforderungen. Die Zusammenarbeit mit privaten Anbietern ermöglicht es den Kommunen, auch ohne eigene technische Infrastruktur eine hohe Qualität bei der Parameterpflege zu erreichen. Diese Dienstleister können sowohl technische als auch fachliche Parameterpflege übernehmen und bieten oft zusätzliche Dienstleistungen wie Schulungen und Support an.

#### Auslagerung an Hersteller

In einigen Fällen kann es sinnvoll sein, die Parameterpflege direkt an den Hersteller des Onlinedienstes auszulagern, sofern dieser dies anbietet. Hersteller verfügen über das tiefgehende Verständnis der technischen Anforderungen und können sicherstellen, dass die Parameter korrekt und effizient gepflegt werden. 

#### Prozess der Auslagerung

Bei der Auslagerung der Parameterpflege ist es wichtig, klare Vereinbarungen über die Verantwortlichkeiten zu treffen. Die auslagernde Stelle behält in der Regel die [Ergebnisverantwortung](verantwortung_fuer_Datenintegritaet.md#verantwortlichkeiten), während die Ausführungsverantwortung an den externen Dienstleister übertragen wird. Es empfiehlt sich, Prozesse zu etablieren, bei denen die eingegebenen Daten von den Wissensträgern geprüft und freigegeben werden, um die Qualität und Richtigkeit der Daten zu gewährleisten.

Ein Beispiel für die erfolgreiche Auslagerung könnte eine kleine Gemeinde sein, die ihre technischen Parameter an einen regionalen IT-Dienstleister auslagert und die fachlichen Parameter in Zusammenarbeit mit einem privaten Anbieter pflegen lässt. Dadurch kann die Gemeinde sicherstellen, dass alle Parameter korrekt und aktuell sind, ohne dass eigene personelle Kapazitäten überlastet werden.


### Prozessorientierte Kontrolle und Prüfung

Unabhängig davon, wie die Aufgaben verteilt sind, ist es entscheidend, Prozesse zu etablieren, in denen die eingegebenen Daten von den Wissensträgern geprüft und freigegeben werden. Dies stellt sicher, dass die Qualität und Korrektheit der Daten gewährleistet ist. In größeren Kommunen können hierfür spezielle Prüfprozesse implementiert werden, während in kleineren Gemeinden eine engere Zusammenarbeit zwischen den Wissensträgern und den Personen, die die Daten eingeben, erforderlich ist.

Durch diese klare Trennung und spezialisierte Aufgabenverteilung kann die Pflege von EfA-Onlinedienst-Parametern effizient und zuverlässig durchgeführt werden, unabhängig von der Größe der jeweiligen Kommune oder Organisationseinheit.