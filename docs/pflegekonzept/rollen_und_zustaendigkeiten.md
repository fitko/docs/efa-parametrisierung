---
id: rollen_und_zustaendigkeiten
title: "Rollen und Zuständigkeiten in der Parameterpflege"
---

# Rollen und Zuständigkeiten in der Parameterpflege

Die Einführung der EfA-Parametrisierung erfordert eine klare Definition und Verteilung von Rollen und Zuständigkeiten, unabhängig von der konkreten organisatorischen Umsetzung in den jeweiligen Bundesländern oder Kommunen. Dabei bestehen erhebliche Unterschiede zwischen den Ländern: In manchen Ländern können alle Kommunen direkt im Landesredaktionssystem pflegen, während in anderen nur die Landesredaktion darauf Zugriff hat. Auch die Bedarfe der Länder unterscheiden sich zum Teil sehr - so sind die Verwaltungsebenen in den Stadtstaaten grundsätzlich anders gegliedert als in Flächenländern. Es muss daher klar sein, dass keine allgemeingültige Zuweisung von Verantwortlichkeiten möglich ist: Die Zuweisung konkreter Zuständigkeiten und Verantwortlichkeiten obliegt den Ländern und Kommunen. Wichtig ist, dass alle Aufgaben erfüllt werden, unabhängig davon, welche spezifische Stelle sie übernimmt.

## Rollen

Im Rahmen der EfA-Parametrisierung müssen verschiedenen Rollen bei der Pflege und Verwaltung von Parametern für Onlinedienste betrachtet werden. Die folgenden Rollen sind in diesem Kontext besonders relevant:

### Bereitstellende

Die Bereitstellenden sind die Hauptakteure bei der Entwicklung, Bereitstellung und dem Betrieb von EfA-Onlinediensten. Ihre Aufgaben umfassen die technische Entwicklung der Dienste, die initiale Beauftragung der Onlinedienst-Entwicklung, die Sicherstellung der Verfügbarkeit und Korrektheit der fachlichen Stamminformationen sowie die Bereitstellung der Dienste zur Nachnutzung durch andere Organisationen, beispielsweise auf dem EfA-Marktplatz. Darüber hinaus sind sie für den technischen Support und die Unterstützung bei der Implementierung der Dienste in verschiedenen Regionen verantwortlich.

### Nachnutzende Organisationen

Nachnutzende Organisationen umfassen die Bundesländer, Landes- und Kommunalbehörden die die von den Bereitstellenden entwickelten Onlinedienste übernehmen und in ihrem Zuständigkeitsbereich einsetzen.  Die nachnutzenden Organisationen sind für die Anpassung und Pflege von Parametern, die den spezifischen Anforderungen ihrer Regionen entsprechen, verantwortlich.  Dies kann sowohl die Pflege fachlicher als auch technischer Parameter umfassen. Innerhalb der nachnutzenden Organisationen können grob drei Rollen unterschieden werden: Wissensträger, Redakteure und zentrale Stellen.


#### Wissensträger

Wissensträger sind Personen oder Organisationseinheiten, die über spezifische Informationen zu den jeweiligen Leistungen verfügen. Diese Informationen können beispielsweise Zuständigkeiten, aktuelle Rechtsgrundlagen oder spezifische Details zu den Leistungen umfassen. Wissensträger sind oft in den Kommunen oder zuständigen Behörden verankert und spielen eine wichtige Rolle bei der Aktualisierung und Validierung der Stamminformationen, die in den Onlinediensten verwendet werden.

#### Redakteure

Redakteure sind diejenigen, die direkt für die Pflege der Daten im Redaktionssystem verantwortlich sind. Sie übernehmen die Eingabe und Aktualisierung der Informationen, die von den Wissensträgern bereitgestellt werden. Redakteure können sowohl auf Landesebene, in den Landesredaktionen, als auch in den Kommunen tätig sein. In einigen Fällen können die Rollen von Wissensträgern und Redakteuren von derselben Person übernommen werden, jedoch ist es wichtig, diese Rollen klar zu differenzieren, um eine strukturierte und effiziente Pflege der Informationen zu gewährleisten.

#### Zentrale Stellen

Zentrale Stellen auf Landesebene sind für die Koordination und Unterstützung der Pflege von Parametern verantwortlich. Diese Rolle wird in vielen Fällen von den Landesredaktionen übernommen, die zentral Vorlagen und Parameter pflegen, um die Konsistenz und Qualität der Informationen zu gewährleisten. Bei der Erstmaligen Einrichtung von Onlinediensten werden auch oft dedizierte Roll-in-Teams für ein Bundesland etabliert. Sie fungieren als Schnittstelle zwischen den Bereitstellenden und den nachnutzenden Organisationen und unterstützen bei der Implementierung und Anpassung der Onlinedienste.

## Aufgaben in der EfA-Parametrisierung

Die verschiedenen Rollen innerhalb der EfA-Parametrisierung sind entscheidend für den erfolgreichen Betrieb und die Wartung der Onlinedienste. Unabhängig von der konkreten Umsetzung müssen bestimmte Aufgaben erfüllt werden, um die Funktionalität und Anpassungsfähigkeit der Dienste sicherzustellen. Der folgende Text beschreibt, welche Aufgaben zu übernehmen sind, um dieses Ziel zu erreichen.

### Pflege von FIM-Stamminformationen

Die Pflege von FIM-Stamminformationen unterscheidet sich je nach Art der Informationen:

- **Stamminformationen zur Leistung**: Diese Informationen, wie Rechtsgrundlagen, erforderliche Unterlagen und Fristen, werden gemäß den FIM-Richtlinien gepflegt. Die Zuständigkeit hängt dabei vom Leistungstyp ab. Allgemein obliegt die Erstellung von Leistungsinformationen der Bundesredaktion. Wenn zu Leistungen, die ein Onlinedienst abdeckt, keine Stamminformationen vorhanden sind, oder die vorhandenen Informationen als qualitativ nicht ausreichend befunden werden, steht allgemein die Bereitstellenden des Dienstes in der Pflicht, diesen Mangel zu beseitigen, wie in [Schritt 2 des Nachnutzungprozesses](nachnutzungsprozess.md#3-fim-landesredaktionssystem-leistungen) beschrieben.
- **Stamminformationen zur zuständigen Organisationseinheit**: Diese Daten umfassen Namen der Organisationseinheit, Anschriften und zuständige Kontaktpersonen. Diese Informationen sind oft sehr spezifisch auf einzelne Organisationseinheiten bezogen und können nur von diesen angegeben werden. Die Pflege erfolgt daher in der Regel dezentral durch die jeweiligen Kommunen und Behörden, kann aber auch zentral durch die Landesredaktionen oder spezialisierte Stellen erfolgen. Bei einer zentralen Pflege müssen [Prozesse zur Regelmäßigen Kommunikation](./kommunikationsprozesse.md#kommunikationsprozesse-zwischen-verschiedenen-mitarbeitern-in-der-parameterpflege) zwischen Wissensträgern und mit der Dateneingabe beauftragten Stelle etabliert sein.

### Pflege von Parametern zur Steuerung von Onlinediensten

Die Parameter zur Steuerung von Onlinediensten umfassen technische und organisatorische Einstellungen, die die Funktionalität und Anpassungsfähigkeit der Dienste betreffen. Dazu gehören Parameter zur Steuerung des Antragsprozesses sowie Adressierungsinformationen und ePayment-Daten. Die Zuständigkeit für diese Parameter muss von den Ländern und Kommunen selbst zugewiesen werden. Dies kann durch Landesredaktionen, spezialisierte Stellen oder IT-Dienstleister erfolgen. [Standard-Parameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) sollten im Landesredaktionssystem gepflegt werden können. Grundsätzlich ist die Pflege von [Individual-Parametern](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-individual-parameter) in Redaktionssystemen möglich, sofern diese das entsprechende Datenformat in XZuFi unterstützen. Im Fall von Individualparameter sind verschiedene Szenarien denkbar:

1. **Pflege im Redaktionssystem**: wenn das Redaktionssystem die Eingabe von Individualparametern erlaubt, können diese Angaben direkt in einer Eingabemaske vorgenommen werden. Eingabemasken können auf individuelle Parameter angepasst sein, z.B. indem zulässige Werte in einem Dropdown-Menu angeboten werden. Grundsätzlich genügt aber auch ein Freitextfeld, in dem alle Individualparameter zusammen erfasst werden, z.B. direkt im XZuFi-Format. 
2. **Pflege in einem Self-Service-Portal**: Parametrisierung findet bei vielen EfA-Onlinediensten noch über eigene Systeme statt, bei denen Nachnutzende die Pflege in einem vom OD-Bereitsteller betriebenen System durchführen und nicht im Redaktionssystem. Diese Art von Lösung sollte allgemein durch eine zentralisierte Parametrisierung abgelöst werden, aber es kann Fälle geben in denen eine Abbildung der Individualparameter im Redaktionssystem nicht praktikabel ist, z.B. weil es zu viele einzelne Parameter gibt, oder die Verwaltung der Parameter Funktionen voraussetzt, die ein Redaktionssystem nicht bieten kann.  
3. **Pflege durch den Onlinedienst-Hersteller**: Parameter, die nicht durch das PVOG bezogen werden können und für die es kein eigenes Portal gibt, müssen im Zweifel von dem Betreiber des Onlinedienstes eingestellt werden. Vor dieser Option ist allgemein abzuraten, aber sie kann eine Daseinsberechtigung haben, wenn nur sehr seltene Änderungen an den Parametern zu erwarten sind, oder die Konfiguration ein tiefes technisches Wissen erfordert. 

### Erstkonfiguration von EfA-Onlinediensten

Die Erstkonfiguration beinhaltet die initiale Einrichtung der Parameter und Systeme für neue Onlinedienste. Roll-Out-Teams der Bereitstellenden und Roll-In-Teams der Nachnutzenden arbeiten zusammen, um die Dienste einsatzbereit zu machen und an regionale Anforderungen anzupassen. Für eine Erstkonfiguration empfiehlt es sich, [eine Onlinedienst-Vorlage](./Onlinedienst-Vorlagen.md), die von den Bereitstellenden ausgefüllt ist zu verwenden.

### Szenarien mit verschiedenen Zuständigkeiten

Die folgenden Szenarien veranschaulichen die unterschiedlichen Ansätze zur Zuweisung von Rollen und Zuständigkeiten in der EfA-Parametrisierung. Diese Szenarien zeigen, wie Bundesländer ihre organisatorischen Strukturen anpassen können, um den spezifischen Anforderungen gerecht zu werden.

#### Szenario 1: Bundesland A

In Bundesland A haben die Kommunen die volle Verantwortung für die Pflege der Parameter. Jede Kommune greift direkt auf das Landesredaktionssystem zu und verwaltet ihre spezifischen Parameter eigenständig. Es gibt keine zentralen Vorlagen oder Parameter, die von der Landesredaktion bereitgestellt werden. Der zentrale IT-Dienstleister kümmert sich ausschließlich um die technischen Aspekte, ohne in die inhaltliche Pflege der Parameter einzugreifen.

#### Szenario 2: Bundesland B

In Bundesland B hat nur die Landesredaktion Zugriff auf das Landesredaktionssystem. Alle Parameter, sowohl fachlich als auch technisch, werden zentral gepflegt. Für die Anpassung und Pflege von Onlinedienst-spezifischen Parametern wird eine spezialisierte Stelle auf Landesebene eingerichtet.

#### Szenario 3: Bundesland C

In Bundesland C teilen sich die Landesredaktion und die Kommunen die Aufgaben. Die Landesredaktion erstellt und pflegt zentrale Parameter und Vorlagen, die als Grundlage dienen, während die Kommunen für die Pflege der spezifischen Parameter zuständig sind. Ein IT-Dienstleister unterstützt bei der technischen Pflege und der Erstkonfiguration der Onlinedienste, wobei sowohl die zentralen als auch die lokalen Anforderungen berücksichtigt werden.
