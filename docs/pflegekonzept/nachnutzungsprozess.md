---
id: nachnutzungsprozess
title: "Nachnutzungsprozess"
slug: /nachnutzungsprozess
---


# Soll-Prozess der Nachnutzung
![Idealtypische Darstellung der Nachnutzung ausgehend von dem EfA-Marktplatz](../images/Nachnutzung_BPMN.svg)  

Der Nachnutzungsprozess beschreibt den Ablauf von der Anfrage zur Bereitstellung eines Onlinedienstes durch einen Bereitsteller bis zur Produktivsetzung des Dienstes. Der Fokus liegt dabei auf der Aufteilung der Aufgaben auf die verschiedenen Akteure (Bereitsteller, Nachnutzende, vollziehende Behörden), ohne in die Details der einzelnen Aufgaben zu gehen. Auf dieser Seite wird zudem bewusst darauf verzichtet, die Akteure einiger Schritte explizit zu nennen, da die jeweiligen Regelungen in den Bundesländern variieren. Es ist die Aufgabe der Bundesländer, den Prozess diesbezüglich zu konkretisieren.

Ein zentraler Bestandteil des Prozesses ist das Zusammenspiel der beteiligten Akteure, insbesondere der umfangreiche Pflegeprozess. Dieser wird durch [Anbindungsleitfäden](./Onlinedienst-Vorlagen.md#anbindungsleitfäden-mit-parameterlisten) unterstützt, die auf dem [EfA-Marktplatz](https://mp.govdigital.de/) bereitgestellt werden. Diese Leitfäden helfen dabei, die Parameterpflege konsistent und effizient zu gestalten. 

Der Prozess wurde in der AG EUX durch Mitglieder der UAG-Leistungen und Vertreter des Projekts "Nachnutzung MVP EfA-Parametrisierung" entwickelt. Die Abstimmung des Prozesses erfolgte in enger Zusammenarbeit mit der UAG Leistungen, ausgewählten OZG-Koordinatoren und Vertretern der OZG-Themenfelder. 

## 1. Bereitsteller (Bundesländer, IT-Dienstleister)

![Prozess für Bereitsteller](../images/Nachnutzung_BPMN_Bereitsteller.svg)  
**Prozess für Bereitsteller**

Der Prozess beginnt mit der Anfrage zur Bereitstellung eines Onlinedienstes. Hierfür wird zunächst die Akkreditierung des Bereitstellers (Bundesland oder IT-Dienstleister) geprüft. Sollte eine Akkreditierung bei FITKO und/oder govdigital erforderlich sein, wird diese veranlasst. Dies kann verschiedene Schritte umfassen, wie die Überprüfung der Voraussetzungen und die Einreichung notwendiger Dokumente. Eine detaillierte Anleitung zur Akkreditierung ist unter folgendem Link zu finden: [Anleitung zur Akkreditierung](https://www.marktplatz.govdigital.de/downloads/anleitung_registrieren_akkreditieren/).

#### Fallunterscheidung:
- **Akkreditierung erforderlich**: Veranlassung der Akkreditierung bei FITKO und/oder govdigital.
- **Akkreditierung nicht erforderlich**: Direkte Bereitstellung des Dienstes und Übergang zur Veröffentlichung.

Nach der Bereitstellung wird der Dienst auf dem Marktplatz veröffentlicht. Hierbei wird die Einhaltung aller notwendigen Voraussetzungen für die Nachnutzung von EfA-Diensten überprüft. Dieser Schritt schließt den Bereitstellungsprozess ab und ermöglicht es anderen Akteuren, den Dienst nachzunutzen.

## 2. Nachnutzende (Bundesländer, Kommunen oder IT-Dienstleister)

![Prozess für Nachnutzende](../images/Nachnutzung_BPMN_Nachnutzende.svg)  
**Prozess für Nachnutzende**

Die Nachnutzenden beginnen den Prozess mit der Prüfung ihrer eigenen Akkreditierung. Nach erfolgreicher Akkreditierungsprüfung erfolgt die Recherche nach dem Dienst auf dem Marktplatz. Bevor eine Prüfung des Dienstes erfolgen kann, muss das Verhältnis der Nachnutzenden zu den Anbietern geklärt werden: FITKO ist zuständig für die unmittelbare Verwaltung des Bundes und der Bundesländer. Govdigital für Genossenschaftsmitglieder, IT-Dienstleister und regionale IT-Dienstleister, inklusive deren Träger.

Nach einer ersten Prüfung der grundsätzlichen Eignung des Dienstes wird im Landesredaktionssystem ein vertiefter Prozess angestoßen, bei dem unter anderem geprüft wird, [ob alle FIM-Artefakte wie Stammtexte, Stammprozesse und Stammdatenschemata vorhanden sind](#3-fim-landesredaktionssystem-leistungen). Nachdem anschließend das Landesredaktionssystem selbst auf Parametrisierungsanforderungen (z.B. Anschlussfähigkeit an PVOG bzw. DVDV) geprüft wurde, können drei Szenarien eintreten:

#### Fallunterscheidung:
- **Dienst grundsätzlich nachnutzbar**: Der Dienst ist ohne weitere Anpassungen nachnutzbar und weitere Schritte zur Nachnutzung des Dienstes können unternommen werden.
- **Dienst unter Bedingungen nachnutzbar**: Anpassungswünsche werden an den Bereitsteller mitgeteilt. Diese Anpassungen können spezifische regionale Parameter oder zusätzliche Funktionen umfassen. Für die Umsetzung der Anforderungen ist es wichtig, dass der Dienst in der Testinstanz des Landesredaktionssystems vorhanden ist und dort die Anforderungen aus der Anbindung erfüllt werden.
- **Dienst nicht nachnutzbar**: Bedarfsträger werden informiert, dass der Dienst zum aktuellen Zeitpunkt nicht nachgenutzt werden kann.

Ist der Dienst als nachnutzbar befunden worden, bekunden im nächsten Schritt die Nachnutzenden ihr Interesse an der Nachnutzung auf dem Marktplatz. Dies umfasst die formale Anfrage und die Abstimmung mit dem Bereitsteller. Eine Übersicht des Prozesses von der Interessenbekundung bis zum Vertragsschluss ist hier zu finden: [Übersicht Prozess](https://www.marktplatz.govdigital.de/downloads/Uebersicht_Prozess_Status_Interessenbekundung_bis_Vertragsschluss).

Sobald die Anfrage bearbeitet und genehmigt wurde, wird die Bestellung des Dienstes durchgeführt. Dieser Prozess endet mit dem Abschluss eines Nachnutzungsvertrags, wodurch die rechtliche Grundlage für die Nutzung des Dienstes geschaffen wird.

## 3. FIM Landesredaktionssystem (Leistungen)

![Prozess im Landesredaktionssystem](../images/Nachnutzung_BPMN_Landesredaktion.svg)  
**Prozess im Landesredaktionssystem**


Im FIM-Landesredaktionssystem erfolgt die detaillierte Prüfung und Pflege der Leistungsbeschreibung. Zunächst wird überprüft, ob die Leistung bereits im System vorhanden ist oder ob sie neu angelegt werden muss. Dies umfasst sowohl den FIM-Katalog als auch das Landesredaktionssystem.

#### Fallunterscheidung:
- **Leistung im FIM-Katalog vorhanden**: Import und Ergänzung der Stammtexte aus dem FIM-Katalog. Dies stellt sicher, dass alle rechtlichen und organisatorischen Informationen korrekt und vollständig sind.
- **Leistung im Landesredaktionssystem vorhanden**: Direkte Verwendung der bestehenden Daten und Vorbereitungen für die Nachnutzung.
- **Leistung weder im FIM-Katalog noch im Landesredaktionssystem vorhanden**: Rückmeldung an Bereitsteller mit Aufforderung, den Stammtext im FIM-Katalog zu hinterlegen: Bereitstellende tragen die Verantwortung die Verfügbarkeit von Stammtexten zu den betreffenden Leistungen sicherzustellen.

Anschließend werden Vorbereitungen für die Nachnutzung der Onlinedienste getroffen. Dies umfasst ggf. das Anlegen von [Onlinedienst-Vorlagen](./Onlinedienst-Vorlagen.md#vorlage-funktion-im-redaktionssystem), die spezifisch für die Nachnutzung durch verschiedene Behörden angepasst werden. Hierbei könnten Parameterwerte hinterlegt und gegebenenfalls zusätzliche Parameter gepflegt werden, um regionale Besonderheiten zu berücksichtigen. 

#### Fallunterscheidung bei der Vorbereitung:
- **Vorbereitungen nötig (für eine Behörde)**: Der Onlinedienst wird im Landesredaktionssystem zentral für die jeweilige Behörde gepflegt - die Behörde selbst muss keine Parameter pflegen.
- **Vorbereitungen nötig (für mehrere Behörden)**: Es wird eine Onlinedienst-Vorlage für die Nachnutzung angelegt, damit vollziehende Behörden nur einen Teil der Parameter pflegen müssen.
- **Vorbereitungen nicht nötig**: Es wird keine Vorbereitung an zentraler Stelle vorgenommen, die vollziehenden Behörden pflegen selbst alle Parameter des Onlinedienstes.

## 4. Vollziehende Behörden

![Prozess für vollziehende Behörden](../images/Nachnutzung_BPMN_Vollziehende.svg)  
**Prozess für vollziehende Behörden**

Die vollziehenden Behörden beginnen mit der Ergänzung der Leistungsbeschreibung um spezifische Vollzugsinformationen. Dabei wird auch die zuständige Organisationseinheit (OE) zugeordnet. Diese Einheit wird als Ansprechpartner und verantwortliche Stelle im Portal angezeigt.

Im nächsten Schritt wird der Onlinedienst angelegt und die notwendigen Parameter hinterlegt. Hier gibt es eine wichtige Unterscheidung zu treffen: Ist eine Vorlage für den Onlinedienst vorhanden oder nicht?

![Fallunterscheidung Onlinedienst-Vorlage](../images/Nachnutzung_BPMN_Vorlage.svg)  
**Fallunterscheidung Onlinedienst-Vorlage**

#### Fallunterscheidung:
- **Vorlage vorhanden**: Wenn eine Vorlage vorhanden ist, wird der Onlinedienst aus der Vorlage erstellt und das entsprechende Gebiet zugewiesen. Anschließend werden die Parameterwerte hinterlegt und gegebenenfalls zusätzliche Parameter gepflegt.
- **Keine Vorlage vorhanden**: Ist keine Vorlage vorhanden, wird der Onlinedienst manuell angelegt und sowohl das Gebiet als auch die Leistungen zugewiesen. Danach werden die benötigten Parameter angelegt und die entsprechenden Werte hinterlegt.

Nachdem der Onlinedienst angelegt wurde, erfolgt eine [Prüfung auf Funktionsfähigkeit](./tests_und_produktivsetzung.md#durchführung-von-tests) gemeinsam mit dem Betreiber – hierfür kann das Onlinedienst-Objekt in XZuFi 2.3 direkt an das PVOG übertragen werden, ohne eine URL angeben zu müssen, sofern der [Status des Onlinedienstes](../parameter_katalog/18.3_onlinedienst_status.md) auf „Initialisierung“ gepflegt ist. Hierbei wird sichergestellt, dass alle Funktionen ordnungsgemäß funktionieren und die Anforderungen erfüllt werden. Falls der Test erfolgreich ist, wird der Status des Onlinedienstes geändert und gegebenenfalls eine URL hinterlegt. Sollte der Test nicht erfolgreich sein, werden die Parameter angepasst und der Test wird wiederholt.

Schließlich wird der Onlinedienst veröffentlicht und im Portalverbund verfügbar gemacht.
