---
id: dokumentation_und_schulung
title: "Dokumentation und Schulung"
---


# Dokumentation und Schulung

## Einführung
Die Vermittlung von Informationen über Datenpflegeprozesse gehört zu den wichtigsten Aufgaben in der Umsetzung des EfA-Prinzips. [Mangelnde technische Kenntnisse](./herausforderungen.md#wir-haben-keine-technischen-kenntnisse) und [fehlende Informationen](./herausforderungen.md#wir-haben-keinen-überblick-über-alle-relevanten-informationen) werden häufig als wesentliche Hindernisse bei der Umsetzung des EfA-Prinzips genannt. Neben personellen und finanziellen Ressourcen ist es daher entscheidend, dass Informationen klar, strukturiert und zielgerichtet bereitgestellt werden. Nur so können Akteure auf allen Ebenen – von den technischen Spezialisten bis hin zu den operativen Mitarbeitenden – befähigt werden, effektiv zur Parametrisierung und Pflege der notwendigen Parameter beizutragen.

Diese Seite zielt darauf ab, eine umfassende Hilfestellung zu bieten, wie die Weitergabe von Informationen optimal gestaltet werden kann. Dabei wird nicht nur die Bedeutung der Informationsvermittlung betont, sondern auch aufgezeigt, welche Mittel und Formate sich besonders eignen, um die verschiedenen Zielgruppen in der Verwaltung anzusprechen. Die Bandbreite reicht von technischer Dokumentation über Bekanntmachungen bis hin zu gezielten Schulungsmaßnahmen, die je nach Zielgruppe und Informationsbedarf variieren können. Jede dieser Methoden hat ihre eigenen Vorzüge und Herausforderungen, die sorgfältig abgewogen werden müssen, um den größtmöglichen Nutzen zu erzielen.

Ein besonderer Fokus liegt auf der strukturierten und kontinuierlichen Bereitstellung von Informationen, die es ermöglicht, komplexe Prozesse verständlich und zugänglich zu machen. Dabei sollen nicht nur technische Aspekte abgedeckt, sondern auch organisatorische und kommunikative Elemente berücksichtigt werden. Indem wir die richtigen Instrumente und Methoden zur Informationsvermittlung einsetzen, schaffen wir die Grundlage dafür, dass alle Beteiligten die EfA-Parametrisierung erfolgreich unterstützen und vorantreiben können.

## Dokumentation

### Kategorien von Dokumentation

Im Rahmen der Dokumentation zur EfA-Parametrisierung lassen sich zwei wesentliche Kategorien von Dokumentation unterscheiden: die technische Dokumentation und die Prozess-Dokumentation. Beide Kategorien spielen eine zentrale Rolle in der erfolgreichen Umsetzung des EfA-Prinzips, da sie spezifische Anforderungen und Herausforderungen der Informationsvermittlung adressieren. Während die technische Dokumentation sich auf die Beschreibung von Systemen und deren Konfiguration konzentriert, zielt die Prozess-Dokumentation darauf ab, Abläufe und Verantwortlichkeiten klar zu definieren und nachvollziehbar zu machen.

#### Technische Dokumentation

Die technische Dokumentation umfasst in erster Linie die Beschreibung technischer Systeme wie Onlinedienste, Redaktionssysteme und deren Konfigurationsmöglichkeiten. Sie stellt sicher, dass alle Beteiligten die notwendigen Informationen haben, um Systeme korrekt einzurichten und zu nutzen. Diese Dokumentation wird häufig von den Herstellern der Systeme erstellt und legt besonderen Wert auf technische Details und Spezifikationen. Ein Beispiel für eine solche technische Dokumentation sind die Anbindungsleitfäden für EfA-Onlinedienste, die detaillierte Anweisungen zur Konfiguration und Integration dieser Dienste bieten. Zur technischen Dokumentation zählen z.B. auch die Ressourcen auf dem [Entwicklungsportal der FITKO](https://docs.fitko.de/resources/).

Best Practices in diesem Bereich umfassen die Verwendung einer zielgruppengerechten Sprache, die es auch nicht-technischen Akteuren ermöglicht, die Dokumentation zu verstehen. Zu diesem Zweck kann es sinnvoll sein, Dokumentation nach Zielgruppen aufzutrennen - nach diesem Prinzip besteht z.B. auf dem vorliegenden Entwicklungsportal bewusst eine Trennung zwischen technischen Inhalten im [Parametrisierungsleitfaden](../parametrisierungsleitfaden.md) und den eher prozessbezogenen Inhalten im [Pflegekonzept](../pflegekonzept.md). 
Darüber hinaus ist die Standardisierung von Formaten ein hilfreiches Mittel, um die Auffindbarkeit und Konsistenz der Informationen zu gewährleisten. Die [Formular-Vorlage der AG EUX](./Onlinedienst-Vorlagen.md#formular-vorlage-der-ag-eux) bietet hier eine Hilfe, um technische Dokumente zu strukturieren und zu vereinheitlichen, was die Qualität und Verständlichkeit der technischen Dokumentation weiter verbessert.

#### Prozess-Dokumentation

Die Prozess-Dokumentation befasst sich mit der Beschreibung von Abläufen, Methoden, Rollen und Verantwortlichkeiten innerhalb der Organisation. Sie ist entscheidend für das Verständnis und die Einhaltung von Prozessen, insbesondere bei der Pflege von Parametern und der Qualitätssicherung. Diese Dokumentation wird in der Regel von den koordinierenden Stellen oder leitenden Personen innerhalb der Organisation erstellt und gepflegt, um sicherzustellen, dass alle Beteiligten ihre Aufgaben und Verantwortlichkeiten klar verstehen und effizient ausführen können.

Ein Beispiel für eine gut strukturierte Prozess-Dokumentation ist die Anleitung zum FIM-Leistungszuschnitt, die die notwendigen Schritte zur Pflege von Leistungsinformationen in Redaktionssystemen beschreibt, welche auf dem [FIM-Portal](https://fimportal.de/fim-haus) als PDF angeboten wird, sowie die [Darstellung des Nachnutzungprozesses](./nachnutzungsprozess.md), die als BMPN-Graph auf diesem Entwicklungsportal vorliegt.

Best Practices in der Prozess-Dokumentation umfassen ein aktives Prozessmanagement. Dabei sollten die Prozesse regelmäßig überprüft werden, um sicherzustellen, dass sie den aktuellen Rahmenbedingungen entsprechen. Anschließend sollten bei Bedarf sowohl die Prozesse als auch die Prozessdokumentation angepasst werden, um stets den aktuellen Anforderungen und Gegebenheiten gerecht zu werden.
Ein weiterer wesentlicher Aspekt ist die klare Definition von Zuständigkeiten, um Verwirrung und Fehlkommunikation zu vermeiden. Durch die eindeutige [Zuweisung von Rollen](./rollen_und_zustaendigkeiten.md) und [Verantwortlichkeiten](./verantwortung_fuer_Datenintegritaet.md) wird sichergestellt, dass alle Beteiligten wissen, wer für welche Aufgaben zuständig ist, was die Effizienz und Effektivität der Prozesse erheblich steigert. 

### Dokumentationsformate

Im Kontext der EfA-Parametrisierung stehen verschiedene Dokumentationsformate zur Verfügung, die jeweils ihre eigenen Vor- und Nachteile mit sich bringen. Diese Formate reichen von analogen Dokumenten über statische elektronische Dokumente bis hin zu interaktiven Anleitungen, wobei jedes Format für unterschiedliche Anwendungsfälle und Zielgruppen geeignet ist. 

#### Analoge Dokumente

Analoge Dokumente, wie gedruckte Handbücher, Anleitungen oder handschriftliche Notizen, finden trotz zunehmender Digitalisierung weiterhin Anwendung, insbesondere in Umgebungen, in denen digitale Zugänge eingeschränkt oder physische Formate bevorzugt werden. Sie bieten einfache Handhabung, sind unabhängig von technischen Ressourcen und können parallel zur Arbeit konsultiert werden. Allerdings ist ihre Aktualisierung und Verteilung aufwändiger, und die Digitalisierung analoger Inhalte erfordert zusätzliche Zeit und Ressourcen. Best Practices umfassen die Nutzung gedruckter Dokumente für stabile, selten aktualisierte Inhalte, wie technische Handbücher, und die Ergänzung durch handschriftliche Notizen in dynamischen, flexiblen Arbeitsumgebungen, wie beispielsweise bei der Prozessverfolgung auf [Kanban-Boards](https://de.wikipedia.org/wiki/Kanban-Board).

#### Statische elektronische Dokumente

Statische elektronische Dokumente, meist in Form von PDFs, sind plattformunabhängig, leicht zu erstellen und können offline genutzt werden, was sie besonders in Umgebungen mit eingeschränktem Internetzugang attraktiv macht. Sie eignen sich gut für Inhalte, die selten aktualisiert werden, wie technische Spezifikationen, Richtlinien und Schulungsunterlagen. Der größte Nachteil ist ihre eingeschränkte Anpassbarkeit, da Änderungen nach der Erstellung aufwändig sind. Best Practices umfassen die konsequente Versionierung, die Bereitstellung der Dokumente über konsistente, gut strukturierte Verzeichnisse oder URLs sowie die Einbindung von Inhaltsverzeichnissen, um die Navigation und das Auffinden relevanter Informationen zu erleichtern.

#### Dokumentations-Webseiten

Dokumentations-Webseiten sind ein dynamisches und flexibles Format zur Bereitstellung von Informationen, das besonders in der digitalen Verwaltung von großer Bedeutung ist. Sie bieten eine Plattform, auf der Inhalte zentralisiert und schnell angepasst werden können, was sie ideal für die Veröffentlichung von technischen Anleitungen, Richtlinien und lebenden Dokumenten macht, die regelmäßig aktualisiert werden müssen. Beispiele sind z.B. die Ressourcen auf dem [Entwicklungsportal der FITKO](https://docs.fitko.de/resources/).

##### Vorteile von Dokumentations-Webseiten

Ein wesentlicher Vorteil von Dokumentations-Webseiten liegt in ihrer Anpassungsfähigkeit. Inhalte können unmittelbar nach ihrer Erstellung oder Aktualisierung online gestellt werden, wodurch Informationen stets auf dem neuesten Stand gehalten werden können. Dies ist besonders wichtig in Bereichen, in denen sich technische Standards und Prozesse schnell ändern, wie es bei der EfA-Parametrisierung oft der Fall ist. Die Verwendung von Versionierungssoftware wie Git oder SVN ermöglicht eine automatische Verwaltung und Nachverfolgbarkeit der verschiedenen Dokumentversionen. Dies stellt sicher, dass ältere Versionen archiviert werden und bei Bedarf wiederhergestellt werden können, was die Konsistenz und Qualität der Dokumentation gewährleistet.

Ein weiterer Vorteil ist die hervorragende Auffindbarkeit durch Suchmaschinen. Durch eine gute SEO-Optimierung können Nutzer die benötigten Informationen schnell und zielgerichtet finden, ohne lange suchen zu müssen. Dies erhöht die Zugänglichkeit der Dokumentation und sorgt dafür, dass auch weniger versierte Nutzer die Inhalte leicht nutzen können.

##### Nachteile von Dokumentations-Webseiten

Trotz ihrer zahlreichen Vorteile haben Dokumentations-Webseiten auch einige Nachteile. Die Verwaltung der zugrunde liegenden IT-Infrastruktur ist eine Herausforderung, die nicht unterschätzt werden sollte. Die ständige Verfügbarkeit der Webseite muss gewährleistet sein, was regelmäßige Wartung und Updates erfordert. Dies kann gerade für kleinere Organisationen, die nicht über umfangreiche IT-Ressourcen verfügen, eine Hürde darstellen.

Ein weiterer Nachteil ist, dass die Anpassung der Webseite oft tiefere technische Kenntnisse voraussetzt. Dies bedeutet, dass nicht alle Mitarbeitenden in der Lage sind, Änderungen oder Aktualisierungen eigenständig vorzunehmen, was den Prozess verlangsamen kann. Insbesondere wenn es darum geht, komplexe Inhalte zu integrieren oder die Webseite funktional zu erweitern, sind oft spezialisierte Entwickler erforderlich.

##### Best Practices für Dokumentations-Webseiten

Bei der Erstellung und Pflege von Dokumentations-Webseiten sollte besonderes Augenmerk auf das Zugangsmanagement gelegt werden. Es ist wichtig, nur die Informationen zu veröffentlichen, die für eine breite Öffentlichkeit bestimmt sind, während sensible oder interne Dokumente geschützt werden müssen. Zudem ist es ratsam, das Versionsmanagement klar zu strukturieren, indem die Dokumentationsseiten eindeutig mit den beschriebenen Versionen verknüpft werden. Dies erleichtert es den Nutzern, die richtigen Informationen für ihre spezifischen Bedürfnisse zu finden und Missverständnisse zu vermeiden.

##### Dokumentations-Webseiten: Geeignet für...

Dokumentations-Webseiten eignen sich besonders gut für die Bereitstellung technischer Dokumentationen, die regelmäßig aktualisiert werden müssen. Sie sind ideal für „lebende“ Dokumente, die fortlaufend erweitert und ergänzt werden, um sicherzustellen dass Informationen stets aktuell und leicht zugänglich sind. Zudem sind sie die beste Wahl für Informationen, die einem breiten Publikum zugänglich gemacht werden sollen, da sie eine hohe Reichweite und Zugänglichkeit bieten.


#### Videos

Videos sind ein zunehmend beliebtes Format zur Wissensvermittlung, das sich durch seine visuelle und auditive Ansprache besonders gut für die Erklärung komplexer Themen und Prozesse eignet. In der Dokumentation und Schulung bieten sie eine effektive Möglichkeit, Inhalte zugänglicher zu machen und Lernprozesse zu unterstützen, indem sie das Lernen durch Sehen und Hören kombinieren.

##### Vorteile von Videos

Einer der größten Vorteile von Videos ist ihre Zugänglichkeit. Sie ermöglichen es, komplexe Informationen auf eine Weise zu vermitteln, die für viele Menschen leichter verständlich ist als reine Textdokumente. Besonders bei der Demonstration von Software, technischen Abläufen oder praktischen Prozessen können bewegte Bilder und Ton eine wertvolle Ergänzung zu schriftlichen Anleitungen sein. Videos können zudem eine emotionale Verbindung zum Zuschauer aufbauen, was das Lernen und das Verständnis erleichtern kann.

##### Nachteile von Videos

Trotz ihrer Vorteile sind Videos in der Produktion oft aufwändiger als andere Formate. Die Herstellung und das Editing von Videos erfordern nicht nur technische Fähigkeiten, sondern auch Zeit und Ressourcen. Zudem erzeugen Videos größere Dateien, die mehr Speicherplatz benötigen und bei der Verteilung zusätzliche Bandbreite beanspruchen. Dies kann besonders in Organisationen mit eingeschränkten IT-Ressourcen oder langsamen Internetverbindungen problematisch sein.

Ein weiterer Nachteil von Videos ist die lineare Präsentation der Informationen. Im Gegensatz zu schriftlichen Dokumenten, in denen spezifische Informationen gezielt gesucht und direkt angesteuert werden können, muss ein Video oft komplett durchgesehen werden, um die benötigten Details zu finden. Dies kann die Effizienz der Informationssuche erheblich beeinträchtigen, insbesondere wenn der Zuschauer auf der Suche nach einem bestimmten Abschnitt oder einer speziellen Information ist.


##### Best Practices für Videos

Bei der Erstellung von Videos ist es wichtig, auf eine klare Struktur und Qualität zu achten. Präsentationssoftware wie PowerPoint bietet die Möglichkeit, Tonaufnahmen zu einer Präsentation hinzuzufügen, wodurch Videos leicht produziert werden können. Auch die Aufnahme von Schulungen durch Videokonferenz-Software ist eine effektive Methode, um Inhalte festzuhalten und wiederholt zur Verfügung zu stellen.

Ein weiterer wichtiger Aspekt ist die Nutzerfreundlichkeit. Es sollte darauf geachtet werden, dass die Videos so gestaltet sind, dass sie leicht verständlich und ansprechend sind. Zudem ist es ratsam, längere Videos in kürzere Segmente zu unterteilen, die sich gezielt auf spezifische Themen oder Schritte konzentrieren. Dies erleichtert es den Nutzern, genau die Informationen zu finden, die sie benötigen, und verbessert die allgemeine Zugänglichkeit und Nützlichkeit der Videos.

##### Videos: Geeignet für...

Videos sind besonders geeignet für Inhalte, die eine lineare Struktur aufweisen, wie zum Beispiel Schulungsprozesse oder die Erklärung von Vorgehensweisen. Sie eignen sich hervorragend, wenn visuelle Informationen präsentiert werden sollen, die in schriftlicher Form nur schwer verständlich wären, wie beispielsweise die Bedienung von Software oder die Durchführung von technischen Abläufen. Auch für die Dokumentation von Schulungen oder Präsentationen, die später erneut genutzt werden sollen, bieten Videos eine praktische Lösung.

#### Interaktive Anleitungen

Interaktive Anleitungen sind ein fortschrittliches Dokumentationsformat, das Nutzern kontextbasierte Hilfestellungen direkt innerhalb der Arbeitsumgebung bietet. Sie sind besonders nützlich, wenn es darum geht, komplexe Systeme oder Prozesse zu erklären, indem sie den Nutzern genau dort Unterstützung bieten, wo sie benötigt wird. Dieses Format fördert das Lernen durch Handeln und unterstützt den Nutzer dabei, die benötigten Informationen in Echtzeit zu erhalten.

##### Vorteile von interaktiven Anleitungen

Ein bedeutender Vorteil von interaktiven Anleitungen ist ihre unmittelbare Verfügbarkeit und Anpassung an den Kontext, in dem sie verwendet werden. Durch die Integration von Hilfetexten direkt in das System, in dem die Nutzer arbeiten, können Anleitungen genau dann und dort angezeigt werden, wenn sie benötigt werden. Dies reduziert die Notwendigkeit, zwischen verschiedenen Dokumentationsquellen zu wechseln, und erhöht die Effizienz des Arbeitsflusses. Die interaktiven Anleitungen können dabei helfen, die Lernkurve zu verkürzen und die Anwenderfreundlichkeit von Systemen zu verbessern, indem sie Nutzern praktische Unterstützung bieten, die auf ihre aktuellen Bedürfnisse zugeschnitten ist.

Ein weiterer Vorteil ist die Möglichkeit, interaktive Anleitungen dynamisch anzupassen. Da sie oft in digitale Systeme integriert sind, können sie relativ einfach aktualisiert werden, um neuen Anforderungen gerecht zu werden oder um Feedback von Nutzern einfließen zu lassen. Dies macht sie zu einem flexiblen Werkzeug, das kontinuierlich verbessert werden kann, um den sich wandelnden Anforderungen gerecht zu werden.

##### Nachteile von interaktiven Anleitungen

Obwohl interaktive Anleitungen viele Vorteile bieten, gibt es auch Herausforderungen bei ihrer Implementierung. Die Erstellung solcher Anleitungen kann zeitaufwändig und technisch anspruchsvoll sein, da sie oft eine tiefe Integration in die vorhandenen Systeme erfordert. Dies bedeutet, dass sowohl technische Ressourcen als auch umfassendes Wissen über die zu dokumentierenden Prozesse erforderlich sind. Eine Anpassung bestehender interaktiver Anleitungen kann ebenfalls umständlich sein, insbesondere wenn sie in komplexen Systemen eingebettet sind, was die Flexibilität in der Wartung und Anpassung einschränken kann.

Ein weiteres potenzielles Problem ist, dass interaktive Anleitungen in manchen Fällen zu einer Abhängigkeit der Nutzer führen können. Wenn Nutzer sich zu stark auf diese kontextbezogenen Hilfestellungen verlassen, besteht die Gefahr, dass sie weniger selbstständig in der Nutzung des Systems werden und Schwierigkeiten haben, wenn keine Anleitung zur Verfügung steht. Dies kann die Lernprozesse behindern, die auf eigenständigem Erkunden und Verstehen basieren.

##### Best Practices für interaktive Anleitungen

Bei der Implementierung von interaktiven Anleitungen ist es wichtig, sicherzustellen, dass sie so gestaltet sind, dass sie den Bedürfnissen der Nutzer entsprechen und leicht zugänglich sind. Eine gute Praxis ist es, Hilfetexte und Anleitungen so flexibel zu gestalten, dass sie von den Nutzern angepasst oder erweitert werden können. Dies erhöht die Relevanz der Anleitungen und ermöglicht es, sie an spezifische Anforderungen oder individuelle Arbeitsweisen anzupassen.

Ein weiteres Best Practice besteht darin, die Benutzeroberfläche so zu gestalten, dass die Anleitungen nahtlos integriert sind und den Arbeitsfluss nicht stören. Die Anleitungen sollten klar, prägnant und direkt auf die aktuelle Aufgabe bezogen sein, um den Nutzern die benötigte Unterstützung zu bieten, ohne sie zu überfordern. In Schulungsumgebungen können interaktive Anleitungen durch Demo- oder Schulungsversionen von Systemen ergänzt werden, die es den Nutzern ermöglichen, neue Funktionen oder Prozesse in einer sicheren, simulationsbasierten Umgebung zu erlernen.

##### Interaktive Anleitunge: Geeignet für...

Interaktive Anleitungen sind besonders geeignet für Systeme, die eine hohe Komplexität aufweisen und in denen Nutzer häufig Unterstützung benötigen. Sie eignen sich hervorragend für Softwareanwendungen, bei denen kontextsensitive Hilfestellungen direkt in die Benutzeroberfläche integriert werden können. Insbesondere bei der Einführung neuer Systeme oder Funktionen können interaktive Anleitungen den Nutzern helfen, sich schneller zurechtzufinden und produktiver zu arbeiten. Sie sind auch ideal für Schulungsumgebungen, in denen praktische Anwendung und sofortige Rückmeldung eine wichtige Rolle spielen.

#### Demo- oder Schulungsumgebungen von Systemen

Demo- oder Schulungsumgebungen sind eine Erweiterung der interaktiven Anleitungen und bieten Nutzern eine praxisnahe Methode, um neue Inhalte oder Funktionen in einem kontrollierten Umfeld kennenzulernen. Diese Umgebungen simulieren reale Arbeitsbedingungen und ermöglichen es den Nutzern, praktische Erfahrungen zu sammeln, ohne die Risiken eines Fehlers in der Live-Umgebung.

Der Vorteil von Demo- oder Schulungsumgebungen liegt in ihrer Praxisnähe. Sie ermöglichen es den Nutzern, das Erlernte sofort anzuwenden und zu vertiefen, was den Lerneffekt erheblich steigert. Zudem bieten sie eine sichere Umgebung, in der Fehler gemacht und daraus gelernt werden kann, ohne dass dies negative Auswirkungen auf reale Daten oder Prozesse hat.

Allerdings kann die Erstellung und Wartung solcher Umgebungen aufwendig und kostspielig sein. Sie erfordert nicht nur eine technische Infrastruktur, sondern auch die kontinuierliche Anpassung an neue Anforderungen oder Systemupdates. Dennoch sind Demo- oder Schulungsumgebungen besonders geeignet für Schulungen, bei denen Nutzer tiefere Einblicke in die Funktionsweise eines Systems erhalten sollen. Sie sind auch eine wertvolle Ergänzung zu interaktiven Anleitungen, da sie es den Nutzern ermöglichen, das Gelernte in einer praxisnahen Umgebung zu erproben und zu festigen. 


## Bekanntmachungen

Bekanntmachungen sind ein effektives Mittel, um Informationen schnell und gezielt an verschiedene Zielgruppen zu vermitteln. Je nach Art der Bekanntmachung und der Zielgruppe eignen sich unterschiedliche Formate und Kanäle, von analogen Aushängen bis hin zu digitalen Newslettern und Veranstaltungen. 

### Analoge Bekanntmachungen

Analoge Bekanntmachungen sind ein traditionelles, aber weiterhin effektives Mittel der Kommunikation, insbesondere innerhalb von Organisationen und lokalen Gemeinschaften. Ihre Bedeutung nimmt jedoch stetig ab, besonders im Bereich der EfA-Parametrisierung, wo digitale Kanäle klar im Vordergrund stehen.

#### Pinnwände und Informations-Poster

Pinnwände in Büros ermöglichen die zentrale Platzierung von Informationen, die kurzfristig und intern von Bedeutung sind. Informations-Poster wiederum können langfristig relevante Themen sichtbar machen. Diese Methoden sind praktisch, um sicherzustellen, dass alle Mitarbeitenden z.B. über wichtige Parameteränderungen informiert sind. Allerdings verlieren solche Bekanntmachungen zunehmend an Relevanz, da digitale Alternativen effektiver und flexibler sind.

#### Externe analoge Kanäle

Externe analoge Kanäle, wie Flyer oder Aushänge, haben in der Kommunikation von neuen Onlinediensten an Bürger eine untergeordnete Bedeutung, da digitale Plattformen, Webseiten und Social Media weitreichender und kosteneffizienter sind. Ein gezielter Einsatz, z.B. in Warteräumen von Ämtern, kann allerdings ein gutes Mittel sein, um Bürger zu erreichen, die für digitale Kommunikation weniger zugänglich sind.

### Online-Bekanntmachungen

Online-Bekanntmachungen sind ein vielseitiges und effektives Instrument zur Kommunikation zwischen verschiedenen Akteuren im Rahmen der EfA-Parametrisierung. Sie ermöglichen eine schnelle und breite Verbreitung von Informationen und können sowohl für interne als auch externe Zielgruppen genutzt werden. Je nach Art der Informationen und der Zielgruppe stehen verschiedene Online-Kanäle zur Verfügung, die unterschiedliche Kommunikationsbedürfnisse erfüllen. Im Kontext der EfA-Parametrisierung spielen Online-Bekanntmachungen eine entscheidende Rolle, um die Zusammenarbeit zwischen den beteiligten Behörden, Redaktionen und IT-Dienstleistern zu koordinieren und sicherzustellen, dass alle relevanten Akteure über die neuesten Entwicklungen informiert sind.

#### Interne Kanäle

Interne Online-Kanäle sind besonders geeignet, um Informationen innerhalb einer Organisation oder zwischen eng kooperierenden Partnern zu verbreiten. Diese Kanäle sind ideal, um regelmäßig Updates zu teilen, die für die tägliche Arbeit relevant sind, oder um wichtige Änderungen und Neuerungen zeitnah zu kommunizieren.

##### Interne Webseiten

Interne Webseiten bieten eine zentrale Anlaufstelle für alle Mitarbeitenden und Akteure, die an der Parametrisierung und Pflege von Onlinediensten beteiligt sind. Landesredaktionen und IT-Dienstleister können hier umfangreiche und strukturierte Informationen bereitstellen, die jederzeit zugänglich sind. Beispielsweise können Anleitungen zur Nutzung von Redaktionssystemen, aktuelle Parametrisierungsvorgaben und technische Spezifikationen veröffentlicht werden. Diese Inhalte können regelmäßig aktualisiert werden, um sicherzustellen, dass die Belegschaft und Partner immer auf dem neuesten Stand sind. Zudem können interne Webseiten als Plattform dienen, auf der Schulungsunterlagen und Best Practices zur Verfügung gestellt werden.

##### Newsletter / E-Mail-Verteiler

Newsletter und E-Mail-Verteiler sind besonders effektiv für die regelmäßige Kommunikation innerhalb einer Organisation oder zwischen Landesredaktionen und Kommunen. Sie eignen sich hervorragend für monatliche oder wöchentliche Updates, die einen Überblick über aktuelle Entwicklungen, anstehende Änderungen oder wichtige Termine bieten. Ein Beispiel könnte die Kommunikation von Updates im FIM-Landesredaktionssystem sein, bei denen Landesredaktionen die Kommunen über neue Funktionen oder notwendige Anpassungen in den Parametern informieren. Roll-in und Roll-out Teams können ebenfalls von E-Mail-Verteilern profitieren, um den aktuellen Stand der Dienstimplementierung mitzuteilen oder auf anstehende Schulungen hinzuweisen. 

##### Interne Kommunikationsplattformen (z.B. Slack, MS Teams)

Interne Kommunikationsplattformen wie Slack oder MS Teams sind ideal für die schnelle und direkte Kommunikation. Sie bieten die Möglichkeit, kurzfristige Ankündigungen oder Erinnerungen in Echtzeit zu verbreiten, was besonders nützlich ist, wenn schnelle Reaktionen erforderlich sind. Beispielsweise könnten Landesredaktionen eine dringende Mitteilung an kommunale Redakteure senden, wenn kurzfristige Änderungen an den Parametern eines Onlinedienstes erforderlich sind. Diese Plattformen ermöglichen auch die einfache Organisation von Besprechungen und den Austausch von Dokumenten, was die Zusammenarbeit in dezentralen Teams erheblich erleichtert. 

#### Externe Kanäle

Externe Online-Kanäle sind darauf ausgelegt, Informationen an ein breites Publikum außerhalb der eigenen Organisation zu kommunizieren. Sie sind besonders nützlich, um offizielle Bekanntmachungen zu machen, die für die Öffentlichkeit oder externe Partner relevant sind, und um eine breite Reichweite zu erzielen.

##### Öffentliche Webseiten

Öffentliche Webseiten dienen als Hauptplattform für die Verbreitung von Informationen an die allgemeine Öffentlichkeit oder an spezifische externe Zielgruppen. Für Anbieter von Onlinediensten ist es wichtig, hier regelmäßig Aktualisierungen zu veröffentlichten Diensten bereitzustellen. So können beispielsweise Änderungen an den Parametern eines EfA-Onlinedienstes oder Hinweise zur Nachnutzung neuer Dienste kommuniziert werden. Auch Landesredaktionen können öffentliche Webseiten nutzen, um kommunale Redakteure und interessierte Bürger über neue Entwicklungen im FIM-Prozess zu informieren. 

##### Social Media

Social Media Plattformen wie Twitter, LinkedIn oder Facebook bieten eine schnelle und effektive Möglichkeit, aktuelle Ankündigungen und Neuigkeiten zu verbreiten. Diese Kanäle sind besonders geeignet, um kurzfristige Bekanntmachungen, Event-Hinweise oder Erfolge und Meilensteine in der EfA-Parametrisierung zu kommunizieren. Anbieter von Onlinediensten können Social Media nutzen, um neue Funktionen oder Updates zu bewerben und so die Aufmerksamkeit auf ihre Dienste zu lenken. Auch Roll-in und Roll-out Teams können hierüber schnell und unkompliziert ihre Zielgruppen über Fortschritte informieren.

##### Externe E-Mail-Verteiler

Externe E-Mail-Verteiler eignen sich hervorragend für die regelmäßige Kommunikation mit Partnern, Kunden oder der Öffentlichkeit. Diese Kanäle sind ideal, um strukturierte Informationen, wie zum Beispiel Berichte über den Fortschritt von Parametrisierungsprojekten oder Einladungen zu Schulungen und Veranstaltungen, zu verbreiten. Ein Beispiel wäre der Versand eines regelmäßigen Newsletters an alle beteiligten Kommunen, um über den aktuellen Stand der EfA-Dienste zu informieren und kommende Schritte in der Parametrisierung zu erläutern. Diese E-Mail-Kommunikation kann auch dazu genutzt werden, spezifische Zielgruppen gezielt anzusprechen, etwa bei der Vorbereitung einer neuen Dienstnachnutzung durch interessierte Kommunen.

### Informationsveranstaltungen

Informationsveranstaltungen bieten die Möglichkeit, komplexe Themen in einem interaktiven Format zu präsentieren, Fragen zu klären und den direkten Austausch zwischen verschiedenen Akteuren zu fördern. Die Veranstaltungen können in verschiedenen Formaten stattfinden, je nachdem, ob sie sich an interne oder externe Zielgruppen richten und welchen Informationsbedarf sie decken sollen.

#### Interne Veranstaltungen

Interne Veranstaltungen dienen vor allem der Schulung, Koordination und dem Wissensaustausch innerhalb einer Organisation oder zwischen verschiedenen Verwaltungseinheiten. Sie sind unerlässlich, um sicherzustellen, dass alle Beteiligten über die neuesten Entwicklungen, Prozesse und Anforderungen informiert sind.

##### Interne Meetings und Workshops

Interne Meetings und Workshops eignen sich besonders für die Kommunikation zwischen verschiedenen Organisationseinheiten, wie zum Beispiel zwischen der Landesredaktion und den Kommunalredaktionen. In diesen Treffen können Parameteranforderungen und -änderungen diskutiert werden, insbesondere wenn neue Onlinedienste eingeführt oder bestehende Dienste angepasst werden müssen. Roll-in- und Roll-out-Teams nutzen solche Meetings, um sicherzustellen, dass alle notwendigen Schritte zur Parametrisierung und Implementierung der Dienste klar verstanden und koordiniert sind.

##### Schulungen und Trainings

[Schulungen und Trainings](#schulungen) richten sich an spezifische Zielgruppen innerhalb der Verwaltung, wie z.B. [IT-Beauftragte](#schulungen-für-it-beauftragte), [Redakteure](#schulungen-für-redakteure) oder [Support-Mitarbeiter](#schulungen-für-support-mitarbeiter). Beispielsweise könnten Schulungen für IT-Beauftragte durchgeführt werden, um ihnen die neuen Funktionalitäten eines Onlinedienstes zu erläutern oder um sie in die Nutzung eines neuen Redaktionssystems einzuweisen. 

#### Externe Veranstaltungen

Externe Veranstaltungen sind darauf ausgerichtet, Informationen an ein breiteres Publikum außerhalb der eigenen Organisation zu kommunizieren. Sie eignen sich besonders für die Vorstellung neuer Dienste, die Erklärung komplexer Themen an ein spezialisiertes Publikum oder für die Öffentlichkeitsarbeit.

##### Veranstaltungen für ein spezialisiertes Publikum

Diese Veranstaltungen, wie Fachseminare oder Workshops, richten sich an Partner, Fachleute oder nachnutzende Kommunen, die tiefergehendes Wissen benötigen. Anbieter von Onlinediensten nutzen diese Formate, um Änderungen oder Erweiterungen an ihren Diensten zu kommunizieren. Beispielsweise könnten sie ein Webinar abhalten, um den Landesredaktionen und IT-Dienstleistern neue Funktionen zu erläutern und deren Integration in bestehende Prozesse zu unterstützen. Diese Veranstaltungen können je nach Bedarf regelmäßig oder einmalig stattfinden, abhängig von der Komplexität der zu vermittelnden Informationen.

##### Konferenzen und öffentliche Veranstaltungen

Konferenzen und öffentliche Veranstaltungen bieten eine Plattform, um wichtige Themen in einem größeren Rahmen zu präsentieren und die Sichtbarkeit von Projekten wie der EfA-Parametrisierung zu erhöhen. Diese Formate sind ideal, um die Öffentlichkeit über neue digitale Verwaltungsdienste zu informieren oder um Erfolgsgeschichten und Best Practices zu teilen. Behörden können diese Veranstaltungen nutzen, um Bürger über neue Onlinedienste zu informieren oder um Feedback zu erhalten, welches in die Weiterentwicklung der Dienste einfließen kann. Solche Veranstaltungen finden in der Regel jährlich statt und erfordern eine umfassende Vorbereitung, um sicherzustellen, dass sie die gewünschten Zielgruppen erreichen und ihre Wirkung entfalten.

### Relevanz für Akteure der Parameterpflege

Die verschiedenen Arten von Informationsveranstaltungen bieten unterschiedliche Vorteile und sollten je nach Zielgruppe und Informationsbedarf gezielt eingesetzt werden. Anbieter von Onlinediensten können Fachseminare nutzen, um technische Details und Anpassungsmöglichkeiten zu erläutern, während Roll-in- und Roll-out-Teams Workshops veranstalten, um die operative Umsetzung der Dienste in den Kommunen zu koordinieren. Landesredaktionen können regelmäßige Schulungen organisieren, um sicherzustellen, dass die Kommunen in der Lage sind, die notwendigen Parameter korrekt zu pflegen und anzupassen. Schließlich bieten Konferenzen und öffentliche Veranstaltungen die Möglichkeit, die breitere Öffentlichkeit und externe Stakeholder über die Fortschritte und Erfolge der EfA-Parametrisierung zu informieren und das Vertrauen in die digitalen Verwaltungsdienste zu stärken.

## Schulungen

Die Schulung der Beteiligten ist ein zentraler Baustein für die erfolgreiche Umsetzung der EfA-Parametrisierung. Um sicherzustellen, dass alle Akteure – von technischen Spezialisten bis hin zu operativen Mitarbeitenden – die notwendigen Kenntnisse und Fähigkeiten erwerben, ist ein umfassendes Schulungsprogramm unerlässlich. Dieses Programm muss sich an den spezifischen Anforderungen der unterschiedlichen Zielgruppen orientieren und verschiedene Formate bieten, die sowohl in Präsenz als auch online effektiv eingesetzt werden können.


### Zielgruppen und Inhalte
Die Schulungen im Rahmen der EfA-Parametrisierung müssen passgenau auf die unterschiedlichen Zielgruppen zugeschnitten sein, um die spezifischen Anforderungen und Aufgaben jeder Gruppe effektiv zu adressieren. Während IT-Beauftragte tiefgehende technische Schulungen benötigen, um die Konfiguration und Anpassung von Onlinediensten zu meistern, brauchen Redakteure detaillierte Kenntnisse in der Pflege von Parametern und der Nutzung von Redaktionssystemen. Support-Mitarbeitende wiederum müssen in die Lage versetzt werden, kompetent und effizient auf Anfragen zu reagieren und häufig auftretende Probleme zu lösen. Jede Zielgruppe erfordert maßgeschneiderte Inhalte, die den jeweiligen Verantwortlichkeiten und dem Kenntnisstand der Teilnehmenden gerecht werden.

#### Schulungen für IT-Beauftragte

Die IT-Beauftragten, die für die technischen Aspekte der EfA-Parametrisierung verantwortlich sind, benötigen spezialisierte Schulungen, die ihnen das notwendige Wissen zur Konfiguration und Anpassung von Onlinediensten vermitteln. Diese Schulungen sollten sich auf technische Parameter, die Nutzung von APIs und den Umgang mit spezifischen Einstellungen konzentrieren, um die regionale Anpassungsfähigkeit der Dienste zu gewährleisten. Besonders relevant sind hier Inhalte, die sich auf die Implementierung und Pflege von Parametern beziehen, welche in Redaktionssystemen gepflegt und an das PVOG übermittelt werden. 

Da die technischen Anforderungen und Systeme von Bundesland zu Bundesland variieren können, sollten die Schulungen idealerweise von den Herstellern der entsprechenden Systeme durchgeführt werden. Diese Hersteller verfügen über das notwendige Know-how, um praxisnahe und spezifische Schulungsinhalte anzubieten, die auf die jeweilige IT-Infrastruktur und die Besonderheiten der technischen Systeme abgestimmt sind. 

Als zentrale Ressourcen können der [Parametrisierungsleitfaden](../parametrisierungsleitfaden.md), der [Parameterkatalog](../parameterkatalog.md) und die [Sammlung der Verfahren und Best Practices](../parametrisierung_verfahren_und_best_practices.md) herangezogen werden. Diese Dokumente bieten detaillierte Anleitungen und bewährte Verfahren, die den IT-Beauftragten helfen, die Parametrisierung effizient umzusetzen und die Anpassungsfähigkeit der Onlinedienste zu maximieren.

#### Schulungen für Redakteure

Redakteure, die für die Pflege und Anpassung von Parametern in Onlinediensten und Redaktionssystemen verantwortlich sind, benötigen ein fundiertes Verständnis der spezifischen Anforderungen und technischen Rahmenbedingungen der EfA-Parametrisierung. Die Schulungen für diese Zielgruppe sollten daher nicht nur die grundlegenden Aufgaben der Parametrisierung vermitteln, sondern auch tiefergehende Kenntnisse in der Handhabung von FIM-Stamminformationen und deren Implementierung in den Redaktionssystemen vermitteln.

Ein wichtiger Ansatzpunkt ist die Integration und Erweiterung bestehender [FIM-Schulungen](https://fimportal.de/schulung), die bereits umfassend in der Verwaltung etabliert sind. Diese Schulungen, die ursprünglich darauf ausgerichtet sind, den Umgang mit den FIM-Bausteinen „Leistungen“, „Datenfelder“ und „Prozesse“ zu lehren, bieten eine solide Grundlage, auf der aufgebaut werden kann. Redakteure, die bereits an FIM-Basisseminaren und weiterführenden Schulungen teilgenommen haben, verfügen über ein grundlegendes Verständnis der Methodik und können diese Kenntnisse gezielt erweitern, um den spezifischen Anforderungen der EfA-Parametrisierung gerecht zu werden.

Die bestehenden FIM-Schulungsstrukturen bieten dabei eine gute Ausgangsbasis, um die Redakteure in die komplexen Prozesse der Parameterpflege einzuführen. Diese Schulungen können durch zusätzliche Module ergänzt werden, die sich spezifisch mit der EfA-Parametrisierung, der Pflege von Onlinedienst-bezogenen Parametern und der Integration dieser Parameter in die Redaktionssysteme befassen. Beispielsweise könnte ein vertiefendes Schulungsmodul entwickelt werden, das sich mit der effizienten Verwaltung kommunalspezifischer Datenobjekte befasst.

Darüber hinaus sollte die Schulung von Redakteuren auch praktische Übungen beinhalten, bei denen sie in einer simulierten Umgebung die Parametrisierung von Onlinediensten anhand konkreter Beispiele durchspielen können. Diese Übungen sollten eng mit den Inhalten der FIM-Methodenexperten-Schulung verknüpft sein, um sicherzustellen, dass die Redakteure die Methodik nicht nur theoretisch, sondern auch praktisch anwenden können.

Schließlich bietet das bestehende Netzwerk von FIM-Coaches eine wertvolle Ressource, die bei der Erweiterung und Anpassung der Schulungsinhalte genutzt werden kann. FIM-Coaches, die bereits Erfahrung in der Schulung und Beratung im Rahmen des Föderalen Informationsmanagements haben, könnten als Multiplikatoren für die erweiterten Schulungen zur EfA-Parametrisierung fungieren. Durch die Einbindung dieser Coaches kann sichergestellt werden, dass die Schulungsinhalte konsistent und auf einem hohen Qualitätsniveau vermittelt werden.

Indem die bestehenden FIM-Schulungsstrukturen nachgenutzt und durch spezifische Inhalte zur EfA-Parametrisierung ergänzt werden, können Redakteure optimal auf ihre Aufgaben vorbereitet werden. Dies stellt sicher, dass sie die Parametrisierung effizient umsetzen und so die Funktionalität und Qualität der Onlinedienste aufrechterhalten können.

#### Schulungen für Support-Mitarbeiter

Support-Mitarbeiter, insbesondere diejenigen im 1st-Level-Support wie bei der 115, spielen eine entscheidende Rolle in der Unterstützung von Bürgern und der Lösung von Problemen im Zusammenhang mit Onlinediensten. Die Schulungen für diese Zielgruppe sollten darauf ausgerichtet sein, grundlegende Kenntnisse über die EfA-Parametrisierung zu vermitteln und ihnen zu zeigen, wie sie typische Probleme identifizieren und effektive Lösungen anbieten können.

Ein wesentlicher Bestandteil dieser Schulungen ist das Verständnis für die verschiedenen Ebenen des Supports (1st-Level, 2nd-Level und 3rd-Level) und die jeweiligen Zuständigkeiten. Support-Mitarbeiter müssen wissen, wann sie Anfragen weiterleiten und an wen sie sich wenden müssen, um spezialisierte Unterstützung zu erhalten. Hierbei ist es wichtig, dass sie die Unterschiede zwischen fachlichem und technischem Support erkennen und entsprechend agieren können.

Darüber hinaus sollten diese Schulungen praktische Anleitungen zur Nutzung der vorhandenen FIM-Informationen bieten, die im Rahmen des Föderalen Informationsmanagements bereitgestellt werden. Diese Informationen können als Grundlage dienen, um den Support-Mitarbeitern ein umfassendes Verständnis für die Parameter und deren Auswirkungen auf die Funktionalität der Onlinedienste zu vermitteln. 

### Schulungsformate

Je nach Zielgruppe, Schulungsinhalt und organisatorischen Rahmenbedingungen können unterschiedliche Schulungsformate eingesetzt werden – von intensiven Präsenzschulungen über flexible Online-Webinare bis hin zu unterstützenden Dokumentationen. Jedes Format hat seine eigenen Stärken und Einsatzgebiete, die es ermöglichen, den Lernprozess optimal zu gestalten und die Teilnehmenden nachhaltig zu befähigen. Durch eine kluge Kombination der Schulungsformate lassen sich die unterschiedlichen Lernbedürfnisse effektiv abdecken und die Effizienz der Schulungen maximieren.

#### Präsenzschulungen

Präsenzschulungen  eignen sich hervorragend für tiefgehende Schulungen, etwa zur Parametrisierung von Onlinediensten, Redaktionssystemen oder zur Einführung neuer Technologien. 

Insbesondere bei der Implementierung neuer Systeme oder bei der Einführung von Redaktionssystemen und Onlinediensten können Roll-in- und Roll-out-Teams in Zusammenarbeit mit den Herstellern Präsenzschulungen anbieten, um den Teilnehmenden ein solides Fundament zu vermitteln. Es kann sich besonders lohnen, im selben Präsenztermin Inhalte für mehrere Onlinedienste zu präsentieren, wenn mehrere Dienste nachgenutzt werden, die auf derselben Plattform basieren. Dies ermöglicht es, Synergieeffekte zu nutzen und den Schulungsaufwand zu reduzieren. Dabei ist es sinnvoll, die Schulungsinhalte in einer strukturierten [Prozess-Dokumentation](#prozess-dokumentation) festzuhalten, um den Lernenden eine klare Orientierung zu bieten.

#### Online-Schulungen und Webinare

Online-Schulungen und Webinare bieten eine flexible und kostengünstige Alternative zu Präsenzschulungen. Sie sind besonders geeignet für regelmäßige Updates, die Einführung neuer Funktionen und als Aufbau oder Auffrischung von Grundlagenschulungen. Durch die Möglichkeit, Schulungen ortsunabhängig durchzuführen, lassen sich größere Teilnehmerkreise erreichen und organisatorische Hürden reduzieren. Webinare eignen sich besonders für die Vermittlung von neuen technischen Updates oder für die Vorbereitung auf kommende Änderungen, wie sie häufig in [Bekanntmachungen](#online-bekanntmachungen) angekündigt werden.

Ein wichtiger Aspekt bei der Durchführung von Webinaren ist deren interaktive Gestaltung. Dies kann durch die Nutzung von „Breakout-Rooms“ zur Gruppenarbeit oder durch interaktive Tools wie Quizze und Umfragen innerhalb des Webinars erreicht werden. Dadurch wird die aktive Beteiligung der Teilnehmenden gefördert und das Verständnis der vermittelten Inhalte gestärkt. Um den Lernenden nach dem Webinar die Möglichkeit zu geben, die Inhalte zu wiederholen oder bei Bedarf erneut darauf zuzugreifen, sollten die Webinare aufgezeichnet und in der [Dokumentation](#videos) als Videoressource bereitgestellt werden.

#### Dokumentation

Ein ergänzendes Format zur Schulung ist die Nutzung von Dokumentationen, wie sie im Abschnitt [Dokumentation](#dokumentation) beschrieben sind. Dokumentationen können in verschiedenen Formaten erstellt werden, von statischen PDF-Dokumenten bis hin zu interaktiven Anleitungen, die es den Lernenden ermöglichen, die Schulungsinhalte eigenständig und in ihrem eigenen Tempo zu erarbeiten. 

Ein besonders nützliches Dokumentationsformat sind Videoaufzeichnungen von Schulungen. Diese können auf internen Plattformen oder über [Dokumentations-Webseiten](#dokumentations-webseiten) zur Verfügung gestellt werden, sodass die Lernenden jederzeit darauf zugreifen können. Ebenso können Anbindungsleitfäden, wie sie in der [technischen Dokumentation](#technische-dokumentation) beschrieben werden, als Referenzmaterial und zur Selbstschulung genutzt werden.

Zusätzlich zur Bereitstellung von Dokumentationen und Videos kann es sinnvoll sein, spezielle [Schulungsumgebungen](#demo--oder-schulungsumgebungen-von-systemen) zu nutzen, die es den Teilnehmenden ermöglichen, das Erlernte in einer sicheren und kontrollierten Umgebung praktisch anzuwenden. Dies fördert das Verständnis und die Anwendung der Schulungsinhalte im realen Arbeitsumfeld.

### Allgemeine Empfehlungen zu Schulungen

#### Schulungsumgebungen nutzen: 
Wenn verfügbare Systeme über spezielle Schulungsumgebungen verfügen, sollten diese intensiv genutzt werden. Diese Umgebungen bieten den Vorteil, dass die Teilnehmenden die Schulungsinhalte unter realitätsnahen Bedingungen anwenden können, ohne das Risiko einzugehen, produktive Systeme zu beeinträchtigen. Schulungsumgebungen können beispielsweise in [interaktiven Anleitungen](#interaktive-anleitungen) eingebettet werden, um den Nutzern eine kontextsensitive Unterstützung zu bieten.

#### Gruppenarbeit statt Frontalunterricht: 
Um die aktive Teilnahme und den Austausch unter den Schulungsteilnehmenden zu fördern, sollte vermehrt auf Gruppenarbeit anstelle von reinem Frontalunterricht gesetzt werden. Gruppenarbeit ermöglicht es den Teilnehmenden, das Erlernte in einem kollaborativen Umfeld zu diskutieren und anzuwenden, was das Verständnis der Inhalte vertieft. Die praktische Anwendung von Wissen in Gruppen stärkt zudem die Teamarbeit und fördert den Wissenstransfer innerhalb der Gruppe, wie auch in [internen Meetings und Workshops](#interne-meetings-und-workshops) häufig genutzt.

#### Wissensabfrage durch Tests: 
Um den Lernerfolg zu messen und sicherzustellen, dass die vermittelten Inhalte verstanden wurden, sollten nach Abschluss der Schulungen Wissensabfragen durchgeführt werden. Diese können in Form von Tests oder Quizfragen gestaltet werden und helfen dabei, den Kenntnisstand der Teilnehmenden zu überprüfen. Regelmäßige Wissensabfragen ermöglichen es zudem, Schwachstellen zu identifizieren und gezielt nachzubessern, ähnlich wie bei [Regelmäßigen Auffrischungen](#regelmäßige-auffrischung), die sicherstellen, dass das Wissen der Teilnehmenden stets aktuell bleibt.

#### Regelmäßige Auffrischung: 
Schulungsinhalte sollten regelmäßig aufgefrischt werden, um sicherzustellen, dass das Wissen auf dem neuesten Stand bleibt. Insbesondere in sich schnell entwickelnden technischen Bereichen ist eine kontinuierliche Weiterbildung essenziell. Regelmäßige Webinare und die Aktualisierung von [Dokumentationen](#dokumentations-webseiten) tragen dazu bei, dass alle Beteiligten stets auf dem neuesten Stand der Technik und der Prozesse sind. Dies kann durch die regelmäßige Veröffentlichung von Updates über [interne Webseiten](#interne-webseiten) oder durch den Versand von [Newslettern](#newsletter--e-mail-verteiler) effektiv unterstützt werden.