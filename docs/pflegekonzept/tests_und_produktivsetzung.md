---
id: tests_und_produktivsetzung
title: "Tests und Produktivsetzung"
---


:::info
Der Begriff "Onlinedienst" bezieht sich im Kontext dieser Seite auf ein Datenobjekt `Onlinedienst`, das eine Zuständigkeit für ein Gebiet (i.e. ein ARS) hat. 
Die Tests, die auf dieser Seite behandelt werden, beziehen sich immer auf eine für ein spezifisches Gebiet Konfigurierte Instanz eines Onlinedienstes und sind von allgemein von Nachnutzenden durchzuführen.  
Die hier beschriebenen Tests sind nicht zu verwechseln mit solchen, die vom Hersteller des Onlinedienstes im Zuge der Software-Entwicklung durchgeführt werden. 
:::

# Tests und Produktivsetzung

## Einführung

Diese Seite beschreibt die wesentlichen Prozesse und Anforderungen, die für die effektive und sichere Inbetriebnahme von EfA-Onlinediensten notwendig sind. Im Mittelpunkt steht die Testphase, die entscheidend ist, um die Funktionsfähigkeit und Sicherheit eines Onlinedienstes vor dessen Produktivsetzung zu gewährleisten. Der Text gibt einen umfassenden Überblick über den Lebenszyklus eines Onlinedienstes und erklärt, wie die Übergänge zwischen den Phasen, insbesondere von der Test- zur Produktionsphase, gesteuert werden können. Dabei wird detailliert auf die [Phasen des Lebenszyklus](#lebenszyklus-eines-onlinedienstes) und die [Anforderungen an die Testfähigkeit](#anforderungen-an-die-testfähigkeit) eingegangen.

Besondere Aufmerksamkeit wird den Szenarien gewidmet, in denen die notwendigen Systemvoraussetzungen für umfassende Tests nicht vollständig erfüllt sind, was in der Praxis häufig vorkommt. Es werden [alternative Prozesse und Maßnahmen](#umgang-mit-eingeschränkter-testfähigkeit) aufgezeigt, die es ermöglichen, auch unter eingeschränkten Bedingungen Tests durchzuführen. 

In den nachfolgenden Abschnitten wird die [Planung und Durchführung der Tests](#durchführung-von-tests), die schrittweise [Produktivsetzung des Onlinedienstes](#produktivsetzung-des-onlinedienstes) sowie [Risiken und Risikomanagement](#risiken-und-risikomanagement) detailliert erläutert. Abschließend werden Strategien zur Minimierung von Risiken vorgestellt, um eine erfolgreiche Produktivsetzung sicherzustellen. 

## Lebenszyklus eines Onlinedienstes


Ein Onlinedienst durchläuft während seiner Nutzungsdauer verschiedene Phasen, die als Lebenszyklus bezeichnet werden. Dieser Lebenszyklus beginnt mit der Planung und geht über die Initialisierung und den Testbetrieb bis zur produktiven Nutzung. In einigen Fällen schließt sich eine Wartungsphase an, bevor der Dienst schließlich deaktiviert wird. Jede dieser Phasen erfordert spezifische Maßnahmen und Konfigurationen, um sicherzustellen, dass der Onlinedienst den Anforderungen entspricht und ordnungsgemäß funktioniert.

#### Phasen des Lebenszyklus

Der Lebenszyklus eines Onlinedienstes gliedert sich in mehrere Phasen:

1. **Planung**: In dieser Phase wird der Onlinedienst konzipiert und die grundlegenden Anforderungen werden definiert. Dies umfasst auch die Festlegung von Parametern, die später den Betrieb des Dienstes steuern.

2. **Initialisierung**: Der Onlinedienst wird eingerichtet und für die Inbetriebnahme vorbereitet. Dies beinhaltet die technische Installation sowie die Konfiguration der erforderlichen Fachverfahren und Redaktionssysteme.

3. **Test**: Der Onlinedienst wird überprüft, um sicherzustellen, dass alle Funktionen korrekt arbeiten. Dieser Schritt ist entscheidend, um Fehler vor der Produktivsetzung zu identifizieren und zu beheben. 

4. **Produktion**: Nach erfolgreich abgeschlossenen Tests wird der Onlinedienst für den öffentlichen Betrieb freigegeben. Ab diesem Punkt ist er für die Nutzer verfügbar und wird regulär betrieben.

5. **Wartung**: Während der Wartungsphase kann der Onlinedienst vorübergehend außer Betrieb genommen werden, um Aktualisierungen durchzuführen oder Probleme zu beheben.

6. **Deaktivierung**: Schließlich wird der Onlinedienst außer Dienst gestellt, wenn er nicht mehr benötigt wird oder durch einen neuen Dienst ersetzt wurde.

#### Verweis auf relevante Statusinformationen

Der Status eines Onlinedienstes ist von zentraler Bedeutung für die Steuerung seines Lebenszyklus. Jeder Phase ist ein spezifischer Status zugeordnet, der sowohl in den Redaktionssystemen als auch in den Fachverfahren hinterlegt ist. Diese Statusinformationen steuern, in welcher Phase sich der Onlinedienst befindet und welche Aktionen zu diesem Zeitpunkt zulässig sind. Weitere Details zur Ermittlung und Nutzung dieser Statusinformationen sind auf der Seite [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md) beschrieben. 

Für eine umfassendere Übersicht über den gesamten Lebenszyklus und die damit verbundenen Anforderungen an die Aktivierung des Onlinedienstes wird auf die Seite [Aktivierung des Onlinedienstes](../parametrisierung_anhänge/6_15_aktivierung_des_onlinedienstes.md) verwiesen. Hier werden die spezifischen Schritte, die zur Aktivierung und Pflege des Onlinedienstes erforderlich sind, detailliert erläutert.

## Anforderungen an die Testfähigkeit

Die Testfähigkeit eines EfA-Onlinedienstes beschreibt die vorhandenen Möglichkeiten zur Durchführung von Tests. Diese sind stark von den verfügbaren Systemen und Ressourcen abhängig. Der Abschnitt zeigt auf, welche Voraussetzungen idealerweise erfüllt sein sollten, und gibt Empfehlungen für den Umgang mit typischen Einschränkungen.

### Systemvoraussetzungen für Tests

Für eine erfolgreiche Nachnutzung von EfA-Onlinediensten ist es wichtig, dass sowohl die Redaktionssysteme als auch die Fachverfahren Testmöglichkeiten bieten. In der Praxis sind diese jedoch oft nur teilweise vorhanden.

**Testumgebungen in Redaktionssystemen und Onlinediensten:** Ideal wäre eine realitätsnahe Testumgebung. Wenn diese nicht verfügbar ist, können Tests in begrenzten Systemteilen oder eine Überwachung des Produktivsystems erwogen werden.

**Prüfung der Nachnutzbarkeit:** Bereits bei der Entwicklung eines Onlinedienstes sollte geprüft werden, ob die vorhandenen Systeme Tests ermöglichen. Die Einbindung der Nachnutzenden kann dabei helfen, spezifische Anforderungen an die Testfähigkeit zu stellen.

### Testdaten und ihre Behandlung

**Verwendung von synthetischen Daten:** Synthetische, repräsentative Testdaten sind vorzuziehen, um den Datenschutz zu gewährleisten. Wenn dies nicht möglich ist, sollten Testdaten klar von produktiven Daten getrennt und entsprechend gekennzeichnet werden.

**Trennung von Test- und Produktionsdaten:** Eine strikte Trennung ist wünschenswert, aber oft herausfordernd. Wo dies nicht möglich ist, müssen Prozesse etabliert werden, um sicherzustellen, dass Testdaten nicht in produktive Abläufe einfließen.

### Zusammenarbeit mit Fachverfahren

**Planung und Durchführung von Tests:** Die enge Zusammenarbeit mit den Anbietern von Fachverfahren ist zentral, um Testkonfigurationen festzulegen und sicherzustellen, dass Testdaten korrekt verarbeitet werden. Dies sollte frühzeitig in der Planungsphase geklärt werden.

**Konfiguration der Fachverfahren:** Für realitätsnahe Tests müssen Fachverfahren so eingestellt sein, dass Testdaten zuverlässig erkannt und verarbeitet werden. Falls dies nicht möglich ist, müssen alternative manuelle Prozesse implementiert werden.

### Umgang mit eingeschränkter Testfähigkeit

In der Praxis fehlen oft vollständige Testsysteme. Hier sollten alternative Testansätze verfolgt werden, etwa manuelle Prüfungen und eine genaue Überwachung der Produktivsysteme. Auch die Kennzeichnung von Testdaten im Produktivsystem sowie der eingeschränkte Zugriff auf das Produktivsystem während der Testphase sind mögliche Ansätze. Wenn keine Tests vor der Produktivsetzung möglich sind, müssen sich Nachnutzende der [Risiken](#risiken-und-risikomanagement) bewusst sein und besondere Vorsicht walten lassen, um Fehler frühzeitig zu erkennen und zu korrigieren.

## Durchführung von Tests

Die Durchführung von Tests ist entscheidend, um die Funktionalität und Sicherheit eines EfA-Onlinedienstes vor dessen Produktivsetzung sicherzustellen. Die folgenden Abschnitte bieten Empfehlungen zur Planung, den verschiedenen Testphasen und den erforderlichen [Rücksetzprozessen](#rücksetzprozesse).

### Testszenarien

Der Umfang der zu testenden Funktionen hängt von verschiedenen Szenarien ab. Vor der Produktivsetzung eines neuen Onlinedienstes sind umfassende Tests notwendig. Bei Versionsupdates eines bestehenden Onlinedienstes müssen Tests sicherstellen, dass keine unerwarteten Probleme auftreten, insbesondere bei [sicherheitskritischen Updates](../versionierung_onlinedienst_produkt.md#kritische-änderungsszenarien). Änderungen an grundlegenden Parametern, die Auswirkungen auf Authentifizierungsmethoden, Schnittstellen zu externen Systemen oder Datenverarbeitungslogiken haben, erfordern ebenfalls gezielte Tests.

### Vorbereitung und Planung

Eine sorgfältige Planung der Tests ist notwendig, um sicherzustellen, dass alle relevanten Aspekte des Onlinedienstes überprüft werden. Dazu gehört die Erstellung eines detaillierten Testplans, der Zeitpläne und Checklisten umfasst.

**Erstellung eines Testplans:** Der Testplan sollte die zu testenden Funktionen des Onlinedienstes klar definieren und Prioritäten setzen. Es ist hilfreich, die Testphasen auf den [Lebenszyklus](#lebenszyklus-eines-onlinedienstes) des Onlinedienstes abzustimmen, wie sie in den Abschnitten "Initialisierung", "Test", und "Produktion" beschrieben sind. Der Status des Onlinedienstes kann über den Parameter [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md) im Redaktionssystem gesetzt werden, um den Übergang zwischen den Phasen zu steuern. Weitere Details hierzu finden sich auf der Seite [Aktivierung des Onlinedienstes](../parametrisierung_anhänge/6_15_aktivierung_des_onlinedienstes.md). 

**Zeitpläne und Checklisten:** Der Testplan sollte einen realistischen Zeitrahmen für die Durchführung der verschiedenen Testphasen vorsehen. Eine Checkliste kann dabei helfen, sicherzustellen, dass alle notwendigen Schritte vor der Produktivsetzung durchgeführt werden, einschließlich der Überprüfung aller relevanten Parameter und Schnittstellen.

### Testphasen

Die Testphasen sollten schrittweise durchgeführt werden, um mögliche Fehler frühzeitig zu identifizieren und zu beheben. Jede Phase hat ihre eigene Bedeutung im Gesamtprozess.

**Initialisierungstest:** In dieser Phase wird überprüft, ob der Onlinedienst korrekt eingerichtet und alle notwendigen Systeme und Schnittstellen bereitstehen. Der Status des Onlinedienstes sollte hier auf "Initialisierung" gesetzt werden. Die Festlegung des entsprechenden Statuscodes (`20`) kann über die Redaktionssysteme und das PVOG erfolgen. Detaillierte Anweisungen zur Statusverwaltung finden sich auf der Seite [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md).

**Funktionstest:** Sobald der Onlinedienst initialisiert ist, sollte ein Funktionstest durchgeführt werden, um sicherzustellen, dass alle wesentlichen Funktionen ordnungsgemäß arbeiten. Hier wird der Status des Onlinedienstes auf "Test" (Statuscode `30`) gesetzt. Es sollten sowohl automatisierte Tests als auch manuelle Prüfungen durchgeführt werden, um alle möglichen Fehlerquellen abzudecken.

**Integrationstest:** Der Integrationstest stellt sicher, dass der Onlinedienst nahtlos mit den beteiligten Fachverfahren und anderen Systemen zusammenarbeitet. Besonderes Augenmerk sollte auf die Datenflüsse zwischen den Systemen gelegt werden. Sollte kein dediziertes Testsystem vorhanden sein, kann es erforderlich sein, diese Tests in einer eingeschränkten Produktivumgebung durchzuführen, wobei Testdaten klar gekennzeichnet und von Echt-Daten getrennt werden sollten.

**Dokumentation und Auswertung:** Alle Testergebnisse sollten sorgfältig dokumentiert und ausgewertet werden. Diese Dokumentation bildet die Grundlage für die Entscheidung, ob der Onlinedienst in die Produktionsphase überführt werden kann.

### Vorgehensweisen bei Tests in Abhängigkeit von der Verfügbarkeit von Testumgebungen

Die Verfügbarkeit von Testumgebungen beeinflusst maßgeblich die Vorgehensweise bei Tests. Im Folgenden werden die Unterschiede und Herausforderungen für Redaktionssysteme, Onlinedienste und Fachverfahren beschrieben.

**Tests mit Test-Redaktionssystemen**: Wenn eine Testumgebung für das Redaktionssystem existiert, können dort Parameter-Sets konfiguriert werden, die alle relevanten Testfälle abdecken, unabhängig von den realen Verhältnissen. Test-Redaktionssysteme können auch verwendet werden, um ein reales, aber zukünftiges Parameter-Set für eine Kommune zu entwicklen, während gleichzeitig für diese Kommune ein anderes Parameter-Set in der produktiven Umgebung genutzt wird. Die Details der Teststategie bei Tests mit Test-Redaktionssystemen hängen auch davon ab, auf welchem Weg ein Test-Parameter-Set zum Onlinedienst gelangt, damit es dort operativ getestet werden kann. Die Herausforderung liegt im Übergang dieser Konfigurationen in die Produktion. Dabei müssen produktionsrelevante Parameter identifiziert und testumgebungsspezifische Daten ausgeschlossen werden. Dieser Prozess erfolgt oft manuell, was Fehleranfälligkeit birgt. Eine abschließende Qualitätssicherung ist daher notwendig.
Falls kein Test-Redaktionssystem verfügbar ist, muss ein zukünftiges Parameter-Set im produktiven Redaktionssystem erarbeitet werden, während gleichzeitig ein aktuell produktives Parameter-Set für dieselbe Leistung und dieselbe Kommune vorhanden ist. Dies kann gelingen, indem eine fiktive Test-Kommune (ARS) als Zuständigkeits-Gebiet eingetragen und bei den Testdurchführungen genutzt wird. Bei der Produktivsetzung des qualitätsgesicherten Parameter-Sets nach Abschluss der Tests muss dann das bestehenden Parameter-Set gelöscht oder zeitlich ungültig gesetzt werden und das neue Parameter-Set muss für die reale Kommune als zuständig eingetragen werden.

**Tests mit Test-Onlinediensten**: Test-Onlinedienste können beliebig verwendet werden, um Testfälle ohne Auswirkungen auf die reale Welt auszuführen. Die Testfälle werden bestimmt durch Test-Parameter-Sets, die idealerweise aus Test-Redaktionssystemen und aus einer Test-PVOG-Instanz bzw. einem Test-DVDV zugespielt werden. Ein Test-Onlinedienst sendet seine Antragsdaten stets an ein Test-Fachverfahren. Davon völlig unabhängig ist die rein technische Konfiguration der Onlinedienst-Umgebungen, die in der Verantwortung von IT-Betrieb und Bereitsteller liegt und sich nur selten ändert. Auch diese Konfiguration und ihre Änderungen müssen qualitätsgesichert werden. Dies ist aber unabhängig vom Parameter-Pflegeprozess. Falls kein Test-Onlinedienst verfügbar ist, kann der Test eines zu entwickelnden Parameter-Sets im Rahmen der Parameter-Pflege auch relativ gut mit der produktiven Onlinedienst-Instanz durchgeführt werden, vorausgesetzt, das Test-Parameter-Set ist dem produktiven Onlinedienst zugänglich. Wichtig ist, dass der produktive Onlinedienst z.B. anhand des Parameters OnlinedienstStatus erkennen kann, dass es sich um einen Testbetrieb in Produktion handelt. Ist auch das nicht möglich, muss sehr sorgfältig darauf geachtet werden, dass die entstehenden Test-Antragsdaten nicht an ein produktives Fachverfahren gesendet werden.

**Tests mit Test-Fachverfahren:** Wenn ein Test-Fachverfahren vorhanden ist, müssen die getesteten Konfigurationen in die Produktionsumgebung übertragen werden. Ist dies nicht der Fall und wurde das produktive Fachverfahren für Tests genutzt, müssen nach Abschluss der Tests entsprechende Anpassungen für den Übergang in den produktiven Modus vorgenommen werden.

### Rücksetzprozesse

Es ist wichtig, Verfahren zur Rücksetzung auf vorherige Systemstände zu haben, falls während der Tests kritische Fehler auftreten.

**Wiederherstellung vorheriger Versionen:** Falls während der Tests Fehler auftreten, sollte die Möglichkeit bestehen, das System auf einen früheren, stabilen Zustand zurückzusetzen. In vielen Systemen gibt es keine automatische Versionierung, daher sollten manuelle Backups vor jeder Testphase angelegt werden.

**Manuelle Prozesse bei fehlender Funktionalität:** Wenn automatische Rücksetzmechanismen fehlen, müssen manuelle Prozesse etabliert werden. Dazu gehört die detaillierte Dokumentation aller Änderungen und die klare Kennzeichnung der Versionen, die getestet werden. Sollte ein Rollback notwendig sein, kann der Status des Onlinedienstes im Redaktionssystem zurückgesetzt werden, um den Dienst wieder in den Test- oder Initialisierungszustand zu versetzen. Weitere Informationen zur Verwaltung des Status sind auf der Seite [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md) zu finden.

**Strategien zur Fehlervermeidung:** In Fällen, in denen keine ausreichenden Rücksetzprozesse vorhanden sind, sollten alternative Strategien wie die schrittweise Einführung des Onlinedienstes oder die Durchführung von Tests in einer isolierten Umgebung in Betracht gezogen werden. Falls Tests direkt in der Produktionsumgebung durchgeführt werden müssen, sollten diese nur unter strengen Sicherheitsvorkehrungen und mit eingeschränktem Zugang erfolgen.

## Produktivsetzung des Onlinedienstes

Die Produktivsetzung eines EfA-Onlinedienstes folgt auf die erfolgreichen Tests und markiert den Übergang des Dienstes in den öffentlichen Betrieb. Dieser Abschnitt beschreibt die notwendigen Schritte zur Vorbereitung, Qualitätssicherung und Aktivierung des Onlinedienstes, einschließlich der Verwendung von Statusinformationen zur Steuerung des Prozesses.

### Vorbereitende Schritte

Die technische Installation und Konfiguration des Onlinedienstes bildet die Grundlage für die Produktivsetzung. Diese Schritte umfassen die Bereitstellung und Validierung aller notwendigen Parameter in den relevanten Systemen wie PVOG und DVDV.

**Installation und Konfiguration:** Der Onlinedienst muss in der Zielumgebung installiert und konfiguriert werden. Dies schließt die Anpassung an die spezifischen Gegebenheiten der nachnutzenden Gebietskörperschaft ein. Die notwendigen Parameter sollten dabei sorgfältig in den entsprechenden Systemen hinterlegt und überprüft werden. Für detaillierte Anweisungen zu diesen Prozessen wird auf die Seite [Aktivierung des Onlinedienstes](../parametrisierung_anhänge/6_15_aktivierung_des_onlinedienstes.md) verwiesen.

**Validierung von Parametern:** Vor der endgültigen Produktivsetzung sollten alle Parameter, die für den Betrieb des Onlinedienstes erforderlich sind, im PVOG und DVDV validiert werden. Dies gewährleistet, dass der Dienst in der Produktionsumgebung korrekt funktioniert und alle regionalen Anpassungen berücksichtigt wurden. 

### Qualitätssicherung

Die Qualitätssicherung stellt sicher, dass alle vorhergehenden Testphasen erfolgreich abgeschlossen wurden und dass der Onlinedienst bereit für die Produktivsetzung ist.

**Abschließende Freigabe:** Bevor der Onlinedienst in den Produktivbetrieb übergeht, sollten die Ergebnisse der Testphasen überprüft und dokumentiert werden. Diese Prüfung umfasst die Kontrolle, ob alle identifizierten Fehler behoben wurden und ob die definierten Anforderungen vollständig erfüllt sind. Eine formale Freigabe durch die zuständigen Stellen ist erforderlich, um sicherzustellen, dass der Dienst alle Qualitätsstandards erfüllt.

**Prozesse zur Freigabe:** Die abschließende Freigabe des Onlinedienstes sollte einen strukturierten Prozess umfassen, der sicherstellt, dass alle relevanten Beteiligten, einschließlich technischer und fachlicher Ansprechpartner, den Übergang in die Produktion unterstützen und genehmigen. Dies umfasst auch die abschließende Überprüfung der in PVOG und DVDV hinterlegten Parameter und deren Konsistenz mit den Testergebnissen.

### Aktivierung und Steuerung über Statusinformationen

Die Aktivierung des Onlinedienstes erfolgt durch die gezielte Steuerung über Statusinformationen, die den Übergang in den produktiven Betrieb ermöglichen.

**Anwendung von Statusinformationen:** Der Status des Onlinedienstes wird über das PVOG verwaltet und signalisiert den Übergang von der Test- in die Produktionsphase. Der relevante Statuscode für die Produktivsetzung ist "Produktion" (Statuscode `40`). Dieser Status sollte im Redaktionssystem gesetzt werden, um den Onlinedienst für den öffentlichen Betrieb freizugeben. Weitere Informationen zur Verwaltung dieser Statusinformationen sind auf der Seite [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md) verfügbar.

**Bedeutung für unterschiedliche Systeme:** Die Statusinformationen spielen eine zentrale Rolle bei der Steuerung des Onlinedienstes in den verschiedenen Systemen, wie Portalen und Onlinedienst-IT-Systemen. Diese Systeme nutzen die Statusinformationen, um festzustellen, ob der Dienst aktiv und für den Nutzer verfügbar ist. Der Status "Produktion" signalisiert, dass der Onlinedienst öffentlich zugänglich ist und regulär genutzt werden kann.

## Risiken und Risikomanagement

Die Produktivsetzung eines Onlinedienstes ohne ausreichende Tests birgt mehrere Risiken, die die Funktionalität, Sicherheit und rechtliche Verlässlichkeit des Dienstes beeinträchtigen können. Zudem können fehlende oder fehlerhafte Statusinformationen den Lebenszyklus und die Verfügbarkeit des Onlinedienstes negativ beeinflussen. In diesem Abschnitt werden die potenziellen Risiken erläutert und Maßnahmen zur Risikominimierung beschrieben.

### Risiken bei einer Produktivsetzung ohne ausreichende Tests

Eine unzureichende Testabdeckung vor der Produktivsetzung kann zu verschiedenen Problemen führen, die sowohl technische als auch rechtliche Konsequenzen haben.

- **Ausfall des Systems:** Wenn ein Onlinedienst ohne ausreichende Tests in Betrieb genommen wird, besteht das Risiko, dass unerwartete Fehler zu einem vollständigen oder teilweisen Ausfall des Systems führen. Um dieses Risiko zu minimieren, sollten zumindest grundlegende Tests in einer isolierten Umgebung durchgeführt werden. Bei bekannten Schwachstellen ist es ratsam, Notfallpläne für die schnelle Wiederherstellung des Systems bereitzuhalten.

- **Fehlerhafte Funktionsweise / Fehlende Verfügbarkeit von Funktionen:** Unzureichend getestete Onlinedienste können Funktionen nicht wie vorgesehen ausführen oder bestimmte Funktionen könnten ganz fehlen. Dies kann dazu führen, dass Nutzer nicht in der Lage sind, ihre Anliegen korrekt zu bearbeiten. Um dem vorzubeugen, sollten manuelle Überprüfungen der wichtigsten Funktionen durchgeführt werden, falls umfassende Tests nicht möglich sind.

- **Inhaltliche Fehler in Anträgen:** Ohne ausreichende Tests könnten Onlinedienste falsche oder unvollständige Daten verarbeiten, was zu inhaltlichen Fehlern in Anträgen führt. Diese Fehler können schwerwiegende Folgen haben, wenn sie rechtlich verbindliche Amtshandlungen betreffen. Es empfiehlt sich, kritische Datenverarbeitungsprozesse manuell zu überwachen und zu prüfen, um solche Risiken zu verringern.

- **Verzögerungen in der Verarbeitung von Anträgen:** Fehler in der Antragsbearbeitung, die auf unzureichende Tests zurückzuführen sind, können zu erheblichen Verzögerungen bei der Bearbeitung von Anträgen führen. Um dies zu vermeiden, sollten Engpässe im Prozess identifiziert und manuell überwacht werden, insbesondere in den frühen Phasen der Produktivsetzung.

- **Anträge gehen verloren:** Das Risiko, dass Anträge verloren gehen, ist besonders hoch, wenn die Testphase unzureichend war. Solche Verluste können vermieden werden, indem die Datenübertragung und -speicherung manuell überprüft wird, bis die Stabilität des Systems bestätigt ist.

- **Rechtlich verbindliche Amtshandlungen aufgrund falscher Tatsachen:** Ein schwerwiegendes Risiko besteht darin, dass fehlerhafte Anträge zu rechtlich verbindlichen Entscheidungen führen, die auf falschen Tatsachen beruhen. Um solche Situationen zu verhindern, sollten Rechtsabteilungen in den Prozess einbezogen werden, um kritische Entscheidungen vor der endgültigen Umsetzung zu überprüfen.

### Risiken im Zusammenhang mit fehlenden oder fehlerhaften Statusinformationen

Statusinformationen spielen eine zentrale Rolle im Lebenszyklusmanagement eines Onlinedienstes. Fehlerhafte oder fehlende Statusinformationen können den Betrieb erheblich beeinträchtigen.

- **Falsche oder veraltete Statusinformationen führen zu Verwirrung:** Wenn der Status des Onlinedienstes nicht korrekt gesetzt oder nicht rechtzeitig aktualisiert wird, kann dies dazu führen, dass der Dienst als verfügbar angezeigt wird, obwohl er sich in einer Test- oder Wartungsphase befindet. Ebenso kann der Dienst als produktiv dargestellt werden, obwohl er noch nicht einsatzbereit ist. Dies führt zu Verwirrung bei den Nutzern und möglicherweise zu Fehlanwendungen des Dienstes.

- **Der Onlinedienst kann Statusinformationen nicht korrekt auswerten:** Ein erhebliches Risiko besteht darin, dass der Onlinedienst den Statusparameter nicht korrekt auswertet und sich somit stets so verhält, als sei er in einem produktiven Zustand. Dies kann dazu führen, dass der Dienst keine sichtbaren Hinweise wie ein Banner anzeigt, die Antragsdaten nicht als Testdaten kennzeichnet oder Daten fälschlicherweise an produktive Adressaten sendet. Dadurch wird eine saubere Trennung zwischen Test- und Produktionsphasen unmöglich. 

### Maßnahmen zur Risikominimierung

Zur Minimierung der beschriebenen Risiken sollten folgende Maßnahmen ergriffen werden:

- **Sorgfältige Planung der Testphasen:** Automatische Tests aller Funktionen und Systeme sollten angestrebt werden. Auch wenn vollständige Tests nicht möglich sind, sollten alle kritischen Bereiche zumindest manuell getestet werden. Ein strukturierter Testplan kann helfen, die wichtigsten Funktionen und Datenverarbeitungsprozesse abzudecken.

- **Etablierung von Notfallplänen:** Für den Fall, dass ein Onlinedienst nach der Produktivsetzung Fehler aufweist, sollten Notfallpläne existieren, die eine schnelle Wiederherstellung des Dienstes ermöglichen. Dazu gehört auch die Möglichkeit, den Dienst temporär abzuschalten und auf eine frühere Version zurückzusetzen.

- **Regelmäßige Überprüfung und Anpassung von Statusinformationen:** Die Statusinformationen sollten regelmäßig überprüft und bei Bedarf angepasst werden, um sicherzustellen, dass der Onlinedienst korrekt gesteuert wird und Missverständnisse vermieden werden. Hierfür sollte ein klar definierter Prozess existieren, der sowohl manuelle als auch automatische Überprüfungen umfassen kann.

- **Implementierung von Alarmsystemen bei falschen oder veralteten Statusinformationen**: Alarmsysteme können genutzt werden, um den Betrieb zu benachrichtigen, wenn Statusinformationen falsch oder veraltet sind. Diese Alarme sollten frühzeitig eingreifen, um potenzielle Fehlanwendungen des Dienstes zu verhindern.

- **Klare Anforderungen an die Fähigkeit des Onlinedienstes, Statusparameter auszuwerten**: Der Onlinedienst muss so konfiguriert sein, dass er den Statusparameter zuverlässig interpretiert und seine produktive Funktion nur dann ausführt, wenn der Status auf „Produktion“ gesetzt ist. Diese klare Anforderung ist entscheidend, um sicherzustellen, dass Tests und Wartungsphasen klar von produktiven Phasen getrennt sind und Nachnutzende die notwendige Kontrolle über den Zustand des Dienstes haben.