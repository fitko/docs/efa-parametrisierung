---
id: verantwortung_fuer_Datenintegritaet
title: "Verantwortung für die Integrität von Parameterwerten"
---

# Verantwortung für die Integrität von Parameterwerten

Die Festlegung von Verantwortlichkeiten in einem heterogenen System wie der Parametrisierung ist eine anspruchsvolle Aufgabe. Diese Seite bietet einen Einblick in die verschiedenen Herausforderungen und Lösungsansätze, die notwendig sind, um die Integrität der Daten sicherzustellen und die Qualität der digitalen Dienste zu gewährleisten.

## Parameter und Integrität 

Die Integrität im Bereich der Informationssicherheit beschreibt die Korrektheit, Konsistenz und Vollständigkeit der Informationen über ihre gesamte Lebensdauer hinweg. Integrität bezeichnet dabei einerseits den Zustand der Informationen als solches wie auch den Prozess der Sicherstellung dieses Zustands. Im Kontext der Parametrisierung bedeutet dies, dass zu jedem Zeitpunkt
* alle Angaben zu einer Leistung korrekt sind,
* keine widersprüchlichen Angaben zu der Leistung gemacht werden und
* alle notwendigen Informationen vorhanden sind.

Diese Seite behandelt die Frage, wer für die Integrität der Parameter-Daten verantwortlich ist und schlägt eine mögliche Aufteilung vor. <!--beleuchtet beispielhaft, wer das Impressum eines Onlinedienstes bereitstellt.-->

## Verantwortlichkeiten

Die Erstellung eines Parameter-Sets, also der Gesamtheit aller Parameter und ihrer Werte zur Steuerung eines Onlinedienstes, ist ein anspruchsvoller Prozess. Das Ergebnis ist ein komplexes Gebilde, das für die korrekte Funktion des Onlinedienstes konsistent sein und den rechtlichen Vorgaben der jeweiligen Behördenleistung sowie allgemeinen rechtlichen Normen wie der [DSGVO](https://dsgvo-gesetz.de/) und dem [Digitale-Dienste-Gesetz](https://gesetz-digitale-dienste.de/) entsprechen muss.

Um eine hohe Datenqualität sicherzustellen, müssen Verantwortlichkeiten klar definiert und Qualitätssicherungsprozesse etabliert werden. Dies ist besonders wichtig, da die Prozesse und Infrastrukturen, mit denen die Landessysteme ihre Datenpflege organisieren, von Bundesland zu Bundesland unterschiedlich sind. 
Es gibt zentralistische Ansätze, bei denen eine Landesredaktion einen großen Teil der operativen Datenpflege durchführt. Es gibt aber auch dezentrale Ansätze, bei denen kommunale Mitarbeiter die Daten pflegen. Innerhalb dieses Spektrums gibt es Modelle mit direktem Zugriff der kommunalen Mitarbeiter auf die zentralen Landesredaktionssysteme und Modelle, bei denen kommunale Redaktionssysteme dezentral genutzt werden und die dort gepflegten Daten zunächst an das Landesredaktionssystem übertragen werden, bevor sie an das PVOG weitergereicht werden. Die Hoheit für die Gestaltung der Datenpflegeprozesse und der Infrastrukturen in den Ländern und Kommunen liegt bei den Ländern und Kommunen.

Der Prozess der Datenpflege im Rahmen der Parametrisierung umfasst viele Beteiligte. Daher ist es sinnvoll, allgemein zwischen der Ausführungsverantwortung und der Ergebnisverantwortung zu unterscheiden. 

* **Die Ausführungsverantwortlichen** sind dafür zuständig, die Daten nach Auftrag korrekt in die Systeme einzupflegen. Das bedeutet, sie müssen die Daten vollständig und im richtigen Format an den vorgesehenen Stellen im Redaktionssystem eintragen.

* **Die Ergebnisverantwortlichen** hingegen sorgen dafür, dass die eingepflegten Daten am Ende korrekt sind. Sie müssen den gesamten Prozess überblicken und gegebenenfalls organisatorische Maßnahmen ergreifen, um sicherzustellen, dass die richtigen Daten rechtzeitig bereitgestellt werden.

## Allgemeine Integritätsverantwortung

Die Frage nach der Verantwortung für die Integrität der Parameter-Daten kann nicht pauschal beantwortet werden. Vielmehr hängt sie von der Art der Daten und vom landesspezifischen Pflegekonzept ab.

Im Folgenden sollten die relevanten Kategorien der Parameter-Daten aufgezählt werden, für die jeweils eigene Regeln der Verantwortlichkeit zu erwarten sind:
* Daten, die primär genutzt werden, um dem Antragsteller zu informieren („Informationsdaten“), z.B. Informationstexte, Angaben zur zuständigen Behörde, Angaben zu landes- oder kommunal-spezifischen Leistungsdetails, etc.
* Daten zur Antrags-Steuerung, z.B. Ein-/Ausblenden von Eingabefeldern, die im gewählten Gebiet auszufüllen / nicht auszufüllen sind, gebiets-spezifisch verfügbare Elemente in Auswahl-Listen, etc.
* Daten der Zuständigkeit und Adressierung von Organisationseinheiten als Anbieter der Behördenleistung, d.h. die Information, an welchen Adressaten der Online-Dienst den digitalen Antrag versendet
* Daten zur Abwicklung von Zahlungsdiensten, z.B. welcher Bezahldienstleister für die Leistung und in dem gewünschten Gebiet die Zahlung abwickelt sowie Zahlungs-Metadaten wie Kassenzeichen
* Ggf. weitere Daten

Jede dieser Kategorien lässt sich weiter in Subkategorien unterteilen, z.B. Informationsdaten zur Leistung vs. Informationsdaten zur Behörde, weil es unterschiedliche Kompetenzbereiche gibt, die mit diesen Informationskategorien jeweils verbunden sind.

## Vorschläge zur Pflegeverantwortung

Die Pflegekonzepte regeln die Datenpflegeprozesse auf verschiedenen Ebenen. Das vorliegende allgemeine Pflegekonzept legt grundlegende Leitlinien fest, ohne die Autonomie der Länder und Kommunen zu verletzen. Wo zentrale Vorgaben nicht möglich sind, werden konkrete Empfehlungen ausgesprochen. Länder und Kommunen müssen ergänzende Pflegekonzepte erstellen, um ihre spezifischen Prozesse und Vorgaben zu beschreiben.

Die Prozesse zur Pflege der steuernden Daten sind aufgrund ihrer Heterogenität unterschiedlich festzulegen. Beispielsweise werden Daten zur Steuerung der Bezahlprozesse von anderen Personen gepflegt als die Angaben zu kommunal-spezifischen Leistungsdetails oder die Daten zur technischen Adressierung der Fachverfahren.

Die Verzeichnisdienste PVOG und DVDV haben unterschiedliche technisch-organisatorische Rahmenbedingungen. Das PVOG erhält seine Daten aus den dezentralen Redaktionssystemen der Länder, während die Daten im DVDV ausschließlich von berechtigten Personen der *[pflegenden Stellen](https://docs.fitko.de/dvdv/pflegende-stellen/Kontaktdaten_pflegende-stellen)* gepflegt werden. Informationen für das DVDV müssen von Wissensträgern zusammengestellt und zur Eintragung beauftragt werden.

Die folgenden Zuordnungen von Verantwortung bei der Pflege der verschiedenen Datenarten sind als <u>Vorschläge</u> zu verstehen.

| Datenart | Ausführungsverantwortung | Ergebnisverantwortung | Kommentar |
|----------|--------------------------|------------------------|------------|
|Informationsdaten zu Leistungen, die landesweit homogen angeboten werden | Landesredaktion | Landesredaktion | Innerhalb der Landesredaktion sollte eine Qualitätssicherung durch eine separate Abteilung oder Person durchgeführt werden, die dann auch die Ergebnisverantwortung hat.|
|Informationsdaten zu Leistungen, die regional unterschiedlich angeboten werden | Jeweils die betroffene Behörde; Ausführungsverantwortlichkeit beinhaltet inhaltliche Qualitätssicherung | Landesredaktion | Die Behörden sind die beste Quelle für diese Informationen. Die Landesredaktion sollte als externe Organisation die Qualitätssicherung übernehmen. Dabei muss sich die abschließende Qualitätssicherung ggf. auf die formelle Korrektheit beschränken.|
|Informationsdaten zur Behörde inkl. fachlicher Zuständigkeit | Jeweils die betroffene Behörde; Ausführungsverantwortlichkeit beinhaltet inhaltliche Qualitätssicherung | Landesredaktion | Die Behörden sind die beste Quelle für diese Informationen. Die Landesredaktion sollte als externe Organisation die Qualitätssicherung übernehmen. Dabei muss sich die abschließende Qualitätssicherung ggf. auf die formelle Korrektheit beschränken.|
|Informationsdaten zum Onlinedienst | Landesredaktion unterstützt durch das Roll-In Team | Land als Auftraggeber der Onlinedienst (Nach-)Nutzung | Dies bezieht sich auf alle Angaben, die tatsächlich den Onlinedienst betreffen, nicht aber Angaben wie Link zum regionalen Impressum, die nur aus technischen Gründen am Onlinedienst verortet sind, sich tatsächlich aber auf die Behörde beziehen.|
|Daten zur Antrags-steuerung | Landesredaktion unterstützt durch das Roll-In Team | Land / Landesbehörde als Auftraggeber der Onlinedienst (Nach-)Nutzung | Die Verantwortung sollte auf Landes-Ebene liegen, falls die Rechtsgrundlagen im gesamten Bundesland einheitlich sind. Falls es kommunale Abweichungen gibt, sollte die Ausführungsverantwortung bei den jeweils betroffenen Behörden, unterstützt durch das Roll-In-Team liegen.|
|Daten der technischen Adressierung | Jeweils die betroffene Behörde unterstützt durch den IT-Dienstleister des Fachverfahrens und das Roll-In Team | Land / Landesbehörde als Auftraggeber der Onlinedienst (Nach-)Nutzung | Die Empfehlung geht von regionalen bzw. kommunalen Fachverfahren aus. Im Fall von landesweit zentralisiert betriebenen Fachverfahren sollte die Ausführungsverantwortung bei der Landesredaktion (ebenfalls mit Unterstützung) liegen.|
|Daten zur Abwicklung von Zahlungsdiensten, die sich auf den Zahlungsdienstleister beziehen | Pflegende Stelle für das DVDV | Zahlungsdienstleister | Die Daten, die sich auf den Zahlungsdienstleister beziehen sind für alle Onlinedienste, die den Zahlungsdienst nutzen, identisch. Die beste Quelle ist der Zahlungsdienstleister selber, sie können aber nur von der Pflegenden Stelle eingetragen werden.|
|Daten zur Abwicklung von Zahlungsdiensten, die sich auf den Bezahlvorgang bzw. die Leistung und die Behörde beziehen | Jeweils die betroffene Behörde unterstützt durch den Zahlungsdienstleister und das Roll-In Team | Land / Landesbehörde als Auftraggeber der Onlinedienst (Nach-)Nutzung | Die Festlegung, welche Daten hier konkret einzutragen sind, muss zwischen dem Zahlungsdienstleister und der betroffenen Behörde eng abgestimmt werden.|
|Ggf. weitere Daten | Individuell zu bestimmen | Individuell zu bestimmen | Dies könnten z.B. Steuerdaten für die Kommunikation mit nicht-standardisierten Drittsystemen sein. Hier ist keine Empfehlung möglich.|

<!-- Ich bin mir nicht sicher, ob dieser Teil hier hin gehört - evtl. wäre er besser für eine einzelne Best-Practice-Seite geeignet

# Beispiel Impressum



Die Impressumspflicht ist ab dem 14.05.2024 durch das Digitale Dienste Gesetz (DDG) §5 geregelt. Dort heißt es "Diensteanbieter haben für geschäftsmäßige, in der Regel gegen Entgelt angebotene digitale Dienste folgende Informationen [...] ständig verfügbar zu halten:". Das Impressum muss also inhaltlich den Diensteanbieter i.S.d. DDG beschreiben und sollte daher auch von diesem inhaltlich bestimmt werden. Wer in einem Szenario der (Nach-)Nutzung eines OZG-Dienstes der Diensteanbieter ist, muss fach-juristisch geklärt werden, da das DDG selbst dazu nur sagt: "Im Sinne dieses Gesetzes ist oder sind [...] „Diensteanbieter“ Anbieter digitaler Dienste" (DDG §1 (4) Nr.5). So ist es denkbar, dass der Betreiber des OZG-Dienstes, z.B. das Themenführer-Bundesland oder eine Behörde desselben, als Diensteanbieter i.S.d. DDG ist. Zuvor ist fach-juristisch zu klären, ob OZG-Dienste "in der Regel gegen Entgelt angebotene digitale Dienste" i.S.d §5 DDG sind und daher überhaupt eine Impressumspflicht gilt.

Diese juristische Prüfung kann nicht abschließend durch das Projekt „Nachnutzung Parametrisierungs-MVP“ geleistet werden, da dazu eine juristische Ausbildung und eine entsprechend juristisch angemessene Vorgehensweise notwendig ist.

Eine pragmatische Lösung – ohne juristische Absicherung – besteht darin, die Behörde innerhalb des Themenführer-Bundeslandes, die den Nachnutzenden Ländern gegenüber als Anbieter des Onlinedienstes auftritt, als Diensteanbieter i.S.d. DDG §1 (4) Nr.5 anzusehen. Da die Formulierung des Impressums dann immer gleich wäre, könnte die Formulierung aus einer Konfiguration entnommen werden, die technisch dem Onlinedienst IT-System zuzuordnen ist, also keine EfA-Parametrisierung im eigentlichen Sinne ist. Bei der Präsentation des Impressums im Rahmen des Onlinedienstes wäre dann der Antragsteller ggf. darauf hinzuweisen, dass der Dienst im Sinne des DDG und damit die Angabe des Impressums sich nur auf die Erstellung und Weitergabe der Antragsdaten an die zuständige Behörde bezieht, nicht aber auf die Behördendienstleistung an sich.

Eine alternative pragmatische Lösung – ebenfalls ohne juristische Absicherung – besteht darin, die zuständige Behörde als Dienstanbieter i.S.d. DDG anzusehen. Leider ist es nicht möglich, eine Impressumsangabe aus den FIM-Stammdaten der zuständigen Behörde zu generieren. Grund ist die Anforderung, die sich aus §5 (1) Nr. 1 DGG ergibt. Dort heißt es: „[Dienstanbieter haben verfügbar zu halten: ] den Namen und die Anschrift, unter der sie niedergelassen sind, bei juristischen Personen zusätzlich die Rechtsform [und] den Vertretungsberechtigten […]“. Die Angaben der Rechtsform und vor allem des Vertretungsberechtigten, d.h. Name und Anschrift einer natürlichen Person, die die Organisationseinheit vertritt, sind in den FIM-Stammdaten nicht enthalten. Der Onlinedienst kann sie nicht aus dem PVOG ermitteln.

Stattdessen muss die zuständige Organisationseinheit eine Webseite mit dem ausformulierten Impressum bereitstellen und den Parameter Link zum regionalen Impressum  mit der URL zu dieser Seite befüllen. Eine solche Webseite existiert in den meisten Fällen ohnehin, da anzunehmen ist, dass jede Behörde einen Internetauftritt hat, der ein entsprechendes Impressum enthalten muss. Der Onlinedienst muss diesen Link an geeigneter Stelle auf den Dialogseiten des Onlinedienstes präsentieren.

Diese Lösung ist vermutlich rechtlich optimal, hat aber einen Nachteil: Es entsteht dadurch ein Zwang, für jede zuständige Behörde ein eigenes Onlinedienst Datenobjekt im PVOG anzulegen, nur um einen Behörden-spezifischen Link zum regionalen Impressum signalisieren zu können. Dieses Problem hat seine Ursache darin, dass in XZuFi zwar Onlinedienste Parameter tragen können, nicht jedoch Organisationseinheiten. Eine Möglichkeit Parameter auch an Organisationseinheiten zu verorten und logisch ihnen zugeordnete Informationen damit besser ablegen zu können, wäre ein Feature, das in einer zukünftigen Version des XZuFi-Standards umgesetzt werden sollte.

Die zweite Alternative erscheint aus juristischer Laien-Sicht die sinnvollere Alternative zu sein, da sie die Behördendienstleitung in ihrer Logik mit einbezieht und die größte Flexibilität bietet. Sie wird daher hier – ohne juristische Absicherung! – empfohlen. 

-->