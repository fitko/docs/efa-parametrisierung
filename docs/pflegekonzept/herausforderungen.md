---
id: herausforderungen
title: "Herausforderungen in der Parameterpflege"
---


# Herausforderungen in der Pflege von Parametern

Seit das MVP EfA-Parametrisierung 2021 zuerst konzipiert wurde, haben sich zahlreiche Akteure zu dem Konzept geäußert und haben aktiv ihre Rückmeldungen beigetragen, sowohl in dedizierten Expertengruppen-Sitzungen, wie auch im direkten Austausch, ob persönlich oder schriftlich. 
Diese Seite soll einen Teil der Herausforderungen dokumentieren, denen Akteure auf verschiedenen Ebenen begegnen. Es handelt sich bei den Aussagen allgemein nicht um direkte Zitate, sondern um zusammengefasste Meinungen und Beobachtungen, die im Laufe der Zeit gesammelt wurden.
Nach jeder Aussage wird auf Ressourcen auf dem Entwicklungsportal verwiesen, die dabei hilfreich sein können, der entsprechenden Herausforderung zu begegnen. Somit soll diese Seite auch als Wegweiser durch die Informationen zur EfA-Parametrisierung dienen. 


### Mitarbeiter in Behörden

#### **"Ich habe keine Zeit, irgendwelche Informationen digital zu pflegen."**
In vielen Behörden fehlen die personellen Kapazitäten, um die Pflege von Leistungsinformationen zu übernehmen. Viele Kommunen sind weder an FIM angeschlossen noch in der Lage, Onlinedienst-Parameter zu pflegen. Der tägliche Arbeitsaufwand lässt wenig Raum für zusätzliche Aufgaben, wie die digitale Pflege von Informationen, was zu Verzögerungen und Inkonsequenzen in der Datenaktualisierung führt.

##### Ressourcen


* Es ist sinnvoll, [kommunal-spezifische Onlinedienst-Datenobjekte](./pflegeprozesse_parametertypen.md#kommunal-spezifische-onlinedienst-datenobjekte) zu vermeiden, um den Pflegeaufwand zu reduzieren.
* Die [Beauftragung externer Dienstleister](./aufgabenverteilung.md#externe-unterstützung-und-auslagerung) kann Entlastung bringen, wenn die personellen Kapazitäten fehlen.
* Durch eine [Aufgabenverteilung auf spezialisiertes Personal](./aufgabenverteilung.md#spezialisierung-innerhalb-der-organisationseinheit) lässt sich der Aufwand effizienter gestalten.
* Die Nutzung von [Onlinedienst-Vorlagen im Redaktionssystem](./Onlinedienst-Vorlagen.md#vorlage-funktion-im-redaktionssystem) hilft, repetitive Aufgaben zu automatisieren.

#### **"Wir haben keine technischen Kenntnisse."**
Die Mitarbeitenden in vielen Behörden verfügen nicht über das technische Know-how, um komplexe Parametrisierungsvorgänge durchzuführen. Es mangelt an Schulungen und fortlaufender Unterstützung durch technische Experten. Dies erschwert die Umsetzung und Pflege von Onlinedienst-Parametern erheblich.

##### Ressourcen

* Mit [Schulungen über die Grundlagen](./dokumentation_und_schulung.md#schulungen) können technische Kenntnisse vermittelt werden.
* Eine [verständliche Dokumentation](./dokumentation_und_schulung.md#dokumentation) erleichtert den Zugang zu komplexen Themen.
* Eine gezielte [Aufgabenverteilung auf spezialisiertes Personal](./aufgabenverteilung.md#spezialisierung-innerhalb-der-organisationseinheit) kann technische Defizite ausgleichen.

#### **"Unsere Prozesse sind etabliert und funktionieren gut - Digitalisierung macht uns die Arbeit schwerer."**
Besonders in kleineren Behörden kann die Digitalisierung der Verwaltungsleistungen als unnötig oder sogar kontraproduktiv empfunden werden. Der zusätzliche Aufwand für die Schulungen der Mitarbeiter und die Umstellung der Systeme kann unverhältnismäßig erscheinen.

##### Ressourcen

* Durch die Implementierung von [Feedback-Schleifen](./kommunikationsprozesse.md#feedback-schleifen-und-optimierung) können Prozesse kontinuierlich optimiert werden.
* Die Verwendung von [analogen Dokumenten](./dokumentation_und_schulung.md#analoge-dokumente) kann eine Brücke zur digitalen Welt schlagen, ohne bestehende Prozesse komplett zu ändern.

#### **"Wir benötigen klare Verantwortlichkeiten."**
Die Verantwortung für die Korrektheit der gepflegten Parameter in den verschiedenen beteiligten Systemen (DVDV, Gebietsservice, Redaktionssysteme, PVOG) muss eindeutig festgelegt werden. Ohne klar definierte Zuständigkeiten kommt es häufig zu Verwirrungen und Fehlern, was die Effizienz und Genauigkeit der Parameterpflege beeinträchtigt.

##### Ressourcen

* Eine genaue [Definition der Verantwortlichkeiten](./verantwortung_fuer_Datenintegritaet.md) schafft Klarheit und Struktur.
* Optimierte [Kommunikationsprozesse](./kommunikationsprozesse.md#kommunikationsprozesse-zwischen-verschiedenen-mitarbeitern-in-der-parameterpflege) sorgen dafür, dass die Pflege der Parameter reibungsloser abläuft.
* [Rollen und Verantwortlichkeiten](./rollen_und_zustaendigkeiten.md) lassen sich in der Dokumentation festhalten und durch [Aushänge](./dokumentation_und_schulung.md#pinnwände-und-informations-poster) für alle transparent machen.

#### **"Wir haben keinen Überblick über alle relevanten Informationen."**
Die Menge an benötigten Informationen und deren Aktualisierung ist umfangreich, was dazu führt, dass Mitarbeitende oft nicht den Überblick haben, welche Informationen aktuell und relevant sind. Dies führt zu veralteten oder unvollständigen Datensätzen, die die Qualität der Onlinedienste beeinträchtigen.

##### Ressourcen
* Der [Parametrisierungsleitfaden](../parametrisierungsleitfaden.md) gibt eine klare Orientierung, welche Informationen wo und wie gepflegt werden müssen.
* Eine konsequente [Dokumentation](./dokumentation_und_schulung.md#dokumentation) ermöglicht einen schnellen Zugriff auf relevante Informationen.
* Mithilfe von [Anbindungsleitfäden](./Onlinedienst-Vorlagen.md#anbindungsleitfäden-mit-parameterlisten) können die notwendigen Parameter identifiziert und gepflegt werden.

#### **"Wir sind nicht in alle Prozesse integriert."**
Mitarbeitende in Behörden werden nicht immer ausreichend in die Prozesse zur Parametrisierung integriert und haben daher oft keinen Einfluss auf wichtige Entscheidungen. Dies führt zu einer mangelnden Einbindung und einem Gefühl der Hilflosigkeit, wenn es um die Pflege und Aktualisierung von Parametern geht.

##### Ressourcen
* Durch [Feedback-Schleifen und Optimierung](./kommunikationsprozesse.md#feedback-schleifen-und-optimierung) kann sichergestellt werden, dass alle relevanten Akteure involviert werden.
* Der [Soll-Prozess der Nachnutzung](./nachnutzungsprozess.md) bietet eine Grundlage, um inklusive Prozess in der Parametrisierung zu etablieren. 


<!-- Auskommentiert, da Fragen zu Bezahldiensten außerhalb der Standardisierung liegen

#### **"Die Pflege von Bezahldaten ist besonders anspruchsvoll."**
Die Pflege von Payment-bezogenen Parametern erfordert besondere Sorgfalt und technische Kenntnisse, da hier eine hohe Datensicherheit gewährleistet werden muss. Dies stellt für viele Mitarbeiter eine zusätzliche Herausforderung dar, da sie mit den entsprechenden Sicherheitsstandards vertraut sein müssen.

##### Ressourcen
* [Dokumentation PaymentItem Reference](../parameter_katalog/04_paymentservicebaseurl_des_bezahldienstes.md)

-->

#### **"Wir wissen nicht, an wen wir uns bei Problemen wenden müssen."**
Es fehlt oft an klaren Ansprechpartnern und definierten Supportstrukturen für die Pflege und Problemlösung im Bereich der Onlinedienst-Parametrisierung. Ohne eindeutige Kontaktpersonen oder eine zentrale Anlaufstelle bleibt unklar, wer bei technischen oder inhaltlichen Problemen weiterhelfen kann. Dies führt zu Verzögerungen und erhöhten Frustrationen bei den Mitarbeitenden, da sie Schwierigkeiten haben, schnelle und effiziente Lösungen für ihre Anliegen zu finden.

##### Ressourcen

* Eine [Prozessdokumentation](./dokumentation_und_schulung.md#prozess-dokumentation) klärt, wer in welchem Schritt verantwortlich ist.
* Durch optimierte [Kommunikationsprozesse](./kommunikationsprozesse.md#kommunikationsprozesse-zwischen-verschiedenen-mitarbeitern-in-der-parameterpflege) lassen sich Ansprechpartner schnell finden.
* Ein zukünftiges **Supportkonzept** wird helfen, zentrale Anlaufstellen für Problemlösungen zu etablieren.

### Landesredaktionen

#### **"Die Pflege von Parametern ist zeitaufwendig und kompliziert."**
Die Erstellung und Aktualisierung eines standardisierten Datenschemas sowie die kontinuierliche Pflege und Überprüfung von Parametern erfordert erheblichen Zeitaufwand und Detailgenauigkeit. Dies stellt eine große Herausforderung dar, insbesondere angesichts begrenzter personeller Ressourcen. Neben der fachlichen Validierung müssen die Parameter auch technisch überprüft werden. Dies erfordert spezielle technische Kenntnisse und zusätzliche Ressourcen, um sicherzustellen, dass die Daten korrekt und funktional sind.

##### Ressourcen

* Die [Auslagerung der initialen Konfiguration von Parametern an Deinstleister oder Onlinedienst-Hersteller](./aufgabenverteilung.md#externe-unterstützung-und-auslagerung) kann dabei helfen, den Pflegeaufwand zu reduzieren und technische Aufgaben effizienter zu bewältigen.
* Eine [Aufgabenteilung innerhalb der Dateneingabe](./aufgabenverteilung.md#aufgabenverteilung-innerhalb-der-dateneingabe) ermöglicht eine klarere Rollenverteilung und entlastet das Personal.
* Eine effektive [Kommunikation mit dem Hersteller](./kommunikationsprozesse.md#kommunikationsprozesse-zwischen-bereitstellenden-und-nachnutzenden-von-onlinediensten) kann dabei helfen, die Komplexität des Onlinedienstes zu reduzieren.

#### **"Es fehlen automatisierte Prüfmechanismen."**
Onlinedienste sollten die Prüfung der Parameter auf Vollständigkeit und Korrektheit automatisiert durchführen können. Ohne diese Automatisierung sind manuelle Prüfungen fehleranfällig und zeitintensiv, was die Effizienz der Parameterpflege stark beeinträchtigt.

##### Ressourcen

* Die Einführung von [Prozessorientierter Kontrolle und Prüfung](./aufgabenverteilung.md#prozessorientierte-kontrolle-und-prüfung) stellt sicher, dass automatisierte Tests in die Pflegeprozesse integriert werden.
* Die Implementierung eines [Versionskontrollsystems](./change-management_und_versionskontrolle.md#versionskontrolle-und-versionierung-von-onlinediensten) kann ebenfalls dazu beitragen, Fehler durch automatisierte Prüfungen zu minimieren.

#### **"Kommunikation mit kommunalen Stellen ist schwierig."**
Die Koordination und Kommunikation mit den kommunalen Redaktionen und Behörden gestaltet sich oft als schwierig und zeitaufwendig. Unterschiedliche Arbeitsweisen und Prioritäten können zu Missverständnissen und Verzögerungen führen, was die Effektivität der Zusammenarbeit mindert.

##### Ressourcen

* Optimierte [Kommunikationsprozesse mit Kommunen](./kommunikationsprozesse.md#kommunikationsprozesse-zwischen-landesredaktionen-bzw-roll-in-teams-und-vollziehenden-behörden) können dazu beitragen, die Zusammenarbeit zu verbessern und Missverständnisse zu vermeiden.
* [Feedback-Schleifen und Optimierung](./kommunikationsprozesse.md#feedback-schleifen-und-optimierung) fördern eine transparente und regelmäßige Kommunikation.
* Die Nutzung von [Informationsveranstaltungen](./dokumentation_und_schulung.md#informationsveranstaltungen) kann helfen, den Informationsfluss zwischen den Stellen zu verbessern.

#### **"Wir haben keine standardisierten Prozesse."**
Ein Mangel an standardisierten Prozessen für die Erstellung und Pflege von Parametern führt zu Inkonsistenzen und erhöhtem Aufwand. Ohne klare Richtlinien und Verfahren kann die Qualität und Konsistenz der gepflegten Daten nicht sichergestellt werden.

##### Ressourcen
* Der [Soll-Prozess der Nachnutzung](./nachnutzungsprozess.md) bietet einen strukturierten Ansatz, um Standardisierung bei der Pflege von Parametern sicherzustellen.
* Eine klare [Dokumentation von Prozessen](./dokumentation_und_schulung.md#prozess-dokumentation) ist essenziell, um Standards festzulegen und Konsistenz zu gewährleisten.
* Die [Erstkonfiguration von EfA-Onlinediensten](./rollen_und_zustaendigkeiten.md#erstkonfiguration-von-efa-onlinediensten) gibt ebenfalls Leitlinien für die Parametrisierung und Standardisierung vor.

#### **"Fehlende finanzielle Ressourcen."**
Die geplanten Anpassungen an den Redaktionssystemen müssen finanziell geprüft und abgesichert werden, um eine nachhaltige Implementierung und Pflege zu gewährleisten. Oftmals sind die notwendigen Mittel nicht ausreichend vorhanden, was zu Verzögerungen und unvollständigen Implementierungen führt.

##### Ressourcen
* Eine [Vorprüfung der Nachnutzbarkeit](./nachnutzungsprozess.md#2-nachnutzende-bundesländer-kommunen-oder-it-dienstleister) kann helfen, unnötige finanzielle Ausgaben zu vermeiden, indem nur anpassbare Dienste übernommen werden.
* [Nachnutzungsallianzen](./kommunikationsprozesse.md#nachnutzungsallianz-verschiedener-länder) ermöglichen eine Bündelung der finanziellen Ressourcen und gemeinsame Lösungen.
* Eine effektive [Aufgabenverteilung auf spezialisiertes Personal](./aufgabenverteilung.md#spezialisierung-innerhalb-der-organisationseinheit) kann langfristig finanzielle Entlastung schaffen.

<!-- Auskommentiert, da Fragen zu Bezahldiensten außerhalb der Standardisierung liegen

#### **"Die Integration und Aktualisierung von Bezahldienstanbietern ist komplex."**
Die Integration und Aktualisierung von Bezahldienstanbietern in Onlinedienste stellt eine technische Herausforderung dar. Es müssen verschiedene Bezahlanbieter unterstützt und ihre Parameter korrekt gepflegt werden, was detaillierte technische Kenntnisse und kontinuierliche Anpassungen erfordert.

##### Ressourcen
-->

#### **"Die Koordination der Qualitätskriterien ist schwierig."**
Die Qualitätskriterien für die Datenerfassung und -pflege müssen von einer zentralen Redaktion vorgegeben und überwacht werden. Die Einhaltung dieser Standards auf kommunaler Ebene sicherzustellen, ist eine Herausforderung, da dies eine effektive Kommunikation und Zusammenarbeit zwischen den verschiedenen Redaktionen und Behörden erfordert.

##### Ressourcen
* Die [Aufforderung zur Pflege](./kommunikationsprozesse.md#aufforderung-zur-pflege) stellt sicher, dass alle Stellen zur Einhaltung der Qualitätsstandards angehalten werden.
* Eine [regelmäßige Schulung](./dokumentation_und_schulung.md#schulungen) der Mitarbeiter kann sicherstellen, dass Qualitätskriterien einheitlich angewendet werden.

<!-- auskommentiert, da Pflegekonzept an Nachnutzende gerichtet ist

### Onlinedienst-Hersteller

#### **"Wir müssen zahlreiche regionale Besonderheiten berücksichtigen."**
Onlinedienst-Hersteller müssen sicherstellen, dass ihre Lösungen flexibel genug sind, um unterschiedliche regionale Anforderungen und Parameter zu unterstützen. Dies erfordert erhebliche Entwicklungsressourcen und kann die Komplexität der Systeme deutlich erhöhen.

##### Ressourcen

#### **"Die Integration von bestehenden Systemen ist komplex."**
Die Integration von Onlinediensten in bestehende lokale Systeme und die Sicherstellung der Kompatibilität mit diesen Systemen stellt eine erhebliche technische Herausforderung dar. Unterschiedliche technische Standards und Infrastrukturen erschweren die nahtlose Einbindung und erfordern umfangreiche Anpassungen.

##### Ressourcen

#### **"Schnelle Anpassungen sind schwierig umzusetzen."**
Onlinedienst-Hersteller müssen in der Lage sein, schnell auf gesetzliche und organisatorische Änderungen zu reagieren und ihre Dienste entsprechend anzupassen. Dies erfordert eine flexible und effiziente Entwicklungs- und Implementierungsstrategie.

##### Ressourcen

#### **"Unsere Onlinedienste sind schon fertig entwickelt."**
Viele Onlinedienst-Hersteller stehen vor der Herausforderung, dass ihre Dienste bereits fertig entwickelt sind und eine nachträgliche Anpassung an die neuen Parametrisierungsvorgaben erhebliche Ressourcen und Zeit erfordert. Die Integration neuer Parameter in bestehende Systeme kann zu technischen und organisatorischen Komplikationen führen.

##### Ressourcen

#### **"Es ist nicht klar, welche Parameter wie verwendet werden sollen."**
Die Unklarheit über die genaue Verwendung und Spezifikation der verschiedenen Parameter stellt eine große Herausforderung dar. Ohne klare Anweisungen und standardisierte Vorgaben ist es schwierig, die richtigen Parameter zu implementieren und sicherzustellen, dass sie korrekt und konsistent genutzt werden.

##### Ressourcen

#### **"Unsere Onlinedienste brauchen ganz andere Parameter als die im Standard definierten."**
Onlinedienst-Hersteller haben oft spezifische Anforderungen, die von den standardisierten Parametern abweichen. Die Notwendigkeit, zusätzliche oder abweichende Parameter zu verwenden, kann zu Inkompatibilitäten und erhöhtem Entwicklungsaufwand führen, um die Funktionalität und Anpassungsfähigkeit der Dienste zu gewährleisten.

##### Ressourcen

**"Die Verwaltung von Zugriffen und Berechtigungen ist aufwendig."**
Die Sicherstellung, dass nur berechtigte Personen Zugriff auf die Pflege von Parametern haben, und die Verwaltung der verschiedenen Rollen und Berechtigungen ist eine komplexe Aufgabe. Die Implementierung eines sicheren und flexiblen Rechte- und Rollenkonzepts erfordert sorgfältige Planung und kontinuierliche Anpassung an sich ändernde Anforderungen.

##### Ressourcen

-->