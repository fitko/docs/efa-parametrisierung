---
id: parameterkatalog
title: "Parameterkatalog"
---


:::info[Version]

1.0.0

:::

# Parameterkatalog

Im Parameterkatalog sollen die einzelnen, standardisierten EfA-Parameter für Onlinedienste und die damit verbundenen Vorgaben und Best Practices dokumentiert werden. Dabei werden für jeden Parameter die folgenden Informationen strukturiert präsentiert:

| Thema | Inhalt |
| --- | --- |
| Name | Der fachliche Name des Parameters. Er dient zur Identifikation in Konzepten und Dokumentationen, hat aber keine technische Bedeutung. (Der Name ist als Titel und Überschrift der jeweiligen Seite dokumentiert.) |
| Fachliche Bedeutung | Bedeutung des Parameters: Welche Aspekte oder Funktionen des Onlinedienstes werden durch diesen Parameter wie gesteuert? |
| Fachliche Kategorie | Die fachliche Kategorie entsprechend Abschnitt [Fachliche Kategorien](kategorisierung_der_parametrisierung#fachliche-kategorien) |
| Technische Kategorie | Die technische Kategorie entsprechend Abschnitt [Technische Kategorien](kategorisierung_der_parametrisierung#technische-kategorien) |
| Wertemenge | Welche Werte kann dieser Parameter annehmen (fachlicher Datentyp)? Ggf. Hinweise für eine Codierung von Werten in Form von Zeichenketten |
| Verortung im Datenmodell | Wie wird der Parameter-Wert im XZuFi-Datenmodell bzw. im DVDV Datenmodell abgelegt? |
| Ermittlungsmöglichkeit PVOG-API | Wie kann der Wert durch Benutzung welcher Endpunkte der PVOG API ermittelt werden; in welcher Datenstruktur der Response-Nachricht ist er wie codiert? (Nur relevant, wenn der Parameter über diese API ermittelbar ist) |
| Ermittlungsmöglichkeit FIT-Connect Routing API | Wie kann der Wert durch Benutzung welcher Endpunkte der FIT-Connect Routing API ermittelt werden; in welcher Datenstruktur der Response-Nachricht ist er wie codiert? (Nur relevant, wenn der Parameter über diese API ermittelbar ist) |
| Ermittlungsmöglichkeit DVDV API |  Wie kann der Wert durch Benutzung welcher Endpunkte der DVDV API ermittelt werden; in welcher Datenstruktur der Response-Nachricht ist er wie codiert? (Nur relevant, wenn der Parameter über diese API ermittelbar ist) |
| Best Practices | Raum für weitere Anmerkungen und Best Practices sofern relevant |

:::info

In den Erläuterungen zu Ermittlungsmöglichkeiten über die verschiedenen APIs wird die Bezeichnung „URL-Parameter“ als Oberbegriff für Pfad-Parameter (_path parameters_) und Anfrage-Parameter (_query parameter_) verwendet. Ob ein konkreter Parameter als Pfad- oder als Anfrage-Parameter in der URL zu encodieren ist, ist hier nicht relevant und muss der jeweiligen API-Spezifikation entnommen werden.

:::

Die standardisierten Parameter sind in der Navigationsleiste aufklappbar aufgelistet.

## Liste weiterer Parameter, die in Zukunft möglicherweise standardisiert werden

Die folgenden Parameter werden vorerst nicht in den Standard-Katalog an Parametern aufgenommen, weil ihre Benutzung an Rahmenbedingungen und Einschränkungen gekoppelt sind, die vermutlich bei vielen Onlinediensten nicht gegeben sind. Ihre Nutzung als Standard verbindlich vorzuschreiben würde bedeuten, die Hersteller der Onlinedienste vor technische Probleme zu stellen. Auch wenn diese Parameter also (noch) nicht in den Standard-Katalog aufgenommern wurden, ist es jedem Hersteller erlaubt, sie so zu nutzen, wie dies im XZuFi-Standard vorgesehen ist - vorausgesetzt, die Rahmenbedingungen sind erfüllt.  

1. [Kosten](./parameter_katalog/09_kosten.md)
1. [Onlinedienst Logo](./parameter_katalog/43_onlinedienst_logo.md)


