---
id: zertifikat_des_bezahldienstes
title: "Zertifikat des Bezahldienstes"
---


:::info[Version]

1.0.0

:::

# Zertifikat des Bezahldienstes

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

**Bemerkung**: Verschiedene Onlinedienste können denselben Bezahldienst nutzen. In einem solchen Nutzungs-Szenario muss dieser Parameter nur einmal konfiguriert werden.
 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter)| [DVDV-basierter Parameter](../kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter) |

### Fachliche Bedeutung

Das Zertifikat für die Verschlüsselung bei der Kommunikation mit dem Bezahldienst.

Es soll ermöglicht werden, dass Bezahldienstanbieter die Transportverschlüsselung und die Authentifizierung der Server mit Hilfe von Selbst-signierten Zertifikaten umsetzen. Der Onlinedienst muss dann vor dem TLS-Verbindungsaufbaus das Zertifikat zunächst aus dem DVDV ermitteln und in den eigenen Truststore einfügen o.ä., damit die TLS-Schicht das Zertifikat des Servers als vertrauenswürdig akzeptiert.

Details siehe auch [Spezifikation von _XBezahldienste_](https://docs.fitko.de/xbezahldienste/APIDoku/API-Parametrisierung). 

### Wertemenge

Ein PEM-kodiertes Zertifikat, d.h. ein _binary large object_ encodiert in einer Base64-Zeichenkette

### Verortung im Datenmodell
Die _Organisation_, die den Bezahldienstanbieter repräsentiert, ist mit einem _DienstElement_ vom Typ "Bezahldienst-Server" verknüpft. Dieses _DienstElement_ hat ein Element _cipherCertificate_, das wiederum ein Element _content_ hat. Dieses Element _content_ enthält das Zertifikat als Base64-encodierte Zeichenkette.

### Ermittlungsmöglichkeit PVOG API
(Der Parameter kann nicht über den PVOG-Suchdienst ermittelt werden)

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
Der Onlinedienst [ermittelt zunächst das Bezahldienst-Server Dienstelement über die DVDV-API](../parametrisierung_anhänge/6_13_ermittlung_dienstelement_bezahldienstserver.md).

Das so ermittelte DienstElement liefert mit `getCipherCertificate()` ein `Certificate`-Objekt, das diverse Daten und Meta-Daten des Zertifikats enthält. Mit der Methode `getContent()` kann die Base64-encodierte Zeichenkette ermittelt werden. Mit `getEncoded()` kann das Zertifikat direkt als byte-Array ermittelt werden. Für weitere Metadaten muss hier auf die [Bibliotheks-Dokumentation](https://docs.fitko.de/dvdv/javadoc-api/de/dvdv2/object/Certificate.html) verwiesen werden.


### Best Practices
Siehe [Best Practice: Nutzung DVDV-Bibliothek](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md)