---
id: beschreibung_der_organisationseinheit
title: "Beschreibung der Organisationseinheit"
---


:::info[Version]

1.0.0

:::

# Beschreibung der Organisationseinheit

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Beschreibung der Organisationseinheit

### Wertemenge
[Internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat mehrere optionalen Elemente _beschreibung_. Diese bilden zusammen eine [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette).

#### Codebeispiel XZuFi 2.2
```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:name>
        <xzufi:name languageCode="de">Bezirksamt Bergedorf - Fachamt Gesundheit</xzufi:name>
        <xzufi:name languageCode="en">Bergedorf District Office - Health Department</xzufi:name>
      </xzufi:name>
      <xzufi:beschreibung languageCode="de">Bezirksamt Bergedorf - Fachamt Gesundheit</xzufi:beschreibung>
      <xzufi:beschreibung languageCode="en">Bergedorf District Office - Health Department</xzufi:beschreibung>

    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `beschreibung[]`, das die Beschreibung der Organisationseinheit als [Internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) enthält. 

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)