---
id: übermittlung_von_daten_des_antragstellers_an_bezahldienst
title: "Übermittlung von Daten des Antragstellers an den Bezahldienst"
---


:::info[Version]

1.0.0

:::

# Übermittlung von Daten des Antragstellers an den Bezahldienst

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [DVDV-basierter Parameter](../kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter) +  [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Einige Bezahldienste implementieren den Prozess von der Bezahlung bis zu möglichen Mahn- und Inkassoverfahren. Diese Bezahldienste benötigen daher die Daten des Antragstellers für diese Prozessschritte. Andere Bezahldienste implementieren nur die eigentliche Bezahlung und das Bearbeiten einer möglichen Rücklastschrift o.ä. wird durch die zuständige Behörde bearbeitet. Diese Bezahldienste dürfen entsprechend Datenschutzgesetz die personenbezogenen Daten des Antragstellers nicht erhalten (Datensparsamkeit). Dasselbe gilt, falls ein grundsätzlich vorhandenes Mahn/Inkasso-Verfahren auf Seiten des Bezahldienstleisters nicht genutzt werden soll.

### Wertemenge
Zwei Boolean-Werte. Ein Wert signalisiert die grundsätzliche Verfügbarkeit eines Mahn-/Inkassoverfahrens beim Bezahldienstleister. Ein weiterer Wert signalisiert, dass das Mahn-Inkassoverfahren tatsächlich genutzt werden soll. Der zweite Wert ist optional; sein Default-Wert ist "Ja".

Die Datenübertragung der personenbezogenen Daten darf nur erfolgen, wenn beide Boolean-Werte "Ja" sind (UND-Verknüpfung).

Zur Encodierung siehe [Verortung im Datenmodell](#verortung-im-datenmodell)

### Verortung im Datenmodell
Die _Organisation_, die den Bezahldienstanbieter repräsentiert, ist mit einem _DienstElement_ vom Typ "Bezahldienst-Server" verknüpft. Dieses _DienstElement_ hat ein Element _ServiceElementText_, das eine JSON-codierte Zeichenkette enthält.
Die JSON-encodierte Zeichenkette enthält ein Element mit Namen `submitRequestorData` vom Typ Boolean, das den gesuchten Wert enthält.

:::warning

Eine alternative Festlegung `Antragstellerdatenübermittlung` für den Namen des Elements entstammt einer Mail, die augenscheinlich Daten aus dem XBezahldienste-PoC enthält. Das [experimentelle MVP-Eintragungskonzept](https://docs.fitko.de/dvdv/assets/files/20221020_DVDV-Eintragungskonzept_MVP_v.0.7-6973941d702366bb379b7b13548c952a.pdf) macht keine klare Aussage darüber, wie der Name lauten soll, spricht aber vom "Parameter SubmitRequestor-Data" (mit Bindestrich). Auch in der [Dokumentation zur Parametrisierung bei XBezahldienste](https://docs.fitko.de/xbezahldienste/APIDoku/API-Parametrisierung/) heißt der Parameter "SubmitRequestorData" (ohne Bindestrich).

Zukünftige Eintragungskonzepte müssen daher die oben gemachte Festlegung `submitRequestorData` zur Verortung im Datenmodell explizit übernehmen, um Missverständnisse zu vermeiden. 

:::

Aus BW wurde die Anforderung formuliert, dass eine Parametrisierungsmöglichkeit je Kommune/Behörde geschaffen werden müsse.
Es macht augenscheinlich Sinn, auch einen Bezahldienstleister, der ein Mahn/Inkassoverfahren anbietet, ohne dieses Feature nutzen zu können.

Es wird daher das Folgende festgelegt:
- Der DVDV-Parameter `submitRequestorData` signalisiert, ob der Bezahldienst prinzipiell auch für ein Mahn/Inkassoverfahren verwendet werden kann.
- _zusätzlich_ wird ein neuer, optionaler EfA-Standard-Parameter im PVOG mit dem Namens-Schlüssel `efa.payment.submitRequestorData` mit den beiden möglichen Werten „Ja” und „Nein” definiert. Er signalisiert mit einem "Nein" die Unterdrückung der Übertragung für den Fall, dass ein verfügbares Mahn/Inkassoverfahren nicht genutzt werden soll.
- Beide Parameter sollen vom Onlinedienst ausgewertet werden. Wenn der DVDV-Parameter den Wert „Ja” hat UND ( der PVOG-Parameter nicht gesetzt ist ODER den Wert „Ja” hat ) , werden die Antragstellerdaten übermittelt.

:::warning

Der Wert `efa.payment.submitRequestorData` ist noch nicht in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) enthalten. Daher ist es bei einer Migration zu XZuFi 2.3 noch nicht möglich, diesen Parameter als [Standard-Parameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) abzubilden. Vielmehr muss er nach der Migration bis zur Aufnahme des Namens-Schlüssels in die Code-Liste wie ein [Individual-Parameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-individual-parameter) behandelt werden.

:::

#### Codebeispiel XZuFi 2.2 für den PVOG-basierten Teil
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.payment.submitRequestorData</xzufi:parameterName>
        <xzufi:parameterWert>Nein</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Hinweis: Die PVOG-API kann zur Ermittlung der kommunalen bzw. behördenspezifischen Parametrisierung (i.e. für die "Unterdrückung der Übertragung") genutzt werden. Die PVOG-API kann nicht für die Ermittlung der grundsätzlichen Verfügbarkeit des Bezahldienstleiter-seitigen Mahn/Inkasso-Verfahrens genutzt werden.

Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.submitRequestorData`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API

Hinweis: Die DVDV-API kann nur für die Ermittlung der grundsätzlichen Verfügbarkeit des Bezahldienstleiter-seitigen Mahn/Inkasso-Verfahrens genutzt werden. Die DVDV-API kann nicht zur Ermittlung der kommunalen bzw. behördenspezifischen Parametrisierung (i.e. für die "Unterdrückung der Übertragung") genutzt werden. 

Der Onlinedienst [ermittelt zunächst das Bezahldienst-Server Dienstelement über die DVDV-API](../parametrisierung_anhänge/6_13_ermittlung_dienstelement_bezahldienstserver.md).

Das so ermittelte DienstElement liefert mit `getServiceElementText()` eine JSON-codierte Zeichenkette. Diese enthält ein Element `submitRequestorData` vom Typ Boolean, das den gesuchte Wert enthält.

Um das Boolean-Objekt zu erhalten, kann der Onlinedienst die Methode `getServiceElementCustomValues()` nutzen, die die JSON-codierte Zeichenkette parst und zunächst ein `java.util.Map<java.lang.String,​java.lang.Object>` (bzw. .Net-Äquivalent) zurück gibt. Diese Map enthält für den Schlüssel "`submitRequestorData`" dann einen Wert vom Typ `java.lang.Boolean`, das den gesuchten Wert  enthält. 

Hinweis: Es sollte vor dem Zugriff auf dieses Array geprüft werden, ob der Cast von `​java.lang.Object` zu `java.lang.Boolean` möglich ist, um ClassCastExceptions zu vermeiden. Wenn der Cast nicht möglich ist, liegt eine Fehlkonfiguration im DVDV vor, die entsprechend gemeldet/behandelt werden muss. (.Net entsprechend) 


### Best Practices
Siehe [Best Practice: Nutzung DVDV-Bibliothek](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md)