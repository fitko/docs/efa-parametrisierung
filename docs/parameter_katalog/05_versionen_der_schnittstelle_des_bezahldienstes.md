---
id: versionen_der_schnittstelle_des_bezahldienstes
title: "Versionen der Schnittstelle des Bezahldienstes"
---


:::info[Version]

1.0.0

:::

# Versionen der Schnittstelle des Bezahldienstes

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

**Bemerkung**: Verschiedene Onlinedienste können denselben Bezahldienst nutzen. In einem solchen Nutzungs-Szenario muss dieser Parameter nur einmal konfiguriert werden.
 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter)| [DVDV-basierter Parameter](../kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter) |

### Fachliche Bedeutung
Die Bezeichnungen der aktuell unterstützten Versionen der Schnittstelle des Bezahldienstes. Der Onlinedienst muss sicherstellen, dass die Nachrichten, die er an den Bezahldienst schickt und die Versionsbezeichnung, um die er die Basis-URL ergänzt, vom Bezahldienst auch tatsächlich unterstützt werden. Falls diese Prüfung fehlschlägt, muss der Onlinedienst dies als Konfigurationsfehler behandeln.

Beispiel: Wenn `2` eine unterstützte Version ist, und die BaseUrl `https://payserver.intern.berlin.de/payservice` ist, so kann der Onlinedienst mittels `GET https://payserver.intern.berlin.de/payservice/v2/paymenttransaction/{originatorId}/{endPointId}/{transactionIdId}` die PaymentTransaction via Version 2 der Schnittstelle abfragen.

### Wertemenge
Eine Menge von Zeichenketten. Zur Encodierung siehe [Verortung im Datenmodell](#verortung-im-datenmodell)

:::info

Das DVDV-Eintragungskonzept gibt Hinweise, welche Werte für Versionsangaben zulässig sind. Stand Q2/2024 existiert nur ein [experimentelles MVP-Eintragungskonzept](https://docs.fitko.de/dvdv/assets/files/20221020_DVDV-Eintragungskonzept_MVP_v.0.7-6973941d702366bb379b7b13548c952a.pdf), das als gültige Werte die Zeichenketten "1" und "2" definiert.

:::

### Verortung im Datenmodell

Die _Organisation_, die den Bezahldienstanbieter repräsentiert, ist mit einem _DienstElement_ vom Typ "Bezahldienst-Server" verknüpft. Dieses _DienstElement_ hat ein Element _ServiceElementText_, das eine JSON-codierte Zeichenkette enthält.
Die JSON-encodierte Zeichenkette enthält ein Element `Versionen` vom Typ Array of Strings. Die Elemente dieses Arrays bilden die gesuchte Menge von Zeichenketten.

### Ermittlungsmöglichkeit PVOG API
(Der Parameter kann nicht über den PVOG-Suchdienst ermittelt werden)

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API

Der Onlinedienst [ermittelt zunächst das Bezahldienst-Server Dienstelement über die DVDV-API](../parametrisierung_anhänge/6_13_ermittlung_dienstelement_bezahldienstserver.md).

Das so ermittelte DienstElement liefert mit `getServiceElementText()` eine JSON-codierte Zeichenkette. Diese enthält ein Element `Versionen` vom Typ Array of Strings. Die Elemente dieses Arrays bilden die gesuchte Menge von Zeichenketten.

Um das String-Array zu erhalten, kann der Onlinedienst die Methode `getServiceElementCustomValues()` nutzen, die die JSON-codierte Zeichenkette parst und zunächst ein `java.util.Map<java.lang.String,​java.lang.Object>` (bzw. .Net-Äquivalent) zurück gibt. Diese Map enthält für den Schlüssel `"Versionen"` dann einen Wert vom Typ `java.lang.String[]`, das die unterstützten Versionsbezeichnungen enthält. 

Hinweis: Es sollte vor dem Zugriff auf dieses Array geprüft werden, ob der Cast von `​java.lang.Object` zu `java.lang.String[]` möglich ist, um ClassCastExceptions zu vermeiden. Wenn der Cast nicht möglich ist, liegt eine Fehlkonfiguration im DVDV vor, die entsprechend gemeldet/behandelt werden muss. (.Net entsprechend) 

### Best Practices
Siehe [Best Practice: Nutzung DVDV-Bibliothek](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md)