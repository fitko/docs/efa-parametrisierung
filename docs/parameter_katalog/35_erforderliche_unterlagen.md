---
id: erforderliche_unterlagen
title: "Erforderliche Unterlagen"
---


:::info[Version]

1.0.0

:::

# Erforderliche Unterlagen

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Erforderliche Unterlagen. Diese Angabe spezifiziert für eine Leistung beizubringende Dokumente. Ein Dokument bezeichnet einen physischen oder elektronischen Informationsträger.

### Wertemenge
Eine Menge von komplexen Datenstrukturen _Dokument_, die jeweils die folgenden Elemente enthalten:
- _typ_: Ein Dokumententyp als Code aus der Code-Liste [`urn:de:fim:codeliste:dokumenttyp`](https://www.xrepository.de/details/urn:de:fim:codeliste:dokumenttyp)
- _bezeichnung_: Eine Bezeichnung als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)
- _beschreibung_: Eine Beschreibung als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)
- _referenzFormularID_: Referenzen auf Formulare (i.e. FormularIDs) 
- _angabeSignatur_: Einen Code aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.signatur`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.signatur), der bestimmt, ob bzw. welche Stufe einer Signatur notwendig ist.

:::warning

Die (optionalen) Referenzen auf Formular-IDs sind Stand Q2/2024 für einen Onlinedienst nur schwer nutzbar, da es keine Möglichkeit gibt, Formulardaten anhand einer ID direkt aus dem PVOG abzufragen. Vielmehr sind Formulare im PVOG stets Stammdaten der Organisationseinheiten. Weil für eine Leistung in der Regel mehrere - oder sogar sehr viele - Organisationseinheiten zuständig sind, und deren Zuständigkeit wieder an ein Gebiet gekoppelt ist, das in der Regel kleiner ist als das Zuständigkeitsgebiet der Leistung, ist es nicht möglich, die Formulardaten, auf die hier referenziert wird, zu ermitteln. Der Mechanismus der FormularIDs ist in XZuFi vollständig generisch, so dass es zusätzlicher Abprachen und Vorgaben bedarf, um diese Daten maschinell auswertbar zu machen. Besteht eine solche Anforderung, so ist es die Aufgabe des Architekten des Onlinedienstes, individuell einen Mechanismus zu finden, der von nachnutzenden Ländern genutzt werden kann.

All dies ist aber nur relevant für erforderliche Unterlagen, die _Formulare_ sind. Andere Dokumente wie z.B. Geburtsurkunden, haben ohnehin keine _referenzFormularID_.

:::

### Verortung im Datenmodell
Die Leistungsinformationen zu den erforderlichen Unterlagen werden in optionalen Elementen _dokumentEingehend_ abgelegt, die Teil des ebenfalls optionalen Elements _modulDokument_ sind, das Teil des Elements _Leistung_ ist.

<!-- Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

### Ermittlungsmöglichkeit PVOG API
Zunächst [ermittelt der Onlinedienst die Daten der Leistungsbeschreibung](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md).

In der Response findet sich ein Element `modulDokument:dokumentEinhegend[]`, das die gesuchten Informationen in den Kind-Elementen `typ`, `bezeichnung[]`, `beschreibung[]`, `referenzFormularID[]` und `angabeSignatur` enthält.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

:::info

Über die FIT-Connect Routing API können Informationen zu den erforderlichen Unterlagen in unstrukturierter Form ermittelt werden, sofern diese im PVOG vorhanden sind. Diese Informationen stammen aus einer **anderen Quelle** im XZuFi-Datenmodell, nämlich einem _TextModul_ mit dem Diskriminator _leikaTextmodul:code_ = `08` (für "erforderliche Dokumente"). Dieser Typ von Textmodul ist z.B. für die Anzeige in einem Informationsportal gut geeignet, aber nicht, um z.B. das vorliegen aller erforderlicher Dokumente automatisiert und dialog-gestützt validieren zu können.

Der Onlinedienst benutzt zur Ermittlung dieser alternativen Informationen den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:requiredDocuments:description`, das einen [internationalisierten Text](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) enthält, der die erforderlichen Unterlagen beschreibt.

:::

<!--
Frage: Woher kommt dieser Text? Vermutlich kommt er aus dem Element `DokumentModul:Beschreibung[]`, das in XZuFi dokumentiert ist als „Allgemeine (textuelle) Beschreibung des Dokumentenmoduls. Es soll angestrebt werden, die konkreten Dokumente unter "dokument" als strukturierte Daten anzugeben.“ Dies ist aber nicht die Beschreibung der erforderlichen (i.e. der eingehenden) Dokumente, da das `modulDokument` auch die ausgehenden Dokumente beschreibt und die eingehenden Dokumente jeweils ihre eigenen Beschreibungen haben.
Oder nimmt die FIT-Connect Routing API eine ganz andere Quelle, nämlich ein _modulText_, das Teil von _Leistung_ ist, und mit dem Diskriminator _leikaTextmodul:code_ = `08` (für "erforderliche Dokumente") ausgezeichnet wurde? Wenn dem so ist, was passiert mit den Elementen _leikaTextmodulAbweichendeBezeichnung_ und _hyperlinkErweitert_?

Antwort (Teleport, 5.6.2024): Der Routingdienst nutzt nur die Beschreibungsfelder und gibt alle Inhalte in einer Map<Sprache, Inhalt> zurück. Es wird nur das Textmodul „08“ beachtet.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)

