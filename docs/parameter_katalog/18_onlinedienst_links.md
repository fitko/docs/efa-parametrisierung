---
id: onlinedienst_links
title: "Onlinedienst Links"
---


:::info[Version]

1.0.0

:::

# Onlinedienst Links

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Links, über die der Onlinedienst erreichbar ist, zusammen mit einigen Meta-Daten der Links

### Wertemenge
[Internationalisierte Datenstruktur](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) _OnlinedienstLink_. Die Datenstruktur enthält die folgenden Elemente:
- _typ_: der Typ des Links codiert als Code der Code-Liste [`urn:xoev-de:fim:codeliste:onlinedienstlinktyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:onlinedienstlinktyp)
- _link_: URL zum Onlinedienst inklusive der URL-Parameter. Vorhandene dynamische Parameter müssen zusätzlich hinzugefügt werden.
- _dynamischeParameter_: Parameter, welche während des Renderns der Links je nach Kontext dynamisch belegt werden. Sie enthalten jeweils ein Paar aus folgenden Elementen:
    - _typ_: Der Typ des dynamischen Parameters codiert als Code der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.onlinedienstdynamischerparametertyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.onlinedienstdynamischerparametertyp)
    - _parameterName_: Der Name des dynamischen Parameters
- _titel_: Titel der URI
- _beschreibung_: Beschreibung der URI, z.B. kurze Hinweise zur Zielseite
- _positionDarstellung_: Position in der gesamten Darstellung der Links

Als dynamische Parameter sind die folgenden Typen in der Code-Liste definiert:
| Code | Beschreibung |
| --- | --- |
| 001 | Leistung |
| 002 | Gebiet |
| 003 | Organisationseinheit |
| 004 | LeiKa-Leistung |
| 005 | Formular |
| 006 | Sprache |

Über den Mechanismus des Elements _dynamischeParameter_ wird parametrisiert, wie der Onlinedienst aufgerufen werden muss. Ist z.B. ein _OnlinedienstLink_ parametrisiert, bei dem _link_ den Wert `https://example.com/mein_online_dienst` hat und _dynamischeParameter_ ein Werte-Paar aus _typ_ = `005` und _parameterName_ = `applicationform` enthält, dann muss ein Aufrufer den Onlinedienst mit der URL `https://example.com/mein_online_dienst?applicationform=PassierscheinA38` aufrufen, falls `PassierscheinA38` das passende Formular (Code 005!) für den Anwendungsfall ist.

Die Datenstruktur lässt es über das Element  _positionDarstellung_ zu, mehrere fachlich unterscheidbare Links internationalisiert zu definieren. So ist es möglich z.B. vier Elemente anzugeben, bei denen die Werte von _positionDarstellung_ und _languageCode_ (i.e. das Internationalisierungs-Attribut von _OnlinedienstLink_) wie folgt belegt sind:
- **Element 1**: _positionDarstellung_ = "1", _languageCode_ = "de"
- **Element 2**: _positionDarstellung_ = "2", _languageCode_ = "de"
- **Element 3**: _positionDarstellung_ = "1", _languageCode_ = "en"
- **Element 4**: _positionDarstellung_ = "2", _languageCode_ = "en"

Elemente 1 und 3 bilden zusammen den ersten internationalisierten Link, der an erster Position in einer Liste dargestellt werden soll. Elemente 2 und 4 bilden hingegen einen zweiten internationalisierten Link, der an zweiter Position in der Liste angezeigt werden soll. Ist die Spracheinstellung aktuell "Englisch", soll der Onlinedienst in einer Liste zuerst Element 3 und dann Element 4 darstellen.

### Verortung im Datenmodell
Die Onlinedienst Links werden in potenziell mehrfach vorhandenen Elementen _link_ im Datenobjekt _Onlinedienst_ abgelegt.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _link_-Elements
- Laut XZuFi 2.2 Spezifikation ist das Element _beschreibung_ vom Datentyp `xs:string` und soll eine "Beschreibung der URI, z.B. kurze Hinweise zur Zielseite" enthalten. Die Spezifikation macht keine Aussage darüber, ob darin enthaltene HTML-Elemente korrekt verarbeitet werden.

:::

```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
    <xzufi:link languageCode="de">
        <xzufi:typ listURI="urn:xoev-de:fim:codeliste:onlinedienstlinktyp" listVersionID="1">
            <code>01</code>
        </xzufi:typ>
        <xzufi:link>https://serviceportal.hamburg.de/HamburgGateway/Service/Entry/AFM-KitaG</xzufi:link>
        <xzufi:titel>Kita-Gutschein</xzufi:titel>
        <xzufi:beschreibung>Kita-Gutschein</xzufi:beschreibung>
    </xzufi:link>
    <xzufi:link languageCode="en">
        <xzufi:typ listURI="urn:xoev-de:fim:codeliste:onlinedienstlinktyp" listVersionID="1">
            <code>01</code>
        </xzufi:typ>
        <xzufi:link>https://serviceportal.hamburg.de/HamburgGateway/Service/Entry/AFM-KitaG</xzufi:link>
        <xzufi:beschreibung>Daycare voucher</xzufi:beschreibung>
    </xzufi:link>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein Element `links[]`, das vereinfachte Abbildungen der XZuFi-Datenstruktur _OnlinedienstLink_ enthält. Durch die Vereinfachung können die Informationen der Sprache nicht mehr ermittelt werden und die Information aus _positionDarstellung_ kann ebenfalls nicht ermittelt werden.

Die Suchdienst-API unterstützt den "Accept-Language" HTTP-Header. Mit diesem kann man bei der Anfrage z.B. steuern, welche Links tatsächlich in der Response enthalten sind. Gibt man z.B. beim Request den HTTP-Header "Accept-Language: en" an, so gibt die API nur die Link-Elemente aus dem PVOG zurück, deren _languageCode_-Elemente den Wert "en" haben. 

:::warning

Die PVOG Suchdienst-API implementiert keinen Fallback-Mechanismus, der die deutschen _OnlinedienstLink_-Elemente liefert, wenn im Accept-Language-Header eine Sprache angegebenen ist, die nicht in den Elementen _Onlinedienst.sprachversion_ verzeichnet ist. Es ist daher wichtig, dass diese Angabe _nur_ solche Sprachen enthält, für die _alle_ internationalisierten Elemente vollständig vorhanden sind.  

:::

Die Angabe der Position wird nicht mehr an den Onlinedienst weitergegeben, sondern nur intern als Diskriminator ausgewertet, um die verschiedenen Sprach-Versionen der Links einander korrekt zuordnen zu können.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)