---
id: referenz_auf_bezahldienst
title: "Referenz auf Bezahldienst"
---


:::info[Version]

1.0.0

:::

# Referenz auf Bezahldienst

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

**Bemerkung**: Dieser Parameter wird benötigt, um die gemeinsame Nutzung von PVOG und DVDV bei der Ermittlung von Parameter-Werten zu ermöglichen: Der Onlinedienst ermittelt aus dem PVOG die Referenz auf den Bezahldienst, um im nächsten Schritt mit dieser Referenz die weiteren Bezahldienst-bezogenen Werte aus dem DVDV ermitteln zu können.
 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Ein Referenzschlüssel (OrganizationKey), mit dem sich der Bezahldienst im DVDV identifizieren lässt.

:::info

Zur Identifikation des Bezahldienstes ist ein weiterer Schlüssel _ServiceSpecificationUri_ bei der Benutzung der DVDV-API anzugeben. Der Wert dieses Schlüssels ist für alle Bezahldienste identisch und dem DVDV-Eintragungskonzept für Bezahldienste zu entnehmen. Stand Q2/2024 existiert nur ein [experimentelles MVP-Eintragungskonzept](https://docs.fitko.de/dvdv/assets/files/20221020_DVDV-Eintragungskonzept_MVP_v.0.7-6973941d702366bb379b7b13548c952a.pdf), das den Wert für _ServiceSpecificationUri_ auf "urn:dvdv:setup:example:bezahldienst:1" festlegt.

:::

:::info

Mit der Einführung von XZuFi 2.3 wird es möglich, ein Element _Kommunikationssystem_ an dem Element _Zustaendigkeit_ zu pflegen. Gleichzeitig wird mit der Nutzung der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp) ein Code für den Typ Bezahldienst eingeführt. Daher wird nach der Migration auf XZuFi 2.3 die Verortung von OrganizationKey und ServiceSpecificationUri neu festgelegt:
- An der _Zustaendigkeit_, die die zuständige _Organisationseinheit_ mit der _Leitungsbeschreibung_ und dem ARS verknüpft, kann es _Kommunikationssystem_-Elemente geben, von denen eines den _kanal_ Bezahldienst (encodiert entsprechend der Code-Liste) trägt. In diesem _Kommunikationssystem_-Element enthählt das Element _kennung_ den OrganizationKey und das Element _kennungZusatz_ die ServiceSpecificationUri.

:::

### Wertemenge
Zeichenkette

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.paymentserviceid`".

Weil der Parameter Referenz auf Bezahldienst mit der Migration zu XZuFi 2.3 anders im Datenmodell verankert wird, ist der Wert `efa.payment.paymentserviceid` nicht in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) enthalten. Dieser Namens-Schlüssel wird dann nicht mehr benötigt.

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.payment.paymentserviceid</xzufi:parameterName>
        <xzufi:parameterWert>47110815</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.paymentserviceid`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API

(Der Parameter kann nicht über DVDV API ermittelt werden - vielmehr wird er für die Nutzung dieser API benötigt.)

### Best Practices
Der Onlinedienst kann bis zur Mirgration nach XZuFi 2.3 den Wert für ServiceSpecificationUri nicht über einen Auskunftsdienst ermitteln, sondern muss ihn selber "kennen". Dafür sollte der Onlinedienst eine Konfigurationsmöglichkeit der IT-Betriebsplattform des Onlinedienstes nutzen. Die Konfiguration ist zwar spezifisch für die Bezahldienste (im Gegensatz zu anderen OZG-Leistungen und anderen Diensten), aber gilt regional unbeschränkt und wird sich voraussichtlich höchstens sehr selten ändern, so dass ein einfacher Konfigurationsmechanismus genutzt werden kann.