---
id: formular_singnaturerfordernis
title: "Formular-Signaturerfordernis"
---


:::info[Version]

1.0.0

:::

# Formular-Signaturerfordernis

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Formular-Signaturerfordernis, ein optionaler Wert aus der Menge 
- Keine Signatur
- Handschriftliche Signatur
- Einfache elektronische Signatur
- Fortgeschrittene elektronische Signatur
- Qualifizierte elektronische Signatur
- Qualifizierte elektronische Signatur mit Anbieterakkreditierung 

### Wertemenge
Code aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.signatur`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.signatur)

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat mehrere optionale Elemente _formular_. Dieses hat ein optionales Elemente _angabeSignatur_. Dieses Element enthält einen Code aus der Liste [`urn:xoev-de:fim:codeliste:xzufi.signatur`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.signatur).


:::info

Mit der Migration zu XZuFi 2.3 [wird das Element _angabeSignatur_ am Element _formular_ ersatzlos gestrichen](https://www.pivotaltracker.com/n/projects/2558236/stories/180295745). Damit entfällt auch dieser Parameter!

:::

<!-- Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Array-Element `formular[]`, bei dem jedes Element ein `zustaendigkeit`-Element enthält. Der Onlinedienst muss dasjenige Element auswählen, das für die relevante Leistung im relevanten Gebiet (i.e. ARS) "zuständig" ist. Dafür muss er `formular[]:zustaendigkeit[]:leistungId[]` und `formular[]:zustaendigkeit[]:gebietId[]` auswerten und dabei ggf. die Hierarchie der ARS berücksichtigen. Das so identifizierte `formular[]` enthält die Angabe zum Signaturerfordernis als Element `formular[]:angabeSignatur:code`, das den Code aus der Liste [`urn:xoev-de:fim:codeliste:xzufi.signatur`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.signatur) enthält. 

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)
