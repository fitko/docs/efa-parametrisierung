---
id: zustellungskanal_osci_xta
title: "Zustellungskanal (OSCI/XTA)"
---


:::info[Version]

1.0.0

:::

# Zustellungskanal (OSCI/XTA)

**Bemerkung**: Dieser Parameter ist nur vorhanden, wenn es einen OSCI/XTA-basierten Zustellungskanal gibt. Er besteht aus mehreren Bestandteilen, die in verschiedenen Systemen gepflegt werden und daher über verschiedene APIs ermittelt werden müssen.

Konkret sind dies
- Der organizationKey für die Nutzung der [DVDV-API](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md) zur Ermittlung von weiteren technischen Adressierungsinformationen. Der organizationKey kann aus dem PVOG ermittelt werden (vgl. auch [Best Practices](#best-practices)).
- Die weiteren Adress- und Zertifikat-Informationen für die Adressierung und die kryptografische Absicherung beim Versand des Antrags über den Zustellkanal OSCI/XTA. Diese Informationen können aus dem DVDV ermittelt werden.

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Parameter mit Bezug zum Antrags-Versand](../kategorisierung_der_parametrisierung.md#parameter-mit-bezug-zum-antrags-versand) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) + [DVDV-basierter Parameter](../kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter) |

### Fachliche Bedeutung

Die Adress- und Zertifikat-Informationen, die bei Benutzung des OSCI/XTA-Transportdienstes verwendet werden müssen, um den korrekten Adressaten ansprechen und mit ihm kommunizieren zu können.

### Wertemenge

- organizationKey: Zeichenkette
- weiteren Adress- und Zertifikat-Informationen: verschiedene DVDV-_DienstElemente_

**Bemerkung**: Die Verwendung der DVDV-_DienstElemente_ muss für jeden Onlinedienst individuell geklärt werden. In der Regel sollte die Nutzung der verschiedenen _DienstElemente_ wie z.B. [OSCI-Intermediär, OSCI-Empfänger, Signaturzertifikat und Verschlüsselungszertifikat](https://docs.fitko.de/dvdv/allgemeines/AT_03Nutzung1B) im jeweiligen DVDV-Eintragungskonzept und der weiteren DVDV-Dienst-Dokumentation geregelt sein. 

### Verortung im Datenmodell

#### XZuFi / PVOG
Die zuständige _Organisationseinheit_ enthält ein oder mehrere Elemente _Kommunikationssystem_. Eines dieser Elemente enthält im Element _kanal_ den Wert "006" (Bezeichnung "System mit OSCI-Schnittstelle")[^1] UND im Element _kennungZusatz_ einen bestimmten, Leistungs-spezifischen Wert. Dieser Wert ist die serviceSpecificationUri, die im DVDV-Eintragungskonzept festgelegt wurde und den Dienst der Leistung repräsentiert. In diesem _Kommunikationssystem_-Element enthält das Element _kennung_ den Wert des organizationKey.

<!-- Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

#### DVDV
Im DVDV gibt es eine _Organisation_, die den Anbieter der Leistung (i.e. die zuständige Organisationseinheit) repräsentiert. Diese _Organisation_ ist mit einem oder mehreren _DienstElementen_ und dem Dienst verknüpft, der durch die oben genannte serviceSpecificationUri identifiziert ist. Diese _DienstElemente_ enthalten die gesuchten Informationen. Details sind dem jeweiligen DVDV-Eintragungskonzept und der weiteren DVDV-Dienst-Dokumentation zu entnehmen.

### Ermittlungsmöglichkeit PVOG API

Hinweis: Zur Ermittlung des _OrganizationKey_ kann die PVOG-API genutzt werden (vgl. auch [Best Practices](#best-practices)). Die PVOG-API kann nicht für die Ermittlung der weiteren Adress- und Zertifikats-Informationen genutzt werden.

Zunächst muss der Onlinedienst [das zuständige Organisationseinheit-Datenobjekt mit Hilfe der PVOG-API ermitteln](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Der Onlinedienst ermittelt dann den Wert für die _serviceSpecificationUri_ aus seiner überregional-gültigen Konfiguration. 

Die Response, die der Endpunkt [/v1/organisationunits/jzufi](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `kommunikationssystem[]`, das die Kommunikationssysteme der Organisationseinheit enthält. Der Onlinedienst muss dasjenige `kommunikationssystem`-Element identifizieren, das die folgenden Bedingungen erfüllt:
- `kommunikationssystem[].gueltigkeit[]` ist leer oder enthält mindestens ein Objekt mit den Elementen `beginn`und `ende`, das den gewünschten Zeitpunkt umschließt
- `kommunikationssystem[].kanal.code` = "006"
- `kommunikationssystem[].kennungZusatz` = \<die zuvor ermittelte serviceSpecificationUri\>

Das so identifizierte `kommunikationssystem`-Element enthält dann im Element `kommunikationssystem[].kennung` den Wert des _OrganizationKey_. 

:::warning[Hinweis]

Es ist denkbar, dass es in einzelnen Ländern Redaktionssysteme gibt, die es nicht erlauben, das Feld _kennungZusatz_ zu pflegen oder die Pflege dieses Feldes kann ggf. vergessen werden. Der Onlinedienst bzw. dessen Plattform kann gegenüber diesem Problem fehlertolerant gebaut werden.

Sollte die oben dargestelle Suche nach dem passenden _kommunikationssystem_ kein Ergebnis liefern, weil keines der gefundenen _kommunikationssysteme_ die serviceSpecificationUri als _kennungZusatz_ trägt, so kann der Onlinedienst noch das Folgende versuchen: Er filtert alle _kommunikationssysteme_ der Organisationseinheit wie zuvor nach _gueltigkeit_ und _kanal_ aber zusätzlich nach leerem _kennungZusatz_. Diese Filterung  kann mehrere Ergebnisse liefern. Nun muss er versuchen, mit jeder _kennung_ (i.e. _OrganizationKey_), die er in einem der herausgefilterten _kommunikationssysteme_ gefunden hat, und der serviceSpecificationUri im DVDV nach Adressdaten zu suchen. In genau einem Fall sollte er dabei ohne Fehlermeldung erfolgreich sein.

:::

:::info

In XZuFi 2.3 wird das Element _Kommunikationssystem_ nicht mehr direkt der _Organisationseinheit_, sondern deren _Zustaendigkeit_ für eine _Leistung_ und einer _gebietID_ (i.e. ARS) zugeordnet sein. Damit sollte es dann dort auch nur noch für jeden Gültigkeitszeitpunkt genau ein _Kommunikationssystem_-Element geben, für das `kommunikationssystem[].kanal.code` = "006". Aus diesem kann dann der zu verwendende Wert für serviceSpecificationUri aus dem Element `kennungZusatz` ausgelesen werden. Die Notwendigkeit für eine überegional-gültige Konfiguration und die Nutzung des Wertes als Diskriminator wird dann entfallen.

:::

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API

Hinweis: Die [DVDV-API](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md) kann und muss zur Ermittlung der weiteren Adress- und Zertifikat-Informationen genutzt werden. Zuvor muss die PVOG-API für die Ermittlung des _OrganizationKey_ genutzt werden (vgl. auch [Best Practices](#best-practices)), wofür die DVDV-API selbst nicht genutzt werden kann.

Zunächst ermittelt der Onlinedienst den Wert der _serviceSpecificationUri_ aus seiner Konfiguration und [ermittelt damit den Wert des _OrganizationKey_ mit Hilfe der PVOG-API](#ermittlungsmöglichkeit-pvog-api).

Mit diesen Informationen benutzt der Onlinedienst dann die Methode `findServiceDescription` der [DVDV-API-Bibliothek](https://docs.fitko.de/dvdv/apidocs) und ruft damit indirekt den Endpunkt [`/{entryPath}/findservicedescription`](https://docs.fitko.de/dvdv/spec/#get-/-entryPath-/findservicedescription) mit den Parametern \<organizationKey\> und \<serviceSpecificationUri\> auf - siehe [Dokumentation der DVDV-API](https://docs.fitko.de/dvdv/spec/#get-/-entryPath-/findservicedescription).

Über das `ServiceDescription`-Rückgabeobjekt der Bibliotheksfunktion kann der Onlinedienst mittels `getServiceElements()` die Liste der DienstElemente ermitteln. Aus dieser Liste wählt er diejenigen DienstElemente aus, die laut DVDV-Eintragungskonzept und und der weiteren DVDV-Dienst-Dokumentation die gesuchte Informationen enthalten und wertet diese aus.

### Best Practices
Der Onlinedienst kann bis zur Migration nach XZuFi 2.3 den Wert für ServiceSpecificationUri nicht über einen Auskunftsdienst ermitteln, sondern muss ihn selber "kennen". Dafür sollte der Onlinedienst eine Konfigurationsmöglichkeit der IT-Betriebsplattform des Onlinedienstes nutzen. Die Konfiguration ist zwar spezifisch für die jeweilige Leistung, die der Onlinedienst unterstützt, aber gilt regional unbeschränkt und wird sich voraussichtlich höchstens sehr selten ändern, so dass ein einfacher Konfigurationsmechanismus genutzt werden kann.

Das hier standardisierte Verfahren, den organizationKey aus dem PVOG-Element _Kommunikationssystem_ zu ermitteln ist Stand Q2/2024 unüblich. So gibt es auch andere Ansätze, den OrganizationKey zu bestimmen. Einer dieser Ansätze ist, anhand der vorhandenen Information des Antrags - insbesondere des ARS bzw. AGS der Antragstellung - und der im DVDV-Eintragungskonzept dokumentierten Regel der Konstruktion der OrganizationKeys, den gesuchten Schlüssel durch den Onlinedienst selbst "nachbauen" zu lassen. Ein solches Verfahren ist riskant, da nicht immer gewährleistet ist, dass diese "Schlüssel-Rekonstruktion" erfolgreich ist bzw. den Schlüssel der richtigen Organisationseinheit erzeugt. So ist es denkbar, dass einer Organisationseinheit im DVDV ein AGS auf der Basis ihrer geografischen Verortung ihres Sitzes zugeordnet wurde, der Antrag sich aber auf eine andere Region bezieht, für die die Organisationseinheit trotzdem zuständig ist. Zum Beispiel sind alle Oberlandesgerichte eines Bundeslandes für die Beantragung des Juristischen Vorbereitungsdienstes (i.e. Rechtsreferendariat in der Juristenausbildung) zuständig. Jedem OLG sind dabei einige Landgerichte zugeordnet, an denen der Juristische Vorbereitungsdienst operativ stattfindet, wobei die Zuordnung aber nicht streng der Hierarchie der ARS folgt. Wie soll in einem solchen Fall der Organisationsschlüssel für das gewünschte OLG prozesssicher (re-)konstruiert werden?

[^1]: Diese Festlegung ist ein Vorgriff auf XZuFi 2.3: In XZuFi 2.2 ist die Code-Liste für das Element _kanal_ als Typ-4-Codeliste (i.e. ohne festen Wertebereich) definiert. Um eine Standardisierung zu erreichen, bezieht sich die hier gemachte Festlegung bereits auf die Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp), die für das Entsprechende Element in XZuFi 2.3 vorgesehen ist.
