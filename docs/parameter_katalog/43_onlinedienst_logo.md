---
id: onlinedienst_logo
title: "Onlinedienst Logo"
---


:::info[Version]

1.0.0

:::

# Onlinedienst Logo

:::warning[Wichtig]


Aufgrund der Probleme, die die Nutzung von XZuFi 2.2 und des PVOG bei der Ablage und der Ermittlung dieses Parameters bereiten, wird der Parameter "Onlinedienst Logo" vorerst **nicht** in den Katalog der Standard-Parameter aufgenommen!

Dies bedeutet nicht, dass ein Onlinedienst diese XZuFi-Datenstrukturen nicht nutzen dürften. Vielmehr bedeutet es, dass der Hersteller des Onlinedienstes zunächst recherchieren sollte, wie die Datenstruktur _logo_ (Datentyp `xzufi:Identifikator`) im Sinne der Spezifikation XZuFi 2.2 genutzt werden soll und ob diese Nutzung bzw. die Möglichkeiten, die die Datenstruktur bietet, den Anforderungen aller potenziell nachnutzenden Bundesländern und ihre Kommunen gerecht wird. Der Hersteller hat in jedem Fall die Möglichkeit, eine Parametrisierungsmöglichkeit auf der Basis von [Individual-Parametern](../kategorisierung_der_parametrisierung.md#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) zu definieren. Siehe dazu auch die [Best Practices](#best-practices).

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Das Logo des Onlinedienstes als Referenz

### Wertemenge

Im PVOG können Referenzen auf Logos in Form des Datentyps `xzufi:Identifikator` abgelegt werden. Dies können in XZuFi 2.2 auch mehrere Referenzen sein.
Der Datentyp enthält verschiedene Metadaten-Attribute, die die Art der Referenz näher beschreiben: 
- schemeAgencyID: ID der Stelle, welche das Identifikationsschema verwaltet
- schemeAgencyName: Bezeichnung der Stelle, welche das Identifikationsschema verwaltet
- schemeDataURI: URI, welche definiert, wo sich die Daten des Identifikationsschemas befinden
- schemeID: ID des Identifikationsschemas
- schemeName: Bezeichnung des Identifikationsschemas
- schemeURI: URI, welche definiert, wo sich das Identifikationsschema befindet
- schemeVersionID: Version des Identifikationsschemas

Darüber hinaus enthält ein Element vom Datentyp `xzufi:Identifikator` immer auch einen Wert von Datentyp `xs:token`. Dieser enthält dann die eigentliche Referenz, d.h. die ID.

Es gibt aktuell keine klare Vorgabe, welche Art von Referenzen an dieser Stelle verwendet werden sollen oder können. Auch können diese Referenzen nicht im PVOG aufgelöst werden.
Vielmehr heißt es im Kommentar zum _logo_-Element: "Logo des Onlinedienstes als Referenz. Die Logo-Datei kann mittels der Nachricht allgemein.anfrage|antwort.datei
abgefragt bzw. übermittelt werden." Ein Onlinedienst müsste daher per XZuFi-Nachricht sich an das betreffende Quellsystem (z.B. Redaktionssystem) wenden, um dort die Logo-Datei zu ermitteln.

:::info

Aufgrund der Tatsache, dass in XZuFi 2.2 das Element _logo_ nur eine Menge von Referenzen unbestimmter Art ist und diese Referenzen nicht ohne eine XZuFi-Interaktion mit dem zuständigen Quellsystem auflösbar sind, ist der Parameter "Onlinedienst Logo" zum Stand Q2/2024 nicht nutzbar.

Für XZuFi 2.3 ist ein Umbau der Strukturen vorgesehen, so dass dann sowohl _Onlinedienst_-spezifische Logos als auch _Organisationseinheit_-spezifische Logos abgelegt und ermittelt werden können. In XZuFi 2.3 wird es dazu ein Element _logo_ im _Onlinedienst_ geben, das den neuen Datentyp `xzufi:Bild` hat. Ein solches Element hat grob die folgende Struktur
- bezeichnung
- alternativeBeschreibung
- dateiReferenz
    - 0..1 dateiId: `xzufi:Identifikator`
    - 0..1 dateiReferenz: `xs:anyURI`
- copyright
- positionDarstellung

Mit dieser Datenstruktur wird es nicht nur möglich, [internationalisierte](../parametrisierung_anhänge/6_11_internationalisierung.md) Bezeichnungen und alternative Beschreibungen sowie Copyright-Vermerke einem Logo beizufügen. Es wird auch möglich, eine Referenz auf ein Logo einfach per URL anzugeben. Wichtig ist, dass der Onlinedienst beim Nachladen des Logos Sicherheits-Prüfungen bezüglich der vorgefundenen URL sowie bezüglich der heruntergeladenen Bild-Daten durchführt, bevor er die URL abruft und die Bilddaten in seine eigene Darstellung integriert.

:::

### Verortung im Datenmodell
Die Referenzen auf die Logos des Onlinedienstes werden in XZuFi 2.2 als optionale Elemente _logo_ des Elements _Onlinedienst_ abgelegt.

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein Element `logos[]`, das die Wert-Bestandteile, aber nicht die Schema-Metadaten der Referenzen als String enthält. Aus den oben genannten Gründen, kann ein Onlinedienst diese Informationen aber nicht verwerten.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Bis zum Abschluss der Migration auf XZuFi 2.3 empfiehlt es sich, einen Mechanismus auf der Basis von [Individual-Parametern](../kategorisierung_der_parametrisierung.md#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) umzusetzen. Dazu sollte der Onlinedienst Hersteller die folgenden Parameter definieren und durch den Onlinedienst auswerten lassen:
- `onlinedienst.logo.bezeichnung`: Eine Zeichenkette mit Titel/Bezeichnung des Bildes
- `onlinedienst.logo.alternativeBeschreibung`: Eine Zeichenkette mit einer alternativen Beschreibung des Bildes zur Erhöhung der Barrierearmut (Bildbeschreibung).
- `onlinedienst.logo.dateiReferenz`: Eine URL, über die die Logo-Datei herunterladbar ist. Die URL muss auf die gängige Dateinamen-Endung wie ".jpg" oder ".svg" enden, mit der das Format der Logo-Datei bestimmt werden kann.
- `onlinedienst.logo.copyrightHinweis`: Eine Zeichenkette mit Angabe zum Copyright in textueller Form.

Alle diese Parameter sollten dabei optional sein. 

All diese Parameter und deren Nutzung durch den Onlinedienst müssen jedoch sorgfältig dokumentiert werden, da es sich nicht um Standard-Parameter handelt.