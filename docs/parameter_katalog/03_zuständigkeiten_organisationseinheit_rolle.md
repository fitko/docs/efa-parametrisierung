---
id: zuständigkeiten_organisationseinheit_rolle
title: "Zuständigkeiten Organisationseinheit (Rolle)"
---


:::info[Version]

1.0.0

:::

# Zuständigkeiten Organisationseinheit (Rolle)

**Bemerkung**: Die Rolle der zuständigen Organisationseinheit ist nur indirekt ein Parameter für den Onlinedienst. Vielmehr ist es ein Selektionskriterium an einer Organisationseinheit, mit dessen Hilfe ein Onlinedienst aus einer Menge zuständiger Organisationseinheiten diejenige auswählen kann, die für die relevante Leistung im relevanten Gebiet _in einer bestimmte [Rolle](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)_ zuständig ist.

So können z.B. für die Stadt Neustadt und die Leistung „Antrag auf Baumfällung“ die Behörden A und B zuständig sein:
- Behörde A: zuständig in der Rolle "Ansprechpunkt" 
- Behörde B: zuständig in der Rolle "Zuständige Stelle und Ansprechpunkt" 

Möchte der Onlinedienst ermitteln, an welche Behörde der Antrag versendet werden soll, so muss er die Organisationseinheit identifizieren, die in der Rolle "zuständige Stelle" oder "zuständige Stelle und Ansprechpunkt" zuständig ist. Im Beispiel ist dies Behörde B.

:::warning

Neben der häufig verwendeten _Rolle_ kann auch das selten verwendete [_Kriterium_-Feature](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md) relevant für die eindeutige Identifikation der zuständigen Organisationseinheit sein.

:::

 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Die Rollen der Organisationseinheit in der Zuständigkeitsbeziehung für eine Leistung in einem Gebiet

### Wertemenge
Ein(!) Code aus der Code-Liste [`urn:de:xzufi:codeliste:zustaendigkeitsrolle`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zustaendigkeitsrolle)

### Verortung im Datenmodell
Das Element _zustaendigkeit_ (Datentyp `xzufi:zustaendigkeitOrganisationseinheit`) enthält ein optionales Element _rolle_ und darin ein Element _code_, das einen(!) Code aus der Code-Liste enthält und damit die Rollen(!) beschreibt.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _zustaendigkeit_-Elements.

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:zustaendigkeit>
        <xzufi:leistungID schemeAgencyID="S100002">S1000020010000012932</xzufi:leistungID>
        <xzufi:gebietID schemeAgencyID="DESTATIS" schemeID="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs">024020404404</xzufi:gebietID>
        <xzufi:rolle listURI="urn:de:xzufi:codeliste:zustaendigkeitsrolle" listVersionID="20210401">
          <code>01</code>
          <name>Zuständige Stelle und Ansprechpunkt</name>
        </xzufi:rolle>
      </xzufi:zustaendigkeit>

    </xzufi:organisationseinheit>
```
### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Parameter kann Stand Q2/2024 nicht über das FIT-Connect Routing API ermittelt werden. Vielmehr erwartet die FIT-Connect Routing API, dass für eine Leistung in einem Gebiet genau eine _Organisationseinheit_ existiert, bei der das _zustaendigkeit_-Element für die betrachtete Leistung eine _idSekundaer_ mit DestinationSignature enthält. Diese Organisationseinheit sollte auch die Organisationseinheit in der gesuchten Rolle sein, da reine Ansprechpunkte keine DestinationSignature brauchen.

Das [_Kriterium_-Feature](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md) wird Stand Q2/2024 von der FIT-Connect Routing API nicht ausgewertet. Daher müssen Onlinedienste zusätzliche Schritte durchführen, wenn in einem Gebiet die Zuständigkeit der Organsationseinheit zusätzlich von einem Kriterium wie dem Anfangsbuchstaben des Familiennamens oder der KFZ-Klasse abhängt. In einem solchen Szenario liefert die FIT-Connect Routing API mehrere Routen (i.e. Parameter-Wert-Pakete). Der Onlinedienst muss dann zusätzlich die PVOG API benutzen, um die Zuständigkeits-Kriterien zu ermitteln und diese dem Antragsteller zur Auswahl der für ihn zuständigen Organisationseinheit präsentieren (siehe auch siehe [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)).

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Siehe [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)