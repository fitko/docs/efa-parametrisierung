---
id: identifizierungsmittel
title: "Identifizierungsmittel"
---


:::info[Version]

1.0.0

:::

# Identifizierungsmittel

**Bemerkung:** Der Software-Architekt des Onlinedienst-Entwicklungsprojektes muss klären, ob die verwendete Software Plattform es erlaubt, dass der Onlinedienst die akzeptierten Identifizierungsmittel selbst per Parameter zur Laufzeit  bestimmt und/oder selbst Einfluss darauf hat, ein ggf. bestehendes Missverhältnis aus vorliegendem und notwendigem Identifizierungsmittel zu prüfen und behandeln zu können. Erst wenn diese Frage geklärt ist, kann der Architekt die Nutzung dieses Parameters einplanen.

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Die Identifizierungsmittel, die der Onlinedienst im Zuge der Anwender-Authentifizierung für eine Authentifizierung akzeptiert.

### Wertemenge
Code aus der Code-Liste [`urn:xoev-de:fim:codeliste:identifizierungsmittel`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:identifizierungsmittel)

### Verortung im Datenmodell
Die Identifizierungsmittel werden im Element _code_ des Elements _identifizierungsmittel_ im Datenobjekt _Onlinedienst_ abgelegt. 

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
    <xzufi:identifizierungsmittel listURI="urn:xoev-de:fim:codeliste:identifizierungsmittel"
        listVersionID="4">
        <code>1101</code>
        <name>"Elektronische Identifizierung mittels nationalen eID Mittel - Personalausweis"</name>
    </xzufi:identifizierungsmittel>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein String-Array Element `identificationMethods[]`, das jedoch **nicht** die Codes aus der Liste [`urn:xoev-de:fim:codeliste:identifizierungsmittel`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:identifizierungsmittel) enthält, sondern Texte, die die Identifizierungsmittel beschreiben. Die Texte sind in der Sprache, die im Aufruf von [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) in einem Accept-Language Header als bevorzugte Sprache angegeben wurde, oder in deutscher Sprache, wenn die Texte in der bevorzugten Sprache im Datenbestand der Code-Liste nicht vorhanden sind.

:::warning

Der Onlinedienst kann Stand Q2/2024 keine Geschäftslogik auf der Basis des Parameters Identifizierungsmittel aufbauen, da die API nicht die Codes der Identifizierungsmittel, sondern nur erklärende Texte liefert. Der Onlinedienst kann jedoch die möglichen Identifizierungsmittel dem Antragsteller zur Information darstellen.

:::

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Jedes Redaktionssystem sollte für diesen Parameter eine Dropdown-Auswahl mit den Bezeichnungen der Identifizierungsmittel aus [`urn:xoev-de:fim:codeliste:identifizierungsmittel`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:identifizierungsmittel) anbieten. 