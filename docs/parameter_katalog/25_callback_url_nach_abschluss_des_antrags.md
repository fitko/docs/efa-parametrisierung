---
id: callback_url_nach_abschluss_des_antrags
title: "Callback-URL nach Abschluss des Antrags"
---


:::info[Version]

1.0.0

:::

# Callback-URL nach Abschluss des Antrags

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Wenn dieser Parameter gesetzt ist, dann wird der Benutzer nach Abschluss des Antragsprozesses nicht auf eine Standard-Seite der Onlinedienst Plattform sondern auf die angegebene, spezielle URL weitergeleitet.

### Wertemenge
Datentyp URL, im XZuFi codiert mit einer Zeichenkette, die den [Regeln des URL-Encoding](https://de.wikipedia.org/wiki/URL-Encoding) entsprechen

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.callback.url`".

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.callback.url</xzufi:parameterName>
        <xzufi:parameterWert>https://www.hamburg.de/onlinedienste/weitereschritte</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.callback.url`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden.)

### Best Practices
Siehe [Best Practices zu URL-wertigen Parametern](../parametrisierung_anhänge/6_10_best_practices_url_wertige_parameter.md)