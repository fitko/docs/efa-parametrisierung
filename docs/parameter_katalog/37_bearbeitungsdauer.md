---
id: bearbeitungsdauer
title: "Bearbeitungsdauer"
---


:::info[Version]

1.0.0

:::

# Bearbeitungsdauer

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Bearbeitungsdauer

### Wertemenge
Eine optionale komplexe Datenstruktur _modulBearbeitungsdauer_, die drei Elemente enthält:
- _bearbeitungsDauer_: Eine Menge verschiedener möglicher Bearbeitungsdauern (Datentyp `xzufi:FristOhneTyp`). 
Jede Bearbeitungsdauer ist entweder angegeben als eine (minimale) Dauer und eine optionale maximale Dauer, jeweils als Wert mit Einheit, z.B. _3 Tage_ bis _2 Wochen_. Ist keine maximale Dauer (_dauerBis_) angegeben, so ist die minimale Dauer (_dauer_) auch als maximale Dauer zu verstehen.
Alternativ ist die Bearbeitungsdauer angegeben als Paar von optionalen Stichtagen, wobei diese absolut ("1. April 2025") oder wiederkehrend ("jährlich zum 31. Mai", "monatlich zum 21.") angegeben werden kann. Zusätzlich hat jede einzelne _bearbeitungsDauer_ eine _beschreibung_, eine _positionDarstellung_ und eine _gueltigkeit_.
- _beschreibung_: textuelle Erläuterungen zur Bearbeitungsdauer als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)
- _weiterführenderLink_: Link zu weiteren Informationen in Form einer [internationalisierten Datenstruktur](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) _HyperlinkErweitert_. 

Für eine detaillierte Darstellung dieser Datenstruktur wird hier auf die [XZuFi-Dokumentation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen.


:::warning

Es ist möglich, dass es zu einem Zeitpunkt mehrere gültige Bearbeitungsdauern gibt. Diese können dann nur noch anhand der beigefügten Beschreibungen interpretiert werden. Ein Onlinedienst sollte daher darauf verzichten, die Bearbeitungsdauer maschinell auszuwerten und in seiner Geschäftslogik zu verwenden. So wäre es denkbar, dass der Onlinedienst die Antragstellung abweist, wenn darin eine Genehmigung beantragt wird, die nicht mehr rechtzeitig beschieden werden kann. Eine solche Logik ist aber nicht möglich, weil der Onlinedienst nicht entscheiden kann, welche von mehreren _bearbeitungsdauer_-Angaben die entscheidende für die Bescheidung ist.

<!-- 
Teleport schrieb am 19.6.: 
Es können theoretisch mehrere Bearbeitungsdauern in FristOhneTyp übertragen werden. Auch wenn diese zu einem Zeitpunkt alle zeitlich gültig sind. Insbesondere könnten in der FristOhneTyp.beschreibung weiterführende Infos oder Bedingungen genannt sein. 

(*) Kleiner Einschub: Daten für eine vollständig maschinenlesbare/auswertbare Berechnung von Fristen oder Kosten sowie zur Beibringung von Dokumenten kann der Standard aktuell nicht ermöglichen. Dies würde im Änderungsbeirat intensiv diskutiert. Hier kommen komplexe Regelwerke mit verschachtelten Bedingungen zur Anwendung – eine Herausforderung für Redaktion und Verarbeitung. Das Thema steht aber zukünftig auf der Agenda.

Teleport (Telko 20.6.): Mehrere Bearbeitungsdauern sind denkbar und werden dann durch die Beschreibungen differenziert. Eine automatische Auswertung zur Steuerung der Geschäftslogik erscheint dann nicht mehr machbar. Möglicherweise wird es in Zukunft auch Bearbeitungsdauer-Typen geben, so wie es heute schon Frist-Typen gibt.
-->

:::


### Verortung im Datenmodell
Die Bearbeitungsdauer wird im optionalen Element _modulBearbeitungsdauer_ abgelegt, das Teil des Elements _Leistung_ ist.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _modulBearbeitungsdauer_-Elements.

:::

```xml
    <xzufi:leistung>

      <xzufi:modulBearbeitungsdauer>
        <xzufi:beschreibung languageCode="de">Die Bearbeitungsdauer für die Ausstellung variiert, die Zeugnisse werden jedoch grundsätzlich bis zum vorgesehenen Ausgabetag erstellt. Der Zeugnisausgabetag ist grundsätzlich am Ende des Schulhalbjahres oder Schuljahres. In der Regel wird das Zeugnis ausgefüllt, ausgedruckt, unterzeichnet und gesiegelt. Der Prozess kann einige Tage bis wenige Wochen in Anspruch nehmen.</xzufi:beschreibung>
        <xzufi:beschreibung languageCode="en">The processing time for issuing the certificates varies, but the certificates are generally issued by the scheduled issue date. The certificate issue date is generally at the end of the school semester or school year. Typically, the certificate is filled out, printed, signed and sealed. The process can take a few days to a few weeks.</xzufi:beschreibung>
      </xzufi:modulBearbeitungsdauer>
      
    </xzufi:leistung>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst ermittelt der Onlinedienst die Daten der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md)).

In der Response findet sich ein Element `modulBearbeitungsdauer:bearbeitungsdauer[]`, das die gesuchten Informationen enthält. Von allen `bearbeitungsdauer`-Elementen muss der Onlinedienst diejenigen weiter auswerten, dessen Element `gueltigkeit[]` ein Element enthält, das mit den zeitlichen Daten der Antragsstellung[^1]  übereinstimmt. 

Jedes `bearbeitungsdauer`-Element enthält ein `fristauswahl`-Element, das wiederum ein `fristDauer`-Element oder ein `fristStichtag`-Element enthält.
Ein `fristDauer`-Element besteht aus vier Angaben:
- `dauer`: Ganzzahl-Angabe der minimalen Dauer
- `einheit:code`: Code der Einheit der minimalen Dauer aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.zeiteinheit`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zeiteinheit)
- `dauerBis`: Ganzzahl-Angabe der maximalen Dauer
- `einheitBis:code`: Code der Einheit der maximalen Dauer aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.zeiteinheit`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zeiteinheit)

Ein `fristStichtag`-Element besteht aus zwei Elementen:
- `von`: Datum, zu dem die Bearbeitung frühestens beendet ist
- `bis`: Datum, zu dem die Bearbeitung spätestens beendet ist

Beide Elemente enthalten wiederum drei alternativen Angaben `datum`, `monatTag` und `tag`, die das betreffende Datum spezifizieren. Mit `fristStichtag` ist es also möglich ein konkretes Datum (z.B. "1. April 2025") anzugeben, oder nur einen Tag im Jahr (z.B. "1. April") oder einen Tag im Monat (z.B. "1. in jedem Monat")

Jedes `bearbeitungsdauer`-Element enthält zusätzlich ein `positionDarstellung`-Element vom Typ Ganzzahl, anhand dessen der Onlinedienst die Liste aller Angaben die dem Antragsteller präsentiert werden sortieren kann.

Daneben enthält das `modulBearbeitungsdauer`-Element weitere Angaben, wie einen [internationalisierten Beschreibungstext](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) und eine Menge [internationalisierter weiterführender Links](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen). Details können der [API-Spezifikation](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getJzufiLbV2)  entnommen werden.


### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:processingDuration`, das drei Kind-Elemente enthält: `minDuration`, `maxDuration` und `unit`. Die ersten beiden sind Ganzzahlen (Integer), die Angabe `unit` ist ein String, in dem eine Zeiteinheit „`year`“, „`month`“, „`week`“, „`day`“, „`workday`“, „`hour`“, „`minute`“ oder „`second`“ in englischer Sprache enthalten ist. 

:::danger

Die FIT-Connect Routing API lässt es in der Version 1.2.0 Stand Q2/2024 nicht zu, Bearbeitungsdauern zu ermitteln, die in Form von Stichtagen im PVOG abgelegt sind. Weiterhin liefert die FIT-Connect Routing API im Falle mehrerer gleichzeitig gültiger Angaben von Bearbeitungsdauern nur eine davon. Auch kann die Datenstruktur der FIT-Connect Routing-API Response nicht zwischen der Einheit der _dauer_ und der _dauerBis_ unterscheiden (_3 Tage_ bis _2 Wochen_). Daher kann die FIT-Connect Routing API in der Version 1.2.0 für die Ermittlung der Bearbeitungsdauer nur eingesetzt werden, wenn der Onlinedienst abweichend vom Standard die möglichen Angaben einschränkt. So muss vorausgesetzt werden, dass höchstens eine Bearbeitungsdauer und diese nicht in Form von Stichtagen angegeben wird und dass die Einheiten bei _dauer_ und _dauerBis_ übereinstimmen. Der Onlinedienst kann die Einhaltung dieser Vorgaben nicht sicher prüfen.

:::

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Bezüglich `hyperlinkErweitert` sind unbedingt die Hinweise zu [Internationalisierten Datenstrukturen](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) zu beachten!

[^1]: _Zeitpunkt der Antragstellung_ oder _Zeitpunkt, für den der Antrag gestellt wird_, etc. Diese Regeln sind fachlich jeweils für die Leistung zu klären