---
id: kosten
title: "Kosten"
---


:::info[Version]

1.0.0

:::

# Kosten

:::warning[Wichtig]

Aufgrund der Probleme, die die Nutzung von XZuFi 2.2 und des PVOG bei der Ablage und der Ermittlung dieses Parameters bereiten, wird der Parameter "Kosten" vorerst **nicht** in den Katalog der Standard-Parameter aufgenommen!

Dies bedeutet nicht, dass ein Onlinedienst diese XZuFi-Datenstrukturen nicht nutzen dürfte. Vielmehr bedeutet es, dass der Hersteller des Onlinedienstes zunächst recherchieren sollte, wie die Datenstruktur _Kostenmodul_ im Sinne der Spezifikation XZuFi 2.2 genutzt werden soll und ob diese Nutzung bzw. die Möglichkeiten, die die Datenstruktur bietet, den Anforderungen aller potenziell nachnutzenden Bundesländern und ihre Kommunen gerecht wird. Der Hersteller hat in jedem Fall die Möglichkeit, eine Parametrisierungsmöglichkeit auf der Basis von [Individual-Parametern](../kategorisierung_der_parametrisierung.md#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) zu definieren. Diese Möglichkeit bedeutet dann aber, dass die Kosten nur pro Datenobjekt _Onlinedienst_ konfiguriert werden können. Besteht die Anforderung, in verschiedenen Gebieten (z.B. Kreisen, Kommunen) auch verschiedene Kostenstrukturen konfigurieren zu können, so muss dann pro Gebiet ein _Onlinedienst_-Datenobjekt im PVOG erstellt und vollständig gepflegt werden.

:::

**Bemerkung**: Das Kostenmodul und die Möglichkeiten, die sich damit ausdrücken lassen, sind zu komplex, als dass sie hier dargestellt werden könnten. Für eine Detaillierte Darstellung des Kostenmoduls muss auf die [Dokumentation des XZuFi-Standards](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen werden.
 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung) |

### Fachliche Bedeutung
Kosten, die dem Antragstellenden entstehen. Es kann eine komplexe Kostenstruktur angegeben werden, nicht nur ein einzelner Betrag. 

### Wertemenge

Die Datenstruktur ist so umfänglich, dass sie hier nicht dokumentiert werden kann. Vielmehr muss auf die [XZufi 2.2 Spezifikation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen werden.

### Verortung im Datenmodell
Die Kosten werden im Elementen _Kostenmodul_ abgelegt, das Teil des Elements _Leistung_ ist.

### Ermittlungsmöglichkeit PVOG API
Zunächst ermittelt der Onlinedienst die Daten der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md)).

In der Response findet sich ein Element `modulKosten`, das die gesuchten Informationen enthält.

Die Datenstruktur des Elements `modulKosten` ist zu komplex, als dass sie hier dargestellt werden kann. Daher muss hier auf die [Dokumentation der Schnittstelle](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/) verwiesen werden. 

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Jedes Redaktionssystem sollte für diesen Parameter einen komplexen Pflegedialog bereitstellen, in dem ein Redakteur eine valide Kostenstruktur aufbauen kann.