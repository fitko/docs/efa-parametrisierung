---
id: kennzeichen_schriftformerfordernis
title: "Kennzeichen Schriftformerfordernis"
---


:::info[Version]

1.0.0

:::

# Kennzeichen Schriftformerfordernis

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Das Kennzeichen Schriftformerfordernis gibt an, ob für die Leistung das Schriftformerfordernis gilt, d.h. eine eigenhändige Unterschrift geleistet werden muss. Im Fall von Onlinediensten bedeutet dies, dass eine juristisch äquivalente digitale Unterschrift geleistet werden oder der Antrag ausgedruckt, händisch unterschrieben und physisch oder per Fax an die zuständige Behörde gesendet werden muss. Einzelheiten regeln §§ 126 und 127 BGB.

### Wertemenge
Datentyp Boolean, im XZuFi codiert als `xs:boolean`

### Verortung im Datenmodell
Das Kennzeichen Schriftformerfordernis wird im optionalen Element _kennzeichenSchriftformerfordernis_ der Datenstruktur _Leistung_ abgelegt.

<!--Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

### Ermittlungsmöglichkeit PVOG API
Zunächst ermittelt der Onlinedienst die Daten der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md)).

In der Response findet sich ein Element `kennzeichenSchriftformerfordernis`, das die gesuchten Informationen enthält.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)

