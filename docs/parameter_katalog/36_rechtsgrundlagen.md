---
id: rechtsgrundlagen
title: "Rechtsgrundlagen"
---


:::info[Version]

1.0.0

:::

# Rechtsgrundlagen

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Rechtsgrundlagen

### Wertemenge
Eine komplexe Datenstruktur _Textmodul_, die die folgenden Elemente enthält:
- _inhalt_: ein [internationalisierter Text](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette), 
- _leikaTextmodul_: der Code "`07`" aus der Code-Liste [`urn:de:xzufi:codeliste:leistungstextmodulleika`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:leistungstextmodulleika) ("Handlungsgrundlage(n)"/"Basis for legal action") zur Festlegung der Kategorie des Textmoduls als Informationen zu den Rechtsgrundlagen
- _leikaTextmodulAbweichendeBezeichnung_: eine internationalisierte abweichende Bezeichnung und 
- _weiterfuehrenderLink_: ein [oder ggf. mehrere](#best-practices) Links zu weiteren Informationen in Form einer [internationalisierte Datenstruktur](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) _HyperlinkErweitert_.

Für eine detaillierte Darstellung dieser Datenstruktur wird hier auf die [XZuFi-Dokumentation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen.

### Verortung im Datenmodell
Die Rechtsgrundlagen werden in einem optionalen Element _modulText_, das Teil des Elements _Leistung_ ist, abgelegt und mit dem Code "`07`" aus der Code-Liste [`urn:de:xzufi:codeliste:leistungstextmodulleika`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:leistungstextmodulleika) gekennzeichnet.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _modulText_-Elements.

:::

```xml
    <xzufi:leistung>

      <xzufi:modulText>
        <xzufi:positionDarstellung>5</xzufi:positionDarstellung>
        <xzufi:inhalt languageCode="de">§ 15 Absatz 1 - 3 und 5 Bundesjagdgesetz (BJagdG)</xzufi:inhalt>
        <xzufi:inhalt languageCode="en">§ 15 paragraphs 1 - 3 and 5 Federal Hunting Act (BJagdG)</xzufi:inhalt>
        <xzufi:leikaTextmodul listURI="urn:de:xzufi:codeliste:leistungstextmodulleika" listVersionID="20201201">
          <code>07</code>
        </xzufi:leikaTextmodul>
      </xzufi:modulText>
      
    </xzufi:leistung>
```
### Ermittlungsmöglichkeit PVOG API
Zunächst [ermittelt der Onlinedienst die Daten der Leistungsbeschreibung](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md).

In der Response findet sich ein Element `modulText[]` das Informationen zu verschiedenen Kategorien enthält. Aus diesen Elementen muss der Onlinedienst dasjenige auswählen, bei dem das Element `modulText[]:leikaTextmodul:code` den Wert `07` enthält und dessen Element `modulText[]:gueltigkeit[]` leer ist oder mindestens ein Objekt enthält, dessen `beginn` und `ende`-Elemente den gewünschten Zeitpunkt umschließen.

Das so identifizerte `modulText`-Element enthält dann die Rechtsgrundlagen.

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:legalBasis:description`, die Texte aus dem _inhalt_-Element der XZuFi-Datenstruktur als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette). Die Informationen aus den XZuFi-Elementen _leikaTextmodulAbweichendeBezeichnung_ und _weiterfuehrenderLink_ können nicht über die FIT-Connect Routing API ermittelt werden.

:::danger

Stand Q2/2024 wertet die FIT-Connect Routing API die Elemente `modulText[]:gueltigkeit[]` nicht aus. Es besteht daher das Risiko, dass die API Informationen liefert, die nicht mehr oder noch nicht gültig sind.

:::

<!--
Frage: Welche Teile der _Textmodul_-Struktur werden hier zusammengefasst? Nur die Beschreibungen oder auch alle anderen Teile? 

Antwort (Teleport, 5.6.2024): Der Routingdienst nutzt nur die Beschreibungsfelder und gibt alle Inhalte in einer Map<Sprache, Inhalt> zurück. Weitere Logik ist hier nicht implementiert.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Bezüglich `hyperlinkErweitert` sind unbedingt die Hinweise zu [Internationalisierten Datenstrukturen](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) zu beachten!

