---
id: onlinedienst_durchführungsprachen
title: "Onlinedienst Durchführungsprachen"
---


:::info[Version]

1.0.0

:::

# Onlinedienst Durchführungsprachen

<!--
Bemerkung: Dieser Parameter ist erstmals in der Liste Parameterliste_Bezahldienste_xzufi.xlsx verzeichnet. Dort hat er den Namen „Onlinedienst-Sprachversion“
Um eine Verwechslung mit dem gleichnamigen, obsoleten(?) Parameter „Onlinedienst-Sprachversion“ aus älteren Parameter-Listen zu vermeiden, wurde dieser Parameter nach seiner Bedeutung in „Onlinedienst Durchführungsprachen“ umbenannt.
-->

**Bemerkung:** Dieser Parameter wendet sich weniger an den Onlinedienst selbst, sondern an die Plattform, auf der der Onlinedienst implementiert wurde und auf der er ausgeführt wird. Diese Plattform kann anhand der in diesem Parameter signalisierten Sprachen eine Sprachauswahl für den Antragsteller anbieten.  **Achtung:** Die Angabe einer verfügbaren Sprache in diesem Parameter garantiert nicht, dass alle Benennungen und Texte auch tatsächlich in dieser Sprache verfügbar sind! Der Redakteur muss dies sorgfältig prüfen, bevor er eine Sprache in die Liste der Durchführungsprachen einträgt.

:::danger

Wenn allerdings die zuständige Organisationseinheit erst im Zuge des Online Dienst Dialogflusses bestimmt wird, dann kann es passieren, dass Namen und Bezeichnungen in den Stammdaten dieser Organisationseinheit nicht in der gewünschten Sprache vorliegen...

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Angabe der Sprachen, in welchen der Onlinedienst durchgeführt bzw. in Anspruch genommen werden kann.

### Wertemenge
Menge von Datenstrukturen _Sprache_. Jede dieser Strukturen besteht aus drei Elementen:
- _languageCode_: der W3C-Sprachcode der Sprache. Dieses Attribut kann jeweils als Wert einen Code der Sprache des Texts annehmen, abgebildet mit [W3C-Datentyp language](https://www.w3.org/TR/xmlschema-2/#language). Zum Beispiel "`en`", "`en-US`", "`de`" oder "`de-DE`".
- _sprachbezeichnungDeutsch_: (optional) die Bezeichnung der Sprache in Deutsch
- _sprachbezeichnungNativ_: (optional) die Bezeichnung der Sprache in dieser Sprache

### Verortung im Datenmodell
Die Onlinedienst Durchführungsprachen werden in potenziell mehrfach vorhandenen Elementen _durchfuehrungSprachen_ des Datenobjekts _Onlinedienst_ abgelegt. 

<!-- Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein Element `executionLanguages[]`, das die Angaben zu den Durchführungsprachen in Form von Locale-Strings wie z.B. `"de"` enthält.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)