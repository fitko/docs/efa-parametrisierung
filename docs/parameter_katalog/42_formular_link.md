---
id: formular_link
title: "Formular-Link"
---


:::info[Version]

1.0.0

:::

# Formular-Link

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Formular-Link

### Wertemenge
Eine Menge von URLs

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat mehrere optionale Elemente _formular_. Dieses hat ein oder mehrere Elemente _datei_, die von verschiedenen Typen sein können. Wenn ein _datei_-Element vom Typ _FormulardateiLink_ ist, so enthält es ein Attribut _mimeCode_ mit dem MIME Code des Dokuments als Zeichenkette, dessen URL um Element _uri_ gegeben ist.

<!-- Codebeispiel XZuFi 2.2.: Im HaSI-Export wird dieses Element nicht gesetzt. Hier ein komplett synthetisches Beispiel zu konstruieren, ist riskant. -->

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `formular[]:datei[]`. Der Onlinedienst muss das passende formular-Element identifizieren. Es muss ein Element `uri` enthalten.

:::warning

Laut Spezifikation der API ist dies nicht möglich. Die Datenstruktur Formulardatei enthält immer nur ein Element formulardokumentTypZusatz, d.h. es ist in der API-Spezifikation nur der abstrakte Datentyp angegeben worden. 

Die tatsächliche Implementierung liefert aber Datenstrukturen, die das Element `uri` enthalten, sofern die vorhandenen Daten diese Informationen enthalten.

:::

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)
