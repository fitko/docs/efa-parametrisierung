---
id: leistungsadressaten
title: "Leistungsadressaten"
---


:::info[Version]

1.0.0

:::

# Leistungsadressaten

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Angabe der Adressaten einer Leistung. Diese Gruppen können die Leistung in Anspruch nehmen. Zum Beispiel Bürger oder Unternehmen.

### Wertemenge
Eine Menge von Werten aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.leistungsadressat`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.leistungsadressat)

### Verortung im Datenmodell
Das Kennzeichen Schriftformerfordernis wird im optionalen Element _leistungsadressat_ der Datenstruktur _Leistung_ abgelegt.

#### Codebeispiel XZuFi 2.2
```xml
    <xzufi:leistung>

      <xzufi:leistungsadressat listURI="urn:xoev-de:fim:codeliste:xzufi.leistungsadressat" listVersionID="1.0">
        <code>001</code>
        <name>Bürger</name>
      </xzufi:leistungsadressat>
      <xzufi:leistungsadressat listURI="urn:xoev-de:fim:codeliste:xzufi.leistungsadressat" listVersionID="1.0">
        <code>002</code>
        <name>Unternehmen</name>
      </xzufi:leistungsadressat>

    </xzufi:leistung>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst ermittelt der Onlinedienst die Daten der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md)).

In der Response findet sich ein Element `leistungsadressat[]`, das die gesuchten Informationen in seinem Element `code` enthält.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)

