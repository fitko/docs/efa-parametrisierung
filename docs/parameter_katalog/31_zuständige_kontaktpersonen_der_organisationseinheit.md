---
id: zuständige_kontaktpersonen_der_organisationseinheit
title: "Zuständige Kontaktpersonen der Organisationseinheit"
---


:::info[Version]

1.0.0

:::

# Zuständige Kontaktpersonen der Organisationseinheit

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Zuständige Kontaktpersonen der Organisationseinheit. Kontaktperson ist eine natürliche Person, welche eine Aufgabe im Kontext einer Organisationseinheit durchführt. Im Allgemeinen auch ein Mitarbeiter.

### Wertemenge
Der XZuFi-Datentyp _Kontaktperson_ ist eine Datenstruktur, die nicht nur elementare Personendaten wie Name und Anschrift, sondern diverse Metadaten auch zum Zuständigkeitsbereich der Kontaktperson aufnehmen kann. Die Datenstruktur ist so umfänglich, dass sie hier nicht dokumentiert werden kann. Vielmehr muss auf die [XZufi 2.2 Spezifikation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0)  verwiesen werden.

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat mehrere optionale Elemente _kontaktperson_. Diese Elemente enthalten Angaben zu den Kontaktpersonen und deren Zuständigkeiten.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _kontaktperson_-Elements.

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:kontaktperson>
        <xzufi:id schemeAgencyID="S100002">S1000020030000036020</xzufi:id>
        <xzufi:anrede languageCode="de">Herr</xzufi:anrede>
        <xzufi:vorname>Franz</xzufi:vorname>
        <xzufi:familienname>Brötchen</xzufi:familienname>
        <xzufi:anschrift p3:type="xzufi:AnschriftOrganisationseinheit">
          <xzufi:typ listURI="urn:xoev-de:fim:codeliste:xzufi.anschrifttyp" listVersionID="2.0">
            <code>001</code>
          </xzufi:typ>
          <xzufi:strasse>Neuenfelder Straße 19</xzufi:strasse>
          <xzufi:postleitzahl>21109</xzufi:postleitzahl>
          <xzufi:ort>Hamburg</xzufi:ort>
          <xzufi:verwaltungspolitischeKodierung>
            <xzufi:bundesland listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:bundesland" listVersionID="2010-04-01">
              <code>02</code>
              <name>Hamburg</name>
            </xzufi:bundesland>
            <xzufi:gemeindeschluessel listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:ags" listVersionID="2022-09-30">
              <code>02000000</code>
              <name>Hamburg</name>
            </xzufi:gemeindeschluessel>
            <xzufi:regionalschluessel listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs" listVersionID="2022-09-30">
              <code>020000000000</code>
              <name>Hamburg</name>
            </xzufi:regionalschluessel>
            <xzufi:staat listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staat" listVersionID="2023-02-24">
              <code>000</code>
              <name>Deutschland</name>
            </xzufi:staat>
          </xzufi:verwaltungspolitischeKodierung>
        </xzufi:anschrift>
        <xzufi:erreichbarkeit>
          <xzufi:kanal listURI="urn:de:xoev:codeliste:erreichbarkeit" listVersionID="3">
            <code>02</code>
          </xzufi:kanal>
          <xzufi:kennung>+49 40 428 12-3456</xzufi:kennung>
        </xzufi:erreichbarkeit>
        <xzufi:erreichbarkeit>
          <xzufi:kanal listURI="urn:de:xoev:codeliste:erreichbarkeit" listVersionID="3">
            <code>04</code>
          </xzufi:kanal>
          <xzufi:kennung>+49 40 427 98-7654</xzufi:kennung>
        </xzufi:erreichbarkeit>
        <xzufi:erreichbarkeit>
          <xzufi:kanal listURI="urn:de:xoev:codeliste:erreichbarkeit" listVersionID="3">
            <code>01</code>
          </xzufi:kanal>
          <xzufi:kennung>franz.broetchen@bukea.hamburg.de</xzufi:kennung>
        </xzufi:erreichbarkeit>
        <xzufi:zustaendigkeit>
          <xzufi:leistungID schemeAgencyID="S100002">S1000020010000006514</xzufi:leistungID>
          <xzufi:gebietID schemeAgencyID="DESTATIS" schemeID="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs">020000000000</xzufi:gebietID>
        </xzufi:zustaendigkeit>
        <xzufi:gueltigkeit>
          <xzufi:beginn>2024-01-22</xzufi:beginn>
          <xzufi:ende>9999-12-31</xzufi:ende>
        </xzufi:gueltigkeit>
      </xzufi:kontaktperson>


    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `kontaktperson[]`, das die Kontaktpersonen der Organisationseinheit enthält.

Um aus der Menge der Kontaktpersonen die zuständigen Kontaktpersonen zu identifizieren, muss der Onlinedienst zweistufig filtern. Zunächst muss der Onlinedienst für jedes `kontaktperson`-Element das Element `kontaktperson[]:gueltigkeit[]` betrachten. Wenn es leer ist oder mindestens ein Objekt enthält, dessen `beginn` und `ende`-Elemente den gewünschten Zeitpunkt umschließen, so wird das `kontaktperson`-Element weiter betrachtet. 

 In der zweiten Stufe muss der Onlinedienst für jedes verbleibende `kontaktperson`-Element das Element `kontaktperson[]:zustaendigkeit[]` auswerten und diejenigen `kontaktperson`-Elemente weiter auswählen, die ein `zustaendigkeit[]`-Element besitzen, die alle folgende Kriterien erfüllen:
- Das Element `kontaktperson[]:zustaendigkeit[]:leistungId[]` enthält ein Element, das auf die Leistungsbeschreibungs-ID (lbid) aus der Phase ["Ermittlung eines zuständigen Organisationseinheit-Datenobjektes"](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md) verweist.
- Das Element `kontaktperson[]:zustaendigkeit[]:gebiet[]` ist entweder leer oder enthält ein Element, das auf das relevante Gebiet (i.e. ARS) verweist. Dabei ist ggf. die Hierarchie der ARS zu berücksichtigen, d.h. wenn eine zuständige Kontaktperson für die Gemeinde Gerswalde (ARS 120735305201) gesucht wird, ist auch eine Kontaktperson zu wählen, deren Zuständigkeit sich auf den gesamten Landkreis Uckermark (ARS 120730000000) bezieht
- Das Element `kontaktperson[]:zustaendigkeit[]:kriterium[]` ist entweder leer oder enthält ein Element, das auf den Antrag oder den Antragsteller zutrifft, z.B. ein Kriterium "Nachnamen von M-Z" trifft auf einen Antragsteller mit Nachnamen "Müller" zu.

:::warning

Auch für die Ermittlung von Zuständigkeiten von Kontaktpersonen anhand von Kriterien sollte der Onlinedienst keine Automatismen implementieren (siehe auch [Ermittlung einer zuständigen Organisationseinheit](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)). Wenn ein Onlinedienst mehrere Kontaktpersonen identifiziert, deren Zustaendigkeit durch Kriterien geregelt ist, so sollte der Onlinedienst die Daten aller dieser Kontaktpersonen zusammen mit den Zuständigkeits-Kriterien dem Antragsteller präsentieren, so dass dieser selbst die Auswahl treffen kann.

:::

Die so identifizierten `kontaktperson`-Elemente enthalten dann die Daten der zuständigen Kontaktpersonen. Für Details zu den ermittelbaren Daten zu den Kontaktpersonen muss hier auf die [Dokumentation der API](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getDetailOeOutModelV4) verwiesen werden.

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:contactPersons[]` einige Daten der zuständigen Kontaktpersonen. Jedes `contactPersons`-Element enthält in den Elementen `name`, `tel`, `fax` und `email` die elementaren Kontaktdaten der zuständigen Personen.

Die weiteren, in XZuFi vorhandenen Daten der Kontaktpersonen können über das FIT-Connect Routing API nicht ermittelt werden.

:::danger

Stand Q2/2024 gibt die FIT-Connect Routing API im Element `route[]:contactPerson` die Daten aller Kontaktpersonen der zuständigen Organisationseinheit aus, die vom PVOG als zuständig gemeldet werden. Der FIT-Connect Routing-Dienst fragt dazu beim PVOG unter Angabe von LeikaID und ARS an. In der [Spezifikation der betreffenden PVOG-API](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten) heißt es: "Die \[...\] die Kontaktpersonen der Organisationseinheit ergeben sich aus der übergebenen 'lbId'.". Unklar ist aber, ob die Kontaktpersonen auch anhand der übergebenen ARS gefiltert werden. Die Angaben zur _Gueltigkeit_ werden ebenfalls nicht ausgewertet, so dass das Risiko besteht, dass Personen genannt werden, die noch nicht oder nicht mehr die Rolle der Kontaktperson inne haben.

Falls der Onlinedienst diese Information verwendet, besteht daher das Risiko, dass Kontaktdaten von Ansprechpartnern angezeigt werden, die tatsächlich nicht für den Antrag zuständig sind.

:::

<!--
Frage: Wie bestimmt die FIT-Connect Routing API die tatsächlich zuständigen Kontaktpersonen aus der Menge der Kontaktpersonen einer Organisationseinheit?

Antwort (Teleport, 5.6.2024): Aktuell findet keine Filterung der Kontaktpersonen statt. Es werden alle zugeordneten Kontaktpersonen vom Routingdienst zurückgegeben.

Ergänzung der Antwort (Teleport, 7.6.2024): [Es werden nur die Kontaktpersonen zurückgemeldet, die im PVOG als zuständig identifiziert wurden:] Die Spezifikation steht quasi immer als Kommentar in der YAML. [dort findet sich:] Eine Filterung nach der jeweiligen Zuständigkeit (Leistung + ARS) wird bereits duch das PVOG vorgenommen.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)
