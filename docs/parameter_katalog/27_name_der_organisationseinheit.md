---
id: name_der_organisationseinheit
title: "Name der Organisationseinheit"
---


:::info[Version]

1.0.0

:::

# Name der Organisationseinheit

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Name bzw. Bezeichnung der Organisationseinheit

### Wertemenge
Menge von [internationalisierten Zeichenketten](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) jeweils mit Gültigkeitszeitraum


### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat ein oder mehrere Elemente _name_, die selbst wieder ein oder mehrere Elemente enthalten, die ebenfalls _name_ heißen. Die inneren _name_-Elemente sind jeweils eine [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette). 

:::warning

Jedes äußere _name_-Element hat einen optionalen Gültigkeitszeitraum. Um ein eindeutiges und deterministisches Verhalten der Onlinedienste zu bewirken, muss die Redaktion sicherstellen, dass es für jeden Zeitpunkt genau ein gültiges äußeres _name_-Element gibt. Ein Onlinedienst, der für einen Zeitpunkt mehrere gültige äußere _name_-Elemente vorfindet, muss eines dieser Elemente auswählen. Dabei hat der Onlinedienst keine Selektions-Kriterien zur Verfügung, so dass seine Auswahl zufällig erscheinen muss. Tritt dieses Ereignis ein, sollte der Onlinedienst eine entsprechende Warnung loggen, dass hier vermutlich eine Fehlkonfiguration vorliegt.

:::

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des äußeren _name_-Elements.

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:name>
        <xzufi:name languageCode="de">Bundesministerium für Umwelt, Naturschutz und nukleare Sicherheit</xzufi:name>
        <xzufi:name languageCode="en">Federal Ministry for the Environment, Nature Conservation and Nuclear Safety</xzufi:name>
        <xzufi:gueltigkeit>
            <xzufi:beginn>2018-03-14</xzufi:beginn>
            <xzufi:ende>2021-12-07</xzufi:ende>
        </xzufi:gueltigkeit>
      </xzufi:name>
      <xzufi:name>
        <xzufi:name languageCode="de">Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz</xzufi:name>
        <xzufi:name languageCode="en">Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection</xzufi:name>
        <xzufi:gueltigkeit>
            <xzufi:beginn>2021-12-08</xzufi:beginn>
            <xzufi:ende>9999-12-31</xzufi:ende>
        </xzufi:gueltigkeit>
      </xzufi:name>

    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `name[]`, das die Menge der Namen (jeweils mit Gültigkeitszeiträumen) enthält. Der Onlinedienst muss dasjenige `name[]`-Element identifizieren, bei dem `name[]:gueltigkeit` nicht vorhanden ist oder dessen `name[]:gueltigkeit:beginn` und `name[]:gueltigkeit:ende`-Elemente den gewünschten Zeitpunkt umschließen.

Das so identifizierte `name[]`-Element enthält als `name[]:name[]` den Namen der Organisationseinheit als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette). 

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:destinationName` den Namen der Organisationseinheit als einfache (i.e. nicht als internationalisierte) Zeichenkette in deutscher Sprache.

:::warning

Die FIT-Connect Routing API übernimmt den Wert für `route[]:destinationName` zum Stand Q2/2024 aus dem Element `title` aus der Response eines Aufrufes des Endpunktes [`/v4/organisationunits/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getDetailOeOutModelV5) der PVOG-Suchdienst API. Diese API wertet die Gültigkeiten der äußeren _name_-Elemente der Organisationseinheit **nicht** aus. Daher kann das Ergebnis nicht-deterministisch sein, wenn eine Organisationseinheit zwei verschiedene äußere _name_-Elemente hat, auch wenn von denen zu jedem Zeitpunkt nur eines gültig ist. 

<!-- 

Antwort (aus dem PVOG-Team 26.6.2024): Die Unterstützung der Gültigkeiten im Suchdienst ist nur auf der Ebene des Verwaltungsobjektes realisiert.
Als Name wird das erste Element  pro Sprache genommen ohne Auswertung der Gültigkeit.
Gilt im allgemeinen auch für alle Unterelemente. -->

:::

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)