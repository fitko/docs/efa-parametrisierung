---
id: formularbezeichnung
title: "Formularbezeichnung"
---


:::info[Version]

1.0.0

:::

# Formularbezeichnung

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Formularbezeichnung

### Wertemenge
[Internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat mehrere optionale Elemente _formular_. Jedes _formular_ ist über ein _zustaendigkeit_-Element mit einer _leistung_ und einer _gebietId_ (i.e. einem ARS) verknüpft.
Die _formular_-Elemente haben ein oder mehrere Elemente _bezeichnung_. Diese Elemente enthalten als Wert die gesuchte [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette).

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Array-Element `formular[]`, bei dem jedes Element ein `zustaendigkeit`-Element enthält. Der Onlinedienst muss dasjenige Element auswählen, das für die relevante Leistung im relevanten Gebiet (i.e. ARS) "zuständig" ist. Dafür muss er `formular[]:zustaendigkeit[]:leistungId[]` und `formular[]:zustaendigkeit[]:gebietId[]` auswerten und dabei ggf. die Hierarchie der ARS berücksichtigen. Das so identifizierte `formular[]` enthält die Formularbezeichnung als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette). 

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über das FIT-Connect Routing API ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)
