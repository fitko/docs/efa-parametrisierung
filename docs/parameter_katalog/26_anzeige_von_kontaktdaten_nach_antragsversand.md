---
id: anzeige_von_kontaktdaten_nach_antragsversand
title: "Anzeige von Kontaktdaten nach Antragsversand"
---


:::info[Version]

1.0.0

:::

# Anzeige von Kontaktdaten nach Antragsversand

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Falls in der Auswahloption der Wert „Ja“ konfiguriert wurde, so zeigt der Onlinedienst die Kontaktdaten der zuständigen Kontaktperson an nachdem der Antragsteller den Antrag abgesendet hat.

### Wertemenge
Datentyp Boolean, im XZuFi codiert mit den Zeichenketten `Ja` und `Nein`

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.uebermittlungKontaktdaten`".

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.uebermittlungKontaktdaten</xzufi:parameterName>
        <xzufi:parameterWert>Ja</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.uebermittlungKontaktdaten`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden.)

### Best Practices
Jedes Redaktionssystem sollte für diesen Parameter statt einer Textbox, in der man „Ja“ oder „Nein“ einträgt, ein spezialisiertes Eingabefeld (z.B. ein Dropdown) mit den drei Möglichkeiten _nicht definiert_, _ja_ und _nein_ haben.