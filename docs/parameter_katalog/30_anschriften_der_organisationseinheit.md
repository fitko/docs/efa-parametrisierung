---
id: anschriften_der_organisationseinheit
title: "Anschriften der Organisationseinheit"
---


:::info[Version]

1.0.0

:::

# Anschriften der Organisationseinheit

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Anschriften der Organisationseinheit. Jede Anschrift besitzt einen Typ, der die Verwendung der Anschrift betrifft, z.B. _Besucheranschrift_ oder _Postanschrift_.

### Wertemenge
Der XZuFi-Datentyp _AnschriftOrganisationseinheit_ ist eine Datenstruktur, die nicht nur eine Postanschrift, sondern diverse Metadaten zum Gebäude und zur offline-Erreichbarkeit der Organisationseinheit aufnehmen kann. Die Datenstruktur ist so umfänglich, dass sie hier nicht dokumentiert werden kann. Vielmehr muss auf die [XZufi 2.2 Spezifikation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0)  verwiesen werden.

Wichtig ist, dass eine Organisationseinheit mehrere Anschriften haben kann, die sich durch ihren Typ unterscheiden. Die Datenobjekte tragen daher ein Element _typ_, das einen Code aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.anschrifttyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.anschrifttyp) enthält. Mögliche Typen sind:
| Code | Beschreibung |
| --- | --- |
| 001 |	Hausanschrift |
| 002 |	Besucheranschrift |
| 003 |	Postfach |
| 004 |	Großempfängerpostfach |
| 005 |	Lieferanschrift |
| 006 |	Postanschrift |

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ besitzt mehrere optionale Elemente _anschrift_ vom Typ `xzufi:anschriftOrganisationseinheit`, der selbst eine Erweiterung des Datentyps `xzufi:anschrift` ist. Diese Elemente enthalten als Wert die gesuchte Daten und als Element _typ_ ein Element _code_ mit einem Code aus der Liste [`urn:xoev-de:fim:codeliste:xzufi.anschrifttyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.anschrifttyp).

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _anschrift_-Elements (Datentyp `xzufi:AnschriftOrganisationseinheit`).

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:anschrift>
        <xzufi:typ listURI="urn:xoev-de:fim:codeliste:xzufi.anschrifttyp" listVersionID="2.0">
          <code>001</code>
        </xzufi:typ>
        <xzufi:strasse>Herzog-Carl-Friedrich-Platz 1</xzufi:strasse>
        <xzufi:postleitzahl>21031</xzufi:postleitzahl>
        <xzufi:ort>Hamburg</xzufi:ort>
        <xzufi:verwaltungspolitischeKodierung>
          <xzufi:bundesland listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:bundesland" listVersionID="2010-04-01">
            <code>02</code>
            <name>Hamburg</name>
          </xzufi:bundesland>
          <xzufi:gemeindeschluessel listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:ags" listVersionID="2022-09-30">
            <code>02000000</code>
            <name>Hamburg</name>
          </xzufi:gemeindeschluessel>
          <xzufi:regionalschluessel listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs" listVersionID="2022-09-30">
            <code>020000000000</code>
            <name>Hamburg</name>
          </xzufi:regionalschluessel>
          <xzufi:staat listURI="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:staat" listVersionID="2023-02-24">
            <code>000</code>
            <name>Deutschland</name>
          </xzufi:staat>
        </xzufi:verwaltungspolitischeKodierung>
        <xzufi:geokodierung>
          <Point srsName="EPSG:25832" axisLabels="longitude latitude" xmlns="http://www.opengis.net/gml/3.2">
            <pos>580081.456 5927543.071</pos>
          </Point>
        </xzufi:geokodierung>
        <xzufi:id schemeAgencyID="S100002">S1000020050000000132</xzufi:id>
        <xzufi:infoOEPNV languageCode="de">Busse  X32/12/29/X80/136/137/234/332/534 bis Alte Holstenstraße oder S2/RE1 Bergedorf</xzufi:infoOEPNV>
        <xzufi:infoOEPNV languageCode="en">Buses X32 / 12/29/31/136/137/234/332/534 to Alte Holstenstraße or S2 / S21 / R20 Bergedorf</xzufi:infoOEPNV>
      </xzufi:anschrift>


    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `anschrift[]`, das die Anschriften der Organisationseinheit enthält. Der Onlinedienst muss zunächst diejenigen `anschrift[]`-Elemente identifizieren, die zum gewünschten Zeitpunkt gültig sind. Dazu untersucht der Onlinedienst für jedes dieser Elemente das `anschrift[]:gueltigkeit[]`-Element. Wenn es leer ist oder mindestens ein Objekt enthält, dessen `beginn` und `ende`-Elemente den gewünschten Zeitpunkt umschließen, so wird das `anschrift`-Element weiter betrachtet. Wenn nach dem Abschluss dieser Filterung immer noch mehrere `anschrift`-Elemente vorhanden sind, ist dasjenige auszuwählen, dessen Element `typ[]:code[]` dem gesuchten Anschriften-Typ entspricht.

Die weiteren Elemente der `anschrift`-Datenstruktur korrespondieren mit den Elementen der XZuFi-Spezifikation und können der [API-Spezifikation](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) entnommen werden. 

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:address` einige Daten aus demjenigen `anschrift`-Element, dessen Typ den Code `001` (Hausanschrift) trägt. Die Daten aus der XZuFi Anschrift-Datenstruktur sind in den Elementen `street`, `housenumber`, `additional`, `zip` und `city` enthalten.

Die Response-Datenstruktur enthält weiterhin das Element `route[]:postalAddress`, das einige Daten aus demjenigen `anschrift`-Element enthält, dessen Typ den Code `006` (Postanschrift) trägt. Die Daten aus der XZuFi Anschrift-Datenstruktur sind wieder in den Elementen `street`, `housenumber`, `additional`, `zip` und `city`  enthalten.

Die Response-Datenstruktur enthält weiterhin das Element `route[]:postOfficeBox`, das einige Daten aus demjenigen `anschrift`-Element enthält, dessen Typ den Code `003` (Postfach) trägt. Die Daten aus der XZuFi Anschrift-Datenstruktur sind in den Elementen `postOfficeBox`, `zip` und `city` enthalten.

Die Daten anderer Adresstypen und die weiteren, in XZuFi vorhandenen Daten der Adressen können über die FIT-Connect Routing API nicht ermittelt werden.

:::danger

Stand Q2/2024 wertet die Implementierung der FIT-Connect Routing-API die Angaben zur zeitlichen _gueltigkeit_ der _anschrift_-Elemente nicht aus. Daher kann die  FIT-Connect Routing API auch Anschriften-Daten liefern, die nicht mehr oder noch nicht gültig sind.

:::

<!--
Frage: Ist das Mapping aus der XZuFi-Datenstruktur mit den drei Codes 001, 006 und 003 so korrekt wiedergegeben? Ist es richtig, dass die weiteren Adress-Typen über die Routing-Dienst API nicht ermittelbar sind? 

Antwort (Teleport, 5.6.2024): Ja, es werden nur die Codes 001, 006 und 003 beachtet.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices

:::warning

Die fachliche Abgrenzung der verschiedenen Adresstypen geht nicht klar aus den Bezeichnungen in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.anschrifttyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.anschrifttyp) hervor. Wenn z.B. eine Postanschrift von der Hausanschrift abweicht UND eine Postfach-Anschrift ist, sollte sie dann als Postanschrift oder als Postfach-Anschrift abgelegt werden? Ein Onlinedienst sollte aufgrund dieser Unklarheiten ggf. verschiedene Anschriften jeweils mit den Bezeichnungen der zugeordneten Codes anzeigen, so dass der Antragsteller selbst aus dem Nutzungskontext ermitteln kann, welche Adresse er in der Offline-Welt nutzen möchte. Jede Geschäftslogik, die eine Adresse aus der Menge der `anschrift[]`-Elemente automatisch auswählt, sollte sorgfältig in der Onlinedienst-Dokumentation dokumentiert werden.

:::