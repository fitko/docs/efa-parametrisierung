---
id: zustellungskanal_destinationsignature
title: "Zustellungskanal (DestinationSignature)"
---


:::info[Version]

1.0.0

:::

# Zustellungskanal (DestinationSignature)

**Bemerkung**: Dieser Parameter ist nur vorhanden, wenn es einen FIT-Connect-basierten Zustellungskanal gibt. Er enthält dann die DestinationSignature, in der   weitere technische Adressierungsinformationen encodiert sind, so dass der Onlinedienst am Schluss den elektronischen Antrag versenden kann. 

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Parameter mit Bezug zum Antrags-Versand](../kategorisierung_der_parametrisierung.md#parameter-mit-bezug-zum-antrags-versand) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Die Destination Signature ist die wesentliche Adress-Information, die bei Benutzung des FIT-Connect Zustelldienstes verwendet werden muss, um den korrekten Adressaten ansprechen zu können.

### Wertemenge
Eine DestinationSignature ist eine Zeichenkette (String), die einen JSON Web Token (JWT) enthält. Dieser wiederum enthält die Addressinformation DestinationId sowie diverse Metadaten.

Die [FIT-Connect Dokumentation](https://docs.fitko.de/fit-connect/docs/sending/get-destination/#destinationSignature) enthält alle relevanten Informationen zur DestinationSignature und erklärt auch, wie die Signatur des JSON Web Token geprüft werden kann.  

### Verortung im Datenmodell
Stand Q2/2024 wird die DestinationSignature in einem Element _idSekundaer_ abgelegt, das Teil des Elements _zustaendigkeit_ (Datentyp `xzufi:zustaendigkeitOrganisationseinheit`) ist, welches selbst Teil des Datenobjekts _Organisationseinheit_ (Datentyp `xzufi:organisationseinheitErweitert`) ist und sich auf das Gebiet (i.e. den ARS) und die Leistung bezieht. 

Die _idSekundaer_ muss die _schemeID_ "`urn:de:fitko:fit-connect:xzufi:destination`" und die _schemeAgencyId_ "`urn:de:fitko`" tragen. 

Im Element _zustaendigkeit_ muss das Element _code_ im Element _rolle_ den Wert "`01`" oder "`02`" (i.e. Bedeutung mindestens "zuständige Stelle") enthalten.

Die DestinationSignature ist dann der Inhalt des Elements _idSekundaer_ (Datentyp `xs:token`). 

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _zustaendigkeit_-Elements.

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:zustaendigkeit>
        <xzufi:idSekundaer schemeAgencyID="urn:de:fitko" schemeID="urn:de:fitko:fit-connect:xzufi:destination">eyJra ...
            ... nnGEp4</xzufi:idSekundaer> <!-- verkürzte Darstellung des JWT -->
        <xzufi:leistungID schemeAgencyID="S100002">S1000020010000012932</xzufi:leistungID>
        <xzufi:gebietID schemeAgencyID="DESTATIS" schemeID="urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:rs">024020404404</xzufi:gebietID>
        <xzufi:rolle listURI="urn:de:xzufi:codeliste:zustaendigkeitsrolle" listVersionID="20210401">
          <code>02</code>
          <name>Zuständige Stelle</name>
        </xzufi:rolle>
      </xzufi:zustaendigkeit>

    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Der Onlinedienst ermittelt zuerst die ID der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der ID der Leistungsbeschreibung](../parametrisierung_anhänge/6_2_ermittlung_der_id_der_leistungsbeschreibung.md)), die er später zur Identifikation der passenden ZustaendigkeitOrganisationseinheit benötigt.

Der Onlinedienst verwendet dann den Endpunkt [`/v2/relations/{ars}/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Zustandigkeiten/operation/getZsTrByLbIdAndArs) mit dem Pfad-Parameter `ars`= \<ARS\>, sowie dem URL-Parameter `lbids`=\<die zuvor ermittelte LeistungsbeschreibungsID\>. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Angaben auf diejenigen Elemente zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind. Der Onlinedienst erhält damit eine JSON-Datenstruktur als Response.

Dort muss der Onlinedienst ein Element in den `content[]:zustaendigkeit[]:idsekundaer[]`-Arrays identifizieren, das die folgenden Eigenschaften hat:
- `content[]:zustaendigkeit[]:gueltigkeit[]` ist leer oder enthält mindestens ein Objekt mit den Elementen `beginn`und `ende`, das den gewünschten Zeitpunkt umschließt
- `content[]:zustaendigkeit[]:rolle:code` = „`01`“ oder „`02`“, d.h. _Zuständige Stelle und Ansprechpunkt_ oder nur _Zuständige Stelle_
- `content[]:zustaendigkeit[]:rolle:listURI` = „`urn:de:xzufi:codeliste:zustaendigkeitsrolle`“
- `content[]:zustaendigkeit[]:idsekundaer[]:schemeAgencyID` = "`urn:de:fitko`"
- `content[]:zustaendigkeit[]:idsekundaer[]:schemeID` = „`urn:de:fitko:fit-connect:xzufi:destination`“

Zur Interpretation der Zuständigkeit einer Organisationseinheit, siehe auch Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md)

In dem so identifizerten `content[]:zustaendigkeit[]:idsekundaer`-Element enthält das Element `value` die DestinationSignature.

Hinweis: Ein Onlinedienst kann Stand Q2/2024 zwar die DestinationSignature über die PVOG-Suchdienst-API ermitteln, er kann ohne die Nutzung der FIT-Connect APIs aber die Antragsdaten nicht versenden, weil er für das Versenden noch Zertifikat-Informationen benötigt. Es wird daher empfohlen, für die Ermittlung der Parameter zum Antragsversand ausschließlich die FIT-Connect APIs zu nutzen, falls der Kanal _FIT-Connect_ genutzt werden soll.

:::info

In XZuFi 2.3 wird das Element _Kommunikationssystem_ nicht mehr direkt der _Organisationseinheit_, sondern deren _Zustaendigkeit_ für eine _Leistung_ und eine _gebietID_ (i.e. ARS) zugeordnet sein. Es soll hier daher für XZuFi 2.3 festgelegt werden, dass die Adressinformationen für FIT-Connect dann in einem _Kommunikationssystem_-Element hinterlegt werden. Dieses soll dann dadurch identifiziert werden, dass es im Element `kommunikationssystem[].kanal.code` den Wert "004" ("System mit Anbindung an FIT-Connect ") der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.kommunikationssystemtyp) trägt.

In dem so identifizierten _Kommunikationssystem_-Element werden dann die beiden Elemente `kennung` und `kennungZusatz` die Adressinformationen tragen:
- `kennung`= \<DestinationSignature\>
- `kennungZusatz` = \<DestinationID aus der DestinationSignature\> (optional)

Die DestinationID ist eigentlich redundant, da sie auch in der DestinationSignature encodiert ist. Ist das _kennungZusatz_-Element gefüllt, kann der Onlinedienst diese Information direkt weiter verwenden und braucht die DestinationSignature nicht selbst zu decodieren.

:::

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält im Element `route[]:destinationSignature` die DestinationSignature.

Hinweis: Benachbarte Elemente wie `route[]:destinationId` und `route[]:destinationParameters{}` enthalten weitere Metadaten zur DestinationSignature. Das Element `destinationParameters` ist jedoch [abgekündigt](https://docs.fitko.de/fit-connect/docs/sending/get-destination#destination-object), so dass diese Informationen mittelfristig dort nicht mehr ermittelbar sein werden. Insbesondere die für den Versand notwendige `submissionUrl` soll in Zukunft über die [FIT-Connect Submission API](https://docs.fitko.de/fit-connect/docs/apis/submission-api#post-/v1/submissions) ermittelt werden.

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Die Rollen der Organisationseinheiten werden in den Redaktionssystemen nicht immer korrekt gepflegt. Ein Onlinedienst sollte daher nicht nur die `content[]:zustaendigkeit[]:idsekundaer[]` betrachten, die einer `content[]:zustaendigkeit[]` zugeordnet ist, bei der `content[]:zustaendigkeit[]:rolle:code` = „`01`“ oder „`02`“ ist. _Wenn_ der Onlinedienst _keine_ Organisationseinheit findet, die in der Rolle „`01`“ oder „`02`“ zuständig ist, _dann_ sollte der Onlinedienst auch die anderen zuständigen Organisationseinheiten nach `content[]:zustaendigkeit[]:idsekundaer[]:schemeAgencyID` = "`urn:de:fitko`" und `content[]:zustaendigkeit[]:idsekundaer[]:schemeID` = „`urn:de:fitko:fit-connect:xzufi:destination`“ durchsuchen und die so identifizierten Organisationseinheiten als zuständig für die Antragstellung behandeln. Das Fehlen einer zuständigen Organisationseinheit mit der Rolle „`01`“ oder „`02`“ sollte als Warnung bezüglich einer Fehlkonfiguration geloggt werden. Die Implementierung und die Konsequenzen dieser Regel sollten mit dem fachlichen Anforderungsmanagement abgesprochen und im Anbindungsleitfaden dokumentiert werden.

Der konkrete Wert der DestinationSignature muss im Rahmen der Selbst-Registrierung von FIT-Connect Ansprechpunkten beim FIT-Connect SelfService-Portal ermittelt werden. Dieser Wert – eine mehrere tausend Zeichen lange Zeichenkette – muss z.B. in einer Textdatei zwischengespeichert und später im Redaktionssystem an passender Stelle eingetragen werden, so dass das Redaktionssystem diesen Wert wie oben beschrieben als idSekundaer an das PVOG meldet.

