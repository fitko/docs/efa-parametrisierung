---
id: vertrauensniveau
title: "Vertrauensniveau"
---


:::info[Version]

1.0.0

:::

# Vertrauensniveau

**Bemerkung:** Der Software-Architekt des Onlinedienst-Entwicklungsprojektes muss klären, ob die verwendete Software Plattform es erlaubt, dass der Onlinedienst das notwendige Vertrauensniveau selbst per Parameter zur Laufzeit  bestimmt und/oder selbst Einfluss darauf hat, ein ggf. bestehendes Missverhältnis aus vorliegendem und notwendigem Vertrauensniveau zu prüfen und behandeln zu können. Erst wenn diese Frage geklärt ist, kann der Architekt die Nutzung dieses Parameters einplanen.

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Das Vertrauensniveau, das der Onlinedienst im Zuge der Anwender-Authentifizierung voraussetzt, um nutzbar zu sein.

### Wertemenge
Code aus der Code-Liste [`urn:xoev-de:fim:codeliste:vertrauensniveau`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:vertrauensniveau)

### Verortung im Datenmodell
Das Vertrauensniveau wird im Element _code_ des Elements _vertrauensniveau_ im Datenobjekt _Onlinedienst_ abgelegt. 

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
    <xzufi:vertrauensniveau listURI="urn:xoev-de:fim:codeliste:vertrauensniveau" listVersionID="4">
        <code>30</code>
        <name>hoch</name>
    </xzufi:vertrauensniveau>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein Element `levelOfConfidence`, das den Code aus der Liste [`urn:xoev-de:fim:codeliste:vertrauensniveau`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:vertrauensniveau) enthält.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Jedes Redaktionssystem sollte für diesen Parameter eine Dropdown-Auswahl mit den Bezeichnungen der Vertrauensniveaus aus [`urn:xoev-de:fim:codeliste:vertrauensniveau`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:vertrauensniveau) anbieten. 