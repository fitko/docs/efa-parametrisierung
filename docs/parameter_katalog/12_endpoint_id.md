---
id: endpoint_id
title: "EndpointId"
---


:::info[Version]

1.0.0

:::

# EndpointID

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug)| 

### Fachliche Bedeutung
Ein Onlinedienst erstellt einen PaymentRequest, also die Aufforderung, eine konkrete Bezahlung durchzuführen, und sendet diesen an den zuständigen Bezahldienst. Dabei gibt es zwei Path Parameter ([OriginatorID](./11_originator_id.md) und EndPointID), mit denen die Basis-URL des Bezahldienstes erweitert wird. Somit wird es dem Bezahldienst ermöglicht, diesen PaymentRequest für die Verbuchung zuzuordnen. 

Die EndpointID stellt den Bezug zur zuständigen Stelle wie z.B. "11009009“ und wird vom Bezahldienstanbieter in Abstimmung mit der nachnutzenden Organisation abgestimmt. 

### Wertemenge
Eine Zeichenkette der maximalen Länge 36, die dem RegExp-Pattern `^[\w\d-]+$` gehorcht.

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.endpointid`".

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.payment.endpointid</xzufi:parameterName>
        <xzufi:parameterWert>ULV-123-12</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.endpointid`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden.)

### Best Practices
(keine Empfehlungen)