---
id: paymentservicebaseurl_des_bezahldienstes
title: "PaymentServiceBaseUrl des Bezahldienstes"
---


:::info[Version]

1.0.0

:::

# PaymentServiceBaseUrl des Bezahldienstes

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::


**Bemerkung**: Verschiedene Onlinedienste können denselben Bezahldienst nutzen. In einem solchen Nutzungs-Szenario muss dieser Parameter nur einmal konfiguriert werden.
 
| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [DVDV-basierter Parameter](../kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter) |

### Fachliche Bedeutung
Basis-URL, über den der Onlinedienst die Methode POST PaymentRequest am Bezahldienst aufruft. 

### Wertemenge
Datentyp URL, im DVDV codiert mit einer Zeichenkette, die den Regeln des [URL-Encoding](https://de.wikipedia.org/wiki/URL-Encoding) entsprechen

### Verortung im Datenmodell
Die _Organisation_, die den Bezahldienstanbieter repräsentiert, ist mit einem _DienstElement_ vom Typ "Bezahldienst-Server" verknüpft. Dieses _DienstElement_ hat ein Element _ServiceElementUri_, das den Wert des Parameters PaymentServiceBaseUrl enthält.

### Ermittlungsmöglichkeit PVOG API
(Der Parameter kann nicht über den PVOG-Suchdienst ermittelt werden)

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API

Der Onlinedienst [ermittelt zunächst das Bezahldienst-Server Dienstelement über die DVDV-API](../parametrisierung_anhänge/6_13_ermittlung_dienstelement_bezahldienstserver.md).

Das so ermittelte DienstElement liefert mit `getServiceElementUri()` den gesuchten Wert.

### Best Practices
Siehe [Best Practice: Nutzung DVDV-Bibliothek](../parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md)