---
id: weiterführende_informationen_zur_leistung
title: "Weiterführende Informationen zur Leistung"
---


:::info[Version]

1.0.0

:::

# Weiterführende Informationen zur Leistung

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Angabe von zusätzlichen Informationen zur Leistung ohne speziell strukturierte Daten. 

Die weiterführenden Informationen dienen den Nutzern als Auskunft, inwiefern und welche weiterführenden Informationen im Internet bereits veröffentlicht sind. In diesem Rahmen können auf weiterführende Informationen zum Beispiel auch Fachportale, Themenportale oder Broschüren verwiesen werden. Dies soll den Nutzenden tiefergehende Informationen und Umfeld-Informationen liefern. 
Der Parameter „weiterführende Informationen zur Leistung“ ist ein  FIM-Modul und in den Landesredaktionssystemen nicht verpflichtend zu pflegen.

### Wertemenge
Eine komplexe Datenstruktur _Textmodul_, die die folgenden Elemente enthält:
- _inhalt_: ein [internationalisierter Text](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette), 
- _leikaTextmodul_: der Code "`15`" aus der Code-Liste [`urn:de:xzufi:codeliste:leistungstextmodulleika`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:leistungstextmodulleika) (Bezeichnung "weiterführende Informationen") zur Festlegung der Kategorie des Textmoduls
- _leikaTextmodulAbweichendeBezeichnung_: eine internationalisierte abweichende Bezeichnung und 
- _weiterfuehrenderLink_: ein [oder ggf. mehrere](#best-practices) Links zu weiteren Informationen in Form einer [internationalisierten Datenstruktur](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) _HyperlinkErweitert_.

Für eine detaillierte Darstellung dieser Datenstruktur wird hier auf die [XZuFi-Dokumentation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen.

### Verortung im Datenmodell
Die weiterführenden Informationen werden in einem optionalen Element _modulText_, das Teil des Elements _Leistung_ ist, abgelegt und mit dem Code "`15`" aus der Code-Liste [`urn:de:xzufi:codeliste:leistungstextmodulleika`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:leistungstextmodulleika) gekennzeichnet.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _modulText_-Elements.

:::

```xml
    <xzufi:leistung>

      <xzufi:modulText>
        <xzufi:positionDarstellung>14</xzufi:positionDarstellung>
        <xzufi:inhalt languageCode="de">Weiterführende Informationen zur Leistung</xzufi:inhalt>
        <xzufi:inhalt languageCode="en">Further information on the service</xzufi:inhalt>
        <xzufi:leikaTextmodul listURI="urn:de:xzufi:codeliste:leistungstextmodulleika" listVersionID="20201201">
          <code>15</code>
        </xzufi:leikaTextmodul>
        <xzufi:weiterfuehrenderLink languageCode="de">
          <xzufi:uri>https://www.hamburg.de/service/info/11252980</xzufi:uri>
          <xzufi:titel>Anzeigepflicht nach § 14 Sprengstoffgesetz Informationsseite</xzufi:titel>
        </xzufi:weiterfuehrenderLink>
        <xzufi:weiterfuehrenderLink languageCode="en">
          <xzufi:uri>https://www.hamburg.de/behoerdenfinder/hamburg/11252980</xzufi:uri>
          <xzufi:titel>Obligation to notify according to § 14 Explosives Act Information page</xzufi:titel>
          <xzufi:beschreibung>Currently this link is only available in German</xzufi:beschreibung>
        </xzufi:weiterfuehrenderLink>
      </xzufi:modulText>

    </xzufi:leistung>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst [ermittelt der Onlinedienst die Daten der Leistungsbeschreibung](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md).

In der Response findet sich ein Element `modulText[]` das Informationen zu verschiedenen Kategorien enthält. Aus diesen Elementen muss der Onlinedienst dasjenige auswählen, bei dem das Element `modulText[]:leikaTextmodul:code` den Wert `15` enthält und dessen Element `modulText[]:gueltigkeit[]` leer ist oder mindestens ein Objekt enthält, dessen `beginn` und `ende`-Elemente den gewünschten Zeitpunkt umschließen.

Das so identifizerte `modulText`-Element enthält dann die weiterführenden Informationen.

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:furtherInformation:description` die Texte aus dem _inhalt_-Element der XZuFi-Datenstruktur als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette). Die Informationen aus den XZuFi-Elementen _leikaTextmodulAbweichendeBezeichnung_ und _weiterfuehrenderLink_ können nicht über die FIT-Connect Routing API ermittelt werden.

:::danger

Stand Q2/2024 wertet die FIT-Connect Routing API die Elemente `modulText[]:gueltigkeit[]` nicht aus. Es besteht daher das Risiko, dass die API Informationen liefert, die nicht mehr oder noch nicht gültig sind.

:::


<!--
Frage: Welche Teile der _Textmodul_-Struktur werden hier zusammengefasst? Nur die Beschreibungen oder auch alle anderen Teile? 

Antwort (Teleport, 5.6.2024): Der Routingdienst nutzt nur die Beschreibungsfelder und gibt alle Inhalte in einer Map<Sprache, Inhalt> zurück. Weitere Logik ist hier nicht implementiert.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Bezüglich `hyperlinkErweitert` sind unbedingt die Hinweise zu [Internationalisierten Datenstrukturen](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) zu beachten!

