---
id: zahlungsweisen
title: "Zahlungsweisen"
---


:::info[Version]

1.0.0

:::

# Zahlungsweisen

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Aufzählung aller Zahlungsweisen, die dem Antragsteller in diesem Onlinedienst für die Entrichtung von Gebühren angeboten werden, zum Beispiel „giropay“, „Kreditkarte“, „SEPA-Lastschrift“.

### Wertemenge

Menge von Codes aus der Code-Liste [`urn:de:xzufi:codeliste:zahlungsweise`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zahlungsweise)

### Verortung im Datenmodell
Die Zahlungsweisen werden in den Elementen _zahlungsweise_ abgelegt, die Teil des Datenobjekts _Onlinedienst_ sind.

Für jede angebotene Zahlungsweise ist ein Element _zahlungsweise_ vorhanden, das ein Element _code_ enthält, das dann den Code aus der Liste [`urn:de:xzufi:codeliste:zahlungsweise`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zahlungsweise) enthält.

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:zahlungsweise listURI="urn:de:xzufi:codeliste:zahlungsweise" listVersionID="20210401">
        <code>3080100</code>
        <name>giropay</name>
      </xzufi:zahlungsweise>
      <xzufi:zahlungsweise listURI="urn:de:xzufi:codeliste:zahlungsweise" listVersionID="20210401">
        <code>3050100</code>
        <name>Klassische Kreditkarte</name>
      </xzufi:zahlungsweise>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md)

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein String-Array Element `paymentMethods[]`, das jedoch **nicht** die Codes aus der Liste [`urn:de:xzufi:codeliste:zahlungsweise`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zahlungsweise) enthält, sondern Texte, die die Zahlungsweisen beschreiben. Die Texte sind in der Sprache, die im Aufruf von [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) in einem Accept-Language Header als bevorzugte Sprache angegeben wurde, oder in deutscher Sprache, wenn die Texte in der bevorzugten Sprache im Datenbestand der Code-Liste nicht vorhanden sind.

:::warning

Der Onlinedienst sollte Stand Q2/2024 keine Geschäftslogik auf der Basis des Parameters Zahlungsweisen aufbauen, da die API nicht die Codes der Zahlungsweisen, sondern nur erklärende Texte liefert. Der Onlinedienst kann jedoch die möglichen Zahlungsweisen dem Antragsteller zur Information darstellen.

Ohnehin wären die Möglichkeiten für eine Geschäftslogik auf Basis des Parameters Zahlungsweisen eingeschränkt, da die tatsächlich verfügbaren Zahlungsweisen vom Bezahldienstleister bestimmt werden und bei Nutzung von [XBezahldienste 1.0](https://docs.fitko.de/xbezahldienste/) _nicht_ durch einen Parameter _gesteuert_ werden können. Der Parameter hat daher nur einen unverbindlich-informativen Charakter.

:::

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Jedes Redaktionssystem sollte für diesen Parameter eine Checkbox-Liste mit den Bezeichnungen der Zahlungsweisen aus [`urn:de:xzufi:codeliste:zahlungsweise`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zahlungsweise) anbieten. 