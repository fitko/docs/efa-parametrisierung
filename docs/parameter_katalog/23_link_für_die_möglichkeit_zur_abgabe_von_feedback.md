---
id: link_für_die_möglichkeit_zur_abgabe_von_feedback
title: "Link für die Möglichkeit zur Abgabe von Feedback"
---


:::info[Version]

1.0.0

:::

# Link für die Möglichkeit zur Abgabe von Feedback

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Link für die Möglichkeit zur Abgabe von Feedback. Jede Art von Feedback kann hier abgegeben werden, insbesondere auch zur Umsetzung/Benutzerfreundlichkeit der Onlinedienst-Implementierung.

### Wertemenge
Datentyp URL, im XZuFi codiert mit einer Zeichenkette, die den [Regeln des URL-Encoding](https://de.wikipedia.org/wiki/URL-Encoding) entsprechen

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.feedback.url`".

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.feedback.url</xzufi:parameterName>
        <xzufi:parameterWert>https://www.hamburg.de/feedback/</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.feedback.url`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden.)

### Best Practices
Siehe [Best Practices zu URL-wertigen Parametern](../parametrisierung_anhänge/6_10_best_practices_url_wertige_parameter.md)