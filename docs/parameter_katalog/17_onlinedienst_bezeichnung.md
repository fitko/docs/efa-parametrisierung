---
id: onlinedienst_bezeichnung
title: "Onlinedienst Bezeichnung"
---


:::info[Version]

1.0.0

:::

# Onlinedienst Bezeichnung

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Onlinedienst-bezogener Standardparameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-standard-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) |

### Fachliche Bedeutung
Die Bezeichnung des Onlinedienstes

### Wertemenge
[Internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)

### Verortung im Datenmodell
Die Onlinedienst Bezeichnung wird im Element _bezeichnung_ des Datenobjekts _Onlinedienst_ abgelegt. 

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
    <xzufi:bezeichnung languageCode="de">Kinderleicht zum Kindergeld</xzufi:bezeichnung>
    <xzufi:bezeichnung languageCode="en">Easy as pie to child support</xzufi:bezeichnung>

</xzufi:onlinedienst>
```
<!-- im HaSI-Export findet man hier ein HTML-Element <div>. Dies ist bei wörtlicher Auslegung der XZuFi-Spezifikation nicht zulässig:

 <xzufi:bezeichnung languageCode="en">
        <div lang="en-x-mtfrom-de">Easy as pie to child support</div>

-->

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige _Onlinedienst_-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md).

Die Response, die der Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) zurückgibt, enthält ein Element `name`, das die Bezeichnung enthält.
Die Bezeichnung ist in der Sprache, die im Aufruf von [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) in einem Accept-Language Header als bevorzugte Sprache angegeben wurde, oder in deutscher Sprache, wenn die Bezeichnung in der bevorzugten Sprache im Datenbestand nicht vorhanden ist.

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über den FIT-Connect Routing-Dienst ermittelt werden)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)