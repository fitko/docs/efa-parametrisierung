---
id: paymentitem_reference
title: "PaymentItem Reference"
---


:::info[Version]

1.0.0

1.0.1 veraltet

:::

# PaymentItem Reference - veraltet!
:::danger[Veraltet!]

Der Parameter PaymentItem Reference ist zugunsten des Parameters [PaymentItem](./14.5_paymentitem.md) abgekündigt und somit veraltet.
Der neue Parameter [PaymentItem](./14.5_paymentitem.md) fasst die beiden alten Parameter PaymentItem Reference und [PaymentItem Bookingdata](./14_paymentitem_bookingdata.md) zusammen.

Onlinedienste, die auf der Basis von XZuFi 2.2 parametrisiert werden und ein Paar von Parametern PaymentItem Reference und PaymentItem Bookingdata erwarten,
sollten ohne Probleme weiterhin korrekt arbeiten.
Eine möglicherweise vorhandene Unterstützung für die komfortable Pflege der Parameter kann aber in den Redaktionssystemen zurückgebaut werden.
Mit der Migration zu XZuFi 2.3 können diese Onlinedienste weiter betrieben werden, wenn die Parameter als [individuelle Parameter](../kategorisierung_der_parametrisierung.md#onlinedienst-bezogene-individual-parameter) behandelt werden.
Eine Migration der beiden veralteten Parameter zum neuen Parameter [PaymentItem](./14.5_paymentitem.md) wird aber im Zuge der Migration XZuFi 2.2 - 2.3 dringend empfohlen.

:::

:::danger

Die Bezahldienst-bezogenen Parameter dürfen ggf. nur in revisionssicheren Systemen und/oder unter Einhaltung eines Vier-Augen-Prinzips oder anderen Regelungen gepflegt werden.
Eine Prüfung, ob diese Restriktionen bestehen und wenn ja, für welche Parameter sie gelten, steht noch aus.

Die Standardisierung dieser Parameter steht bis zur Klärung dieser Frage noch aus.

:::

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Bezahldienst-bezogener Parameter](../kategorisierung_der_parametrisierung.md#bezahldienst-bezogene-parameter) | [XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) | 

### Fachliche Bedeutung
Die paymentItems, die der Onlinedienst im Zuge des Bezahlvorgangs an den Bezahldienst meldet, enthalten ein Element Reference. Dieses entspricht quasi einer Artikelnummer - aus Sicht des Bezahldienstes.

### Wertemenge
Eine Zeichenkette der maximalen Länge 36, die dem RegExp-Pattern `^[\w\d-]+$` gehorcht.

### Verortung im Datenmodell
Siehe Abschnitt [Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell](../parametrisierung_anhänge/6_1_verortung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.paymentitemreference`".

In einem Szenario, in dem ein Onlinedienst mehrere verschiedene Elemente der Dienstleistung bezahlen lassen muss, müssen auch mehrere Parameter PaymentItem Reference angegeben werden. Es ist daher vorgesehen, dass es mehrere Elemente mit demselben Namens-Schlüssel gibt. 

#### Codebeispiel XZuFi 2.2
```xml
<xzufi:onlinedienst xmlns:p3="http://www.w3.org/2001/XMLSchema-instance"
    p3:type="xzufi:OnlinedienstErweitert">
    
      <xzufi:parameter>
        <xzufi:parameterName>efa.payment.paymentitemreference</xzufi:parameterName>
        <xzufi:parameterWert>001|XYZ22113</xzufi:parameterWert>
      </xzufi:parameter>
      <xzufi:parameter>
        <xzufi:parameterName>efa.payment.paymentitemreference</xzufi:parameterName>
        <xzufi:parameterWert>002|ABC98765</xzufi:parameterWert>
      </xzufi:parameter>

</xzufi:onlinedienst>
```

### Ermittlungsmöglichkeit PVOG API
Siehe Abschnitt [Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_5_ermittlung_xzufi_parameter_mit_bezug_im_pvog.md).
Der Namens-Schlüssel ist "`efa.payment.paymentitemreference`".

### Ermittlungsmöglichkeit FIT-Connect Routing API
(Der Parameter kann nicht über die FIT-Connect Routing-API ermittelt werden.)

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden.)

### Best Practices
Zu den Herausforderungen mit PaymentItem-spezifischen Parametern siehe Abschnitt [Herausforderungen mit PaymentItem-spezifischen Parametern](../parametrisierung_anhänge/6_7_herausforderungen_mit_paymentitem_parametern.md)