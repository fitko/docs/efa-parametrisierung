---
id: kurzbezeichnung_der_organisationseinheit
title: "Kurzbezeichnung der Organisationseinheit"
---


:::info[Version]

1.0.0

:::

# Kurzbezeichnung der Organisationseinheit

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der zuständigen Organisationseinheit und ihrer Formulare](../kategorisierung_der_parametrisierung.md#stammdaten-der-zuständigen-organisationseinheit-und-ihrer-formulare) | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Kurzbezeichnung der Organisationseinheit

### Wertemenge
Menge von [internationalisierten Zeichenketten](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) jeweils mit Gültigkeitszeitraum
 

### Verortung im Datenmodell
Das Datenobjekt _Organisationseinheit_ hat ein oder mehrere Elemente _name_, die selbst wieder mehrere optionale Elemente enthalten, die _kurzbezeichnung_ heißen. Diese bilden jeweils eine [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette).

:::warning

Jedes _name_-Element hat einen optionalen Gültigkeitszeitraum. Um ein eindeutiges und deterministisches Verhalten der Onlinedienste zu bewirken, muss die Redaktion sicherstellen, dass es für jeden Zeitpunkt genau ein gültiges _name_-Element gibt. Ein Onlinedienst, der für einen Zeitpunkt mehrere gültige _name_-Elemente vorfindet, muss eines dieser Elemente auswählen. Dabei hat der Onlinedienst keine Selektions-Kriterien zur Verfügung, so dass seine Auswahl zufällig erscheinen muss. Tritt dieses Ereignis ein, sollte der Onlinedienst eine entsprechende Warnung loggen, dass hier vermutlich eine Fehlkonfiguration vorliegt.

:::

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des äußeren _name_-Elements.

:::

```xml
    <xzufi:organisationseinheit p3:type="xzufi:OrganisationseinheitErweitert" xmlns:p3="http://www.w3.org/2001/XMLSchema-instance">

      <xzufi:name>
        <xzufi:name languageCode="de">Behörde für Wirtschaft und Innovation</xzufi:name>
        <xzufi:name languageCode="en">Authority for Economy and Innovation</xzufi:name>
        <xzufi:kurzbezeichnung languageCode="de">Behörde für Wirtschaft und Innovation</xzufi:kurzbezeichnung>
        <xzufi:kurzbezeichnung languageCode="en">Authority for Economy and Innovation</xzufi:kurzbezeichnung>
      </xzufi:name>

    </xzufi:organisationseinheit>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst muss das zuständige Organisationseinheit-Datenobjekt ermittelt werden. Siehe dazu Abschnitt [Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md).

Die Response, die der Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) zurückgibt, enthält ein Element `name[]`, das die Menge der Namen (jeweils mit Gültigkeitszeiträumen) enthält. Der Onlinedienst muss dasjenige `name[]`-Element identifizieren, bei dem `name[]:gueltigkeit` nicht vorhanden ist oder dessen `name[]:gueltigkeit:beginn` und `name[]:gueltigkeit:ende`-Elemente den gewünschten Zeitpunkt umschließen.

Das so identifizierte `name[]`-Element enthält ein Element `name[]:kurzbezeichnung[]`, das die Kurzbezeichnung der Organisationseinheit als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) enthält. 

### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:destinationShortName` die Kurzbezeichnung der Organisationseinheit als einfache (i.e. nicht als internationalisierte) Zeichenkette.

:::danger

Stand Q2/2024 wird der Wert des Elements `route[]:destinationShortName` von  der FIT-Connect Routing API nicht korrekt gefüllt. Statt der aktuell gültigen Kurzbezeichnung enthält das Element die Kurzbeschreibung. Ein entsprechender Bug-Report wurde erfasst und soll korrigiert werden. 

:::

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
(keine Empfehlungen)