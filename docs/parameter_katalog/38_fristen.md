---
id: fristen
title: "Fristen"
---


:::info[Version]

1.0.0

:::

# Fristen

| Fachliche Kategorie | Technische Kategorie |
| --- | --- |
| [Stammdaten der Leistung](../kategorisierung_der_parametrisierung.md#stammdaten-der-leistung)  | [XZuFi-basierte Parameter ohne direkten Onlinedienst-Bezug](../kategorisierung_der_parametrisierung.md#xzufi-basierte-parameter-ohne-direkten-onlinedienst-bezug) |

### Fachliche Bedeutung
Fristen

### Wertemenge
Eine optionale komplexe Datenstruktur _modulFrist_, die drei Elemente enthält:
- _frist_: Eine Menge verschiedener möglicher Fristen. Jede Frist enthält selbst wieder die folgenden Elemente:
    - _typ_: Codierung des Frist-Typs. Details siehe [XZuFi-Dokumentation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) zu _Fristtyp_Erweiterbar_.
    - _fristauswahl_: Eines der beiden folgenden Elemente
        - _fristDauer_: strukturierte Angabe in Form von _von/bis-Dauer_
        - _fristStichtag_: strukturierte Angabe in Form von _von/bis-Datum_
    - _beschreibung_: Eine Beschreibung der Frist als [internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette)
    - _positionAuswahl_: Position in der Darstellung aller Fristen pro Modul (Ganzzahl)
    - _gueltigkeit_: Der Gültigkeitszeitraum, zu dem die Daten dieses Moduls gültig sind.
- _beschreibung_: textuelle Erläuterungen zu den Fristen ([internationalisierte Zeichenkette](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette))
- _weiterführenderLink_: Link zu weiteren Informationen in Form einer [internationalisierten Datenstruktur](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) _HyperlinkErweitert_. 

:::warning

Der XZuFi-Standard macht keine Aussage über die Interpretation einer Frist-Dauer-Angabe. So bleibt dort undefiniert, wann die Frist beginnt. Es ist daher fachlich zu klären, ob eine Frist-Dauer-Angabe relativ zum Datum der Antragstellung oder relativ zu einem anderen Datum zu interpretieren ist. Das Ergebnis dieser Klärung ist in der Onlinedienst-Dokumentation zu dokumentieren und in den Texten der Antragsdialoge unmissverständlich zu erklären. Kann überregional keine einheitliche Regelung zur Interpretation von Frist-Dauer-Angaben erreicht werden, muss in den Anbindungsleitfäden klar darauf hingewiesen werden, dass die textuellen _beschreibungen_ der Frist-Abgaben jeweils eine Festlegung liefern müssen, z.B. "Die Frist innerhalb der Sie einen Widerspruch gegen den Bescheid eingelegen können. Die Frist beginnt mit dem Datum des Bescheides."

Wenn hier sogar ein Bedarf für eine Parameter-basierte Steuerung besteht, dann bietet es sich an, dies über [Individual-Parameter](../kategorisierung_der_parametrisierung.md#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) zu steuern. So könnte ein Parameter mit dem Namens-Schlüssel `efa.custom.widerspruchsfrist.beginn` und den beiden möglichen Werten `bescheiddatum` und `bescheidzustelldatum` vom Onlinedienst individuell festlegt werden. Der Onlinedienst kann dann anhand dieses Parameters seine Dialoge, Texte und ggf. seine Geschäftslogik steuern.

<!-- 

Dataport (telko 20.6.2024):
Start der Dauer ist nicht definiert. Lösung mit Individualparametern erscheint möglich.
-->

:::

Das _modulFrist_, das nur maximal einmal vorhanden sein kann, kann selbst wieder mehrere _frist_-Elemente enthalten, wobei jedes dieser _frist_-Elemente einen eigenen _typ_ hat. Als mögliche Fristtypen stehen in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.fristtyp`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.fristtyp) die folgenden Werte zur Verfügung:
| Code | Beschreibung |
|------|-------------|
| 001  | Anhörungsfrist |
| 002  | Antragsfrist |
| 003  | Aufbewahrungsfrist |
| 005  | Geltungsdauer |
| 006  | Genehmigungsfiktion |
| 009  | Widerspruchsfrist |

Das _typ_-Element ist in der XZuFi-Spezifikation aber als Datentyp `xzufi:Fristtyp_Erweiterbar` spezifiziert, kann also auch Typ-Codes aufnehmen, die nicht in der Code-Liste vorhanden sind. Diese werden dann in einem Element _typ_/_nichtgelisteterWert_ mit Datentyp `xoev-lc:String.Latin` abgelegt. Hier kann z.B. ein selbst-definierter Code (z.B. `X01`) abgelegt werden, um dem Onlinedienst einen besonderen Fristtyp zu signalisieren. Die faktische Code-Listen-Erweiterung um diese selbst-definierten Codes muss dann in der Dokumentation des Onlinedienstes und im zugehörigen Roll-Out-Plan dokumentiert werden, damit die Redakteure dieses Feature korrekt nutzen können.

Jedes _frist_-Element kann seine eigenen Gültigkeits-Zeiträume haben, so dass ein Redakteur bereits im Januar eine Antragsfrist für das erste Halbjahr und eine abweichende Antragsfrist für das zweite Halbjahr einpflegen kann. Da ein _frist_-Element auch mehrere Gültigkeits-Zeiträume haben kann, ist es möglich ein _frist_-Element in jedem ersten Halbjahr der folgenden zehn Jahre gültig zu setzen, während ein anderes _frist_-Element in jedem zweiten Halbjahr der folgenden zehn Jahre gültig ist. Die Angabe einer _frist_ ohne _gueltigkeit_ bedeutet, dass diese Angabe zu jedem Zeitpunkt gültig ist.

Es muss bei der Pflege der _frist_-Elemente aber sichgestellt werden, dass die Angaben _typ_ und _gueltigkeit_ so gewählt werden, dass zu keinem Zeitpunkt mehrere _frist_-Elemente mit demselben _typ_ gleichzeitig gültig sind. Oder anders ausgedrückt: Zu jedem Zeitpunkt darf es höchstens ein gültiges _frist_-Element für jeden _typ_ geben.

Für eine detaillierte Darstellung dieser Datenstruktur wird hier auf die [XZuFi-Dokumentation](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0) verwiesen.

### Verortung im Datenmodell
Die Fristen werden in einem optionalen Element _modulFrist_ abgelegt, das Teil des Elements _Leistung_ ist.

#### Codebeispiel XZuFi 2.2

:::info

- Das folgende Codebeispiel zeigt nicht alle Features des _modulFrist_-Elements.

:::

```xml
    <xzufi:leistung>

      <xzufi:modulFrist>
        <xzufi:beschreibung languageCode="de">Sechs Wochen vor der vorgesehenen Inbetriebnahme</xzufi:beschreibung>
        <xzufi:beschreibung languageCode="en">Six weeks before the planned commissioning</xzufi:beschreibung>
      </xzufi:modulFrist>

    </xzufi:leistung>
```

### Ermittlungsmöglichkeit PVOG API
Zunächst ermittelt der Onlinedienst die Daten der Leistungsbeschreibung (siehe Abschnitt [Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API](../parametrisierung_anhänge/6_3_ermittlung_daten_der_leistungsbeschreibung_im_pvog.md)).

In der Response findet sich ein Element `modulFrist:frist[]`, das die gesuchten Informationen enthält. Von allen `frist`-Elementen muss der Onlinedienst diejenigen weiter auswerten, deren Element `gueltigkeit[]` ein Element enthält, das zeitlich mit den zeitlichen Daten der Antragsstellung[^1]  übereinstimmt. 

Jedes `frist`-Element enthält entweder ein `typ:code:code`-Element, oder ein `typ:nichtGelisteterWert`-Element, das den _typ_ der _frist_ enthält.

Jedes `frist`-Element enthält weiterhin ein `fristauswahl`-Element, das wiederum ein `fristDauer`-Element oder ein `fristStichtag`-Element enthält.
Ein `fristDauer`-Element besteht aus vier Angaben:
- `dauer`: Ganzzahl-Angabe der minimalen Frist
- `einheit:code`: Code der Einheit der minimalen Frist aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.zeiteinheit`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zeiteinheit)
- `dauerBis`: Ganzzahl-Angabe der maximalen Frist
- `einheitBis:code`: Code der Einheit der maximalen Frist aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.zeiteinheit`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zeiteinheit)

Ein `fristStichtag`-Element besteht aus zwei Elementen:
- `von`: Datum, zu dem die Frist beginnt
- `bis`: Datum, zu dem die Frist endet

Beide Elemente enthalten wiederum drei alternative Angaben `datum`, `monatTag` und `tag`, die das betreffende Datum spezifizieren. Die Alernativn erlauben es, das Datum absolut ("1. April 2025") oder wiederkehrend ("jährlich zum 31. Mai", "monatlich zum 21.") anzugeben. 

Daneben enthält das `modulFrist`-Element weitere Angaben, wie einen [internationalisierten Beschreibungstext](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) und einen [internationalisierten weiterführenden Link](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen). Details können der [API-Spezifikation](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getJzufiLbV2)  entnommen werden.


### Ermittlungsmöglichkeit FIT-Connect Routing API
Der Onlinedienst benutzt den Endpunkt  [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) mit den URL-Parametern zur Identifikation des gesuchten Adressaten, z.B. `leikaKey` = \<LeikaId der Leistung\> und `ars` = \<ARS des gesuchten Gebiets\> und erhält eine JSON-Datenstruktur als Response.

In dieser Datenstruktur enthält das Element `route[]:deadline:description`, das einen [internationalisierten Text](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-zeichenkette) enthält. Der Text beschreibt die Fristen in der angegebenen Sprache. Der Text wird dem Element _beschreibung_ des _modulText_-Elements entnommen und beschreibt die potenziell komplexe Fristsetzung gesamtheitlich.

Die strukturierten Daten aus den _frist_-Element (XZuFi-Typ `xzufi:FristMitTyp`) stehen in der FIT-Connect Routing API Stand Q2/2024 **nicht** zur Verfügung. 

<!--
Frage: Welche Teile der modulFrist-Struktur werden hier zusammengefasst? Nur die Beschreibungen oder auch alle anderen Teile, insbesondere die Frist-Typen, Dauern, Stichtage, …? 

Antwort (Teleport, 5.6.2024): Es wird die Beschreibung  des Frist-Moduls genutzt. Die modulFrist-Struktur wird über das Attribut „RouteProcessingDuration“ zurückgegeben.

Hinweis zur Antwort: das Element `processingDuration` wird aus dem BearbeitungsdauerModul befüllt. Daher stehen die strukturierten Daten aus dem FristModul in der FIT-Connect Routing API nicht zur Verfügung.
-->

### Ermittlungsmöglichkeit DVDV API
(Der Parameter kann nicht über das DVDV-API ermittelt werden)

### Best Practices
Bezüglich `hyperlinkErweitert` sind unbedingt die Hinweise zu [Internationalisierten Datenstrukturen](../parametrisierung_anhänge/6_11_internationalisierung.md#internationalisierte-datenstrukturen) zu beachten!

Die Konfiguration von Fristen ist komplex und es ist ggf. nicht zu erwarten, dass alle Redaktionssysteme alle Features der `xzufi:FristMitTyp`-Datenstruktur unterstützen. Beispielsweise könnte die Angabe eines `nichtGelisteterWert`-Wertes nicht möglich sein. Auch ist ggf. eine komplexe fachliche Fristen-Regelung nicht oder nur mit extremem Aufwand in der Datenstruktur abzubilden: Wenn eine Frist z.B. immer am zweiten Dienstag im Kalendermonat beginnt, so müsste für jeden Monat und auf unbestimmte Zeit in die Zukunft **jeweils** ein _frist_-Element angelegt werden. In einer solche Situation kann nur auf die **Notwendigkeit** einer **strukturierten** Frist-Beschreibung **verzichtet** werden. Statt dessen muss sich die Notwendigkeit der Parametrisierung auf das Element _frist_/_typ_ und _frist_/_beschreibung_ beschränken - sofern eine **Notwendigkeit** für die Angabe von Parameter-Werten überhaupt fachlich gegeben ist. Das _frist_/_beschreibung_-Element enthält dann eine (internationalisierbare) natürlichsprachliche Beschreibung wie "Die Frist beginnt jeweils am zweiten Dienstag im Kalendermonat." Die Steuerung von Geschäftslogik im Onlinedienst bezüglich der Fristen  (z.B. Abweisen von Antragswünschen außerhalb der Antragsfrist) ist dann nicht möglich.

Onlinedienste müssen so implementiert werden, dass sie gegenüber Fehlern in den Parameter-Werten tolerant sind und z.B. bei mehreren _frist_-Elementen vom gleichen _typ_ und mit überlappender _gueltigkeit_ nur eine Warnung anzeigen, dass die Anzeige der Frist für diesen Typ aktuell nicht möglich sei. Eine Steuerung der Geschäftslogik, z.B. das Ablehnen einer Antragstellung bei fehlender oder fehlerhafter Frist-Angabe in den Parametern, ist aus diesen Gründen riskant und muss sorgfältig fachlich geprüft werden. In jedem Fall ist eine solche Logik sorgfältig in der Dokumentation des Onlinedienstes zu dokumentieren.

[^1]: _Zeitpunkt der Antragstellung_ oder _Zeitpunkt, für den der Antrag gestellt wird_, etc. Diese Regeln sind fachlich jeweils für die Leistung zu klären