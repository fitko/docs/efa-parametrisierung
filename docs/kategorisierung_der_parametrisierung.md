---
id: kategorisierung_der_parametrisierung
title: "Kategorisierung der Parametrisierung"
---


:::info[Version]

1.0.0

:::

# Kategorisierung der Parametrisierung

EfA-fähige Onlinedienste müssen in vielen Aspekten auf die Spezifika des Bundeslandes oder sogar der Kommune angepasst werden. Die Fülle dieser Anpassungsmöglichkeiten kann dabei in zwei Dimensionen kategorisiert werden:
- **Fachliche Kategorien:** Anpassungsmöglichkeiten beziehen sich auf verschiedene Phasen des Antragsprozesses oder auf spezifische Themen wie z.B. die Entrichtung von Gebühren. Diese Kategorisierung ist vor allem für das Anforderungsmanagement relevant, weil die Anforderungserhebung und die Menge der Stakeholder in der Regel nach dieser Kategorisierung strukturiert werden können. Auch die Pflegeprozesse in den Ländern für diese Angaben werden sich vermutlich von Kategorie zu Kategorie unterscheiden bzw. können entlang der Kategorien strukturiert definiert werden.
- **Technische Kategorien:** Aus technischen und aus historischen Gründen werden die Parameter-Werte in verschiedenen Systemen verwaltet und müssen zur Laufzeit über verschiedene Wege aus diesen ausgelesen werden. Diese Kategorisierung ist vor allem für die technische Umsetzung der Onlinedienste relevant, definiert aber auch Rahmenbedingungen für einzelne Parameter-Kategorien, die im Rahmen des Anforderungsmanagements und bei der Gestaltung von Pflegeprozessen beachtet werden müssen.

Bevor daher die Standards und Best Practices in diesem Dokument für die einzelnen Parameter aufgezählt werden, sollen die beiden Kategorien-Systeme beschrieben werden, nach denen die Parameter sortiert werden können. Dies soll zur Orientierung dienen und wird auch dazu beitragen, die Standardisierungs-Entscheidungen nachvollziehbar zu machen.
Es soll bereits jetzt darauf hingewiesen werden, dass die Parametrisierbarkeit der angesprochenen Funktionen und Eigenschaften nur in den wenigsten Fälle eine Anforderung an die EfA-Fähigkeit der Onlinedienste sind. So muss ein solcher Onlinedienst zwar laut [EfA-Mindestanforderung](https://www.digitale-verwaltung.de/SharedDocs/downloads/Webs/DV/DE/EfA/efa-mindestanforderungen.pdf) OD3 die individuell zuständige Behörde mit den Kontaktdaten anzeigen, es besteht aber keine Verpflichtung, z.B. eine zuständige Kontaktperson zu nennen oder nach Abschluss des Antragsprozesses den Benutzer auf eine konfigurierbare Web-Seite weiterzuleiten. Nur wenn eine solche Funktion oder Eigenschaft im Onlinedienst umgesetzt wird, dann solle sie der hier dokumentierten Standardisierung und den Best Practices folgen.

## Fachliche Kategorien
Die standardisierten Parametrisierungsmöglichkeiten sollen wie folgt fachlich kategorisiert werden:

### Stammdaten der Leistung
Die Stammdaten der Leistung enthalten Informationen, die dem Antragsteller im Zuge der Nutzung des Onlinedienstes präsentiert werden können. Sie sind aber primär Teil des FIM-Prozesses und sollten bereits erfasst und im PVOG zugreifbar sein, lange bevor ein Onlinedienst die digitale Beantragung ermöglicht.

Beispiele sind:
- Erforderliche Unterlagen
- Rechtsgrundlagen
- Bearbeitungsdauer
- Fristen

### Stammdaten der zuständigen Organisationseinheit und ihrer Formulare 
Auch die Stammdaten der zuständigen Organisationseinheit enthalten Informationen, die dem Antragsteller im Zuge der Nutzung des Onlinedienstes präsentiert werden können und sind ebenfalls primär Teil des FIM-Prozesses. Auch sie sollten bereits im PVOG zugreifbar und in den Portalen präsentiert werden, um über die Behörde an sich zu informieren. Anders als bei den Stammdaten der Leistung, sind diese Daten aber ggf. spezifisch für jede Kommune, so dass der Pflegeprozess ggf. nicht mehr zentralisiert, sondern dezentral organisiert werden muss.
Auch muss bei der Anforderungserhebung für den Onlinedienst der Fall berücksichtigt werden, dass einzelne Angaben für einzelne Organisationseinheiten ggf. nicht vorliegen.

Beispiele sind Angaben wie
- Namen der Organisationseinheit
- Anschriften der Organisationseinheit
- Zuständige Kontaktpersonen der Organisationseinheit

In XZuFi sind die Formulare den Organisationseinheiten zugeordnet. Daher werden die Stammdaten zu den Formularen in dieser Kategorie mit aufgeführt.

### Onlinedienst-bezogene Standard-Parameter
Die Onlinedienst-bezogenen Standard-Parameter steuern querschnittliche Aspekte des Antragsprozesses von der Authentifizierung bis zum Abschluss nach dem Versand der Antragsdaten, die bei vielen Onlinediensten parametrisierbar sind, und keiner abgegrenzten, spezifischen Funktion wie z.B. dem Bezahldienst zugeordnet werden können.

Beispiele sind:
- Link zur regionalen Datenschutzerklärung
- Onlinedienst Logo
- Vertrauensniveau 
- Onlinedienst Durchführungsprachen 
- Callback-URL nach Abschluss des Antrags

### Onlinedienst-bezogene Individual-Parameter
Die Onlinedienst-bezogenen Individual-Parameter steuern Aspekte des Antragsprozesses, die in dieser Form nur bei dem individuellen, aktuell betrachteten Onlinedienst parametrisierbar sind. Diese Parameter lassen sich naturgemäß nicht einzeln standardisieren, aber ihre technische Unterstützung und ihre Berücksichtigung z.B. in Pflegeprozessen kann einheitlich über die Onlinedienste geregelt werden.

Beispiele sind Parameter zur landes-spezifischen Formulierung von Antragsfragen, zur Steuerung des Dialogflusses durch Ein- und Ausblenden ganzer Fragen, landesspezifische Vorgaben von Antwortmöglichkeiten auf spezifische Fragen, etc. So muss der Onlinedienst zur _Anmeldung an einer weiterführende Schule_ die auswählbaren Schularten konfigurierbar machen: In Hamburg muss "Stadtteilschule", aber nicht "Hauptschule" auswählbar sein, in Hessen umgekehrt. Der Parameter, der diese Auswahlmöglichkeit steuert, ist ein Individual-Parameter, der nur für einen (oder sehr wenige) Onlinedienste relevant ist und daher - bis auf den zu verwendenden Basis-Mechanismus - nicht weiter standardisiert wird.

In diese Kategorie können auch technische Parameter fallen, wenn der Onlinedienst technische Dienste nutzt, die nicht weiter standardisiert sind. Für diese Parameter wird ein gesonderter Pflegeprozess vorzusehen sein, da die Personen, die diese technischen Informationen beschaffen und einpflegen, in der Regel nicht dieselben Personen sind, die z.B. Angaben über die Organisationseinheit einpflegen.

### Bezahldienst-bezogene Parameter
In dieser Kategorie werden alle Parameter zusammengefasst, die unmittelbar mit den Bezahl-Prozessen in Verbindung stehen.
Falls ein Onlinedienst ohne Kosten für den Antragsteller angeboten wird, können alle diese Parameter im Zuge der Onlinedienst-Erstellung und des Roll-Outs/Roll-Ins gefahrlos aus der Betrachtung ausgeschlossen werden. Wenn hingegen Kosten und Bezahlprozesse vorgesehen sind, so muss im Rahmen der Anforderungserhebung, der Umsetzung des Onlinedienstes, der Definition der Pflegeprozesse für die Parameter-Werte sowie im Rahmen der Vorbereitung des Roll-Out/Roll-In jeder dieser Parameter betrachtet werden, ob und wie er zu berücksichtigen ist.

Dabei gibt es sowohl rein fachliche als auch Bezahldienst-technische Parameter.

Beispiele sind:
- Zahlungsweisen
- Referenzen auf den Bezahldienst
- Versionen der Schnittstelle des Bezahldienstes

Auch die rein technischen Parameter sollen in die fachliche Kategorie Bezahldienst-bezogener Parameter eingeordnet werden, weil auch sie Rahmenbedingungen für die Nutzbarkeit von Bezahldiensten setzen und z.B. bei der Definition von  Pflegeprozessen berücksichtigt werden müssen – oder eben ausgeklammert werden können, wenn bei der Nutzung des Onlinedienste keine Gebühren erhoben werden. 
Der Bezahlprozess soll immer nach dem standardisierten Verfahren [_XBezahldienste_](https://docs.fitko.de/xbezahldienste/APIDoku/API-Parametrisierung) abgewickelt werden. Die hier dokumentierten Parametrisierungsmöglichkeiten beziehen sich direkt auf die Parameter dieses Verfahrens.

### Parameter mit Bezug zum Antrags-Versand
Die fachliche Kategorie der Parameter mit Bezug zum Antrags-Versand enthält überwiegend Parameter mit technischem Charakter, die aber dennoch auch fachlich betrachtet werden müssen. Sie steuern alle Aspekte der Übermittlung der Antragsdaten an die zuständige Organisationseinheit.

Beispiele sind die Parameter, die die zu nutzenden Zustellungskanäle zusammen mit ihren technischen Adress-Informationen beschreiben
- Zustellungskanal (OSCI/XTA) 
- Zustellungskanal (DestinationSignature)

## Technische Kategorien
Da die fachlichen Zusammenhänge auch eine Rolle bei der Gestaltung der technischen Infrastruktur spielen, sind die fachlichen und technischen Kategorien nicht orthogonal zu einander. Sie verlaufen aber auch nicht parallel, weil aus historischen und praktischen Gründen die Parameter einzelner fachlicher Kategorien technisch unterschiedlich behandelt werden.
Die standardisierten Parametrisierungsmöglichkeiten sollen wie folgt technisch kategorisiert werden:

### XZuFi-basierte Parameter ohne direkten Onlinedienst-bezug
Mit dieser Kategorie werden alle Parameter zusammengefasst, die im PVOG ohne direkten Bezug zum Onlinedienst verwaltet werden. Sie können prinzipiell über die Redaktionssysteme gepflegt werden, sofern die Redaktionssysteme den XZuFi-Standard[^1] ausreichend unterstützen.

Diese Parameter sind die Parameter aus den beiden fachlichen Kategorien zur Leistung und zu den zuständigen Organisationseinheiten und ihren Formularen.

:::info

Stand Q2/2024 gehören auch einige Parameter mit Bezug zum Antragsversand dazu, falls der Versand über den Kanal EGVP erfolgt, weil diese Daten in XZuFi 2.2 nur bezüglich der Organisationseinheit und nicht mit Bezug auf die Leistung oder den Onlinedienst verwaltet werden können. Dies wird sich mit XZuFi 2.3 ändern. Hier muss daher auch fachlich berücksichtigt werden, dass eine Organisationseinheit keine getrennten EGVP-Postfächer für getrennte Leistungen einrichten kann, falls sie für mehrere Leistungen zuständig ist, deren Onlinedienste die Antragsdaten per EGVP verschicken. 

Aufgrund dieser Probleme bleibt der Umgang mit Adressinformationen zum EGVP-Versand zunächst außerhalb der Standardisierung. Es wird aber empfohlen, bei Dedarf eine Lösung im Onlinedienst zu implementieren, die analog zur standardisierten Lösung für OSCI/XTA aufgebaut ist.

:::

### XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug
Die Parameter dieser Kategorie werden im PVOG mit direktem Bezug zum Onlinedienst verwaltet und beziehen sich auf Funktionen, die in vielen Onlinediensten gleich verwendet werden. 

Weil es sich um Parameter handelt, die in vielen Onlinediensten gleich verwendet werden, ist es sinnvoll, dass Hersteller von Redaktionssystemen eine benutzerfreundliche Unterstützung für die Pflege dieser Parameter implementieren.

Dabei muss fachlich bedacht werden, dass ein implementierter Onlinedienst, d.h. ein Onlinedienst IT-System, durch _mehrere_ Datenobjekte _Onlinedienst_ im PVOG repräsentiert werden kann. Dies ist notwendig, wenn Parameter-Werte aus dieser Kategorie für verschiedene Gebiete unterschiedlich festgelegt werden müssen. Für jedes Gebiet wird dann im PVOG ein _Onlinedienst_ Datenobjekt mit eigenem Datensatz von Parameter-Werten aufgebaut, die alle mit dem gemeinsam genutzten Onlinedienst IT-System korrespondieren. In der Regel ist dies bereits auf der Ebene der Bundesländer notwendig, d.h. wenn ein EfA-fähiger Onlinedienst von z.B. drei Bundesländern genutzt wird, gibt es im PVOG (mindestens) drei _Onlinedienst_ Datenobjekte, die jeweils den Parameter-Wertesatz „ihres“ Bundeslandes tragen.

Dieser Zusammenhang hat zur Konsequenz, dass für jede Region, bei der auch nur ein Parameter dieser technischen Kategorie einen abweichenden Wert erhalten muss, ein gesondertes _Onlinedienst_ Datenobjekte im PVOG angelegt werden muss. Dieses gesonderte Datenobjekt muss vollständig konfiguriert werden.

Aufgrund der Notwendigkeit, bei einzelnen abweichenden Parameter-Werten, ganze _Onlinedienst_ Datenobjekte im PVOG anlegen zu müssen, sollten Hersteller von Redaktionssystemen eine Vererbungs-Logik implementieren, die die Werte der nicht-abweichenden Parameter _zentral_ für eine übergeordnete Region pflegbar machen. Beispielsweise sollte eine solche Logik es erlauben, den Link zur Barrierefreiheitserklärung landesweit an einer einzigen Stelle im Redaktionssystem zu pflegen, während der Link zum regionalen Impressum für jede Kommune individuell überschrieben werden kann. Da das PVOG Stand Q2/2024 eine solche Vererbungslogik nicht unterstützt, muss dies bereits im Redaktionssystem umgesetzt werden. Ohne diese Vererbungs-Logik muss im Beispiel der Link zur Barrierefreiheitserklärung für jede Kommune im Redaktionssystem auf denselben, landesweit gültigen Wert konfiguriert werden. Ändert sich dieser landesweit gültige Link, so muss die Parametrisierung ohne eine Vererbungs-Logik für jede Kommune auf dieselbe Weise geändert werden.

Zu dieser Kategorie gehören z.B.
- Die Parameter der fachlichen Kategorie der Onlinedienst-bezogenen Standard-Parameter
- Einige, aber nicht alle Parameter der fachlichen Kategorie der Bezahldienst-bezogenen Parameter
- Der Parameter Übermittlung von Kontaktdaten bei quittierter Antragszustellung aus der fachlichen Kategorie der Parameter mit Bezug zum Antragsversand

Die Kategorie der XZuFi-basierten Standard-Parameter mit direktem Onlinedienst-Bezug kann technisch noch weiter in zwei Unterkategorien unterteilt werden:
- _Onlinedienst_-Elemente, die explizit in XZuFi definiert sind. So definiert XZuFi z.B. explizit ein Element _bezeichnung_ am _Onlinedienst_-Element, das den Parameter [Onlinedienst-Bezeichnung](./parameter_katalog/17_onlinedienst_bezeichnung.md) implementiert.
- _Onlinedienst_-Parameter, die über die generischen _parameter_-Elemente als Namens-Schlüssel/Wert-Paare implementiert sind. Deren Namens-Schlüssel sind in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) definiert. So wird z.B. standardisiert, dass ein _parameter_-Elemente mit dem Namens-Schlüssel `efa.uebermittlungKontaktdaten` den Parameter [Anzeige von Kontaktdaten nach Antragsversand](./parameter_katalog/26_anzeige_von_kontaktdaten_nach_antragsversand.md) implementiert.

:::info

Die Unterscheidung dieser beiden Unterkategorien wird relevant, wenn die Infrastruktur von XZuFi 2.2 nach XZuFi 2.3 migriert wird. Während sich die Verortung der _Onlinedienst_-Elemente, die explizit in XZuFi definiert sind, nicht ändern wird, werden die standardisierten _Onlinedienst_-Parameter nicht mehr im Element _parameter_, sondern dann im Element _parameterEfA_ abgelegt. Dies wird geschehen, um sie von den [Individual-Parametern](#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) zu separieren.

:::


### XZuFi-basierte Individual-Parameter mit direktem Onlinedienst-Bezug
Die Parameter dieser Kategorie werden im PVOG mit direktem Bezug zum Onlinedienst verwaltet und beziehen sich auf Funktionen, die in dieser Form nur bei dem individuellen, aktuell betrachteten Onlinedienst parametrisierbar sind.

Auch für diese Parameter gilt, dass regional abweichende Parameter-Werte zur Konsequenz haben, dass für die Region ein separates _Onlinedienst_ Datenobjekte im PVOG angelegt und vollständig parametrisiert werden muss. Daher sollten die Hersteller von Redaktionssystemen auch für diese Parameter den in Abschnitt [XZuFi-basierte Standard-Parameter mit direkten Onlinedienst-bezug](#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) skizzierten Vererbungs-Mechanismus implementieren.

:::info

Mit einer zukünftigen Migration der Infrastruktur von XZuFi 2.2 nach XZuFi 2.3 werden die Individual-Parameter nicht mehr im Element _parameter_, sondern dann im Element _parameterIndividuell_ abgelegt. Dies wird geschehen, um sie von den [Standard-Parametern](#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug) zu separieren.

:::

Stand Q2/2024 unterscheidet sich die technische Behandlung dieser Parameter im PVOG nicht von der der [XZuFi-basierten Standard-Parameter mit direktem Onlinedienst-Bezug](#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug), die über die generischen _parameter_-Elemente als Namens-Schlüssel/Wert-Paare implementiert sind. Für diese Parameter ist aktuell aber keine benutzerfreundliche Unterstützung in den Redaktionssystemen implementierbar, weil es keine Metadaten (Parametername, Bedeutung, Datentyp, …) zu ihnen gibt, die von einem Redaktionssystem ausgewertet werden könnten. Vielmehr sind die Informationen nur in den Anbindungsleitfäden der jeweiligen Onlinedienste menschenlesbar dokumentiert.

In Zukunft könnte eine noch aufzubauende Komponente _Parameter Registry_ innerhalb des PVOG eine Metadaten-gestützte, benutzerfreundliche Pflege auch für solche Parameter bieten. Es ist sogar denkbar, dass diese Komponente die Metadaten über eine API den Redaktionssystemen zur Verfügung stellt, so dass dann auch die Redaktionssysteme die Pflege benutzerfreundlich unterstützen können.

:::info

Die FIT-Connect Routing API setzt Stand Q2/2024 voraus, dass die Namens-Schlüssel von Individual-Parametern mit dem Präfix `efa.custom.` beginnen. Nur solche Individual-Parameter werden in der Response des Endpunktes [`/routes`](https://docs.fitko.de/fit-connect/docs/apis/routing-api/#get-/routes) im Element `customParameters` ausgegeben. Die Ausgabe erfolgt dort auch mit dem Präfix des Namens-Schlüssels, d.h. als `"efa.custom.configSchluessel": "3e56cbf8"`, _nicht_ als `"configSchluessel": "3e56cbf8"`.

Die PVOG-API erlaubt über den Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) die Ermittlung aller dort hinterlegten Namens-Schlüssel/Wert-Paare im `parameters`-Element der Response. Dort finden sich dann aber auch die [Standard-Parameter](#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug), die in XZuFi 2.2. in den _parameter_-Elementen abgelegt werden. 

:::

### DVDV-basierte Parameter
Neben dem PVOG, das seine Daten aus den Redaktionssystemen bezieht und deren Datenhaltung sich eng am Datenmodell von XZuFi orientiert, existiert auch noch das DVDV als Datenquelle für Parameter.
Die Datenpflege im DVDV erfolgt grundsätzlich anders als im PVOG: Hier muss ein DVDV-Eintragungskonzept erarbeitet und genehmigt werden. Die eigentliche Datenpflege erfolgt nicht über Redaktionssysteme der Länder oder Kommunen, sondern zentral durch speziell berechtigte Datenpfleger. Dies ist beim Design von Pflegeprozessen zu berücksichtigen.

Zu den DVDV-basierten Parametern gehören
- Einige aber nicht alle Parameter aus der fachlichen Kategorie der Bezahldienst-bezogenen Parameter
- Zustellungskanal (OSCI/XTA)

## Methoden der Parameter-Wert Ermittlung
Zur Laufzeit muss jeder Onlinedienst die konkreten Werte der von ihm benötigten Parameter bestimmen. Er tut dies, indem er verschiedene APIs bedient, die von der FITKO dafür zur Verfügung gestellt werden. Ggf. bedient der Onlinedienst diese APIs nicht direkt, sondern durch Nutzung querschnittlicher Dienste, die seine Onlinedienst Plattform[^2] ihm dafür zur Verfügung stellt. Dies kann von einfachen Proxy-Diensten einer Integrationsplattform bis hin zu eigenständigen Parametrisierungskomponenten wie dem Jesaja der OSI-Plattform reichen, die selbst auch wieder auf die FITKO-APIs zugreifen, um einen Teil ihrer Funktionalität zu erbringen.

FITKO bietet dazu drei APIs, über die jeweils ein Teil der Parameter-Werte erfragt werden kann:
- Das [PVOG Suchdienst API](https://pvog.fitko.de/suchdienst/api/swagger-ui/index.html#/) - im Parametrisierungsleitfaden häufig einfach als "PVOG API" bezeichnet, Stand Q2/2024 in der Version 9.9.1 produktiv und in der Version 9.11.3 in der Vorbereitung ([Demo-Umgebung](https://public.demo.pvog.cloud-bdc.dataport.de/suchdienst/api/swagger-ui/index.html#/), [Dokumentation](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/))
- Das [API des FIT-Connect Routing-Dienstes](https://docs.fitko.de/fit-connect/docs/apis/routing-api/), Stand Q2/2024 in der Version 1.1.0 produktiv
- Das [API des DVDV](https://docs.fitko.de/dvdv/spec/) Stand Q2/2024 in der Version 2.9.0 produktiv

Die Verteilung der Parameter auf die drei APIs ist nicht überschneidungsfrei. Daher lässt sich hier keine sinnvolle dritte Kategorisierung definieren. Auch müssen die APIs von Parameter zu Parameter sehr unterschiedlich genutzt werden, so dass dies bei jedem einzelnen Parameter separat dokumentiert werden muss.


[^1]: Stand Q2/2024 ist dabei XZuFi 2.2 maßgeblich
[^2]: Z.B. die [Plattform _OSI_ von Dataport](https://www.dataport.de/services-produkte/it-produkte/produkt/osi/)
