---
id: parametrisierungsleitfaden
title: "Parametrisierungsleitfaden"
---


:::info[Version]

1.0.0

:::

# Parametrisierungsleitfaden

## Über den Parametrisierungsleitfaden

### Ziel und Adressaten des Parametrisierungsleitfadens
Stand Q2/2024 existieren bereits eine Reihe von EfA-fähigen Onlinediensten auf verschiedenen Plattformen, die teilweise auch bereits von verschiedenen Bundesländern nachgenutzt werden oder kurz davorstehen. Aufgrund bisher ausgebliebener Standardisierung setzten diese EfA-Dienste das Thema Parametrisierung z.T. sehr unterschiedlich um. Dies führt dazu, dass …
- … der Prozess der Informationsbeschaffung und der Pflege der Parameter-Werte für jeden Onlinedienst gesondert und spezifisch dokumentiert und befolgt werden muss. Auch können die Redaktionssysteme, über die die Parametrisierungen technisch eingepflegt werden, die Redakteure nicht durch angepasste Benutzerführung unterstützen, weil Onlinedienst-spezifische Anpassungen der Redaktionssysteme nicht wirtschaftlich umsetzbar sind. All dies trägt dazu bei, dass Fehler auftreten, die Datenqualität sinkt oder sogar die Onlinedienste nicht korrekt nutzbar sind.
- … der Prozess der Inbetriebnahme und später der Weiterentwicklung der Onlinedienste erschwert wird, weil jedes Vorhaben die Besonderheiten der Parametrisierung bei dem betrachteten Onlinedienst mitberücksichtigen muss.

Ziel des Parametrisierungsleitfadens ist es daher, Standards und Best Practices zu definieren, wie Onlinedienste die Aufgabe der EfA-Parametrisierung umsetzen sollen.
Dabei ist offensichtlich, dass die bereits umgesetzten EfA-fähigen Onlinedienste, die von diesen Vorgaben abweichen, zeitnah nicht oder nicht vollumfänglich angepasst werden können. Die gesamte Infrastruktur[^1] der Onlinedienste muss daher auch in der näheren Zukunft vorsehen, dass auch solche Onlinedienste nachgenutzt werden können, die von den Standards abweichen, sofern die Nachnutzung zum Stand Q2/2024 bereits möglich ist (Bestandsschutz). Die oben genannten Probleme bleiben für diese Onlinedienste dann bestehen.

Der Parametrisierungsleitfaden hat den Anspruch, die Möglichkeiten der Parametrisierung zu beschreiben, die aktuell, d.h. zum Stand Q2/2024 zur Verfügung stehen. Der Parametrisierungsleitfaden kann also sofort 1:1 umgesetzt werden, vorausgesetzt dass Onlinedienst, Redaktionssystem und Parameter-Pflege-Prozesse dies erlauben. Falls weitere Einschränkungen bestehen, so sind diese jeweils bei den einzelnen Parametern angegeben.

Der Parametrisierungsleitfaden gibt darüber hinaus an einigen Stellen Ausblicke auf die zukünftigen Möglichkeiten, die durch XZuFi 2.3 - nach dessen vollständiger Umsetzung in allen betroffenen Systemen - und durch die (mögliche) Ergänzung des PVOG durch eine Komponente _Parameter Registry_ entstehen. Diese Informationen sind naturgemäß für die Entwicklung von Onlinediensten nur bezüglich der langfristigen Planung relevant.

Adressaten des Parametrisierungsleitfadens sind vor allem zwei Rollen in Projekten zur Entwicklung und Wartung EfA-fähiger Onlinedienste:
- Anforderungsmanager können sich am Standard-Parameterkatalog orientieren und die Anforderungen der (Nach-)nutzenden Länder damit abgleichen. So können sie gezielt nach typischen Bedarfen fragen, die bei anderen Onlinediensten parametrisierbar umgesetzt worden sind, z.B. die Möglichkeit, dem Antragsteller eine – parametrisierbare – Bearbeitungsdauer anzeigen zu können. Sie können weiterhin unscharfe Anforderungen durch Abgleich mit Best Practices und technischen Möglichkeiten konkretisieren, z.B. die Möglichkeit, den Antragsteller nach Abschluss des Antragsprozesses auf eine – parametrisierbare – Webseite weiterleiten zu können. 
- Architekten von Onlinediensten und Onlinedienst Plattformen können die Mechanismen einheitlich und sicher umsetzen, mit denen die Parameter-Werte zur Laufzeit ermittelt und in den Onlinediensten verarbeitet werden.

Darüber hinaus kann der Parametrisierungsleitfaden auch von Herstellern von Redaktionssystemen genutzt werden, um ihre Produkte benutzerfreundlicher und damit prozesssicherer zu gestalten: Die Redaktionssysteme sollten die hier beschriebenen Standards und Best Practices durch spezialisierte Benutzeroberflächen mit geeigneter Benutzerführung unterstützen. Dabei ist auf den oben erwähnten Bestandsschutz zu achten, so dass eine Datenpflege auch für nicht-Standard-konforme Onlinedienste weiterhin möglich[^2] ist. 

### Technische Umsetzung zur Laufzeit: Nutzung von FITKO APIs

Die Parametrisierung erfolgt zur Laufzeit der Onlinedienste über APIs der FITKO-Systeme. D.h. wenn ein Antragsteller den Onlinedienst nutzt, befragt der Onlinedienst die FITKO-Systeme nach konkreten Parameter-Werten, die den Antragstellungsprozess steuern. Dazu bieten die Systeme der FITKO verschiedene APIs, deren Benutzung in diesem Parametrisierungsleitfaden für jeden Parameter grob skizziert wird. Es ist dabei das Ziel dieses Parametrisierungsleitfadens, die Nutzbarkeit der verschiedenen APIs für jeden Parameter soweit zu klären, dass ein Architekt eine fundierte Design-Entscheidung treffen kann, wenn die Anforderung nach einem entsprechenden parametrisierbaren Feature besteht. Die in diesem Parametrisierungsleitfaden angebotenen Informationen haben nicht den Anspruch, als ausschließliche Quelle zur Implementierung der Onlinedienste nutzbar zu sein. Vielmehr müssen weitere Details aus den jeweiligen [API-Dokumentationen](https://docs.fitko.de/resources/) entnommen werden.

Stand Q2/2024 können Onlinedienste bzw. die Software-Plattformen, auf denen sie entwickelt werden, drei APIs nutzen, die jeweils unterschiedliche Charakteristiken haben. Alle diese APIs sind historisch gewachsen und weisen architektonische Kompromisse auf, die durch eine dedizierte, wiederverwendbare Zugriffsschicht innerhalb der Onlinedienst-Architektur bzw. innerhalb der Architektur der Plattform ausgeglichen werden sollten. So können die Onlinedienste und Plattformen leichter angepasst werden, wenn in Zukunft verbesserte APIs angeboten werden.

Hersteller von Onlinediensten und Onlinedienst Plattformen sind verpflichtet, die korrekte Verwendung der APIs sicherzustellen. Dazu gehören eine sorgfältige Vorbereitung durch adäquates Anforderungsmanagement, ein tragfähiges Design der oben genannten Zugriffsschicht, ausführliche Tests, ein Management der Software-Wartung während der Lebenszeit der Onlinedienste und nicht zuletzt eine intensive Zusammenarbeit mit den Ansprechpartnern der FITKO, falls die vorhandene Dokumentation uneindeutig ist - oder Fehler darin gefunden werden.

#### PVOG Suchdienst-API

Die Mehrzahl der standardisierten Parameter-Werte können mit Hilfe der [PVOG Suchdienst-API](https://docs.fitko.de/resources/pvog-suchdienst-api/) ermittelt werden. Diese API wurde ursprünglich erstellt, um interaktiven, Portal-artigen Client-Anwendungen Zugriff auf alle Informationen des PVOG zu geben. Bestes Beispiel ist der [PVOG Such-Client](https://servicesuche.bund.de/) der FITKO. Die API bietet verschiedene REST-WebService Endpunkte, um die Top-Level-Elemente der XZuFi-Datenwelt in unterschiedlichen Detailstufen und Ausschnitten zu ermitteln. Weil die API nicht auf die Bedarfe von Onlinediensten ausgerichtet ist, sind für die Onlinedienst Use Cases in der Regel mehrere Abfragen und eine nachgelagerte Filterung der Antworten erforderlich. Die PVOG Suchdienst-API ist öffentlich und kann ohne Authentisierung benutzt werden.

#### DVDV API

Das [Deutsche Verwaltungsdiensteverzeichnis DVDV](https://docs.fitko.de/resources/dvdv/) ist spezialisiert auf die Beauskunftung von technischen Adressinformationen und anderen, eher technischen "Dienst-Elementen", um eine rechtssichere elektronische Kommunikation von und mit Behörden zu ermöglichen. Für die Anbindung an das DVDV werden kostenfrei nutzbare Software-Bibliotheken in Java und .Net bereitgestellt, so dass eine genaue Betrachtung der APIs auf Nachrichten-Ebene nicht notwendig ist. Die Strukturen der Bibliotheken entsprechen aber 1:1 den Strukturen auf der Nachrichten-Ebene, so dass die Dokumentation mit wenig Übertragungsleistung auf beide Ebenen anwendbar ist.

Zugriff auf das DVDV ist sowohl bei der Datenpflege als auch bei der Abfrage streng reglementiert, so dass für eine Abfrage eine (einmalige) Registrierung der Client-Applikation bei der zuständigen _pflegenden Stelle_ inklusive eines genehmigungspflichtigen Eintragungskonzeptes und eine Zertifikat-basierte Authentisierung zur Laufzeit notwendig ist. Bei der Registrierung ist darauf zu achten, dass die notwendigen Berechtigungen beantragt werden, um alle relevanten Informationen aus dem DVDV auch auslesen zu dürfen.

Im Kontext der Onlinedienst Parametrisierung liefert das DVDV technische Adressierungsinformation für Behörden bzw. Fachverfahren, die Antragsdaten per OSCI/XTA erhalten. Das DVDV kann auch genutzt werden, um Adressierungs- und Stammdaten-Informationen für Bezahldienste zu ermitteln, die per [_XBezahldienste_](https://docs.fitko.de/xbezahldienste/APIDoku/API-Parametrisierung) angesprochen werden. 

#### FIT-Connect Routing API

Die [FIT-Connect Routing API](https://docs.fitko.de/resources/routing-api/) wurde ursprünglich geschaffen, um Onlinediensten die notwendigen technischen Adressierungsinformationen zugänglich zu machen, damit diese im Anschluss den [FIT-Connect](https://docs.fitko.de/fit-connect/docs) Zustelldienst über die [FIT-Connect Submission API](https://docs.fitko.de/resources/submission-api/) nutzen können. Damit können Antragsdaten ebenfalls an Fachverfahren übermittelt werden, sofern diese einen FIT-Connect Zustellpunkt angebunden haben. 

Die Routing API wurde im Zuge des Projekts "MVP Parametrisierung" um zusätzliche Elemente erweitert, so dass sie nun nicht nur für die Ermittlung von technischen Adressierungsinformationen sondern auch für die Ermittlung verschiedenster Onlinedienst Parameter genutzt werden kann. Aufgrund ihrer Historie hat aber auch die FIT-Connet Routing API Limitationen, die vom Onlinedienst bzw. der Onlinedienst Plattform ausgeglichen werden müssen.

:::info

#### Nutzbarkeit der FIT-Connect Routing API
Die FIT-Connect Routing API ist gedacht als one-stop-shop API, mit der ein Onlinedienst in einem Zug alle relevanten Informationen zur Parametrisierung des Antragsprozesses beschaffen kann: Stammdaten zu Leistung und zuständiger Organisationseinheit, standardisierte und individuelle Paratemeter-Werte für Onlinedienst-Funktionen sowie Adressinformationen für den digitalen Antragsversand.

Die FIT-Connect Routing API kann Stand Q2/2024 aber nur unter bestimmten Voraussetzungen genutzt werden
- Die Suchparameter _LeikaKey_ und _ags/ars/areaId_ führen zu einer zuständigen Organisationseinheit, die für die Leistung in dem Gebiet **einen [FIT-Connect Zustellpunkt](https://docs.fitko.de/fit-connect/docs/receiving/destination/)** besitzt.
    Insbesondere kann die FIT-Connect Routing API _nicht_ zur Ermittlung von Parameter-Werten genutzt werden, wenn für die Suchparameter ausschließlich solche Organisationseinheiten zuständig sind, die nur über OSCI/XTA erreichbar sind. In diesem Fall gibt die API keine Informationen zurück.
- Der Onlinedienst benötigt **nur solche Parameter, die von der API auch unterstützt werden.**
    Die FIT-Connect Routing API ist zum Stand Q2/2024 nicht darauf ausgelegt, alle Parameter-Werte des Standard-Parameterkatalogs vollumfänglich zu liefern. Für manche Parameter, werden nur Ausschnitte der im PVOG vorhandenen Informationen ausgeliefert (Beispiel: [Fristen](./parameter_katalog/38_fristen.md)), andere Parameter des Katalogs werden bisher gar nicht unterstützt (Beispiel: [Kennzeichen Schriftformerfordernis](./parameter_katalog/32_kennzeichen_schriftformerfordernis.md)). 

Ein Architekt, der die Parametrisierung eines EfA-fähigen Onlinedienstes plant, muss daher sorgfältig prüfen, ob die Anforderungen an die Parametrisierbarkeit des Onlinedienstes mit den aktuell verfügbaren Informationen, die die FIT-Connect Routing API bietet, umgesetzt werden können. Ist dies aktuell nicht der Fall, muss entweder die FIT-Connect Routing API durch die FITKO funktional erweitert werden oder der Onlinedienst muss - alternativ oder zusätzlich - die [PVOG Suchdienst-API](https://docs.fitko.de/resources/pvog-suchdienst-api/) und/oder die [DVDV-API](./parametrisierung_anhänge/6_12_best_practice_nutzung_dvdv_bibliothek.md) verwenden. Dies ist in der Regel mit höherem Entwicklungsaufwand verbunden, da diese APIs komplexere Interaktionsmuster verlangen.

:::

## Typografische Konventionen
Im Parametrisierungsleitfaden wird versucht, anhand von typografischen Konventionen die verwendeten Bezeichnungen einem Kontext zuzuordnen und damit ihre Bedeutung klarer hervorzuheben. Dies ist vermutlich nicht überall gelungen, so dass diese zusätzlichen Informationen im Text mit etwas Nachsicht zu interpretieren sind.

### _Kursive Schrift_: XZuFi-Datenstrukturen und PVOG-Datenmodell
In der Regel werden Namen von Datenstrukturen bzw. Elementen aus dem Kontext XZuFi und PVOG-Datenmodell in _kursiver Schrift_ gesetzt. Daher bezeichnet z.B. _Onlinedienst_ entweder das XML-Element gleichen Namens (mit allen Unterelementen), das in einer XZuFi-Nachricht von einem Redaktionssystem an das PVOG übermittelt wird. _Onlinedienst_ bezeichnet auch das daraus resultierende Datenobjekt, das in der PVOG-Datenbank abgelegt wurde. Da das PVOG-Datenmodell sich sehr eng am XZuFi-Syntaxmodell orientiert, ist eine Unterscheidung dieser beiden Kontexte in der Regel nicht notwendig.

**Achtung:** gelegentlich wird _kursive Schrift_ auch eingesetzt, um _Teile_ einer Aussage zu betonen. Diese Verwendung _kursiver Schrift_ sollte aus dem Text-Kontext erkennbar sein.

### `Code Schrift`: API-Nachrichten-Elemente, konkrete Werte, Code-Schnipsel, URNs
Wenn der Parametrisierungsleitfaden Bezug auf Elemente nimmt, die buchstäblich so in API-Nachrichten erscheinen können, dann wird in der Regel `Code Schrift` verwendet. Dies betrifft syntaktische Elemente wie einen JSON-Schlüssel `zustaendigkeit`. Es betrifft auch konkrete Werte wie `de`, welches ein Code für die Deutsche Sprache ist. Weiterhin werden Pfadangaben, Programmcode-Schnipsel, XML-Schema-Datentypen, ... und nicht zuletzt Universal Resource Names (URNs), die XÖV Code-Listen eindeutig identifizieren, in `Code Schrift` geschrieben.

In vielen Fällen haben die Response-Nachrichten der PVOG Suchdienst-API eine JSON-Struktur, die sich sehr eng an das XZuFi-Datenmodell bzw. das PVOG-Datenmodell anlehnt. Hier soll durch die Verwendung der `Code Schrift` hervorgehoben werden, dass die beschriebenen Dinge Teil der Response-Nachricht sind. Insbesondere wenn eine Syntax wie z.B. `zustaendigkeit[]` verwendet wird, dann bezieht sich die Aussage des Textes auf den Teil der Response-Nachricht, in der die Zuständigkeiten aufgezählt werden.

### Normale Schrift: Dinge und Konzepte der Welt
Normale Schrift wird im Parametrisierungsleitfaden eingesetzt, um alle Dinge und Konzepte der (realen) Welt zu beschreiben - insbesondere sofern sie nicht XZuFi-Datenstrukturen, PVOG-Datenmodell, API-Nachrichten-Elemente, etc. sind.

:::info

Dies führt zu einer **sehr wichtigen Unterscheidung:**
- Ein _Onlinedienst_ ist eine Datenstruktur, die in einer XZuFi-Nachricht übertragen, im PVOG als Datenobjekt abgelegt und dort wieder ausgelesen werden kann. Diese Datenstruktur enthält u.a. Parameter, die von Datenredakteuren gepflegt worden sind.
- Ein Onlinedienst ist hingegen ein IT-System, das von einem Bereitsteller entwickelt wurde und von einem Betreiber betrieben wird. Wenn der Onlinedienst EfA-fähig ist, so wird er immer wenn er benutzt wird im PVOG dasjenige _Onlinedienst_-Datenobjekt ermitteln, das für die Leistung und das Gebiet des Antrags (z.B. die Stadt Magdeburg, wenn der Antragsteller dort seinen Antrag stellen will) die relevanten Parameter-Werte enthält.

Onlinedienste und _Onlinedienste_ stehen daher in einer 1:n Beziehung. 

:::

## Sprachgebrauch

Es wird versucht, im Parametrisierungsleitfaden einen Sprachstil zu verwenden, der feine aber wichtige Unterschiede erkennbar macht. Gelegentlich werden aber auch verschiedene Bezeichnungen für Konzepte verwendet, die einander sehr nah sind und bei denen die Unterscheidung nicht wirklich trennscharf möglich ist. Hier sind die verschiedenen Bezeichnungen als im Wesentlichen synonym zu verstehen und ihre Unterscheidung hat primär das Ziel einen weniger wichtigen Aspekt zu betonen oder den Text lesbarer zu machen, indem er näher an der Alltagssprache liegt.

### Link vs. URL

Als "URL" soll im Parametrisierungsleitfaden jedes Datum verstanden werden, das den  [Regeln des URL-Encoding](https://de.wikipedia.org/wiki/URL-Encoding) entspricht, also etwa `https://domain.tld/path/path?parameter=value` oder ähnlich. "URLs" sind primär technische Daten, die die Nutzung eines Web-Service ermöglichen oder technisch eine Navigation zu einer Web-Seite.

Als "Link" soll dagegen ein Navigationselement auf einer Webseite bezeichnet werden, das der Benutzer anklicken kann, um die Navigation auszulösen. Ein "Link" wird typischerweise über einen Anzeigetext oder eine grafische Schaltfläche präsentiert, so dass die "URL", die mit dem "Link" verbunden ist, als technisches Detail unsichtbar bleibt.

Die Unterscheidung zwischen "URL" und "Link" ist nicht wirklich trennscharf, weil jeder "Link" eine "URL" enthalten muss, um nutzbar zu sein und weil andere Konzeptdokumente, aus denen der Parametrisierungsleitfaden Begriffe und Bezeichnungen übernimmt, diese Unterscheidung ggf. nicht machen. 

### XML Elementnamen vs. Elementtypen

Die Standard-Dokumente zu XZuFi 2.2 und 2.3 definieren primär XML-Datenstrukturen bzw. ein XML-Schema. Sie tun dies, indem sie Datentypen und ihre Komposition beschreiben. In der Regel bedeutet dies, dass in einem Element, das z.B. eine Leistung beschreibt, ein weiteres Element eingebettet ist, das ein Textmodul beschreibt. Dabei hat das eingebettete Element einen Datentyp, hier `xzufi:TextModul` und einen Elementnamen, hier `modulText`, unter dem es im umschließenden Leitsungs-Element auffindbar ist.

Im Fließtext des Parameterleitfadens - wo es primär um XZuFi und/oder das Datenmodell des PVOG geht - wird bevorzugt der Elementname benutzt, um z.B. die Verortung von bestimmten Angaben zu beschreiben. Dabei besteht das Problem, dass einzelnen Top-Level Objekten kein sinnvoller Element-Name mehr zugeordnet werden kann. Hier wird dann der Datentyp, aber ohne das Präfix `xzufi:` und ohne die Code-Typografie verwendet. Beispiel: "Die Onlinedienst Bezeichnung wird im Element _bezeichnung_ des Datenobjekts _Onlinedienst_ abgelegt."

Dort, wo es um JSON-Strukturen geht, die Teil einer API-Response-Nachricht sind, wird durchgehend der Elementname verwendet. Die Bezeichnungen der Datentypen wie sie in den OpenAPI-Spezifikationen der APIs enthalten sind, werden nicht verwendet.

:::warning[ToDo]

Die Groß/klein-Schreibung, die konsequenterweise alle Elementnamen klein und alle Datentypen groß schreiben müsste, ist an einigen Stellen nicht konsequent umgesetzt. Hier besteht noch Verbesserungsbedarf.

:::


[^1]:Technische Systeme und Plattformen wie das PVOG oder die OSI Plattform, aber auch Prozesse wie die Pflege von FIM-Daten in den Redaktionssystemen, etc.
[^2]:Es ist dann Aufgabe des Herstellers eines nicht-Standard-konformen Onlinedienstes zu dokumentieren, wie ein Redaktionssystem für die Onlinedienst-spezifische Art der Parametrisierung seines Dienstes zu nutzen ist. So könnte z.B. dokumentiert werden: „Der Link zur Datenschutzerklärung muss als *individueller Parameter* mit dem Namen LinkZurDatenschutzerklaerung im Redaktionssystem angelegt werden. Das Feld *Link zur Datenschutzerklärung* im Dialog des Redaktionssystems kann nicht genutzt werden.“

