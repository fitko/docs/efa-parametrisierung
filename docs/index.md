---
slug: /
---

# Dokumentation zur EfA-Parametrisierung

## Einleitung

Die Parametrisierung von „Einer-für-Alle“(EfA) -Onlinediensten dient dazu, eine bundesweite Nachnutzung der Dienste zu ermöglichen trotz landes- und satzungsrechtlicher Unterschiede. Die Flexibilität von EfA-Onlinediensten soll durch die Verwendung von Variablen (Parametern) sichergestellt werden. Dies soll standardisiert und gleichartig für möglichst viele Dienste der öffentlichen Verwaltung erfolgen.

Ein Onlinedienst der öffentlichen Verwaltung, der dem Konzept EfA folgen soll, zeichnet sich dadurch aus, dass er bundesweit allen Gebietskörperschaften zur Mitnutzung zur Verfügung steht. Es besteht also die Herausforderung, dass ein technisch einheitlicher fachlicher EfA-Onlinedienst für mehrere, unterschiedliche landes- und satzungsrechtliche Grundlagen zur Verfügung gestellt wird. 

Die nachfolgenden Abschnitte geben Hinweise, wie die Parametrisierung von Diensten erfolgen soll, wie die Parameter definiert, gepflegt und abgerufen werden sollen und welche Parameter verwendet werden sollen. Es werden Parameter für variable Werte des Onlinedienstes, Kommunikationsparameter für das Antragsrouting, Parameter für die Adressierung von Bezahldiensten sowie Parameter zur Steuerung des Aussehens der Onlinedienste benötigt.

## Zielgruppe der Dokumentation

Das Thema der EfA-Paramtetrisierung ist für verschiedene Stakeholdergruppen relevant. Dabei sind die Ziele und Interessen dieser Stakeholder durchaus verschieden. Die Dokumentation versucht, diesen Interessen gerecht zu werden.

Zielgruppe | Ziele und Interessen
-----------|---------------------------
Mitglieder IT-Planungsrat/ Mitglieder AL Runde | EfA-Onlinedienste sind bezüglich aller Belange der landes- und satzungsrechtlichen Grundlagen anpassbar. Die Anpassung erfolgt nach einheitlichen Regeln und mit einheitlichen Mitteln, damit die Aufgabe der Parametrisierung für die ausführenden Redakteure leicht zu erlernen und prozesssicher ausführbar ist. Die standardisierte Umsetzung der Parametrisierung trägt zur Erreichung dieser Ziele bei.
Entwickler EfA-Onlinedienste und deren Plattformen  | Klare Vorgaben und verständliche Dokumentation der zu verwendenden Mechanismen erleichtern die Entwicklung.  
Anbieter/Betreiber EfA-Onlinedienste          | Die Dokumentation liefert Input für das Anforderungsmanagement und dient als Basis für die Gestaltung der Supportprozesse.
Nachnutzer EfA-Onlinedienste                  | Die Dokumentation kann als Basis einer Checkliste für die Qualitätssicherung der Roll-In- und späteren Pflegeprozesse genutzt werden.
Pflegeverantwortlicher für die Parameterdaten / Redakteure | Die standardisierte Umsetzung der Parametrisierung führt zu einfacheren Pflegeprozessen. Die Dokumentation bietet Hintergrundinformationen und erleichtert damit die Qualitätssicherung der Daten im Rahmen der Pflegeprozesse. 
Produktverantwortliche für betroffene Basisdienste, z. B. Portalverbund Online-Gateway und DVDV | Die Dokumentation liefert Input für Anforderungen, die die jeweiligen Basisdienste umsetzen müssen. 

## Zielbild

Die Digitalisierung von Verwaltungsdienstleistungen erfolgt nach dem EfA-Prinzip. Das bedeutet, dass eine Leistung zentral von einem Land entwickelt und anschließend anderen Ländern zur Verfügung gestellt wird. Dabei müssen die sog. EfA-Mindestanforderungen erfüllt werden, aus denen ein ursprüngliches EfA-Zielbild abgeleitet wurde (siehe auch [Orientierung Parametrisierung](https://www.fitko.de/fileadmin/fitko/foederale-koordination/gremienarbeit/Foederales_IT-Architekturboard/20230227_FITKO-Orientierung_Parametrisierung_v0.80.pdf), V0.80 14.2.2023).

![EfA-Zielbild](./images/EfA-Zielbild.jpg)  
**Ursprüngliches EfA-Zielbild, vorgestellt in der AL-OZG-Runde am 08.12.2020**

Durch die (Weiter-)Entwicklung diverser Onlinedienst-Plattformen und Infrastruktur-Systeme wie dem PVOG der FITKO haben sich einige Gewichte des ursprünglichen Zielbildes verschoben. Es hat sich eine Gesamtarchitektur herausgebildet, die die Prozesse der Parametrisierung unterstützt. Aspekte der Benutzerkontenverwaltung zusammen mit persönlichen Postfächern, der Anbindung von Fachverfahren für die Datenversorgung während der Antragstellung sowie des Versands der Antragsdaten sind im Kontext der Parametrisierung nur von marginaler Bedeutung und werden hier erst einmal nicht weiter betrachtet.

![Heutige Gesamtarchitektur](./images/Heutige%20Gesamtarchitektur.jpg)
**Heutige Gesamtarchitektur des OZG-Ökosystems im Bereich Parametrisierung**

Primäre Quelle der Parametrisierungsdaten sowie Werkzeug zu ihrer Pflege sind die dezentralen Redaktionssysteme, die von den Bundesländern im Zuge der Pflegeprozesse für den [FIM-Baustein Leistung](https://fimportal.de/) genutzt werden, um Behördenportale mit Informationen über behördliche Leistungen zu befüllen. In manchen Bundesländern sind den Landes-Redaktionssystemen noch kommunale Redaktionssysteme vorgeschaltet, die ihre Daten an die Landes-Redaktionssysteme weiterreichen.
Ergänzend zu den Redaktionssystemen kommen das [DVDV](https://docs.fitko.de/dvdv/) sowie [FIT-Connect](https://docs.fitko.de/fit-connect/docs) hinzu, die spezielle Parameter für Bezahlvorgänge und für das Routing von Anträgen verwalten. 

Die Informationen aus den dezentralen Redaktionssystemen werden durch das zentralisierte [Portal Verbund Online Gateway (PVOG)](https://www.fitko.de/produktmanagement/pvog) zu einer bundesweiten und einheitlichen Datensammlung aggregiert. Das PVOG war ursprünglich allein darauf ausgerichtet, Behördenportalen die bundesweiten Informationen zu behördlichen Leistungen überregional zugänglich zu machen ("Zuständigkeitsfinder", "ZuFi"). Mit der Einführung von Onlinediensten nach dem EfA-Prinzip ergab sich aber der Bedarf einer Datenversorung durch das PVOG auch für diese Systeme. Dazu wurde das Datenmodell und Kommunkationsprotokoll zum Datenaustausch zwischen Redaktionssystemen und dem PVOG ("XZuFi") um Elemente wie z.B. Onlinedienstparameter erweitert.

Die Redaktionssysteme zusammen mit DVDV, FIT-Connect und dem PVOG stellen heute die Quell-Seite der standardisierten Parametrisierung dar. Darüber hinaus betreiben einige Länder weitere Systeme, die sie als Quellen zur Parametrisierung ihrer Onlinedienste nutzen. Diese sollen hier aber nicht weiter betrachtet werden, da sie als proprietäre Systeme dem Ziel einer einheitlichen Gesamtlösung entgegen stehen.

Die Abnehmerseite der Parametrisierung besteht aus den Onlinediensten, die ihre Informationsbedarfe aus dem PVOG sowie - für die genannten speziellen Parameter - aus den Systemen DVDV und FIT-Connect befriedigen. Sie nutzen dazu die von diesen Systemen angebotenen Schnittstellen (Application Programming Interface, API) direkt oder indirekt über Onlinedienst-Plattformen. Solche Onlinedienst-Plattformen erleichtern die Softwareentwicklung von Onlinediensten, indem sie die komplizierte Nutzung solcher Systeme wie PVOG und DVDV einmalig realisieren und als ein wesentlich vereinfachtes Programmiermodell den eigentlichen Onlinediensten zur Verfügung stellen. Eine Ausnahmestellung nimmt hier die Routingdienst API von FIT-Connect ein, die selbst auf das PVOG zugreift und diese Daten mit eigenen Daten aggregiert an den Onlinedienst liefert.

### Jüngere Entwicklungen

Mit dem [Beschluss 2024/37 des IT-Planungsrates](https://www.it-planungsrat.de/beschluss/beschluss-2024-37) vom 19.06.2024 ist die standardisierte Vorgehensweise der EfA-Parametrisierung  als Teil des EfA-Prinzips ab dem 01.01.2026 verbindlich zu nutzen. Dies bedeutet konkret, dass standardisierte Parameter - sofern ein EfA-Onlinedienst sie zur Steuerung nutzt - aus den Systemen PVOG bzw. DVDV ermittelt werden müssen, wo sie an definierten Stellen abgerufen werden müssen. Aspekte der _Abläufe_ bei diesen Diensten, z.B. Pflege- und Abfrageprozesse bei ZuFis, die operative Datenzustellung oder der operative Paymentprozess werden an anderer Stelle behandelt. 

Mit diesem Beschluss soll primär erreicht werden, dass die nachnutzenden Länder bzw. deren Behörden die Aufgabe der initialen und fortlaufenden Parameterpflege leichter bewältigen können. So soll verhindert werden, dass die EfA-Dienste auf IT-Plattformen zur Nachnutzung bereitgestellt werden, die jede für sich ein anderes Konzept der Parametrisierung mit eigenen Pflegeoberflächen und eigener Datenhaltung voraussetzen. Ein solches Szenario führt zu einem [Ausbremsen der Behördendigitalisierung](https://www.fitko.de/fileadmin/fitko/veranstaltungen/20240621_Die_FITKO_stellt_vor_EfA-Parametrisierung_Handout.pdf#page=12). Statt dessen soll es allen nachnutzenden Ländern möglich sein, jeden EfA-Onlinedienst auf einheitliche Art und Weise zu parametrisieren, indem die Parameter mit Hilfe der eigenen etablierten Werkzeuge an das PVOG übermittelt werden bzw. die eigenen _pflegenden Stellen_ des DVDV mit der Eintragung beauftragt werden. Damit entfällt auch die Notwendigkeit zur Mehrfachpflege derselben Daten, die durch die getrennte Datenhaltung der diversen IT-Plattformen ausgelöst wird. 


## Weiterführende Links
**Digitale-Verwaltung.de**
* [Einer für Alle - einfach erklärt](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/efa/efa-node.html)
* [OZG-Rahmenarchitektur](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/rahmenarchitektur/rahmenarchitektur-node.html)
* [EfA-Parameterisierung](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/rahmenarchitektur/efa-parametrisierung/efa-parametrisierung-node.html)
* [Orientierungspapier Efa-Parametrisierung](https://www.digitale-verwaltung.de/SharedDocs/downloads/Webs/DV/DE/PDFs/CDR_20210914_v0_60_FITKO-Orientierung_Parametrisierung.pdf?__blob=publicationFile&v=2)

**Fitko.de**
* [Föderales IT-Architekturboard](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard) - Links auf IT-Architekturrichtlinien, IT-Landschaft und Orientierungspapier Parameterisiserung
