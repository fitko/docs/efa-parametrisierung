---
id: standardisierung_versionierung_parameterkatalog
title: "Standardisierung und Versionierung des Parameterkatalogs"
---


:::info[Version]

1.0.0

:::

# Standardisierung der Parametrisierung und Versionierung des Parameterkatalogs

Eine Standardisierung einiger Aspekte der EfA-Parametrisierung soll eine höhere Prozesssicherheit bei der Entwicklung, Wartung und vor allem der (Nach-)Nutzung der Onlinedienste bewirken. Diese Standardisierung kann und soll nicht allumfassend sein. Vielmehr sollen genau die Parameter und Prozesse standardisiert werden, die das höchste Nutzen-Potenzial bieten. Hingegen sollen die Bereiche von der Standardisierung ausgeklammert werden, die heute noch mit technischen und fachlichen Unwägbarkeiten belegt sind.

## Standardparameter und -katalog
Der Parametrisierungsleitfaden enthält einen Katalog von ca. 40 identifizierten Parametern, die in vielen EfA-fähigen Onlinediensten dieselben fachlichen und technischen Features auf dieselbe Art und Weise steuern können und daher standardisiert werden sollen. Eine Standardisierung dieser Parameter bedeutet konkret:

- _Wenn_ ein EfA-fähiger Dienst ein fachliches oder technisches Feature _parametrisierbar_ unterstützen will, _das durch einen Parameter aus dem Standard-Katalog abgedeckt ist_, dann muss der Onlinedienst ...
   - den Parameter in seiner Dokumentation mit dem standardisierten Namen benennen
   - den steuernden Wert zur Laufzeit aus dem standardisierten Quell-System (PVOG oder DVDV) und dort aus der standardisierten Stelle im Datenmodell auslesen können
   - den dort ausgelesenen Wert entsprechend dem Standard interpretieren

Insbesondere darf der EfA-Dienst den Parameter-Wert nicht _ausschließlich_ aus einer anderen Quelle lesen; er darf dort keinen abweichenden Namens-Schlüssel verwenden; er darf die aus der standardisierten Quelle gelesenen Werte nicht anders interpretieren, etc. Verstößt er gegen diese Regeln, ist er nicht mehr [Standard-konform](#standard-konformität).

Diese Festlegungen implizieren, dass es _ausreicht_, den Parameter-Wert im standardisierten Quell-System nach dem standardisierten Verfahren encodiert an der standardisierten Stelle zu hinterlegen, um die parametrisierte Funktion zu steuern. Es soll weiter unten noch dargestellt werden, dass Onlinedienste _als zusätzliche Alternative_ auch andere Quellen für die Parametrisierung nutzen dürfen. Dies erlaubt es Onlinedienst-Herstellern, komfortabler zu bedienende Parameterpflegesysteme anzubieten, _ohne Bundesländer von der Nachnutzung auszuschließen oder einzuschränken, die ihre etablierten XZuFi-basierten Werkzeuge zur Pflege von PVOG-Daten nutzen wollen_.

Der Standard-Katalog enthält einige Parameter, die in XZuFi 2.2 als Namens-Schlüssel/Wert-Paare in _parameter_-Elementen am _Onlinedienst_-Element abgelegt werden. Für diese Parameter definiert der Parametrisierungsleitfaden schon heute die Namens-Schlüssel, die in Form der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) festgeschrieben werden. 

:::info

In XZuFi 2.3 werden diese Namens-Schlüssel/Wert-Paare dann im Element _parameterEfa_ am _Onlinedienst_-Element abgelegt, so dass die Migration bezüglich dieser Parameter erleichtert wird. 

:::

Darüber hinaus definiert der Standard-Katalog diverse Parameter, deren Werte er in anderen Elementen von XZuFi 2.2 verortet, z.B. ["Name der Organisationseinheit"](./parameter_katalog/27_name_der_organisationseinheit.md) an der _Organisationseinheit_. Bei der Standardisierung dieser Parameter handelt es sich also nur um die Festschreibung, dass der Onlinedienst als erstes versuchen muss, bestimmte Informationen an bestimmten Stellen im XZuFi/PVOG-Datenmodell auszulesen.

## Individual-Parameter
Neben der Standardisierung der Parameter aus dem Standard-Katalog soll für [Individual-Parameter mit direktem Onlinedienst-Bezug](./kategorisierung_der_parametrisierung#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) die Verortung im XZuFi/PVOG-Datenmodell standardisiert werden, d.h. wo und wie solche Parameter im PVOG abgelegt werden. Dies betrifft alle technischen und fachlichen Features, für die _keine geeigneten_ Vorgaben und Mechanismen in XZuFi, PVOG, DVDV oder anderen Systemen des OZG-Ökosystems verfügbar sind. Als Beispiel sei hier die Menge der auswählbaren Schulformen im jeweiligen Bundesland genannt: In Hamburg muss diese Auswahl die "Stadtteilschule" enthalten, aber nicht die "Hauptschule"; in Hessen hingegen umgekehrt. Ein EfA-fähiger Onlinedienst für die Anmeldung zur weiterführenden Schule muss dahingehend parametrisierbar sein, dass in Hamburg und in Hessen andere Drop-Down-Einträge angezeigt werden.

- _Wenn_ ein EfA-fähiger Dienst ein fachliches oder technisches Feature _parametrisierbar_ unterstützen will, das nur _individuell_ steuerbar ist, dann muss der Onlinedienst ...
   - im Zuge der Entwicklung 
      - einen Namens-Schlüssel (Zeichenkette) definieren, unter dem der Parameter im PVOG am zuständigen _Onlinedienst_-Element in einem _parameter_-Element abgelegt wird und
      - eine Codierung festlegen, nach der alle denkbaren Werte für den Parameter in Form eines xs:string codiert werden können
   - zur Laufzeit
      - das zuständige _Onlinedienst_-Element im PVOG identifizieren,
      - anhand des Namens-Schlüssels in _parameter.parameterName_ den Parameter identifizieren und
      - den Parameter-Wert aus _parameter.parameterWert_ auslesen, ggf. decodieren und seine Geschäftslogik danach steuern.

Für die Festlegung des Namens-Schlüssels hat der Onlinedienst-Hersteller große Freiheiten, da es - abgesehen von den standardisierten Namen aus der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) - keinen gemeinsamen Namensraum gibt. Jeder Onlinedienst kann nur Parameter suchen und finden, deren Namens-Schlüssel entweder in der Code-Liste aufgeführt sind, oder die er selbst definiert hat. 

Stand Q2/2024 erfordert die Implementierung der FIT-Connect Routing-API, dass die Namens-Schlüssel der Individual-Parameter mit dem Präfix `efa.custom.` beginnen. Nur so werden sie von dieser API gefunden und an den Onlinedienst zurück geliefert. Es kann daher als Best Practice betrachtet werden, die Namens-Schlüssel der Individual-Parameter mit diesem Präfix festzulegen. Die Namenseinschränkung besteht aber nicht, wenn das PVOG Suchdienst-API verwendet wird, so das eine diesbezügliche Angleichung bestehender, funktionierender Onlinedienste keinen Mehrwert bietet. 

:::info

In XZuFi 2.3 werden dieselben Namens-Schlüssel/Wert-Paare dann im Element _parameterIndividuell_ am _Onlinedienst_-Element verwendet, so dass auch hier die Migration bezüglich dieser Parameter erleichtert wird.

Der wesentliche Unterschied zwischen _parameterEfa_ und _parameterIndividuell_ in XZuFi 2.3 besteht darin, dass für _parameterEfa_ nur solche Namens-Schlüssel zulässig sind, die in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) standardisiert sind.

:::

Auch für die Individual-Parameter gilt, dass Onlinedienste als _zusätzliche Alternative_ auch andere Quellen für die Parametrisierung nutzen dürfen. 

## Ausschlüsse 
Auch wenn es einen Katalog von Standard-Parametern gibt, so ist damit _nicht_ festgelegt, dass jeder Onlinedienst _alle_ diese Parameter implementieren muss und seine Fachlogik entsprechend dynamisch auf alle diese Parameterwerte reagieren muss. Es gibt nur wenige Vorgaben, welche Parameter implementiert werden müssen. Diese Vorgaben sind auch nicht Teil des Parametrisierungsleitfadens. So schreibt die DSGVO indirekt vor, dass der Onlinedienst einen Link zur Datenschutz-Erklärung anzeigen muss, der in einem EfA-fähigen Onlinedienst daher parametrisierbar sein muss. Die EfA-Mindestanforderungen schreiben vor, dass der Name der zuständigen Behörde angezeigt werden muss, was auch dieses Feature zu einem verpflichtenden Parameter macht.

Explizit _nicht_ standardisiert werden soll die genaue Funktionsweise von Redaktionssystemen, die zur Parameter-Pflege verwendet werden. Hier muss nur darauf geachtet werden, dass die Redaktionssysteme prinzipiell in der Lage sind, die Pflege der (PVOG-basierten) Parameter des Standard-Katalogs und deren Übermittlung an das PVOG zu ermöglichen. Weiterhin sollte darauf geachtet werden, dass die Art der Encodierung bei Individual-Parametern über die Eingabemasken aller Redaktionssysteme für _Onlinedienst.parameter_-Elemente noch eingebbar ist (z.B. sollten Zeilenumbrüche in der Encodierung nicht zwingend vorgesehen werden, weil einige Redaktionssysteme nur Ein-Zeilen-Eingabefelder für die Werte der _Onlinedienst.parameter_-Elemente anbieten.)

Weiterhin soll _nicht_ standardisiert werden, mit welchen technischen Mitteln der Onlinedienst zur Laufzeit die Werte aus PVOG und DVDV ausliest, solange nur sichergestellt ist, dass die Werte dort die Funktion des Onlinedienstes dem Standard entsprechend steuern. 

### Alternative Quellen
Es wird nicht standardisiert, dass die Werte der benötigten Parameter _ausschließlich_ aus den Standard-Quellen PVOG und DVDV ermittelt werden.
Vielmehr ist es auch zulässig, dass Onlinedienste _zusätzliche_ Quell-Systeme für Parameter-Werte befragen. So ist es zulässig, dass eine Onlinedienst-Plattform ein eigenes Parametrisierungs-Repository mit komfortabler Benutzeroberfläche und einfachen Pflege-Prozessen anbietet - solange die Onlinedienste, die auf dieser Plattform betrieben werden, _auch_ anhand der Parameter-Werte aus dem PVOG und dem DVDV steuerbar sind. Dies ist notwendig, damit nachnutzende Ländern _nicht gezwungen_ werden, Pflege-Prozesse zu etablieren, die an ihren gewohnten Redaktionssystemen vorbei laufen.

### Ausnahmen bei unzumutbaren Rahmenbedingungen
Schließlich soll ebenfalls _nicht_ im Detail standardisiert werden, wie mit Parametern umzugehen ist, für die im OZG-Ökosystem zwar prinzipiell eine Unterstützung vorgesehen ist, die vorhandene Unterstützung aber möglicherweise nicht ausreicht. Als Beispiel sei hier die Parametrisierung von Kosten genannt. Für das fachliche Thema Kosten gibt es in XZuFi und somit auch im PVOG das Element _KostenModul_ an der _Leistung_, das aber trotz seiner hohen Komplexität _nicht alle_ Bedarfe an Ausdruckskraft befriedigen kann und daher _nicht immer_ einsetzbar ist. Für solche Fälle sei der Hersteller eines Onlinedienstes verpflichtet, zunächst die Anforderungen an den Onlinedienst gegen die Möglichkeiten zu prüfen, die die Nutzung von Elementen des OZG-Ökosystems bietet. Lassen sich die Anforderungen mit akzeptablen Kompromissen umsetzen, so soll die passende Komponente des OZG-Ökosystems genutzt werden. Wenn es hingegen Anforderungen gibt, die sich nicht erfüllen lassen, soll er eine Parametrisierbarkeit als [Individual-Parameter mit direktem Onlinedienst-Bezug](./kategorisierung_der_parametrisierung#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug) auf Basis der XZuFi _Onlinedienst.parameter_-Elemente vorsehen. Nur wenn auch dies technisch nicht möglich oder aus anderen Gründen nicht vertretbar ist, darf er eine Lösung außerhalb des OZG-Ökosystems vorsehen. Im Beispiel könnte er im Extremfall eine eigene Kosten-Kalulator-Komponente entwickeln, die völlig eigene Parametrisierungsmechanismen nutzt, um komplexe kommunal-spezifische Kostenstrukturen parametrisiert zu ermöglichen. Für diese Art der Parametrisierung muss er dann aber auch ein Pflege- und Supportkonzept entwickeln, das es jeder nachnutzenden Kommune erlaubt, die Kostenberechnung individuell festzulegen bzw. die Festlegung zu ändern.

Diese Festlegungen sollen es dem Onlinedienst insbesondere ermöglichen, beliebige Informationen aus den Systemen des OZG-Ökosystems auszulesen und im Rahmen seiner Fachlogik so zu verwenden, wie dies nach dem Zweck dieser Daten und Systeme vorgesehen ist. So ist es denkbar und im Sinne der Standardisierung _erlaubt_, die Struktur von FIM-Datenfeldern zur Laufzeit aus dem FIM-Portal auszulesen, um z.B. dynamisch die Struktur von postalischen Adressen zu bestimmen und die Eingabemaske entsprechend anzupassen.

## Standard-Konformität

Die oben dargestellten Regeln für eine Standardisierung erlauben Aussagen über die _Standard-Konformität_ eines Onlinedienstes oder eines Redaktionssystems:
- Ein Onlinedienst ist _konform zum EfA-Parametrisierungsstandard_, wenn er die oben dargestellten Regeln befolgt und insbesondere die fachlichen Features, die der Standard-Parameterkatalog abdeckt, eben durch _diese_ Parameter Standard-konform umsetzt.
- Ein Redaktionssystem ist _konform zum EfA-Parametrisierungsstandard_, wenn er es ermöglicht, die Werte für die Parameter des Standard-Parameterkatalog entsprechend den Vorgaben zu pflegen - mit Ausnahme der [DVDV-basierten Parameter](./kategorisierung_der_parametrisierung.md#dvdv-basierte-parameter). Dies ist der Fall, wenn das Redaktionssystem die relevanten Teile des XZuFi-Standards 2.2 unterstützt.  
Darüber hinaus **sollte** das Redaktionssystem bereits während der Eingabe die Werte für [Standard-Parameter mit direktem Onlinedienst-Bezug, die über die generischen _Onlinedienst.parameter_-Elemente als Namens-Schlüssel/Wert-Paare implementiert sind](./kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug), syntaktisch validieren. Beispielsweise sollte das Redaktionssystem ...
   - beim _Onlinedienst.parameter_ mit dem _parameterNamen_ "`efa.datenschutzerklaerung.url`" sicherstellen, dass im _parameterWert_ eine syntaktisch gültige URL steht.
   - beim _Onlinedienst.parameter_ mit dem _parameterNamen_ "`efa.uebermittlungKontaktdaten`" sicherstellen, dass im _parameterWert_ nur die Werte `Ja` und `Nein` eingetragen werden können.
   - etc.

## Versionierung

Betrachtet werden Versionen des Katalogs, nicht Versionen von Parameterwerten[^1].

Was bedeutet es, wenn es eine neue Version des Katalogs gibt? Die Konformität von EfA-Diensten und Redaktionssystemen ist ggf. für die neue Version nicht mehr gegeben:
- wenn ein Parameter aus dem Katalog entfällt, ist das in der Regel unproblematisch. Dienste und Redaktionssysteme können dadurch "in die Konformität rutschen", wenn sie vorher eine Funktion abweichend vom Standard implementiert haben (Redaktionssystem: ggf. gar nicht implementiert haben) und dies nun nicht mehr im Standard gefordert wird.
- wenn ein Parameter neu hinzukommt, ist das für EfA-Dienste dann unproblematisch, wenn sie die betreffende fachliche Funktion gar nicht durch Parameter steuern, oder "durch Zufall" bereits so steuern, wie es in den Standard aufgenommen wird (was zu erwarten ist, wenn sie sich an die Vorgaben durch XZuFi halten, oder andere Mechanismen des OZG-Ökosystems nutzen). Nur wenn sie eine Implementierung haben, die nach der Erweiterung des Standards nicht mehr dem Standard entspricht, fallen sie heraus und sollten angepasst werden. Vergleichbar ist es bei Redaktionssystemen, die die Pflege eines neu standardisierten Parameters (noch) nicht unterstützen und nach dem Versionssprung des Standard-Katalogs funktional erweitert werden müssen.

### Versionierung von Onlinediensten (Software-System)

Jede Software-Version eines Onlinedienstes definiert ein Set von Parametern, die gepflegt sein müssen, damit diese Version des Onlinedienstes betrieben werden kann. Dabei können einzelne Parameter auch optional gepflegt werden, wenn der Onlinedienst bei fehlenden Werten dort eine Voreinstellung (i.e. einen Default-Wert) wirksam werden lässt.

Mit dem Roll-Out einer neuen Software-Version des Onlinedienstes kann sich dieses Set benötigter Parameter ändern, so dass die Voraussetzungen für die (Nach-)Nutzung sich ebenfalls ändern. Dies kann dazu führen, dass Datenredakteure in den Ländern und Kommunen Werte in ihren Redaktionssystemen pflegen müssen, die sie zuvor noch nicht gepflegt haben. Jeder Betreiber eines Onlinedienstes sollte daher beim Roll-Out einer neuen Software-Version eines Onlinedienstes mit den (nach-)nutzenden Ländern eine Übergangsfrist vereinbaren, während derer beide Software-Versionen des Onlinedienstes parallel betrieben werden. Eine Umschaltung erfolgt dann pro individuell parametrisierter Region (i.e. pro Bundesland, pro Kreis, pro Kommune - je nachdem für welche geografische Granularität jeweils eigenständige Parametrisierungen durchgeführt wurden). Dazu kann ausgenutzt werden, dass das PVOG/XZuFi-Element _Onlinedienst_ für eine Leistung mehrfach im PVOG vorhanden sein kann. So kann sich ein Exemplar mit der Zuständigkeit für eine Kommune A auf die alte Software-Version beziehen, während ein anderes Exemplar mit Zuständigkeit für Kommune B sich auf die neue Software-Version bezieht.

Bei einem solchen rollierenden Versionswechsel muss auch berücksichtigt werden, dass eine neue Software-Version des Onlinedienstes ggf. auch Anpassungen an den Fachverfahren notwendig macht, die die Antragsdaten empfangen und verarbeiten müssen. Diese Betrachtung ist aber unabhängig vom Konzept der Parametrisierung.

Da es bei Verwendung alter oder manipulierter URLs zu subtilen Fehlersituationen kommen kann, wird ein Verfahren empfohlen, das eine Prüfung des Onlinedienst Namens und der Onlinedienst Version durch den Onlinedienst selbst beinhaltet. Detail zu diesen Themen liefert das [Konzept der Versionierung von Onlinedienst Produken](./versionierung_onlinedienst_produkt.md). 

[^1]: Versionen von Parameterwerten müssen über die Gültigkeits-Zeitraum-Mechanismen in XZuFi und dem DVDV sowie über die Pflegemöglichkeiten der Redaktionssysteme und des DVDV unterstützt werden.