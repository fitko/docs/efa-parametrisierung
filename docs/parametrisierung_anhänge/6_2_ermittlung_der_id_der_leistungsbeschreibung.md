---
id: ermittlung_der_id_der_leistungsbeschreibung
title: "Ermittlung der ID der Leistungsbeschreibung"
---


:::info[Version]

1.0.0

:::

# Ermittlung der ID der Leistungsbeschreibung

Die PVOG-Suchdienst-API benötigt in der Regel die ID einer Leistungsbeschreibung, die sich aus der LeikaID und Gültigkeitsgebiet (ARS) ermitteln lässt.

Der Onlinedienst benutzt dazu den Endpunkt [`/v3/servicedescriptions/leikaid`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/findByLeikaIdV3) mit dem URL-Parameter `leikaIds` = \<die LeikaID\> und `ars` = \<der ARS\>. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Angaben auf oberster Ebene auf diejenigen Elemente zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind.

Die Response enthält im Element `content[]` ein Array mit den Daten passender Leistungsbeschreibung-Datenobjekten. Falls an dieser Stelle mehrere solche Datenobjekte vorhanden sind, muss dasjenige identifiziert werden, das am besten zu den aktuellen Bedarfen des anfragenden Onlinedienstes passt. Dazu können die Werte der folgenden Elemente geprüft werden:
- `content[]:ars` Es ist dasjenige `content`-Element zu wählen, dessen `ars`-Wert am genauesten mit dem gesuchten ARS übereinstimmt. Beispiel: eine Suche bezüglich Frankfurt/Main (ARS 064120000000) könnte zwei Leistungsbeschreibungen liefern, bei der die eine für ganz Hessen (ARS 060000000000) und die andere nur für Frankfurt/Main gilt. In diesem Fall ist die zweite Leistungsbeschreibung weiter zu verwenden.  
**Hinweis:** Es ist auch denkbar, den URL-Parameter `useArsHierarchy` auf `false` zusetzen. Dann besteht aber das Risiko, dass eine Suche mit einem Gemeinde-genauen ARS gar keine Ergebnisse liefert, wenn es nur eine Leistungsbeschreibung auf Ebene des Kreis-ARS und eine Leistungsbeschreibung auf Ebene des Bundesland-ARS gibt.
- `content[]:availableLanguages[]` Es ist dasjenige content-Element zu wählen, dessen Menge von Sprach-Codes in `availableLanguages[]` einen Sprach-Code enthält, der mit der Sprachpräferenz des Antragstellers am besten übereinstimmt.  
**Hinweis:** Wenn diese Regel ausgewertet werden muss, um ein eindeutiges `content`-Element zu bestimmen, dann ist dies ein Zeichen für eine riskante Datengestaltung in den Redaktionsystemen (s.u.) und sollte als Warnung geloggt werden.

Die genannten Kriterien sind in dieser Reihenfolge anzuwenden, bis ein einziges `content`-Element identifiziert werden konnte.
Die Response enthält dann in dem identifizierten `content`-Element ein Element `id` dessen Wert (z.B. `L100001.LB.360900603`) die ID der Leistungsbeschreibung ist.

:::warning

Die Möglichkeiten zur Datengestaltung, die XZuFi hier bietet, bergen ein Risiko: So ist es möglich, zwei Leistungsbeschreibungen im PVOG anzulegen, bei denen die eine als _sprachversionen_ (`content[]:availableLanguages[]`) nur `de` und die andere nur `en` enthält, beide aber für dieselbe Leika und in demselben Gebiet genutzt werden. In einem solchen Fall müssen _Zustaendingkeiten_ sowohl für die _Organisationseinheiten_ als auch die _Onlinedienste_ für beide _Leistungsbeschreibungen_ angelegt werden, damit nicht von der Sprachauswahl abhängt, ob Organisationseinheiten und/oder Onlinedienste gefunden werden! Deutlich weniger riskant ist eine Datengestaltung, bei der nur eine einzige _Leistungsbeschreibung_ erstellt wird, deren _sprachversionen_ sowohl `de` als auch `en` enthält.

Dieses Risiko betrifft die Nutzung der Redaktionssysteme, da eine bestimmte Art der Datengestaltung vermieden werden sollte. Onlinedienste könnten nur mit hohem Aufwand mit heuristischen Verfahren versuchen, dieses Risiko zu mitigieren. Dies wird nicht empfohlen.

:::

<!-- Frage: Gibt es einen einfacheren Weg, als den unter hier dokumentierten Ablauf zur Ermittlung der ID der zutreffenden Leistungsbeschreibung?
Antwort (PVOG-Team, 25.6.2024): Nein, hat man als Ausgangslage Leistungsschlüssel und ARS so ist in der Doku  beschriebene Endpunkt die optimalste Lösung.   

-->

