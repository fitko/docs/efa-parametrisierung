---
id: best_practices_url_wertige_parameter
title: "URL-wertige Parameter"
---


:::info[Version]

1.0.0

:::

# Best Practices zu URL-wertigen Parametern
Jedes Redaktionssystem sollte für URL-wertige Parameter statt einer generischen Textbox eine spezielle Textbox anbieten, in der man nur genau eine URL eintragen kann, d.h. die Textbox muss eine Validierung des Inhalts gegen die URL-Encoding Regeln durchführen.

Hinweis: Diese Parameter erlauben in der Regel keine Platzhalter-Dynamik o.ä., die die URL noch einmal umschreibt, bevor sie im Onlinedienst verwendet wird. D.h. die URL wird genau so genutzt, wie sie angegeben ist. Bei URL-wertigen Parametern, bei denen eine Platzhalter-Dynamik möglich ist, ist dies in der Parameter-Beschreibung explizit dokumentiert.
