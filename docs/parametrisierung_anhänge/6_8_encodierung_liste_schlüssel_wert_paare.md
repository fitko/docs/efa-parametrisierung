---
id: encodierung_liste_schlüssel_wert_paare
title: "Enkodierung einer Liste von Schlüssel/Wert-Paaren in einer Zeichenkette"
---


:::info[Version]

1.0.0

:::

# Enkodierung einer Liste von Schlüssel/Wert-Paaren in einer Zeichenkette

Mitunter ist es notwendig eine Liste von Schlüssel/Wert-Paaren in einer einzigen Zeichenkette zu encodieren. Dies ist z.B. bei XZuFi-basierten Individual-Parametern mit direktem Onlinedienst-Bezug der Fall, weil der Wert eines solchen Parameters im Element parameterWert vom Typ xs:string abgelegt werden muss. 

## Encodierung mit einfachen Trennzeichen
Für diese Encodierung existiert kein Standard aber eine Best Practice, der folgende Eigenschaften hat:
- Leicht verwendbar für Menschen – sowohl beim Editieren als auch beim Lesen
- Leicht programmtechnisch umsetzbar
- Gut parsbar
- Geringe Einschänkungen bezüglich der verwendbaren Schlüssel und Werte

Die Encodierung bzw. das Parsen durch den Onlinedienst erfolgt nach den folgenden Regeln:

Die Liste der Schlüssel/Wert-Paare ist in XZuFi wie folgt in einer Zeichenkette encodiert:
- Vor dem ersten Schlüssel/Wert-Paar steht eine Zeichenkette aus beliebig vielen whitespace charactern .
- Zwischen zwei Schlüssel/Wert-Paaren steht eine Zeichenkette aus beliebig vielen whitespace charactern, genau einem Semikolon („`;`“) und erneut beliebig vielen whitespace charactern
- Zwischen dem Schlüssel und dem Wert eines Schlüssel/Wert-Paares steht eine Zeichenkette aus beliebig vielen whitespace charactern, genau einem Pipe-Symbol („`|`“) und erneut beliebig vielen whitespace charactern
- Nach dem letzten Schlüssel/Wert-Paar steht eine Zeichenkette aus beliebig vielen whitespace charactern, höchstens einem Semikolon („`;`“) und erneut beliebig vielen whitespace charactern

Ein Beispiel illustriert die Encodierungsregeln:

Die folgende Liste soll encodiert werden:

| Schlüssel | Wert |
| --- | --- |
| Haushaltsstelle | 1234 |
| ObjektNummer | 4711-0815 |
| Beschreibung | \<p\>Ein Textabsatz, der ein \<em\>betontes\</em\> Wort enthält.\</p\> |

Die Encodierte Liste kann z.B. in den folgenden Zeichenketten codiert werden:
- `Haushaltsstelle|1234;ObjektNummer|4711-0815;Beschreibung|<p>Ein Textabsatz, der ein <em>betontes</em> Wort enthält.</p>`
- `Haushaltsstelle | 1234 ;   ObjektNummer | 4711-0815 ;                     `  
`Beschreibung |  <p>Ein Textabsatz, der ein <em>betontes</em> Wort enthält.</p>  ;   `
- `Haushaltsstelle|1234;`  
`ObjektNummer|4711-0815;`  
`Beschreibung|<p>Ein Textabsatz, der ein <em>betontes</em> Wort enthält.</p>`

Zu beachten: Die Encodierung schränkt die Menge der verwendbaren Schlüssel und Werte ein: 
- Es können keine Schlüssel verwendet werden, die ein Pipe-Symbol enthalten, z.B. kann ein Schlüssel nicht „`vorher|nachher`“ lauten. 
- Auch können Werte kein Semikolon-Zeichen enthalten. So kann der Wert „`Ein Anfang; und ein Ende`“ nicht encodiert werden. 
- Weder Schlüssel noch Wert können führende oder nachlaufende whitespace character haben. So ist der Wert „`      ganz rechts`“ nicht encodierbar. Dasselbe gilt für Schlüssel und Werte, die nur aus whitespace charactern bestehen, z.B. „`	`“. Die leere Zeichenkette „“ ist dagegen zulässig.

Auch wenn diese Encodierung Einschränkungen mit sich bringt, ist zu erwarten, dass in Anwendungsfällen, in denen Schlüssel/Wert-Paare in einer XZuFi-Zeichenkette encodiert werden müssen, diese Einschränkungen nicht relevant sind. Wenn dies doch der Fall ist, muss in diesem speziellen Fall eine andere Encodierung verwendet werden. Um nahe an der Best Practice zu bleiben, sollten nur anderer Trennsymbole gewählt werden. Die Einführung von Escaping-Mechanismen wie „Doppeltes Semikolon wird nicht als Trennsymbol interpretiert, sondern als einfaches Semikolon in der Wert-Zeichenkette“ sollte vermieden werden, weil das Risiko besteht, die Redakteure zu überfordern. 

## Alternative: JSON-artige Encodierung
Als Alternative zur oben dargestellten Encodierung mit einfachen Trennzeichen bietet es sich an, JSON bzw. eine JSON-artige Encodierung festzulegen. Diese hat folgende Eigenschaften:
- Relativ leicht verwendbar für Menschen, speziell für Personen mit IT-Hintergrund
- Leicht programmtechnisch umsetzbar
- Gut parsbar
- Minimale Einschänkungen bezüglich der verwendbaren Schlüssel und Werte

Die Regeln zur Encodierung und zum Parsen werden durch die [JSON-Syntax](https://www.w3schools.com/js/js_json_syntax.asp) definiert und lassen sich am besten anhand des oben eingeführten Beispiels erklären. Die Liste der Schlüssel/Wert-Paare wird wie folgt codiert (zusätzliche Zeilenumbrüche sind möglich):
- `{"Haushaltsstelle": "1234", "Objektnummer": "4711-0815", "Beschreibung": "<p>Ein Textabsatz, der ein <em>betontes</em> Wort enthält.</p>"}`

Der Vorteil dieser Alternative liegt vor allem darin, dass es de-facto keine Einschränkungen für Schlüssel und Werte gibt.
Der Nachteil ist die komplexere Syntax, die ggf. von weniger IT-affinen Datenredakteuren nicht sicher genutzt werden kann. 

## Konsequenzen für Redaktionssysteme

Redaktionssysteme sollten für alle Standard-Parameter, die als Wertemenge eine Liste von Schlüssel/Wert-Paaren vorsehen und deren Endcodierung nach einem dieser beiden Schemata erfolgt, einen speziellen Editor-Dialog implementieren. Dieser Editor-Dialog sollte es dem Redakteur mit einem Button-Klick ermöglichen, neue Schlüssel/Wert-Paare hinzuzufügen und alte zu löschen, sowie Schlüssel und Wert jeweils in einem eigenen Eingabefeld zu pflegen. Der Dialog sollte bereits während der Eingabe prüfen, ob die eingegebene Zeichenkette im Eingabefeld die Beschränkungen der Encodierung verletzten und - bei Benutzung des Schemas mit einfachen Trennzeichen - z.B. bei Benutzung des Trennsymbols „`;`“ im Wert-Eingabefeld eine Fehlermeldung anzeigen. Intern muss das Redaktionssystem nach Abschluss der Pflege die einzelnen Eingaben in den Feldern nach den angegebenen Regeln zu einer einzigen Zeichenkette zusammenfügen, d.h. mit den Trennsymbolen „`|`“ und „`;`“ verketten oder zu einem JSON-Objekt rendern.

Redaktionssysteme können die Redakteure weiterhin unterstützen, indem sie es erlauben, - bei Benutzung des Schemas mit einfachen Trennzeichen - die Trennsymbole „`|`“ und „`;`“abweichend von dieser Best Practice zu wählen: Falls der Hersteller des Onlinedienstes seinen Dienst so gebaut hat, dass der Parser andere Trennzeichen erwartet (z. B. weil er Werte erlauben möchte, die ein „`;`“ enthalten), kann der Redakteur mit einem solchen erweiterten Pflegedialog dann weiterhin den Parameter-Wert komfortabel pflegen. Der Hersteller muss auf eine solche abweichende Festlegung der Trennsymbole in seiner Onlinedienst Parametriesierungsdokumentation dann hinweisen.   

Redaktionssysteme, die keine komfortable Pflegemöglichkeit von Schlüssel/Wert-Paar-Listen-wertigen Parametern bieten können, müssen ein Textfeld zur Pflege des Parameter-Wertes anbieten. In diesem Fall muss der Redakteur dann die Encodierung entsprechend der Onlinedienst Parametriesierungsdokumentation „händisch“ durchführen. Wenn der Hersteller der hier beschriebenen Best Practice gefolgt ist, kennt der Redakteur das Encodierungs-Vorgehen bereits und kann seiner Aufgabe leichter und sicherer bewältigen.

