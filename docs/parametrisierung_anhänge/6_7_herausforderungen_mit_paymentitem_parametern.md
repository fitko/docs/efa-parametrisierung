---
id: herausforderungen_mit_paymentitem_parametern
title: "Herausforderungen mit PaymentItem-spezifischen Parametern"
---


:::info[Version]

1.0.0

1.0.1 veraltet

:::

:::danger[Veraltet!]

Die Parameter [PaymentItem Reference](../parameter_katalog/13_paymentitem_reference.md) und [PaymentItem BookingData](../parameter_katalog/14_paymentitem_bookingdata.md) sind zugunsten des Parameters [PaymentItem](../parameter_katalog/14.5_paymentitem.md) abgekündigt und somit veraltet.
Der neue Parameter [PaymentItem](../parameter_katalog/14.5_paymentitem.md) fasst die beiden alten Parameter zusammen.

Damit enfallen die auf dieser Seite geschilderten Herausforderungen bis auf die Herausforderung der einheitlichen Wert-Festlegung innerhalb einer Roll-Out Region, die aber ohnehin nicht spezifisch für die Payment-Parameter ist.

:::

# Herausforderungen mit PaymentItem-spezifischen Parametern
Innerhalb der Bezahldienst-bezogenen Parameter existieren zwei Parameter, die sich jeweils auf ein PaymentItem beziehen. Die Dokumentation der XBezahldienste-API charakterisiert die PaymentItems so: „Der paymentRequest ist vergleichbar mit einer Bestellung bei einem Online-Shop. Entsprechend dem Warenkorb bei einer Bestellung kann der paymentRequest mehrere paymentItems (Positionen) enthalten.“

PaymentItems sind jeweils durch zwei Angaben charakterisiert, die parametrisierbar sein müssen:
- PaymentItem Reference - Dieses entspricht quasi einer Artikelnummer - aus Sicht des Bezahldienstes.
- PaymentItem BookingData - Die Bezahldienste brauchen teilweise detaillierte Informationen über Titel, Kapitel, Kostenstellen oder sonstige Daten. Daher ist hier – pro PaymentItem – eine flexible Liste von Schlüssel/Wert-Paaren vorzusehen.

## Einheitliche Wert-Festlegung innerhalb einer Roll-Out Region
Der Wert dieser Parameters ist zwischen den Verantwortlichen in den Organisationseinheiten, den Betreibern der Fachverfahren und den Bezahldienstleistern abzusprechen. Dabei sollte versucht werden, für alle Organisationseinheiten innerhalb einer Roll-Out-Region denselben Wert festzulegen.

Gelingt diese einheitliche Festlegung nicht, besteht das Risiko, dass in einer Roll-Out-Region (z.B. ein Bundesland) für jede zuständige Organisationseinheit ein vollständig zu parametrisierendes Datenobjekt _Onlinedienst_ im PVOG angelegt werden muss, nur damit diese  zwei Parameter-Werte für jede Organisationseinheit individuell festgelegt werden können, während alle anderen Parameter-Werte bei all diesen _Onlinedienst_ Datenobjekten identisch sind. Bei einer Änderung eines anderen Parameters müssen dann die Parametrisierungen aller _Onlinedienst_ Datenobjekten auf dieselbe Weise angepasst werden.

## Unterstützung nur eines PaymentItems in XZuFi
Die Verortung der Parameter PaymentItem Reference und BookingData im XZuFi als generische XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug mit je einem Namens-Schlüssel und einem Wert führt dazu, dass ein Onlinedienst im Standard-Fall nur ein PaymentItem beim Bezahldienst signalisieren kann. Falls der Onlinedienst mehrere PaymentItems voneinander unterscheiden möchte, benötigt er die Parametrisierbarkeit mehrerer, **unterscheidbarer** PaymentItem References und mehrerer **unterscheidbarer** PaymentItem BookingData Listen. Dieses Szenario wird als "gemischter Warenkorb" bezeichnet. Die Dokumentation der XBezahldienste-API gibt hier das (fiktive) Beispiel „deutschsprachige Geburtsurkunde“ vs. „internationale Geburtsurkunde“, die unterschiedlich bepreist und/oder zu verbuchen sind.

Zwar ist es denkbar, dass im PVOG mehrere _parameter_-Elemente mit demselben Namens-Schlüssel `efa.payment.paymentitemreference` und verschiedenen Werten an einem _Onlinedienst_-Datenobjekt konfiguriert werden. Die so konfigurierten PaymentItem References sind für den Onlinedienst aber **nicht unterscheidbar**, weil sie keine weiteren Schlüssel tragen können. Dasselbe gilt für BookingData Listen. Der Onlinedienst kann dann nicht wissen, welchen der verschiedenen Werte er in einer spezifischen Situation, z.B. für „deutschsprachige Geburtsurkunde“, er verwenden soll.

In einem solchen Fall könnte der Onlinedienst für jedes PaymentItem individuelle Parameter selbst definieren, deren Struktur an den Standard angelehnt sein sollten. So wäre es im Beispiel deutscher und internationaler Geburtsurkunden denkbar, zwei XZuFi-basierte Individualparameter mit den Namens-Schlüsseln `efa.payment.paymentitemreference.german` und `efa.payment.paymentitemreference.international` zu definieren und den standardisierten Namens-Schlüssel `efa.payment.paymentitemreference` nicht weiter zu nutzen. Analog müsste er zwei individuelle Parameter mit den Namens-Schlüsseln `efa.payment.bookingdata.german` und `efa.payment.bookingdata.international` definieren.

In XZuFi 2.3 wird sich die Verortung des Parameters leicht ändern: beide werden als _parameterEfa_ dem _Onlinedienst_-Datenobjekt zugeordnet sein. Damit werden sie als standardisierter Parameter von den nicht-standardisierten _parameterIndividuell_ abgetrennt, bleiben aber weiterhin jeweils ein Namens-Schlüssel/Wert-Paar. Das Problem, dass keine unterscheidbaren PaymentItem References und keine unterscheidbaren PaymentItem BookingData konfiguriert werden können, bleibt daher bestehen, bzw. wird noch verschärft, weil die Namens-Schlüssel `efa.payment.paymentitemreference.german` und `efa.payment.paymentitemreference.international` nicht in der Code-Liste enthalten sind.

Aus diesem Grund sollte für die Nutzung der Parameter im Szenarion "gemischter Warenkorb" eine Encodierung der Item-IDs in den Werten von PaymentItem Reference und PaymentItem BookingData angestrebt werden. Im Beispiel „deutschsprachige Geburtsurkunde“ vs. „internationale Geburtsurkunde“ bedeutet das, dass jeweils zwei Namens-Schlüssel/Wert Paare mit den standardisierten Namens-Schlüsseln erstellt werden und dass in die _parameterWert_-Elemente auch die Diskriminatoren für "deutschsprachig" und "international" hineincodiert werden, z.B. in Form von JSON-Objekten:

| Namens-Schlüssel | Wert als JSON-Objekt |
|---|---|
| `efa.payment.paymentitemreference` | `{ "deutsch": "DEUTSCHSPRACHIGE_GEBURTSURKUNDE" }` |
| `efa.payment.paymentitemreference` | `{ "international": "INTERNATIONALE_GEBURTSURKUNDE" }` |
| `efa.payment.paymentitembookingdata` | `{ "deutsch": {"A": "0815", "B": "4711", "C": "1234"}}` |
| `efa.payment.paymentitembookingdata` | `{ "international": {"A": "3141", "B": "5926", "C": "5358"}}` |

:::warning

Die Herausforderung bleibt, dass diese Art der Encodierung ggf. nicht von den Redaktionssystemen unterstützt wird, zumal sie nur für den Fall der gemischten Warenkörbe tatsächlich benötigt wird. Die Hersteller von Onlinediensten mit gemischten Warenkörben müssen hier also sehr sorgfältig dokumentieren, wie sie die unzureichende Unterstützung in XZuFi umgehen. Die Redakteure müssen große Sorgfalt darauf verwenden, die jeweiligen Werte der Parameter als JSON-Objekte zu formulieren.

:::

 