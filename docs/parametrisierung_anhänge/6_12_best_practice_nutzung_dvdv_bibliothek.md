---
id: best_practice_nutzung_dvdv_bibliothek
title: "Nutzung der DVDV-Bibliothek"
---


:::info[Version]

1.0.0

:::

# Best Practice: Nutzung der DVDV-Bibliothek
Die Nutzung der [Zugriffsbibliothek für die DVDV-API](https://docs.fitko.de/dvdv/apidocs) wird dringend empfohlen. Ein wesentliches Feature der Bibliothek ist die Ausfallsicherheit. Bundesweit gibt es 14 Instanzen von DVDV-Servern, die sich gegenseitig im Falle von Wartungsfenstern und Ausfällen vertreten. Das ist vertraglich gesichert und die Betreiber sprechen sich auch entsprechend ab. Die Bibliothek unterstützt ein automatisches Failover, das für den Client transparent ist und zwischen fachlichen und technischen Fehlern mit einer komplexen Logik gut unterscheidet. Auch benötigt man für den Zugriff über die REST-Schnittstelle eine Authentifizierung mit einem JWT-Token, dessen Erstellung nicht ganz einfach ist. Dies unterstützt die Bibliothek ebenfalls. Zudem sind die übertragenen Datenstrukturen teilweise komplex und man bekommt sie in aufbereiteter und dokumentierter Form von der Bibliothek zurück.