---
id: ermittlung_xzufi_parameter_mit_bezug_in_fit_connect
title: "Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der FIT-Connect Routing API"
---

<!-- 
Die Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der FIT-Connect Routing API ist entgegen der 
ersten Einschätzung nicht möglich, da die FIT-Connect Routing-API Stand Q2/2024 nur solche _Onlinedienst.parameter_ aus dem PVOG auswertet, deren Namens-Schlüssel
mit "efa.custom" beginnen.

Diese Markdown-Datei soll dennoch als "Rumpf" erhalten bleiben, um die Lücke in der Numerierung zu schließen.
-->