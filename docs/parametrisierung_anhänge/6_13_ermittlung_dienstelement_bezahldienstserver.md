---
id: ermittlung_dienstelement_bezahldienstserver
title: "Ermittlung des Bezahldienst-Server DienstElements mit Hilfe der DVDV API"
---


:::info[Version]

1.0.0

:::

# Ermittlung des Bezahldienst-Server DienstElements mit Hilfe der DVDV API

Der Onlinedienst ermittelt zunächst die [Referenz auf den Bezahldienst](../parameter_katalog/08_Referenz_auf_Bezahldienst.md), die den Behördenschlüssel (organizationKey) enthält. Die ebenfalls notwendige DienstbeschreibungsUri (serviceSpecificationUri) ermittelt der Onlinedienst direkt aus seiner überregional gültigen Konfiguration.

Mit diesen Informationen benutzt der Onlinedienst die methode `findServiceDescription` der [DVDV-API-Bibliothek](https://docs.fitko.de/dvdv/apidocs) und ruft damit indirekt den Endpunkt [`/{entryPath}/findservicedescription`](https://docs.fitko.de/dvdv/spec/#get-/-entryPath-/findservicedescription) mit den Parametern \<organizationKey\> und \<serviceSpecificationUri\> auf (siehe [Dokumentation der DVDV-API](https://docs.fitko.de/dvdv/spec/#get-/-entryPath-/findservicedescription)).

Über das `ServiceDescription`-Rückgabeobjekt der Bibliotheksfunktion kann der Onlinedienst mittels `getServiceElements()` die Liste der DienstElemente ermitteln. Aus dieser Liste wählt er dasjenige DienstElement aus, dessen Methode `getCustomServiceElementType()` den Wert "`Bezahldienst-Server`" liefert. Das so identifizierte DienstElement liefert alle gesuchten Werte für Bezahldienste.

:::info

Zur Identifikation des Bezahldienstes ist der Schlüssel _ServiceSpecificationUri_ bei der Benutzung der DVDV-API anzugeben. Der Wert dieses Schlüssels ist für alle Bezahldienste identisch und dem DVDV-Eintragungskonzept für Bezahldienste zu entnehmen. Stand Q2/2024 existiert nur ein [experimentelles MVP-Eintragungskonzept](https://docs.fitko.de/dvdv/assets/files/20221020_DVDV-Eintragungskonzept_MVP_v.0.7-6973941d702366bb379b7b13548c952a.pdf), das den Wert für _ServiceSpecificationUri_ auf "urn:dvdv:setup:example:bezahldienst:1" festlegt.

:::