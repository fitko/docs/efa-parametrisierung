---
id: ermittlung_zuständige_organisationseinheit_im_pvog
title: "Ermittlung eines zuständigen Organisationseinheit-Datenobjektes mit Hilfe der PVOG-API"
---


:::info[Version]

1.0.0

:::

# Ermittlung eines zuständigen _Organisationseinheit_-Datenobjektes mit Hilfe der PVOG-API

Organisationseinheiten können mit den Leistungen und Gebieten in verschiedenen Rollen verknüpft sein. Die möglichen Rollen sind in der Code-Liste  [`urn:de:xzufi:codeliste:zustaendigkeitsrolle`](https://www.xrepository.de/details/urn:de:xzufi:codeliste:zustaendigkeitsrolle) festgelegt. Stand Q2/2024 existieren die Rollen
- `01`: Zuständige Stelle und Ansprechpunkt 
- `02`: Zuständige Stelle
- `03`: Ansprechpunkt  

Wenn ein Onlinedienst die Stammdaten einer zuständigen Organisationseinheit auslesen möchte, so muss er sich zunächst entscheiden, in welcher Rolle die zu identifizierende Organisationseinheit zuständig sein soll. Benötigt der Onlinedienst die Stammdaten der Zuständigen Stelle oder benötigt er die Stammdaten des Ansprechpunktes? In der Regel wird der Onlinedienst die Stammdaten der Organisationseinheit benötigen, die mindestens die Rolle Zuständige Stelle (Code `01` oder `02`) hat, denn dies ist die Organisationseinheit, an die der digitale Antrag versendet werden soll.

Neben den Zuständigkeits-Rolle kann sich die Zuständigkeit einer Organisationseinheit aber auch nach weiteren Kriterien richten. Dazu besitzt das Element _ZustaendigkeitOrganisationseinheit_ neben dem Element _rolle_ das Element _kriterium_ mit dem Typ `xzufi:Zustaendigkeitskriterium`. Dieses besteht wiederum aus einem Element _codeKriterium_ (Typ [`xzufi:ZustaendigkeitskriteriumCode_Erweiterbar`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zustaendigkeitskriterium)) und einem oder mehreren Elementen _auspraegung_ (Typ `datatypeC`, i.e. eine Zeichenkette mit eingeschränktem Zeichensatz).
Die Code-Liste definiert[^1] Zuständigkeitskriterien wie z.B.
- `001`: AnfangsbuchstabeFamilienname
- `023`: Geburtsjahr
- `063`: Versicherungsnummer
- `101`: Hunderasse
- `111`: KFZ-Klasse

Um zu entscheiden, welche von mehreren Organisationseinheiten im konkreten Fall einer Antragstellung zuständig ist, muss die Angabe _auspraegung_ mit den Umständen der Antragstellung abgeglichen werden. Trägt das _kriterium_ beispielsweise den Code `001` und die _auspraegung_ ist "A-K", so ist die betrachtete Organisationseinheit nur dann zuständig, wenn der Antragsteller bzw. die betroffene Person einen Nachnamen mit entsprechendem Anfangsbuchstaben trägt.

:::info[Best Practice]

Die Hersteller von Onlinediensten sollten nicht versuchen, die Kriterien der Zuständigkeit maschinell auszuwerten, da die _auspraegung_-Zeichenketten ggf. komplexe Angaben wie "vor 1980" für das Geburtsjahr oder "endend auf ...001" für die Versicherungsnummer enthalten können. Vielmehr sollte jeder Onlinedienst alle Kriterien aller gefundenen Organisationseinheiten, die in der gewünschten _rolle_ zuständig sind, dem Antragsteller zusammen mit der Bezeichnung des Kriteriums (im Beispiel "Geburtsjahr" bzw. "Versicherungsnummer") präsentieren. Der Antragsteller sollte dann selbst auswählen, welche Organisationseinheit im konkreten Antrags-Fall zuständig ist.

Falls ein Hersteller von Onlinediensten sich entscheidet, Kriterien maschinell auszuwerten, so sollte er dies sorgfältig in der Dokumentation des Onlinedienstes dokumentieren.  Die [zuständigen Datenredakteure](../pflegekonzept.md) müssen dann die syntaktischen Vorgaben für die _auspraegung_-Elemente mit besonderer Sorgfalt befolgen. Gleichzeitig sollte der Hersteller darauf achten, dass die Auswertungslogik möglichst viele Varianten von Angaben in den _auspraegung_-Elementen korrekt auswertet, inklusive einer gewissen Toleranz gegenüber Tippfehlern.

Eine automatisierte Validierung der Eingaben im Redaktionssystem wäre hier wünschenswert, ist hier aber nicht möglich, solange mit der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.zustaendigkeitskriterium`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.zustaendigkeitskriterium) nicht auch zentral definiert wird, welche _auspraegungen_ zu den jeweiligen Zustaendigkeitskriterium-Codes zulässig sind. Darüber hinaus kann ein Hersteller auch eigene Codes mit eigenen zulässigen Angaben für  _auspraegungen_ definieren. Deren syntaktische Korrektheit kann ein Redaktionssystem nicht prüfen, da die proprietären Syntax-Regeln nicht bekannt sein können. 

:::

Um die ID eines _Organisationseinheit_-Datenobjektes mit Hilfe der PVOG Suchdienst API zu ermitteln, ist wie folgt vorzugehen:

**1. Teilschritt: Ermittlung der ID der Leistungsbeschreibung**

Siehe Abschnitt [Ermittlung der ID der Leistungsbeschreibung](6_2_ermittlung_der_id_der_leistungsbeschreibung.md)

**2. Teilschritt: Ermittlung der ID der Organisationseinheit**

Der Onlinedienst benutzt den Endpunkt [`/v2/organisationunits/titles`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getOrganisationUnitTitlesV2) mit den URL-Parametern `ars` = \<die ARS\> und `lbids` = \<die in Schritt 1 ermittelte ID der Leistungsbeschreibung\>. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Trefferliste auf die Organisationseinheiten zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind.

Die Response enthält auf oberster Ebene ein namenloses Array-Element, dessen einzel-Elemente die folgenden Elemente enthalten:
- `id`: enthält die vollständige ID der Organisationseinheit, so wie sie in nachfolgenden Aufrufen des Endpunktes [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) als Query-Parameter `q` verwendet werden kann.
- `title`: enthält eine deutschsprachige Bezeichnung der Organisationseinheit
- `role` : enthält zwei Elemente `name` und `code` der Zuständigkeits-Rolle
- `criteria` : enthält drei Elemente `name`, `code` und `auspraegungen[]` des Kriteriums

Der Onlinedienst muss diejenigen Elemente des namenlosen Top-Level-Arrays weiter verarbeiten, die entweder kein `role`-Element enthalten oder ein `role`-Element mit `role:code` = `01` oder `02`.

:::info[Best Practice]

Die Rollen der Organisationseinheiten werden in den Redaktionssystemen nicht immer korrekt gepflegt. Ein Onlinedienst sollte daher nicht nur die Organisationseinheiten für den Versand der Antragsdaten betrachten, die (mindestens) die Rolle Rolle „01“ (Zuständige Stelle und Ansprechpunkt) oder „02“ (Zuständige Stelle) haben. Vielmehr sollte ein Onlinedienst als Fallback auch die Organisationseinheiten betrachten, die nur die Rolle „03“ (Ansprechpunkt) haben, **wenn es keine zuständige Stelle mit gültigem Zustellkanal (OSCI/XTA oder DestinationSignature) gibt**.

:::

Enthält die verbleibende Menge noch mehrere Elemente und tragen diese Elemente jeweils ein `criteria`-Element, so muss der Onlinedienst diese Elemente tabellarisch dem Antragsteller zur Auswahl präsentieren, damit dieser entscheiden kann, welches Zuständigkeits-Kriterium für den konkreten Antragsfall zutrifft und damit welche der Organisationseinheiten tatsächlich zuständig ist.

Für den Fall, dass mehrere Organisationseinheiten zuständig sind, aber dabei die Kriterien nicht denselben _codeKriterium_-Wert enthalten, kann auf einen Parametrisierungsfehler geschlossen werden, der dem Antragsteller entsprechend anzuzeigen ist: Es kann nicht sein, dass die eine Organisationseinheit für die Anfangsbuchstaben "A-K" des Familiennames, und die andere Organisationseinheit für die Geburtsjahre "vor 1980" zuständig ist. Auch das Fehlen von Kriterien bei einzelnen Organisationseinheiten in einem solchen Szenario deutet auf einen Fehler hin. Solche möglichen Fehlersituationen sind durch den Hersteller in der Design-Phase des Onlinedienstes sorgfältig zu analysieren und die möglichen Strategien zur Fehlerbehandlung sorgfältig abzuwägen. Fehlen die _Kriterium_-Angaben hingegen bei allen zuständigen Organisationseinheiten, kann dies bedeuten, dass der Antragsteller die freie Wahl hat, bei welcher Organisationseinheit er seinen Antrag stellen möchte. Ob dieses Szenario möglich ist oder einen Parametrisierungsfehler darstellt, muss ebenfalls bereits in der Design-Phase fachlich geklärt werden.

Das `id`-Element der Organisationseinheit, die der Antragsteller als die für seinen Antragsfall zutreffend auswählt, wird im Weiteren verwendet. Mit dieser ID kann dann über den Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) das _Organisationseinheit_ Datenobjekt ausgelesen werden. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Angaben auf diejenigen Elemente zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind.

:::info

Ein Hersteller von Onlinediensten kann sich dafür entscheiden, das _Kriterium_-Feature von XZuFi nicht zu verwenden. Eine solche Entscheidung hat zwei wesentliche Konsequenzen:
- Die Auswahl-Logik, bei der der Antragsteller interaktiv die Kriterien und Ausprägungen auswertet und die für ihn zuständige Organisationseinheit auswählt, braucht nicht implementiert zu werden. Der Dialogfluss wird damit deutlich vereinfacht.
- Der Onlinedienst kann in einem Gebiet (i.e. Bundesland, Kommune), in dem die Zuständigkeit von Organisationseinheiten an Kriterien gekoppelt ist, nicht eingesetzt werden. In einem solchen Gebiet wird die Suche nach zuständigen Organisationseinheiten immer mehrere Treffer liefern, bei denen der Onlinedienst keine Möglichkeit hat, die tatsächlich zuständige Organisationseinheit auszuwählen. Der Onlinedienst muss in einer solchen Situation eine Fehlparametrisierung melden und den Antragsprozess abbrechen.

Für eine solche Entscheidung sollte der Onlinedienst Hersteller daher zuvor mit allen potenziell nachnutzenden Ländern die Anforderungen bezüglich dieses Details klären. Wenn in keinem der potenziell nachnutzenden Ländern die Zuständigkeit der Organisationseinheiten für die betrachtete Leistung von solchen Kriterien abhängig ist, kann das _kriterium_-Feature im Onlinedienst unbeachtet bleiben.

:::

<!-- Frage: Ist es richtig/beabsichtigt, dass ein OD *den Antragsteller* auf Basis der Kriterien (criteria)-Angaben aus `/v2/organisationunits/titles` *befragen* muss, welche OE für ihr gerade zuständig ist, wenn (nach Filterung bezüglich der _gueltigkeit_ noch mehrere OE zuständig sind?

Antwort (aus dem PVOG-Team, 25.06.2024): "Ja, Onlinedienst muss den Antragsteller hinsichtlich Kriterien befragen und diese dann auswerten, falls eine Organisationseinheit eindeutig ermittelt werden muss."

-->

**3. Teilschritt: Ermittlung der Daten der Organisationseinheit**

Mit der in Schritt 2 ermittelten ID der Organisationseiheit können dann über den Endpunkt [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) die Daten der Organisationseinheit ermittelt werden. Dazu ist diese ID als Query-Parameter `q` anzugeben.

:::warning

Auf die Angabe des zusätzlichen URL-Parameters `validDate` kann hier verzichtet werden, wenn bereits in Schritt 2 sichergestellt wurde, dass nur eine ID der Leistungsbeschreibung identifiziert wurde, die auch zum gewünschten Zeitpunkt gültig ist. Die Angabe von `validDate` beim Aufruf von [`/v1/organisationunits/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Organisationseinheiten/operation/getJzufiOe) entfernt Stand Q2/2024 **nicht** die Unter-Elemente der Organisationseinheit mit eigener `gueltigkeit` wie z.B. `name`, die zum angegebenen Zeitpunkt nicht gültig sind. Diese Prüfung und Datenbereinigung muss der Onlinedienst selbst durchführen.

:::


[^1]:Die Code-Liste ist erweiterbar, so dass der Onlinedienst ggf. zusätzliche Zuständigkeitskriterien mit individuell definierten Codes verarbeiten kann, z.B. das _Herkunftsland einer eingeführten Ware_ mit dem Code `999`. Dies ist dann in der Dokumentation des Onlinedienstes sorgfältig zu dokumentieren. Die Klartext-Beschreibung (im Beispiel: "Herkunftsland") dieser Kriterien muss dann in der Gebiets-unabhängigen Konfiguration des Onlinedienstes oder fest ausprogrammiert beim Onlinedienst hinterlegt werden.
