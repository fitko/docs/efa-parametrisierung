---
id: mehrere_zuständigkeiten
title: "Umgang mit mehreren Zuständigkeiten für einen Onlinedienst"
---


# Umgang mit mehreren Zuständigkeiten für einen Onlinedienst

## Einführung

Dieses Dokument befasst sich mit der Frage, wie mit EfA-Online-Diensten umgegangen wird, wenn für eine bestimmte Verwaltungsleistung und Geografie mehrere Zuständigkeiten vorliegen. Dies ist z. B. der Fall für Gewerbeanmeldung, die sowohl durch Handelskammer, Handwerkskammer, als auch durch die Kernverwaltung/Bezirke umgesetzt werden kann.

## Zuständigkeitsszenarien und ihre Behandlung

Onlinedienst-Parameter sind im PVOG an verschiedenen Datenobjekten verortet. So enthält z.B. eine Organisationseinheit z.B. den Namen der Organisationseinheit, den der Onlinedienst dem Antragsteller anzeigen muss. Die Leistung enthält z.B. die Bearbeitungsdauer und der Onlinedienst enthält z.B. die Zahlungsweisen, über die ggf. Gebühren entrichtet werden können. Alle diese Datenobjekte enthalten Angaben zur Zuständigkeit, wobei es Varianten von Zuständigkeitsregelungen gibt. Eine Organisationseinheit in Form eines Datenobjektes OrganisationseinheitErweitert enthält ein Element `zustaendigkeit` vom Typ ZustaendigkeitOrganisationseinheit, das die Zuständigkeit der Organisationseinheit für eine Leistung in einem Gebiet definiert. Die Definition ist attributiert mit optionalen Rollen und optionalen Kriterien. Die Zuständigkeit eines Onlinedienstes in Form eines Datenobjektes OnlinedienstErweitert enthält ein Element `zustaendigkeit` vom Typ Zustaendigkeit, das die Zuständigkeit des Onlinedienstes für eine Leistung in einem Gebiet ohne zusätzliche Attribution definiert. Die Leistung ist selbst Bezugspunkt der Zuständigkeitsfestlegungen, trägt aber auch ein Element gueltigkeitGebietId, dessen Bedeutung Aspekte der Zuständigkeit enthalten. Schließlich gibt es noch untergeordnete Datenobjekte wie Kontaktpersonen, die den Organisationseinheiten (und damit deren Zuständigkeiten) direkt untergeordnet sind, selbst aber auch noch einmal ein Element `zustaendigkeit` vom Typ ZustaendigkeitPerson tragen, das die Zuständigkeit für eine Leistung in einem Gebiet nach optionalen Kriterien attributiert, aber nicht nach Rollen. Da die Daten der Kontaktpersonen auch Onlinedienst-Parameter sind, ist dieser Aspekt ebenfalls relevant.

### Mehrere Leistungen für ein Gebiet gültig
Es ist denkbar, dass bei der Suche nach einer Leistungsbeschreibung im PVOG mit einem zusammengesetzten Suchschlüssel aus Leistungsschlüssel (ehemals Leika-Schlüssel) und Gebietsschlüssel (i.e. ARS) mehrere Datenobjekte gefunden werden, die dem XZuFi-Datentyp Leistung bzw. dem PVOG Datentyp Leistungsbeschreibung entsprechen.
In diesem Konzept soll davon ausgegangen werden, dass eine solche multiple Gültigkeit von mehreren Leistungen in einem Gebiet für einen Leistungsschlüssel entweder 
* ein fachlicher Spezialfall ist, d.h. in dieser Form nur bei dieser Leistung auftreten kann und dann über weitere fachliche Unterschiede zwischen den konkurrierenden Leistungsbeschreibungen aufgelöst werden muss, oder
* ein Konfigurationsfehler ist.

Die korrekte Behandlung für dieses Szenario besteht also darin, dass der Hersteller des Onlinedienstes bereits in der Anforderungsanalysephase klären muss, ob es fachlich denkbar ist, für den Leistungsschlüssel seines Onlinedienstes in irgendeinem Gebiet mehrere Leistungsbeschreibungen zu finden und wie anhand der Datenlage der gefundenen Leistungsbeschreibungen dann die zutreffende Leistungsbeschreibung für den gewünschten Antrag des Benutzers ausgewählt werden kann. Dieses Auswahlverfahren muss der Hersteller dann in seinem Onlinedienst implementieren.
Falls eine solche multiple Gültigkeit von Leistungen fachlich nicht auftreten kann, muss der Onlinedienst im Falle mehrerer Treffer den Antragsprozess mit einer Fehlermeldung abbrechen.

### Mehrere Onlinedienst-Datenobjekte sind zuständig

Nach der eindeutigen Identifikation der Leistung bzw. Leistungsbeschreibung wird der Onlinedienst in der Regel das zuständige Datenobjekt Onlinedienst aus dem PVOG auslesen, um seine Parametrisierung weiter zu vervollständigen. Auch in diesem Fall ist es denkbar, dass eine Abfrage am PVOG mit der ID der zuvor identifizierten Leistungsbeschreibung und dem ARS mehrere Datenobjekte vom Typ OnlinedienstErweitert liefert.
Diese Situation soll stets als Fehlerkonfiguration gedeutet werden. Tritt sie auf, muss der Onlinedienst den Antragsprozess mit einer Fehlermeldung abbrechen.

### Mehrere Organisationseinheit-Datenobjekte sind zuständig

Nach der Ermittlung des zuständigen Onlinedienst-Datenobjekts wird der Onlinedienst die zuständige Organisationseinheit bestimmen und die damit verbundenen Parameter ermitteln. Es ist möglich, dass hierbei mehrere Datenobjekte als zuständig gemeldet werden.

In solchen Fällen sollten die Mechanismen zur Auswahl der richtigen Organisationseinheit genutzt werden, wie sie in der [Ermittlung der zuständigen Organisationseinheit mit der PVOG-API](./6_9_ermittlung_zuständige_organisationseinheit_im_pvog.md) detailliert beschrieben sind. Dieser Leitfaden erläutert, wie Zuständigkeitsrollen und Kriterien analysiert und präsentiert werden sollten, um die korrekte Organisationseinheit für den Antragsprozess auszuwählen.

Falls der Onlinedienst mehrere zuständige Organisationseinheiten identifiziert, muss der Antragsteller diese selbst auswählen, basierend auf den bereitgestellten Informationen. Weitere Details und Best Practices dazu finden sich in der verlinkten Dokumentation.

### Mehrere Kontaktpersonen sind zuständig

Ähnlich wie bei den Organisationseinheiten können auch die Zuständigkeiten von Kontaktpersonen über die Leistungen und Gebiete hinaus durch zusätzliche Kriterien attributiert werden.
Auch hier greifen dieselben Mechanismen und Lösungsmöglichkeiten. Daher sind im Falle mehrerer zuständiger Kontaktpersonen diese dem Antragsteller zur Auswahl zu präsentieren, falls Kriterien angegeben sind. 

### Sonderfall HaSi in Hamburg 

PVOG für Hamburg gibt aus HaSI fast immer mehrere XZuFi-OEs zurück (in HaSI-Notation eigentlich „Ansprechpunkte“, die unter der OE verortet sind und nicht den XZuFi-Ansprechpunkten entsprechen). Um die passende OE auszuwählen, ist unser Vorschlag, eine XZuFi-OE als zentraler Bearbeitungs-/Zustell-Punkt für Online-Anfragen zu kennzeichnen. D.h. an der betreffenden zuständigen OE würde ein Attribut eingefügt werden, das kennzeichnet, für welche „Vertriebskanäle“ die OE zuständig ist. Möglicher Wertebereich: [Offline, Onlinedienst, telefonisch, …], Mehrfachauswahl möglich. (Ggf. reicht für den Anfang auch nur die Angabe, für welchen OD konkret die OE zuständig ist.)

### Sonderfall Verpflichtende Beantragung vor Ort
Im Falle, dass bestimmte Leistungen ausschließlich vor Ort beantragt werden können, ist auch hier eine Vorqualifikation erforderlich. Die Auswahl der zu beantragenden Leistung zeigt in diesem Falle die dem Wohnort des Antragstellers nächstgelegene zuständige Stelle an. Dies ist aber eher ein Aspekt der Funktion von Portalen und weniger relevant für die Frage der Parametrisierung von Onlinediensten, da in diesem Fall für diese Leistung (ggf. eingeschränkt auf dieses Gebiet) kein Onlinedient zuständig sein sollte.

## Fazit

Die Problematik mehrfacher Zuständigkeit lässt sich in der Regel unter Verwendung der von XZuFi 2.2 angebotenen Mechanismen gut lösen. Der Umgang mit – effektiv frei wählbaren – Kriterien ist problematisch, sofern der Anspruch besteht, den Auswahlmechanismus vollständig zu automatisieren. Hier sollte der Antragsteller durch die Präsentation eine Liste möglicher Alternativen zusammen mit den Ausprägungen der Zuständigkeitskriterien einbezogen werden.
