---
id: internationalisierung
title: "Internationalisierung"
---


:::info[Version]

1.0.0

:::

# Internationalisierung
Sowohl XZuFi als auch die APIs der FITKO Dienste unterstützen in einigen Fällen die Internationalisierung im Zuge der Parametrisierung. Dies kann sich auf einzelne Zeichenketten (i.e. Bezeichnungen, Texte und Textabschnitte) aber auch auf ganze Datenstrukturen beziehen, bei denen auch die mit der Zeichenkette inhaltlich verknüpften (Meta-)Daten sprachabhängig sind.

Stand Q2/2024 gibt es kein durchgängiges Internationalisierungskonzept für die APIs der FITKO. In diesem Zusammenhang werden aber verschiedene Muster immer wieder auf dieselbe Art angewendet, so dass ihre Nutzung hier nur einmal dokumentiert werden soll.

## Internationalisierte Zeichenkette
XZuFi kennt den Datentyp xzufi:String.Localized. Dieser Typ ist eine Erweiterung des Basistyps xs:string um das zusätzliche Attribut `languageCode`, das wiederum vom Typ xs:language ist. Dieses Attribut kann jeweils als Wert einen Code der Sprache des Texts annehmen, abgebildet mit [W3C-Datentyp language](https://www.w3.org/TR/xmlschema-2/#language). Zum Beispiel "en", "en-US", "de" oder "de-DE".

Wird ein Parameter, z.B. der Name der Organisationseinheit als internationalisierte Zeichenkette in XZuFi modelliert, so gibt es dort z.B. ein Element `name[]` vom Typ String.Localized und der Kardinalität 0…* oder 1…*. Dabei sollten alle diese `name` -Elemente verschiedene `languageCode`-Attribute haben.

Die PVOG Suchdienst-API bildet diese Datenstruktur in der Regel ab auf ein Array von `StringLocalized`-JSON-Datenstrukturen, die wie folgt strukturiert sind:

```JSON
"StringLocalized": {
  "type": "object",
    "properties": {
    "value": {
      "type": "string"
    },
    "languageCode": {
      "type": "string"
    }
  }
}
```

Ein Beispiel für eine internationalisierte Zeichenkette in der PVOG Suchdienst-API ist
```JSON
"inhalt": [
  {
    "value": "Engineer: Permission to take the professional title",
    "languageCode": "en"
  },
  {
    "value": "Ingenieur / Ingenieurin: Erlaubnis zum Führen der Berufsbezeichnung",
    "languageCode": "de"
  }
]
```

Es ist die Aufgabe des Onlinedienstes, aus allen `StringLocalized`-Elementen dasjenige zu identifizieren, dessen `languageCode`-Element mit der Sprachpräferenz des Antragstellers am besten übereinstimmt und dann das korrespondierende `value`-Element als Text zu verwenden.

Das FIT-Connect Routing API geht bei der encodierung internationalisierter Texte in der Regel einen anderen Weg. Hier gibt es ein Objekt mit einem `description`-Element, das pro Sprach-Code ein entprechend benanntes Element enthält. Beispiel:
```JSON
"inhalt": {
  "description": {
    "de": "Ingenieur / Ingenieurin: Erlaubnis zum Führen der Berufsbezeichnung",
    "en": "Engineer: Permission to take the professional title"
  }
}
``` 

## Internationalisierte Datenstrukturen
XZuFi kennt neben dem Datentyp xzufi:String.Localized weitere internationalisierte Datentypen. Das Architekturmuster ist hier dasselbe: Es werden Arrays des Basis-Datentyps aufgebaut, wobei der Basis-Datentyp um ein Attribut `languageCode` erweitert wird. 

Bei der Erstellung eines internationalisierten Elements muss dann darauf geachtet werden, dass je `languageCode`-Wert nur ein Array-Element erstellt wird.

Bei der Auswertung eines internationalisierten Elements muss dasjenige Array-Element weiterverarbeitet werden, dessen `languageCode`-Wert am besten zu der gewünschten Sprache des Antragstellers passt.

Die Umsetzung dieser Datentypen in den FITKO-APIs ist jedoch uneinheitlich. So wird z.B. das Element _durchfuehrungSprachen_ des _Onlinedienst_-Datenobjekts durch die Suchdienst-API nur als String-Array `executionLanguages` an den Onlinedienst weitergegeben. Die API enthält damit nicht mehr die strukturierten Daten, sondern nur noch den  `languageCode`-Wert.

Die Datenstruktur `HyperlinkErweitert` wird hingegen dem Muster aus der XZuFi-Spezifikation folgend in der PVOG Suchdienst-API weitergegeben (verkürzte Darstellung des OpenAPI schema):
```JSON
      "Bearbeitungsdauermodul": {
        "type": "object",
        "properties": {
          "positionDarstellung": {
            "type": "integer",
            "format": "int32",
          },
          "id": {
            "$ref": "#/components/schemas/Identifikator"
          },
          "idSekundaer": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Identifikator"
            }
          },
          "gueltigkeit": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Zeitraum"
            }
          },
          "bearbeitungsdauer": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/FristOhneTyp"
            }
          },
          "beschreibung": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/StringLocalized"
            }
          },
          "weiterfuehrenderLink": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/HyperlinkErweitert"
            }
          }
        }
      } 
…
      "HyperlinkErweitert": {
        "required": [
          "uri"
        ],
        "type": "object",
        "properties": {
          "uri": {
            "type": "string",
          },
          "titel": {
            "type": "string",
          },
          "beschreibung": {
            "type": "string",
          },
          "positionDarstellung": {
            "type": "integer",
            "format": "int32",
          },
          "languageCode": {
            "type": "string",
          }
        }
      }
```

:::warning

Das Elemente vom Datentyp `xzufi:HyperlinkErweitert[]` bezitzen sowohl ein Attribut `languageCode`für ihre Internationalisierung als auch ein Element `positionDarstellung`. Mit Hilfe dieser Struktur lässt sich in einem `xzufi:HyperlinkErweitert[]`-Element eine Liste internationalisierter Links aufbauen: Möchte man z.B. Links zu zwei verschiedenen weiterführenden Informations-Seiten anbieten, so kann man diese beiden Links durch unterschiedliche Werte im Element `positionDarstellung` separieren, z.B. `1` und `2`. Liegen diese Informations-Seiten jeweils auch noch in verschiedenen Sprachen vor, so kann die Menge der `xzufi:HyperlinkErweitert[]`-Elemente weiter vervielfältigt werden, wobei jede Variante dann mit demselben `positionDarstellung`-Wert, aber einem anderen `languageCode`-Wert versehen wird, z.B. `de` und `en`.

Weil die Möglichkeit einer solchen Nutzung in XZuFi besteht, wird empfohlen, dass der Onlinedienst die Menge aller `xzufi:HyperlinkErweitert[]`-Elemente jedes separaten übergeordneten-Elements zunächst nach den verschiedenen vorgefundenen `positionDarstellung`-Werten sortiert. Die Anzahl der so gebildeten Teilmengen ist die Anzahl der darzustellenden weiterführenden Links dieses übergeordneten Elements. In einem zweiten Schritt nutzt der Onlinedienst dann aus jeder dieser Teilmengen dasjenige `xzufi:HyperlinkErweitert[]`-Element, dessen `languageCode`-Wert die beste Übereinstimmung mit der gewünschten Sprache bietet.

<!-- 
Teleport schrieb am 19.6.2024:
Generell sind die Positionsangaben an Klassen im Standard optional. D.h. auch ohne diese, muss eine Ausgabe fachlich korrekt sein. Es handelt sich jedoch um eine Angabe zur Steigerung der Nutzerfreundlichkeit. Wenn ich als Nutzer der Daten diese darstellen möchte, würde ich einfach „stupide“ danach sortieren. Also ja, es können sich Positionselemente doppeln und das aufgeführte Beispiel finde ich fachlich so sinnvoll.
-->

:::

## Internationalisierung von Individual-Parametern
In XZuFi 2.2 existiert keine Unterstützung für eine Internationalisierung von [Individual-Parametern](../kategorisierung_der_parametrisierung.md#xzufi-basierte-individual-parameter-mit-direktem-onlinedienst-bezug). Es gibt jedoch eine einfache Möglichkeit, über eine Namenskonvention zu einem Internationalisierungsmechanismus zu kommen: Falls ein Onlinedienst einen Individual-Parameter internationalisiert nutzen möchte, so wertet er bei entsprechender Spracheinstellung nicht nur den regulär definierten Suchschlüssel aus, sondern auch den Suchschlüssel mit angehängtem `.` und Sprach-Code. Definiert der Onlinedienst Beispielsweise den Individual-Parameter `logo.alternativeBezeichnung` und hat der Antragsteller die Sprache Türkisch eingestellt, so sucht der Onlinedienst zuerst nach dem Individual-Parameter `logo.alternativeBezeichnung.tr`. Ist dieser Individual-Parameter gesetzt, so verwendet der Onlinedienst dessen Wert statt des Wertes des regulär definierten Parameters. Das Verfahren lässt sich auch auf die [Standard Parameter, die über die generischen _Onlinedienst.parameter_-Elemente als Namens-Schlüssel/Wert-Paare implementiert sind](../kategorisierung_der_parametrisierung.md#xzufi-basierte-standard-parameter-mit-direktem-onlinedienst-bezug), anwenden, für die vor der Migration nach XZuFi 2.3 keine Internationalisierung vorgesehen ist.

Diese Festlegung hat verschiedene Vorteile:
- Sie lässt sich leicht nachvollziehen.
- Sie ist rückwärtskompatibel: Unabhängig davon, ob die Redakteure die Individual-Parameter internationalisiert setzen oder nicht, und unabhängig davon, ob der Onlinedienst diese Form der Internationalisierung unterstützt oder nicht, wird der Onlinedienst immer die deutschen Bezeichnugen bzw. Varianten als Rückgriff (Fallback) verwenden.
- Sie lässt sich in einer Zugriffschicht für Parameter generisch implementieren. Diese kann die genannte Form der Internationalisierung an genau einer Stelle im Code umsetzen, die dann für alle Parameter angewendet wird.

:::info

Mit der Migration anch XZuFi 2.3 werden sowohl die Elemente _parameterEfa_ als auch _parameterIndividuell_ jeweils ein Element _languageCode_ mit dem Typ `xs:language` haben, so dass die Internationalisierung dann innerhalb von XZuFi und innerhalb der PVOG / FIT-Connect APIs umgesetzt werden kann. In XZuFi 2.3 können dann auch einem einzelnen Parameter-Wert mehrere _languageCode_-Werte zugeordnet werden, so dass nicht für jede Sprache ein separater Wert gepflegt werden muss, sondern z.B. zwei "Sprachklassen" gebildet werden können, die jeweils einen Parameter-Wert zugeordnet bekommen. 

:::