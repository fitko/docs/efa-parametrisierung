---
id: ermittlung_zuständiger_onlinedienst_im_pvog
title: "Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API"
---


:::info[Version]

1.0.0

:::

# Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API

Um die Daten eines _Onlinedienst_-Datenobjektes mit Hilfe der PVOG Suchdienst API zu ermitteln ist wie folgt vorzugehen:

**1. Teilschritt: Ermittlung der ID der Leistungsbeschreibung**

Siehe Abschnitt [Ermittlung der ID der Leistungsbeschreibung](6_2_ermittlung_der_id_der_leistungsbeschreibung.md)

**2. Teilschritt: Ermittlung der ID des _Onlinedienstes_**

Der Onlinedienst benutzt den Endpunkt [`/v2/relations/{ars}/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Zustandigkeiten/operation/getZsTrByLbIdAndArs) mit den URL-Parametern `ars` = \<der ARS\> und lbids = \<die in Schritt 1 ermittelte ID der Leistungsbeschreibung\>. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Angaben auf diejenigen Top-Level-Elemente zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind.

:::info

Als Alternative zur Verwendung des Endpunktes [`/v2/relations/{ars}/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Zustandigkeiten/operation/getZsTrByLbIdAndArs) bietet es sich an, die Response eines Aufrufs des Endpunktes [`/v5/servicedescriptions/{ars}/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getSdOutModelV5) auszuwerten. Dies gilt speziell dann, wenn dieser Endpunkt ohnehin bereits benutzt wurde, um Details der Leistungsbeschreibung zu ermitteln.

Die Response dieses Endpunktes enthält das Array `onlineServiceLinks[]` und darin wiederum `onlineServiceLinks[]:id`, das die ID des _Onlinedienstes_ enthält.

Der Endpunkt [`/v5/servicedescriptions/{ars}/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getSdOutModelV5) filtert jedoch mindestens diese Liste der _Onlinedienste_ nach der gewünschten Sprache, die beim Aufruf im Header `accept-Language` übergeben wurde. Wenn ein _Onlinedienst_ nicht die in diesem Header angegebene Sprache in seiner Angabe _sprachversion_ eingetragen hat, so wird er aus `onlineServiceLinks[]` ersatzlos gestrichen. Insbesondere wird **nicht** eine anders-sprachige Variante des _Onlinedienstes_ als Fallback für die gewünschte Sprache aufgeführt.

Der Endpunkt [`/v5/servicedescriptions/{ars}/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getSdOutModelV5) sollte daher zur Identifikation des _Onlinedienstes_ nur dann genutzt werden, wenn sichergestellt ist, dass der  `accept-Language` Header nicht mitgesendet wird oder die Sprache `de` enthält. Nur für Deutsch kann sicher angenommen werden, dass die Daten vorhanden sind.

:::

Die Response enthält ein Element `content[]:zustaendigkeit[]`,dessen Element `id:schemeID` den Wert "`ZustaendigkeitOnlinedienst`" hat. Dieses Element wird im Weiteren ausgewertet.

Zur Bestimmung der ID sind zwei Wege möglich:

**2a. Bestimmung der OnlinedienstID aus den Elementen der `zustaendigkeit`**

Das Element `content[]:zustaendigkeit[]:id:value` enthält als Wert einen String aus drei Segmenten, jeweils durch einen Punkt „`.`“ getrennt:	
\<LeistungId\>.\<OnlinedienstShortId\>.\<ARS\>

Daneben findet sich ein Element `content[]:zustaendigkeit[]:id:schemeAgencyId` dessen Wert \<SchemaAgencyId\> benötigt wird.

Die ID des _Onlinedienstes_ ergibt sich nun durch Konkatenation aus drei Bestandteilen mit dem Trennzeichen „`.`“:  
OnlinedienstID = \<SchemaAgencyId\>`.OD.`\<OnlinedienstShortId\>

**2b. Bestimmung der OnlinedienstID aus den Elementen der `uebergeordnetesObjektId`**

Das `content[]`-Element, dessen `zustaendigkeit[]` ein Element enthält, dessen Element `id:schemeID` den Wert "`ZustaendigkeitOnlinedienst`" hat, enthält neben dem `zustaendigkeit[]`-Element auch ein `uebergeordnetesObjektId`-Element. Die Werte \<SchemaAgencyId\> des Elements `uebergeordnetesObjektId:schemeAgencyId` und \<OnlinedienstShortId\> des Elements `uebergeordnetesObjektId:value` werden benötigt, um die OnlinedienstID zu bilden:

Die ID des _Onlinedienstes_ ergibt sich wieder durch Konkatenation aus drei Bestandteilen mit dem Trennzeichen „`.`“:  
OnlinedienstID = \<SchemaAgencyId\>`.OD.`\<OnlinedienstShortId\>

**3. Teilschritt: Ermittlung der der Daten des zuständigen _Onlinedienstes_**

Mit dieser OnlinedienstID kann dann über den Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) das _Onlinedienst_-Datenobjekt ausgelesen werden. 

:::warning[Hinweis]

Der oben dargestellte Ablauf kann mehrere _Onlinedienst_-Datenobjekte liefern, die alle für die Leistungsbeschreibung im Gebiet des ARS zuständig sind. Dies kann insbesondere dann eintreten, wenn "Hilfsdienste" wie ein Onlinedienst zur Terminvereinbarung im PVOG für diese Leistung in diesem Gebiet hinterlegt sind.

In diesem Fall muss der Onlinedienst alle identifizierten _Onlinedienst_-Datenobjekte vollständig auslesen und bei jedem dieser Datenobjekte den Parameter [idOnlinedienstGlobal](../parameter_katalog/18.4_onlinedienst_idGlobal.md) mit der eigenen Produkt-ID abgleichen. Nur genau eines der zuständigen _Onlinedienst_-Datenobjekt sollte eine passende [idOnlinedienstGlobal](../parameter_katalog/18.4_onlinedienst_idGlobal.md) haben.

Dies macht deutlich, dass die Eintragung von "Hilfsdiensten" wie Terminfindungsdiensten zwingend nach sich zieht, dass der Parameter [idOnlinedienstGlobal](../parameter_katalog/18.4_onlinedienst_idGlobal.md) für den eigentlichen Antragsdienst im Redaktionssystem gepflegt wird und dass der Onlinedienst (i.e. das IT-System) diesen Parameter dann auch wie hier beschrieben auswerten muss.

:::

:::warning

Auf die Angabe des zusätzlichen URL-Parameters `validDate` kann hier verzichtet werden, wenn bereits zu Beginn von Schritt 2 sichergestellt wurde, dass nur eine ID des _Onlinedienstes_ identifiziert wurde, die auch zum gewünschten Zeitpunkt gültig ist. Die Angabe von `validDate` beim Aufruf von [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) entfernt Stand Q2/2024 **nicht** die Unter-Elemente des _Onlinedienstes_ mit eigener `gueltigkeit` wie z.B. `relations`, die zum angegebenen Zeitpunkt nicht gültig sind. Diese Prüfung und Datenbereinigung muss der Onlinedienst selbst durchführen.

:::


<!-- 

Antwort (aus dem PVOG-Team 25.6.2024): Selbst wenn man Relation Endpunkt nutzen möchte (aus welchem Grungd auch immer) muss die ID des _Onlinedienstes_ nicht so hergestellt werden wie es im Doku beschrieben ist. Bsp:   https://pvog.fitko.de/suchdienst/api/v2/relations/010020000000/jzufi?lbids=B100019.LB.102713899&validDate=2024-06-25
Es gibt ein Element „uebergeordnetesObjektId“ dieser enthält Informationen zum berechnen der ID des _Onlinedienstes_

-->




