---
id: ermittlung_daten_der_leistungsbeschreibung_im_pvog
title: "Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API"
---


:::info[Version]

1.0.0

:::

# Ermittlung der Daten der Leistungsbeschreibung mit Hilfe der PVOG-API

Um die Stammdaten der Leistung mit Hilfe der PVOG Suchdienst API zu ermitteln ist wie folgt vorzugehen:

**1. Teilschritt: Ermittlung der ID der Leistungsbeschreibung**

Siehe Abschnitt [Ermittlung der ID der Leistungsbeschreibung](6_2_ermittlung_der_id_der_leistungsbeschreibung.md)

**2. Teilschritt Ermittlung der Daten der Leistungsbeschreibung**

Über den Endpunkt [`/v2/servicedescriptions/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getJzufiLbV2) mit dem URL-Parameter `q` = \<die eben ermittelte LeistungsbeschreibungsID\> ermittelt der Onlinedienst dann die Stammdaten der Leistung.

In der Response finden sich die Daten der Leistungsbeschreibung.

:::warning

Auf die Angabe des zusätzlichen URL-Parameters `validDate` kann hier verzichtet werden, wenn bereits in Schritt 1 sichergestellt wurde, dass nur eine ID der Leistungsbeschreibung identifiziert wurde, die auch zum gewünschten Zeitpunkt gültig ist. Die Angabe von `validDate` beim Aufruf von [`/v2/servicedescriptions/jzufi`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Leistungsbeschreibungen/operation/getJzufiLbV2) entfernt Stand Q2/2024 **nicht** die Unter-Elemente der Leistungsbeschreibung mit eigener `gueltigkeit` wie z.B. `modulText`, die zum angegebenen Zeitpunkt nicht gültig sind. Diese Prüfung und Datenbereinigung muss der Onlinedienst selbst durchführen.

:::
