---
id: verortung_xzufi_parameter_mit_bezug_im_pvog
title: "Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell"
---


:::info[Version]

1.0.0

:::

# Verortung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug im PVOG Datenmodell

Stand Q2/2024 werden alle generischen[^1] XZuFi-basierten Standard-Parameter mit direktem Onlinedienst-Bezug in einem Element _parameter_ (Datentyp `xzufi:OnlinedienstParameter`) abgelegt, das Teil des _Onlinedienst_-Datenobjekts ist. Dieses besitzt wiederum ein Element _zustaendigkeit_ für das Gebiet (i.e. den ARS) und die Leistungsbeschreibung, damit die gewünschte _Onlinedienst_-Instanz identifiziert werden kann.

Der _Onlinedienst.parameter_ muss dabei im Element _parameterName_ den Wert des Namens-Schlüssels tragen. Dann enthält das Element _parameterWert_ den Wert des Parameters.

:::info

Mit der Migration zu XZuFi 2.3 werden solche Parameter in einem analogen Element _parameterEfa_ (Datentyp `xzufi:OnlinedienstParameterEfa_`) abgelegt. Dieses Element enthält ein inneres Element, das ebenfalls _parameterEfA_ heißt (Datentyp `xzufi:Code.EfAParameter`) und denselben Namens-Schlüssel enthalten muss, der auch bereits in XZuFi 2.2 verwendet wurde.
In XZuFi 2.3 besteht aber die formelle Restriktion, dass für die XZuFi-basierten _Standard_-Parameter nur solche Namens-Schlüssel verwendet werden dürfen, die in der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.efaparameter`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.efaparameter) aufgeführt sind.

:::

[^1]: Ein Beispiel für generische XZuFi-basierten Standard-Parameter ist [Link zur regionalen Datenschutzerklärung](../parameter_katalog/20_link_zur_regionalen_datenschutzerklärung.md), da dieser durch ein Namens-Schlüssel/Wert-Paar realisiert ist, wobei Namens-Schlüssel- und Wert-Element im Prinzip beliebige Zeichenketten aufnehmen können. Daher dann die Datenstruktur des Namens-Schlüssel/Wert-Paares für nahezu beliebige Zwecke verwendet werden. Dagegen ist [Zahlungsweisen](../parameter_katalog/10_zahlungsweisen.md) ein Beispiel für _nicht_-generische XZuFi-basierte Standard-Parameter mit direktem Onlinedienst-Bezug: Für diesen Parameter existiert in XZuFi 2.2 ein spezifisches Element _zahlungsweisen_ am _Onlinedienst_-Datenobjekt, das nur für den Zweck vorgesehen ist, Zahlungsweisen zu signalisieren.
