---
id: ermittlung_xzufi_parameter_mit_bezug_im_pvog
title: "Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API"
---


:::info[Version]

1.0.0

:::

# Ermittlung von generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug mit Hilfe der PVOG-API

Die generischen XZuFi-basierten Standard-Parametern mit direktem Onlinedienst-Bezug können mit Hilfe der PVOG-API alle auf dieselbe Weise ermittelt werden. Es unterscheidet sich jeweils nur der Namens-Schlüssel des Parameters.

Die Ermittlung erfolgt in mehreren Schritten:

**1. Teilschritt: Ermittlung des zuständigen _Onlinedienst_-Datenobjektes**

Siehe dazu [Ermittlung eines zuständigen _Onlinedienst_-Datenobjektes mit Hilfe der PVOG-API](6_4_ermittlung_zuständiger_onlinedienst_im_pvog.md)

**2. Teilschritt: Ermittlung des encodierten Parameter-Wertes**

Der Onlinedienst benutzt den Endpunkt [`/v2/onlineservices/detail`](https://anbindung.pvog.cloud-bdc.dataport.de/docs/api/suchdienst-api/#tag/Onlinedienste/operation/getDetailOdV2) mit dem URL-Parameter `q` = \<in Teilschritt 1 ermittelte ID des _Onlinedienst_ Datenobjektes\>. Zusätzlich muss der Onlinedienst einen `validDate` URL-Parameter angeben, um die Angaben auf diejenigen Elemente zu beschränken, die _zum angegebenen Zeitpunkt_ (in der Regel das Datum der Antragstellung) gültig sind.  

In der JSON-Datenstruktur der Response enthält das Element `parameters[]:value` den Wert des Parameters, falls das korrespondierende Element `parameters[]:name` den Namens-Schlüssel enthält. Für manche Parameter ist vorgesehen, dass sie mehrfach vorhanden sein können. Daher muss der Onlinedienst für diese potenziell mehrfach vorhandenen Parameter _alle_ `parameters[]`-Elemente auswerten, deren  `name` mit dem gesuchten Namens-Schlüssel übereinstimmt.  

**3. Teilschritt: Parsen des encodierten Wertes**

Falls der Parameter nicht vom Typ string ist, muss die in Schritt 2 ermittelte Zeichenkette mit einem Datentyp-spezifischen Parser geparst werden. So muss z.B. aus der Zeichenkette „`Ja`“ ein `Boolean true` gebildet werden, der den eigentlichen, getypten Parameter-Wert darstellt.

Dabei auftretende Parsing-Fehler müssen vom Onlinedienst als Fehlkonfigurationen  behandelt werden.

