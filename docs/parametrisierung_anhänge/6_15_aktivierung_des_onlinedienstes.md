---
id: aktivierung_des_onlinedienstes
title: "Aktivierung des Onlinedienstes"
---

# Aktivierung des Onlinedienstes

## Einführung
Die Produktivsetzung eines Onlinedienstes ist, wie auch die Außerdienststellung, ein komplexer Prozess, der zudem nicht in allen Gebieten gleichzeitig voranschreitet. Vorbereitende Schritte wie die Software-Installation, die Konfiguration des empfangenden Fachverfahren sowie die Erstellung eines Parameter-Sets in PVOG und DVDV durch die Nutzung der Redaktionssysteme und durch die Beauftragung der pflegenden Stellen des DVDV müssen koordiniert und ihre Ergebnisse qualitätsgesichert werden, bevor der eigentliche produktive Betrieb beginnen kann.
Um diesen Prozess besser managen zu können, wäre es hilfreich, Steuerdaten oder Statusinformationen im PVOG vorhalten zu können, die sowohl das Verhalten von Auskunftssystemen (i.e. Portalen) als auch des Onlinedienstes selbst steuern können.
In diesem Konzept soll dargestellt werden, wie die Prozesse der Lebenszyklussteuerung durch die Verwendung solcher Steuerinformationen unterstützt werden können. Die Seite [Onlinedienst Status](../parameter_katalog/18.3_onlinedienst_status.md) geht vertieft auf die Möglichkeiten zur Ermittlung des Parameters ein.

## Rahmenbedingungen
Folgende Rahmenbedingungen gelten für die Umsetzung der Lebenszyklussteuerung und müssen berücksichtigt werden: 
- Der EfA-Onlinedienst kann sich – sofern er vollständig installiert und betriebsbereit ist – für verschiedene Gebiete in unterschiedlichen Phasen des Lebenszyklus befinden. Während er beispielsweise für Gebiet A bereits produktiv ist, kann er für Gebiet B in Wartung sein und für Gebiet C in Initialisierung stehen. Dasselbe gilt für verschiedene Leistungen, falls der Onlinedienst mehrere Leistungen (i.e. Leistungsschlüssel) unterstützt.
- Durch die Verwendung alter URLs, die z.B. veralteten Webseiten Dritter entnommen werden, oder durch Manipulation von URLs kann ein vollständig installierter und betriebsbereiter Onlinedienst für ein Gebiet und eine Leistung aufgerufen werden, selbst wenn er für dieses Gebiet und diese Leistung zu diesem Zeitpunkt nicht produktiv eingesetzt werden soll.
- XZuFi 2.2 bietet keine spezifische Unterstützung für die Ablage von Statusinformationen, die den Lebenszyklus des Onlinedienstes betreffen.
- XZuFi 2.3 bietet mit dem verpflichtenden Element `Onlinedienst.status` einen Mechanismus, um den Lebenszyklus-Status zu hinterlegen.

## Anforderungen
Es sollen Verfahren und Vorgehensweisen definiert werden, die es erlauben, Onlinedienste geregelt durch ihren Lebenszyklus, wie er durch das Statusmodell der Code-Liste [`urn:xoev-de:fim:codeliste:xzufi.onlinedienststatus`](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:xzufi.onlinedienststatus)  definiert ist, zu steuern.

Die Phasen des Lebenszyklus sind:
|Code: | Bezeichnung: | Beschreibung: |
|------|--------------|---------------|
|00 | unbestimmt | Der Status ist nicht bekannt.|
|10 | Planung | Der Onlinedienst ist geplant bzw. soll demnächst eingerichtet werden.|
|20 | Initialisierung | Der Onlinedienst wird durch den IT-Dienstleister für die Behörde eingerichtet/konfiguriert.|
|30 | Test | Der Onlinedienst wurde für die Behörde eingerichtet/konfiguriert und wird getestet.|
|40 | Produktion | Der Onlinedienst wurde freigegeben und kann genutzt werden. Es können Anliegen an die Behörde übermittelt werden.|
|50 | Wartung | Der Onlinedienst ist wegen Wartungsarbeiten längere Zeit nicht nutzbar.|
|99 | Deaktivierung | Der Onlinedienst wird durch die Behörde nicht bereitgestellt und ist deaktiviert.|


Ein Statusübergangsmodell ist nicht definiert, d.h. der Onlinedienst kann aus jedem Status kommend in jeden anderen Status wechseln.

## Lösungsvorschlag
### Signalisierung des Status
Die Befüllung des Elements `Onlinedienst.status` wird mit der Migration zu XZuFi 2.3 verpflichtend werden. Bis zur Migration kann ein Onlinedienst-bezogener Individual-Parameter  die Aufgabe dieses Elements übernehmen, soll aber aus Gründen des Bestandsschutzes bis dahin nicht als verpflichtender Parameter betrachtet werden. Weil mit der Migration zu XZuFi 2.3 das Element Onlinedienst.status eingeführt wird, wird auch darauf verzichtet, einen Schlüssel in die Code-Liste `urn:xoev-de:fim:codeliste:xzufi.efaparameter` aufzunehmen. 
Es wird somit festgelegt:
* Der Status des Onlinedienst IT-Systems in seinem Lebenszyklus wird durch ein Element im Onlinedienst-Datenobjekt im PVOG signalisiert.
* Die Angabe des Status bezieht sich nur auf die Leistung und das Gebiet, auf das sich auch das Element Onlinedienst.zustaendigkeit am Onlinedienst-Datenobjekt bezieht. Wenn das Onlinedienst-Datenobjekt eine zeitlich eingeschränkte Gültigkeit hat, so ist auch die Angabe des Status entsprechend zeitlich eingeschränkt.
* Vor der Migration zu XZuFi 2.3 wird dazu der Onlinedienst-bezogene Individualparameter mit dem Schlüssel „efa.custom.onlinedienst.status“. Der Wert dieses Parameters entspricht dem Code aus der Code-Liste` urn:xoev-de:fim:codeliste:xzufi.onlinedienststatus`, z.B. `30` für Test. Die Angabe ist nicht verpflichtend.
* Nach der Migration zu XZuFi 2.3 wird dazu das Element Onlinedienst.status am Onlinedienst-Datenobjekt verwendet. Die Angabe ist dann verpflichtend.

### Auswertung durch Portale und andere Auskunftssysteme

Jedes Auskunftssystem muss durch seinen fachlichen Kontext entscheiden, wie es auf Status-Angaben reagiert, die von `40`/"Produktion" abweichen. Für die typischen Landesportale bietet es sich an, Informationen über Onlinedienste nur dann dem Benutzer zu präsentieren, wenn der Status `40`/"Produktion" oder `50`/"Wartung" ist, in letzteren Fall mit dem Hinweis, dass der Onlinedienst aktuell nicht verfügbar ist.
Auskunftssysteme für Support-Mitarbeiter sollten hingegen jeden Status transparent anzeigen und immer alle auffindbaren Onlinedienst-Datenobjekte dem Benutzer präsentieren.
Wieder andere Typen von Auskunftssystemen können andere Regeln definieren.

### Auswertung durch den Onlinedienst selbst

Auch das Onlinedienst IT-System sollte den Parameter auswerten und damit seine eigene Geschäftslogik steuern. So sollte der Onlinedienst bei jedem vorgefundenen Status, der von `40`/"Produktion" abweicht, ein deutlich sichtbares Warn-Banner anzeigen, dass er aktuell nicht produktiv nutzbar ist und keine Anträge wirksam verschickt werden können.
Darüber hinaus sollte der Onlinedienst nur dann einen Antrag versenden, wenn er den Status `30`/"Test" oder `40`/"Produktion" im Onlinedienst-Datenobjekt vorfindet.

Das testweise Versenden von Daten in einer produktiven Umgebung und an einen produktiven Empfänger birgt immer Risiken und sollte nur dann geschehen, wenn entsprechende Sicherheitsmaßnahmen ergriffen wurden, um die produktive Weiterverarbeitung der Test-Sendungen zu verhindern. Daher sollte mindestens einer der folgenden Punkte gelten:

* Das empfangende Fachverfahren ist für die Dauer der Testphase so konfiguriert, dass es grundsätzlich alle empfangenen Sendungen nach einer Prüfung verwirft. Eine produktive Weiterverarbeitung findet nicht statt.
* Der Onlinedienst kennzeichnet die Sendungen als Test-Sendungen, wenn der Status `30`/"Test" vorliegt. Der Prozess des Fachverfahrens ist darauf ausgerichtet, so gekennzeichnete Test-Sendungen oder daraus resultierende weitere Artefakte früher oder später zu verwerfen.
* Der Onlinedienst versendet die Sendungen nicht an das produktive Fachverfahren, wenn der Status "30"/`Test` vorliegt. Damit wird jedoch ein Testen des Versandes unmöglich.
* Der Status `30`/"Test" wird nicht verwendet. Tests werden im Status `20`/"Initialisierung" durchgeführt. Es werden keine Test-Sendungen verschickt. Auch dies macht das Testen des Versandes unmöglich.

Ein Versand der Antragsdaten aus dem Onlinedienst in der Produktiven Umgebung an eine Test-Instanz des Fachverfahrens in einer Test-Umgebung wird nicht empfohlen. Eine solche Strategie verlangt die Überschreitung der Grenze zwischen Produktion und Test, was grundsätzlich vermieden werden sollte und ggf. auch bereits durch die Transportinfrastruktur technisch verhindert wird.
Wie konkret vorzugehen ist, hängt vom jeweiligen Test-Konzept des Roll-In Teams ab.

## Risikobetrachtung

Die Tatsache, dass der Parameter `Onlinedienst.status` vor der Migration zu XZuFi 2.3 aus Gründen des Bestandsschutzes als optional betrachtet werden muss, birgt ein Risiko.

So kann es geschehen, dass ein Onlinedienst für eine Leistung und ein Gebiet noch in einer nicht-produktiven Lebenszyklus-Phase gefahren werden soll, der Onlinedienst selbst dies aber nicht erkennen kann und im Testbetrieb wirksame Antragsdaten verschickt – oder reale Antragsteller glauben lässt, sie hätten wirksame Antragsdaten verschickt, die jedoch im Testbetrieb verworfen wurden.

Dasselbe Risiko besteht, falls nach einer Migration zu XZuFi 2.3 zwar der Status gesetzt wurde, der Onlinedienst selbst den Parameter weiterhin ignoriert und keine Steuerung der Geschäftslogik enthält.
Auch Default-Regeln wie „Eine fehlende Status-Angabe wird wie `40`/"Produktion" interpretiert“ im Onlinedienst oder in der Datenmigration von XZuFi 2.2 nach 2.3 bergen dasselbe Risiko. 

Dieses Risiko muss aktiv durch entsprechend sorgfältige Planung des Roll-Ins und der Testphasen sowie durch die in Abschnitt [Auswertung durch den Onlinedienst selbst](#auswertung-durch-den-onlinedienst-selbst) dargestellten Maßnahmen mitigiert werden. Ein Verzicht auf den Parameter `Onlinedienst.status` mitigiert das Risiko hingegen nicht, da der Verzicht genauso wirkt wie die oben genannte riskante Default-Regel.

## Fazit

Mit dem Parameter `Onlinedienst.status` steht dem OZG-Ökosystem aus PVOG, Portalen, Auskunftssystemen und den Onlinediensten selbst ein wirksamer Mechanismus für die Steuerung des Onlinedienst-Lebenszyklus zur Verfügung. Über die Redaktionssysteme können die Redakteure – in Zusammenarbeit mit den Roll-In Teams – das Verhalten der beteiligten Systeme, mit Ausnahme der Fachverfahren, in akzeptablen Grenzen steuern. Die Grenzen werden durch die zu erwartende langsame Übernahme der Daten aus den Redaktionssystemen in das PVOG definiert. So ist z.B. eine sofortige Änderung des Status durch Eintrag im Redaktionssystem nicht möglich.

Es verbleibt ein Risiko, dass der Testbetrieb nicht in allen Aspekten als solcher korrekt erkennbar und – in der realen Welt – folgenlos bleibt, wenn die Verwendung des `Onlinedienst.status` nicht in allen Systemen korrekt umgesetzt ist. Daher muss auch mit dem neuen Mechanismus die Test-Phase im Roll-In sorgfältig geplant, durchgeführt und überwacht werden. 
