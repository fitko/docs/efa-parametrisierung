---
id: pflegekonzept
title: "Pflegekonzept"
slug: /pflegekonzept
---


# Pflegekonzept

## Einführung

Die Pflege von Parametern ist ein wesentlicher Bestandteil der EfA-fähigen Onlinedienste, die in der Lage sein müssen, landes- und satzungsrechtliche Unterschiede zu berücksichtigen, um bundesweit genutzt werden zu können. Diese Dienste zielen darauf ab, durch die Verwendung von Parametern eine hohe Flexibilität zu gewährleisten, damit sie in verschiedenen rechtlichen und organisatorischen Umgebungen der Bundesländer und Kommunen eingesetzt werden können. Um dies zu erreichen, ist eine standardisierte und einheitliche Vorgehensweise bei der Pflege dieser Parameter unerlässlich, um die flächendeckende Nutzung der Onlinedienste für möglichst viele Verwaltungsprozesse sicherzustellen.

Damit die Onlinedienste flexibel und anpassbar bleiben, ermitteln sie zur Laufzeit die konkreten Werte für die von ihnen benötigten Parameter. Diese Werte werden aus zwei zentral zugänglichen Verzeichnisdiensten abgerufen: dem [PVOG](https://www.fitko.de/produktmanagement/pvog) und dem [DVDV](https://www.itzbund.de/DE/itloesungen/standardloesungen/dvdv/dvdv.html). Der Onlinedienst verwendet dabei als Suchschlüssel primär Angaben zur Leistung und zum Gebiet, das in der Regel ein Bundesland oder eine Kommune umfasst. Die so ermittelten Parameterwerte steuern dann das Verhalten des Onlinedienstes, sodass die landes- und satzungsrechtlichen Besonderheiten für die Antragsstellung in dem jeweiligen Gebiet berücksichtigt werden.

Die Pflege dieser Parameter umfasst den gesamten Prozess, bei dem die erforderlichen Werte erstellt, aktualisiert und verwaltet werden. Die Parameter beeinflussen verschiedene Aspekte der Onlinedienste, darunter die Benutzerinformation, die Antragssteuerung, die Zuständigkeit von Behörden und die Abwicklung von Zahlungsprozessen. Eine korrekte und sorgfältige Pflege dieser Parameter ist daher entscheidend, um sicherzustellen, dass die Onlinedienste in den unterschiedlichen rechtlichen und organisatorischen Rahmenbedingungen der Bundesländer und Kommunen reibungslos funktionieren.

### Adressaten

Das Pflegekonzept richtet sich an mehrere Zielgruppen, die in den nachnutzenden Ländern für die Pflege dieser Parameter verantwortlich sind. Dazu gehören technische Experten, die in erster Linie dafür verantwortlich sind, die technischen Parameter in den Verzeichnisdiensten zu pflegen. Ihre Aufgabe ist es, sicherzustellen, dass die technischen Informationen korrekt und aktuell sind, sodass die Onlinedienste technisch einwandfrei funktionieren und die Anfragen an die richtigen Stellen geleitet werden. Zu den weiteren Zielgruppen gehören fachliche Redakteure in den Ländern und Kommunen, die für die Pflege der inhaltlichen und rechtlichen Parameter verantwortlich sind. Diese Parameter betreffen landesspezifische und kommunale Besonderheiten, und ihre korrekte Pflege stellt sicher, dass die Informationen, die in den Onlinediensten verwendet werden, den lokalen Vorschriften entsprechen.

Eine wichtige Rolle spielen auch die Roll-Out- und Roll-In-Teams. Die Roll-Out-Teams sind primär für die technische Implementierung und Verbreitung der EfA-Onlinedienste in verschiedenen Regionen verantwortlich, während die Roll-In-Teams sich auf die Erfassung und Anpassung regional spezifischer Parameter konzentrieren. Während die Roll-Out-Teams technische Anpassungen vornehmen oder anstoßen können, liegt der Schwerpunkt der Roll-In-Teams auf der Analyse regionaler Anforderungen und der Parametrisierung der Dienste, wobei sie bei Bedarf auch als Anforderungssteller für notwendige Softwareänderungen fungieren, um die Erfassung regionaler Spezifika zu ermöglichen.

### Zielstellung

Das übergeordnete Ziel des Pflegekonzepts ist es, den beteiligten Akteuren ein klares Verständnis der Systeme und Prozesse zu vermitteln, die zur Pflege der Parameter verwendet werden. Das Konzept identifiziert die wichtigsten Herausforderungen, die bei der Pflege der Parameter auftreten können, und bietet praktische Lösungsansätze, um diese Herausforderungen zu bewältigen. Gleichzeitig soll das Pflegekonzept den Ländern und Kommunen dabei helfen, eigene Pflegeprozesse zu entwickeln und zu optimieren. Es stellt Leitlinien bereit, wie Verantwortlichkeiten festgelegt, Pflegezyklen definiert und eine hohe Datenqualität sichergestellt werden können.

Insgesamt dient dieses Pflegekonzept als Leitfaden für die Beteiligten, um die Anforderungen der unterschiedlichen rechtlichen und organisatorischen Rahmenbedingungen in den verschiedenen Bundesländern und Kommunen zu erfüllen. Es hilft dabei, die komplexen Prozesse der Parameterpflege zu verstehen und umzusetzen, sodass die EfA-Onlinedienste in der gesamten Fläche Deutschlands genutzt werden können.

## Überblick über die Unterseiten des Pflegekonzepts

Das Pflegekonzept besteht aus mehreren Unterseiten, die spezifische Aspekte der Parameterpflege und Nachnutzung von EfA-Onlinediensten behandeln. Nicht jede Seite ist für alle Leser gleichermaßen relevant, daher bietet diese Übersicht eine Hilfestellung, wie die verschiedenen Unterseiten am besten genutzt werden sollten.

### Einstiegspunkt: Herausforderungen

Die Seite [Herausforderungen in der Parameterpflege](./pflegekonzept/herausforderungen) dient als idealer Einstiegspunkt, um die unterschiedlichen Belange der verschiedenen Akteure im Kontext der EfA-Parametrisierung zu verstehen. Hier werden die wichtigsten Herausforderungen identifiziert und jeweils auf relevante Ressourcen verwiesen, die Lösungsansätze bieten. Diese Seite eignet sich besonders für Leser, die einen Überblick darüber erhalten möchten, welche spezifischen Probleme in der Parameterpflege auftreten können und wie diesen begegnet werden kann.

### Grundlagen der Parameterpflege

Für ein grundlegendes Verständnis der Parameterpflege in EfA-Onlinediensten sollten die folgenden Seiten zuerst gelesen werden:

- **[Pflegeprozesse für verschiedene Parametertypen](./pflegekonzept/pflegeprozesse_parametertypen.md):** Diese Seite beschreibt die Pflegeprozesse für verschiedene Parametertypen in EfA-Onlinediensten, die für die landes- und satzungskonforme Nutzung notwendig sind. Sie gibt einen Überblick über die spezifischen Anforderungen und Zuständigkeiten bei der Pflege von Stammdaten, onlinedienstbezogenen Parametern und DVDV-basierten Informationen. Die Seite richtet sich an diejenigen, die für die Verwaltung dieser Parameter verantwortlich sind, und erläutert die jeweils erforderlichen Verfahren.
  
- **[Soll-Prozess der Nachnutzung](./pflegekonzept/nachnutzungsprozess.md):** Hier wird der gesamte Nachnutzungsprozess für EfA-Onlinedienste beschrieben, von der Akkreditierung bis zur Veröffentlichung eines Dienstes.


### Vertiefende Inhalte

Die restlichen Seiten des Pflegekonzepts gehen tiefer in spezifische Aspekte ein, die für die Nachnutzung von EfA-Onlinediensten und die Pflege derer Parameter relevant sind. Diese Seiten sollten je nach Bedarf und spezifischem Interesse gelesen werden:

- **[Rollen und Zuständigkeiten in der Parameterpflege](./pflegekonzept/rollen_und_zustaendigkeiten.md):** Diese Seite erläutert die verschiedenen Rollen und Zuständigkeiten innerhalb der EfA-Parametrisierung, einschließlich der Aufgabenverteilung zwischen Landesredaktionen, Kommunen und zentralen Stellen.

- **[Aufgabenverteilung und Spezialisierung in der Parameterpflege](./pflegekonzept/aufgabenverteilung.md):** Diese Seite bietet eine umfassende Übersicht über die Möglichkeiten der Aufgabenteilung und Spezialisierung innerhalb der Parameterpflege, um Effizienz und Qualität zu steigern, insbesondere in unterschiedlichen Organisationsgrößen und -strukturen.

- **[Verantwortung für die Integrität von Parameterwerten](./pflegekonzept/verantwortung_fuer_Datenintegritaet.md):** Hier wird die Verantwortung für die Korrektheit und Konsistenz der Parameterdaten beschrieben, einschließlich der Festlegung von Rollen und Prozessen zur Qualitätssicherung.

- **[Change Management und Versionskontrolle](./pflegekonzept/change-management_und_versionskontrolle.md):** Diese Seite bietet einen Überblick über die methodischen Ansätze zur Verwaltung von Änderungen an Onlinediensten, einschließlich der Versionskontrolle und des Risikomanagements.

- **[Tests und Produktivsetzung](./pflegekonzept/tests_und_produktivsetzung.md):** Diese Seite beschreibt die notwendigen Schritte und Anforderungen zur Inbetriebnahme von EfA-Onlinediensten, inklusive der Testphasen und des Risikomanagements.

- **[Dokumentation und Schulung](./pflegekonzept/dokumentation_und_schulung.md):** Hier wird beschrieben, wie Informationen effizient vermittelt werden können, um sicherzustellen, dass alle Akteure die notwendigen Kenntnisse zur Parameterpflege erwerben.

- **[Kommunikationsprozesse](./pflegekonzept/kommunikationsprozesse.md):** Diese Seite beschreibt die notwendigen Kommunikationsstrukturen, um eine reibungslose Zusammenarbeit zwischen den Beteiligten sicherzustellen, und bietet Best Practices für eine effektive Kommunikation.

- **[Onlinedienst-Vorlagen](./pflegekonzept/Onlinedienst-Vorlagen.md):** Diese Seite erklärt die Bedeutung und den Einsatz von Onlinedienst-Vorlagen, die eine effiziente Konfiguration und Pflege von EfA-Onlinediensten ermöglichen.
